package com.dimtutac.mobileapp.models.login;

import com.dimtutac.mobileapp.common.AppConstants;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Jalotsav on 9/15/2017.
 */

public class MdlLoginResData implements AppConstants {

    @SerializedName(KEY_PKID)
    private String pkId;
    @SerializedName(KEY_USERNAME)
    private String userName;
    @SerializedName(KEY_FIRSTNAME)
    private String firstName;
    @SerializedName(KEY_LASTNAME)
    private String lastName;
    @SerializedName(KEY_MIDDLENAME)
    private String middleName;
    @SerializedName(KEY_USERTYPE_ID)
    private String userTypeId;
    @SerializedName(KEY_PASSWORD)
    private String password;
    @SerializedName(KEY_COMPANY_NAME)
    private String companyName;
    @SerializedName(KEY_GENDER)
    private String gender;
    @SerializedName(KEY_DATEOFBIRTH)
    private String dateOfBirth;
    @SerializedName(KEY_DEFAULT_LANGUAGE_ID)
    private String defaultLanguageId;
    @SerializedName(KEY_RESTAURANT_ID)
    private String restaurantId;
    @SerializedName(KEY_MAIN_BRANCH_ID)
    private String mainBranchId;
    @SerializedName(KEY_IS_AGREE_TC)
    private boolean isAgreeTC;
    @SerializedName(KEY_IS_PROMOTION_SEND)
    private boolean isPromotionSend;
    @SerializedName(KEY_IS_FACEBOOK_LOGIN)
    private boolean isFacebookLogin;
    @SerializedName(KEY_FACEBOOK_ID)
    private String FacebookId;
    @SerializedName(KEY_ACCOUNT_BARCODE)
    private String accountBarcode;
    @SerializedName(KEY_EMAIL_ID)
    private String emailId;
    @SerializedName(KEY_MOBILENO)
    private String mobileNo;
    @SerializedName(KEY_PHONENO)
    private String phoneNo;
    @SerializedName(KEY_PHOTOURL)
    private String photoURL;
    @SerializedName(KEY_ADDRESS_MASTER_ID)
    private String addressMasterId;
    @SerializedName(KEY_IS_ACTIVE)
    private boolean isActive;
    @SerializedName(KEY_ADDRESS_LINE1)
    private String addressLine1;
    @SerializedName(KEY_ADDRESS_LINE2)
    private String addressLine2;
    @SerializedName(KEY_CITY)
    private String city;
    @SerializedName(KEY_STATE)
    private String state;
    @SerializedName(KEY_COUNTRY)
    private String country;
    @SerializedName(KEY_PINCODE)
    private String pincode;
    @SerializedName(KEY_CUSTOMER_TYPE_ID)
    private String customerTypeId;
    @SerializedName(KEY_CUSTOMER_TYPE)
    private String customerType;
    @SerializedName(KEY_PASSWORD_RESET_ID)
    private String passwordResetId;
    @SerializedName(KEY_DEVICE_ID)
    private String deviceId;

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getUserTypeId() {
        return userTypeId;
    }

    public void setUserTypeId(String userTypeId) {
        this.userTypeId = userTypeId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getDefaultLanguageId() {
        return defaultLanguageId;
    }

    public void setDefaultLanguageId(String defaultLanguageId) {
        this.defaultLanguageId = defaultLanguageId;
    }

    public String getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
    }

    public String getMainBranchId() {
        return mainBranchId;
    }

    public void setMainBranchId(String mainBranchId) {
        this.mainBranchId = mainBranchId;
    }

    public boolean isAgreeTC() {
        return isAgreeTC;
    }

    public void setAgreeTC(boolean agreeTC) {
        isAgreeTC = agreeTC;
    }

    public boolean isPromotionSend() {
        return isPromotionSend;
    }

    public void setPromotionSend(boolean promotionSend) {
        isPromotionSend = promotionSend;
    }

    public boolean isFacebookLogin() {
        return isFacebookLogin;
    }

    public void setFacebookLogin(boolean facebookLogin) {
        isFacebookLogin = facebookLogin;
    }

    public String getFacebookId() {
        return FacebookId;
    }

    public void setFacebookId(String facebookId) {
        FacebookId = facebookId;
    }

    public String getAccountBarcode() {
        return accountBarcode;
    }

    public void setAccountBarcode(String accountBarcode) {
        this.accountBarcode = accountBarcode;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getPhotoURL() {
        return photoURL;
    }

    public void setPhotoURL(String photoURL) {
        this.photoURL = photoURL;
    }

    public String getAddressMasterId() {
        return addressMasterId;
    }

    public void setAddressMasterId(String addressMasterId) {
        this.addressMasterId = addressMasterId;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getCustomerTypeId() {
        return customerTypeId;
    }

    public void setCustomerTypeId(String customerTypeId) {
        this.customerTypeId = customerTypeId;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public String getPasswordResetId() {
        return passwordResetId;
    }

    public void setPasswordResetId(String passwordResetId) {
        this.passwordResetId = passwordResetId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }
}
