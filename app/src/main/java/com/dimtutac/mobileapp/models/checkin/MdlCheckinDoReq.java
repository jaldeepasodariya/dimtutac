package com.dimtutac.mobileapp.models.checkin;

import com.dimtutac.mobileapp.common.AppConstants;
import com.dimtutac.mobileapp.models.MdlPhotoURL;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Jalotsav on 11/14/2017.
 */

public class MdlCheckinDoReq implements AppConstants {

    @SerializedName(KEY_USER_ID_FIRSTCHARCAPS)
    private String userId;
    @SerializedName(KEY_RESTAURANT_ID)
    private String restaurantId;
    @SerializedName(KEY_BRANCH_ID)
    private String branchId;
    @SerializedName(KEY_WRITING_STATUS)
    private String writingStatus;
    @SerializedName(KEY_LATITUDE_PREFIX)
    private String latitude;
    @SerializedName(KEY_LONGITUDE_PREFIX)
    private String longitude;
    @SerializedName(KEY_CHECKIN_PHOTO_MASTER)
    private ArrayList<MdlPhotoURL> arrylstMdlPhotoURL;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getWritingStatus() {
        return writingStatus;
    }

    public void setWritingStatus(String writingStatus) {
        this.writingStatus = writingStatus;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public ArrayList<MdlPhotoURL> getArrylstMdlPhotoURL() {
        return arrylstMdlPhotoURL;
    }

    public void setArrylstMdlPhotoURL(ArrayList<MdlPhotoURL> arrylstMdlPhotoURL) {
        this.arrylstMdlPhotoURL = arrylstMdlPhotoURL;
    }
}
