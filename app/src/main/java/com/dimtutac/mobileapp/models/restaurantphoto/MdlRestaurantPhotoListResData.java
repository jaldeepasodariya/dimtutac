package com.dimtutac.mobileapp.models.restaurantphoto;

import com.dimtutac.mobileapp.common.AppConstants;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Jalotsav on 1/8/2018.
 */

public class MdlRestaurantPhotoListResData implements AppConstants {

    @SerializedName(KEY_PKID)
    private String pkId;
    @SerializedName(KEY_IMAGEPATH)
    private String imagePath;
    @SerializedName(KEY_RESTAURANT_ID)
    private String restaurantId;
    @SerializedName(KEY_IS_DELETED)
    private boolean isDeleted;

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }
}
