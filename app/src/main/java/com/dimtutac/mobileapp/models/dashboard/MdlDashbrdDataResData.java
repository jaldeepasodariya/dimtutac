package com.dimtutac.mobileapp.models.dashboard;

import com.dimtutac.mobileapp.common.AppConstants;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Jalotsav on 9/21/2017.
 */

public class MdlDashbrdDataResData implements AppConstants {

    @SerializedName(KEY_PKID)
    private String pkId;
    @SerializedName(KEY_PHOTOURL)
    private String photoURL;
    @SerializedName(KEY_TITLE1)
    private String title1;
    @SerializedName(KEY_TITLE2)
    private String title2;
    @SerializedName(KEY_TITLE3)
    private String title3;
    @SerializedName(KEY_WITHTEXT)
    private boolean withText;
    @SerializedName(KEY_IS_DELETED)
    private boolean isDeleted;

    public MdlDashbrdDataResData(String pkId, String photoURL, String title1, String title2, String title3, boolean withText, boolean isDeleted) {
        this.pkId = pkId;
        this.photoURL = photoURL;
        this.title1 = title1;
        this.title2 = title2;
        this.title3 = title3;
        this.withText = withText;
        this.isDeleted = isDeleted;
    }

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getPhotoURL() {
        return photoURL;
    }

    public void setPhotoURL(String photoURL) {
        this.photoURL = photoURL;
    }

    public String getTitle1() {
        return title1;
    }

    public void setTitle1(String title1) {
        this.title1 = title1;
    }

    public String getTitle2() {
        return title2;
    }

    public void setTitle2(String title2) {
        this.title2 = title2;
    }

    public String getTitle3() {
        return title3;
    }

    public void setTitle3(String title3) {
        this.title3 = title3;
    }

    public boolean isWithText() {
        return withText;
    }

    public void setWithText(boolean withText) {
        this.withText = withText;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }
}
