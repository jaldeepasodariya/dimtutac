package com.dimtutac.mobileapp.models.dashboard;

import com.dimtutac.mobileapp.common.AppConstants;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Jalotsav on 9/21/2017.
 */

public class MdlDashbrdDataRes implements AppConstants {

    @SerializedName(KEY_SUCCESS_SML)
    private boolean success;
    @SerializedName(KEY_MESSAGE_SML)
    private String message;
    @SerializedName(KEY_DATA_SML)
    private ArrayList<MdlDashbrdDataResData> arrylstDashbrdDataResData;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<MdlDashbrdDataResData> getArrylstDashbrdDataResData() {
        return arrylstDashbrdDataResData;
    }

    public void setArrylstDashbrdDataResData(ArrayList<MdlDashbrdDataResData> arrylstDashbrdDataResData) {
        this.arrylstDashbrdDataResData = arrylstDashbrdDataResData;
    }
}
