package com.dimtutac.mobileapp.models.adminconfigurations;

import com.dimtutac.mobileapp.common.AppConstants;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Jalotsav on 1/10/2018.
 */

public class MdlAdminConfigRes implements AppConstants {

    @SerializedName(KEY_SUCCESS_SML)
    private boolean success;
    @SerializedName(KEY_MESSAGE_SML)
    private String message;
    @SerializedName(KEY_DATA_SML)
    private MdlAdminConfigResData mdlAdminConfigResData;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public MdlAdminConfigResData getMdlAdminConfigResData() {
        return mdlAdminConfigResData;
    }

    public void setMdlAdminConfigResData(MdlAdminConfigResData mdlAdminConfigResData) {
        this.mdlAdminConfigResData = mdlAdminConfigResData;
    }
}
