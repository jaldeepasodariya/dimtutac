package com.dimtutac.mobileapp.models.checkin;

import com.dimtutac.mobileapp.common.AppConstants;
import com.dimtutac.mobileapp.models.MdlPhotoURL;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Jalotsav on 1/25/2018.
 */

public class MdlCheckinListResData implements AppConstants {

    @SerializedName(KEY_PKID)
    private String pkId;
    @SerializedName(KEY_USER_ID_FIRSTCHARCAPS)
    private String userId;
    @SerializedName(KEY_RESTAURANT_ID)
    private String restaurantId;
    @SerializedName(KEY_BRANCH_ID)
    private String branchId;
    @SerializedName(KEY_WRITING_STATUS)
    private String writingStatus;
    @SerializedName(KEY_LATITUDE_PREFIX)
    private String latitude;
    @SerializedName(KEY_LONGITUDE_PREFIX)
    private String longitude;
    @SerializedName(KEY_CHECKIN_DATETIME)
    private String checkInDateTime;
    @SerializedName(KEY_CHECKIN_PHOTO_MASTER)
    private ArrayList<MdlPhotoURL> arrylstMdlPhotoURL;
    @SerializedName(KEY_BRANCH_PHOTOURL)
    private String branchPhotoURL;
    @SerializedName(KEY_BRANCH_NAME)
    private String branchName;

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getWritingStatus() {
        return writingStatus;
    }

    public void setWritingStatus(String writingStatus) {
        this.writingStatus = writingStatus;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getCheckInDateTime() {
        return checkInDateTime;
    }

    public void setCheckInDateTime(String checkInDateTime) {
        this.checkInDateTime = checkInDateTime;
    }

    public ArrayList<MdlPhotoURL> getArrylstMdlPhotoURL() {
        return arrylstMdlPhotoURL;
    }

    public void setArrylstMdlPhotoURL(ArrayList<MdlPhotoURL> arrylstMdlPhotoURL) {
        this.arrylstMdlPhotoURL = arrylstMdlPhotoURL;
    }

    public String getBranchPhotoURL() {
        return branchPhotoURL;
    }

    public void setBranchPhotoURL(String branchPhotoURL) {
        this.branchPhotoURL = branchPhotoURL;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }
}
