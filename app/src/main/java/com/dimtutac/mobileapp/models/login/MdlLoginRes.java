package com.dimtutac.mobileapp.models.login;

import com.dimtutac.mobileapp.common.AppConstants;
import com.google.gson.annotations.SerializedName;

import org.json.JSONObject;

/**
 * Created by Jalotsav on 9/5/2017.
 */

public class MdlLoginRes implements AppConstants {

    @SerializedName(KEY_SUCCESS_SML)
    private boolean success;
    @SerializedName(KEY_MESSAGE_SML)
    private String message;
    @SerializedName(KEY_DATA_SML)
    private JSONObject jsnObjData;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public JSONObject getJsnObjData() {
        return jsnObjData;
    }

    public void setJsnObjData(JSONObject jsnObjData) {
        this.jsnObjData = jsnObjData;
    }
}
