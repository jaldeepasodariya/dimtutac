package com.dimtutac.mobileapp.models.menu;

import com.dimtutac.mobileapp.common.AppConstants;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Jalotsav on 11/3/2017.
 */

public class MdlNewMonthlyItemListResData implements AppConstants {

    @SerializedName(KEY_PKID)
    private String pkId;
    @SerializedName(KEY_BRANCH_ID)
    private String branchId;
    @SerializedName(KEY_MENU_NAME)
    private String menuName;
    @SerializedName(KEY_DESCRIPTION)
    private String description;
    @SerializedName(KEY_PHOTOURL)
    private String photoURL;
    @SerializedName(KEY_PRICE)
    private String price;
    @SerializedName(KEY_CURRENCY)
    private String currency;
    @SerializedName(KEY_IS_DELETED)
    private boolean isDeleted;

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhotoURL() {
        return photoURL;
    }

    public void setPhotoURL(String photoURL) {
        this.photoURL = photoURL;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }
}
