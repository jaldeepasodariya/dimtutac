package com.dimtutac.mobileapp.models.offer;

import com.dimtutac.mobileapp.common.AppConstants;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Jalotsav on 1/31/2018.
 */

public class MdlRedeemOfferReq implements AppConstants {

    @SerializedName(KEY_OFFER_ID)
    private String offerId;
    @SerializedName(KEY_ASSIGNMENT_CODE)
    private String assignmentCode;
    @SerializedName(KEY_USER_ID_FIRSTCHARCAPS)
    private String userId;

    public MdlRedeemOfferReq(String offerId, String assignmentCode, String userId) {
        this.offerId = offerId;
        this.assignmentCode = assignmentCode;
        this.userId = userId;
    }
}
