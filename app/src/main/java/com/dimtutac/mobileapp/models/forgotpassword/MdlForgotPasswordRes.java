package com.dimtutac.mobileapp.models.forgotpassword;

import com.dimtutac.mobileapp.common.AppConstants;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Jalotsav on 2/8/2018.
 */

public class MdlForgotPasswordRes implements AppConstants {

    @SerializedName(KEY_SUCCESS_SML)
    private boolean success;
    @SerializedName(KEY_MESSAGE_SML)
    private String message;
    @SerializedName(KEY_DATA_SML)
    private String data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
