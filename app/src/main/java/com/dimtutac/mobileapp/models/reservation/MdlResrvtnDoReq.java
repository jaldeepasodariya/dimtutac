package com.dimtutac.mobileapp.models.reservation;

import com.dimtutac.mobileapp.common.AppConstants;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Jalotsav on 2/10/17.
 */

public class MdlResrvtnDoReq implements AppConstants {

    @SerializedName(KEY_USER_ID_FIRSTCHARCAPS)
    private String userId;
    @SerializedName(KEY_NAME)
    private String name;
    @SerializedName(KEY_DATE)
    private String date;
    @SerializedName(KEY_FROM_TIME)
    private String fromTime;
    @SerializedName(KEY_TO_TIME)
    private String toTime;
    @SerializedName(KEY_PHONE_NUMBER)
    private String phoneNumber;
    @SerializedName(KEY_EMAIL_ID)
    private String emailId;
    @SerializedName(KEY_RESTAURANT_ID)
    private String restaurantId;
    @SerializedName(KEY_BRANCH_ID)
    private String branchId;
    @SerializedName(KEY_NO_OF_GUEST)
    private int noOfGuest;
    @SerializedName(KEY_COMMENTS)
    private String comments;
    @SerializedName(KEY_STATUS)
    private String status;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getFromTime() {
        return fromTime;
    }

    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }

    public String getToTime() {
        return toTime;
    }

    public void setToTime(String toTime) {
        this.toTime = toTime;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public int getNoOfGuest() {
        return noOfGuest;
    }

    public void setNoOfGuest(int noOfGuest) {
        this.noOfGuest = noOfGuest;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
