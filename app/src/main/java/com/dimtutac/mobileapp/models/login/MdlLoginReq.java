package com.dimtutac.mobileapp.models.login;

import com.dimtutac.mobileapp.common.AppConstants;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Jalotsav on 9/14/2017.
 */

public class MdlLoginReq implements AppConstants {

    @SerializedName(KEY_USERNAME)
    private String username;
    @SerializedName(KEY_PASSWORD)
    private String password;

    public MdlLoginReq(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
