package com.dimtutac.mobileapp.models.branch;

import com.dimtutac.mobileapp.common.AppConstants;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Jalotsav on 1/10/17.
 */

public class MdlBranchListRes implements AppConstants {

    @SerializedName(KEY_SUCCESS_SML)
    private boolean success;
    @SerializedName(KEY_MESSAGE_SML)
    private String message;
    @SerializedName(KEY_DATA_SML)
    private ArrayList<MdlBranchListResData> arrylstMdlBranchListData;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<MdlBranchListResData> getArrylstMdlBranchListData() {
        return arrylstMdlBranchListData;
    }

    public void setArrylstMdlBranchListData(ArrayList<MdlBranchListResData> arrylstMdlBranchListData) {
        this.arrylstMdlBranchListData = arrylstMdlBranchListData;
    }
}
