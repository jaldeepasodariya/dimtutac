package com.dimtutac.mobileapp.models.branch;

import com.dimtutac.mobileapp.common.AppConstants;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Jalotsav on 9/18/2017.
 */

public class MdlLocationListResData implements AppConstants {

    @SerializedName(KEY_PKID)
    private String pkId;
    @SerializedName(KEY_RESTAURANT_ID)
    private String restaurantId;
    @SerializedName(KEY_BRANCH_NAME)
    private String branchName;
    @SerializedName(KEY_CODE)
    private String code;
    @SerializedName(KEY_ADDRESS_ID)
    private String addressId;
    @SerializedName(KEY_MOBILENO)
    private String mobileNo;
    @SerializedName(KEY_TELEPHONE_NO)
    private String telephoneNo;
    @SerializedName(KEY_EMAIL_ID)
    private String emailId;
    @SerializedName(KEY_PHOTOURL)
    private String photoURL;
    @SerializedName(KEY_IS_DELETED)
    private boolean isDeleted;
    @SerializedName(KEY_IS_ACTIVE)
    private boolean isActive;
    @SerializedName(KEY_BUSINESS_HOURS)
    private ArrayList<MdlLocationListBusineshrs> arrylstMdlLoctnLstBusineshrs;
    @SerializedName(KEY_BUSINESS_HOURS_LIST)
    private String[] strArryBusineshrs;
    @SerializedName(KEY_ADDRESS_LINE1)
    private String addressLine1;
    @SerializedName(KEY_ADDRESS_LINE2)
    private String addressLine2;
    @SerializedName(KEY_CITY)
    private String city;
    @SerializedName(KEY_STATE)
    private String state;
    @SerializedName(KEY_COUNTRY)
    private String country;
    @SerializedName(KEY_PINCODE)
    private String pincode;
    @SerializedName(KEY_LATITUDE_PREFIX)
    private String latitude;
    @SerializedName(KEY_LONGITUDE_PREFIX)
    private String longitude;

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getAddressId() {
        return addressId;
    }

    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getTelephoneNo() {
        return telephoneNo;
    }

    public void setTelephoneNo(String telephoneNo) {
        this.telephoneNo = telephoneNo;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getPhotoURL() {
        return photoURL;
    }

    public void setPhotoURL(String photoURL) {
        this.photoURL = photoURL;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public ArrayList<MdlLocationListBusineshrs> getArrylstMdlLoctnLstBusineshrs() {
        return arrylstMdlLoctnLstBusineshrs;
    }

    public void setArrylstMdlLoctnLstBusineshrs(ArrayList<MdlLocationListBusineshrs> arrylstMdlLoctnLstBusineshrs) {
        this.arrylstMdlLoctnLstBusineshrs = arrylstMdlLoctnLstBusineshrs;
    }

    public String[] getStrArryBusineshrs() {
        return strArryBusineshrs;
    }

    public void setStrArryBusineshrs(String[] strArryBusineshrs) {
        this.strArryBusineshrs = strArryBusineshrs;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}
