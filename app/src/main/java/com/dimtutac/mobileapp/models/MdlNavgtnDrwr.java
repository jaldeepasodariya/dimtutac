package com.dimtutac.mobileapp.models;

/**
 * Created by Jalotsav on 2/15/2018.
 */

public class MdlNavgtnDrwr {

    private String title;
    private int iconDrawableId;

    public MdlNavgtnDrwr(String title, int iconDrawableId) {
        this.title = title;
        this.iconDrawableId = iconDrawableId;
    }

    public String getTitle() {
        return title;
    }

    public int getIconDrawableId() {
        return iconDrawableId;
    }
}
