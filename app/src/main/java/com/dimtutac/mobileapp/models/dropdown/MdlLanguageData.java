package com.dimtutac.mobileapp.models.dropdown;

import com.dimtutac.mobileapp.common.AppConstants;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Jalotsav on 9/16/2017.
 */

public class MdlLanguageData implements AppConstants {

    @SerializedName(KEY_SELECTED)
    private boolean selected;
    @SerializedName(KEY_TEXT)
    private String text;
    @SerializedName(KEY_VALUE)
    private String value;

    public MdlLanguageData(boolean selected, String text, String value) {
        this.selected = selected;
        this.text = text;
        this.value = value;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
