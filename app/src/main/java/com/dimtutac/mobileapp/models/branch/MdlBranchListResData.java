package com.dimtutac.mobileapp.models.branch;

import com.dimtutac.mobileapp.common.AppConstants;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Jalotsav on 1/10/17.
 */

public class MdlBranchListResData implements AppConstants {

    @SerializedName(KEY_BRANCH_ID)
    private String branchId;
    @SerializedName(KEY_BRANCH_NAME)
    private String branchName;

    public MdlBranchListResData() {
    }

    public MdlBranchListResData(String branchId, String branchName) {
        this.branchId = branchId;
        this.branchName = branchName;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }
}
