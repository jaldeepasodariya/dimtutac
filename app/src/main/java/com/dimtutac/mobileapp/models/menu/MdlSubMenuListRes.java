package com.dimtutac.mobileapp.models.menu;

import com.dimtutac.mobileapp.common.AppConstants;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Jalotsav on 20/11/17.
 */

public class MdlSubMenuListRes implements AppConstants {

    @SerializedName(KEY_SUCCESS_SML)
    private boolean success;
    @SerializedName(KEY_MESSAGE_SML)
    private String message;
    @SerializedName(KEY_DATA_SML)
    private ArrayList<MdlSubMenuListResData> arrylstMdlSubMenuListResData;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<MdlSubMenuListResData> getArrylstMdlSubMenuListResData() {
        return arrylstMdlSubMenuListResData;
    }

    public void setArrylstMdlSubMenuListResData(ArrayList<MdlSubMenuListResData> arrylstMdlSubMenuListResData) {
        this.arrylstMdlSubMenuListResData = arrylstMdlSubMenuListResData;
    }
}
