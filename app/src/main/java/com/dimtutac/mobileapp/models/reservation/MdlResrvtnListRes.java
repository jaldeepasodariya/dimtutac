package com.dimtutac.mobileapp.models.reservation;

import com.dimtutac.mobileapp.common.AppConstants;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Jalotsav on 9/27/2017.
 */

public class MdlResrvtnListRes implements AppConstants {

    @SerializedName(KEY_SUCCESS_SML)
    private boolean success;
    @SerializedName(KEY_MESSAGE_SML)
    private String message;
    @SerializedName(KEY_DATA_SML)
    private ArrayList<MdlResrvtnListResData> arrylstMdlResrvtnListData;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<MdlResrvtnListResData> getArrylstMdlResrvtnListData() {
        return arrylstMdlResrvtnListData;
    }

    public void setArrylstMdlResrvtnListData(ArrayList<MdlResrvtnListResData> arrylstMdlResrvtnListData) {
        this.arrylstMdlResrvtnListData = arrylstMdlResrvtnListData;
    }
}
