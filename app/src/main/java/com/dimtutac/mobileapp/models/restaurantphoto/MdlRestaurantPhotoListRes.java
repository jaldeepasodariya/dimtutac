package com.dimtutac.mobileapp.models.restaurantphoto;

import com.dimtutac.mobileapp.common.AppConstants;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Jalotsav on 1/8/2018.
 */

public class MdlRestaurantPhotoListRes implements AppConstants {

    @SerializedName(KEY_SUCCESS_SML)
    private boolean success;
    @SerializedName(KEY_MESSAGE_SML)
    private String message;
    @SerializedName(KEY_DATA_SML)
    private ArrayList<MdlRestaurantPhotoListResData> arrylstMdlRestaurantPhotoListData;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<MdlRestaurantPhotoListResData> getArrylstMdlRestaurantPhotoListData() {
        return arrylstMdlRestaurantPhotoListData;
    }

    public void setArrylstMdlRestaurantPhotoListData(ArrayList<MdlRestaurantPhotoListResData> arrylstMdlRestaurantPhotoListData) {
        this.arrylstMdlRestaurantPhotoListData = arrylstMdlRestaurantPhotoListData;
    }
}
