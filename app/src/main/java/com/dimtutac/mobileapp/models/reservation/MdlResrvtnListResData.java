package com.dimtutac.mobileapp.models.reservation;

import com.dimtutac.mobileapp.common.AppConstants;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Jalotsav on 9/27/2017.
 */

public class MdlResrvtnListResData implements AppConstants {

    @SerializedName(KEY_PKID)
    private String pkId;
    @SerializedName(KEY_USER_ID_FIRSTCHARCAPS)
    private String userId;
    @SerializedName(KEY_USERNAME)
    private String userName;
    @SerializedName(KEY_NAME)
    private String name;
    @SerializedName(KEY_DATE)
    private String date;
    @SerializedName(KEY_FROM_TIME)
    private String fromTime;
    @SerializedName(KEY_TO_TIME)
    private String toTime;
    @SerializedName(KEY_PHONE_NUMBER)
    private String phoneNumber;
    @SerializedName(KEY_EMAIL_ID)
    private String emailId;
    @SerializedName(KEY_RESTAURANT_ID)
    private String restaurantId;
    @SerializedName(KEY_BRANCH_ID)
    private String branchId;
    @SerializedName(KEY_NO_OF_GUEST)
    private int noOfGuest;
    @SerializedName(KEY_COMMENTS)
    private String comments;
    @SerializedName(KEY_IS_DELETED)
    private boolean isDeleted;
    @SerializedName(KEY_STATUS_NAME)
    private String statusName;
    @SerializedName(KEY_BRANCH_NAME)
    private String branchName;
    @SerializedName(KEY_STATUS)
    private String status;
    @SerializedName(KEY_PHOTOURL)
    private String photoURL;
    @SerializedName(KEY_IS_ALLOW_UPDATE)
    private boolean isAllowUpdate;

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getFromTime() {
        return fromTime;
    }

    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }

    public String getToTime() {
        return toTime;
    }

    public void setToTime(String toTime) {
        this.toTime = toTime;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public int getNoOfGuest() {
        return noOfGuest;
    }

    public void setNoOfGuest(int noOfGuest) {
        this.noOfGuest = noOfGuest;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPhotoURL() {
        return photoURL;
    }

    public void setPhotoURL(String photoURL) {
        this.photoURL = photoURL;
    }

    public boolean isAllowUpdate() {
        return isAllowUpdate;
    }

    public void setAllowUpdate(boolean allowUpdate) {
        isAllowUpdate = allowUpdate;
    }
}
