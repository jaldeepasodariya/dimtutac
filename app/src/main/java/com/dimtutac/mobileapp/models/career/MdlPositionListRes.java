package com.dimtutac.mobileapp.models.career;

import com.dimtutac.mobileapp.common.AppConstants;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Jalotsav on 10/18/2017.
 */

public class MdlPositionListRes implements AppConstants {

    @SerializedName(KEY_SUCCESS_SML)
    private boolean success;
    @SerializedName(KEY_MESSAGE_SML)
    private String message;
    @SerializedName(KEY_DATA_SML)
    private ArrayList<MdlPositionListResData> arrylstMdlPositionListData;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<MdlPositionListResData> getArrylstMdlPositionListData() {
        return arrylstMdlPositionListData;
    }

    public void setArrylstMdlPositionListData(ArrayList<MdlPositionListResData> arrylstMdlPositionListData) {
        this.arrylstMdlPositionListData = arrylstMdlPositionListData;
    }
}
