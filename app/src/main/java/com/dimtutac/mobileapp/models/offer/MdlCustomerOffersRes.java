package com.dimtutac.mobileapp.models.offer;

import com.dimtutac.mobileapp.common.AppConstants;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Jalotsav on 1/31/2018.
 */

public class MdlCustomerOffersRes implements AppConstants {

    @SerializedName(KEY_SUCCESS_SML)
    private boolean success;
    @SerializedName(KEY_MESSAGE_SML)
    private String message;
    @SerializedName(KEY_DATA_SML)
    private ArrayList<MdlCustomerOffersResData> arrylstMdlCustomerOffersResData;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<MdlCustomerOffersResData> getArrylstMdlCustomerOffersResData() {
        return arrylstMdlCustomerOffersResData;
    }

    public void setArrylstMdlCustomerOffersResData(ArrayList<MdlCustomerOffersResData> arrylstMdlCustomerOffersResData) {
        this.arrylstMdlCustomerOffersResData = arrylstMdlCustomerOffersResData;
    }
}
