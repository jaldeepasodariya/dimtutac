package com.dimtutac.mobileapp.models.user;

import com.dimtutac.mobileapp.common.AppConstants;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Jalotsav on 9/15/2017.
 */

public class MdlUserCreateReq implements AppConstants {

    @SerializedName(KEY_FIRSTNAME)
    private String firstName;
    @SerializedName(KEY_LASTNAME)
    private String lastName;
    @SerializedName(KEY_EMAIL_ID)
    private String emailId;
    @SerializedName(KEY_USERNAME)
    private String userName;
    @SerializedName(KEY_PASSWORD)
    private String password;
    @SerializedName(KEY_PHONENO)
    private String phoneNo;
    @SerializedName(KEY_COMPANY_NAME)
    private String companyName;
    @SerializedName(KEY_GENDER)
    private String gender;
    @SerializedName(KEY_DATEOFBIRTH)
    private String dateOfBirth;
    @SerializedName(KEY_DEFAULT_LANGUAGE_ID)
    private String defaultLanguageId;
    @SerializedName(KEY_IS_FACEBOOK_LOGIN)
    private boolean isFacebookLogin;
    @SerializedName(KEY_FACEBOOK_ID)
    private String FacebookId;
    @SerializedName(KEY_MOBILENO)
    private String mobileNo;
    @SerializedName(KEY_IS_ACTIVE)
    private boolean isActive;
    @SerializedName(KEY_USERTYPE_ID)
    private String userTypeId;
    @SerializedName(KEY_CUSTOMER_TYPE_ID)
    private String customerTypeId;
    @SerializedName(KEY_IS_AGREE_TC)
    private boolean isAgreeTC;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getDefaultLanguageId() {
        return defaultLanguageId;
    }

    public void setDefaultLanguageId(String defaultLanguageId) {
        this.defaultLanguageId = defaultLanguageId;
    }

    public boolean isFacebookLogin() {
        return isFacebookLogin;
    }

    public void setFacebookLogin(boolean facebookLogin) {
        isFacebookLogin = facebookLogin;
    }

    public String getFacebookId() {
        return FacebookId;
    }

    public void setFacebookId(String facebookId) {
        FacebookId = facebookId;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public String getUserTypeId() {
        return userTypeId;
    }

    public void setUserTypeId(String userTypeId) {
        this.userTypeId = userTypeId;
    }

    public String getCustomerTypeId() {
        return customerTypeId;
    }

    public void setCustomerTypeId(String customerTypeId) {
        this.customerTypeId = customerTypeId;
    }

    public boolean isAgreeTC() {
        return isAgreeTC;
    }

    public void setAgreeTC(boolean agreeTC) {
        isAgreeTC = agreeTC;
    }
}
