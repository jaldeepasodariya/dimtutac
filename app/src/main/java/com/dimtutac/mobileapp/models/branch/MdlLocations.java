package com.dimtutac.mobileapp.models.branch;

/**
 * Created by Jalotsav on 27/8/17.
 */

public class MdlLocations {

    private String pkId, restaurantId, title, address, phonenumber, emailId, imagePath, storeHrs, latitude, longitude;

    public MdlLocations(String pkId, String restaurantId, String title, String address, String phonenumber, String imagePath, String storeHrs) {
        this.pkId = pkId;
        this.restaurantId = restaurantId;
        this.title = title;
        this.address = address;
        this.phonenumber = phonenumber;
        this.imagePath = imagePath;
        this.storeHrs = this.storeHrs;
    }

    public MdlLocations(String pkId, String restaurantId, String title, String address, String phonenumber, String emailId, String imagePath,
                        String storeHrs, String latitude, String longitude) {
        this.pkId = pkId;
        this.restaurantId = restaurantId;
        this.title = title;
        this.address = address;
        this.phonenumber = phonenumber;
        this.emailId = emailId;
        this.imagePath = imagePath;
        this.storeHrs = storeHrs;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getStoreHrs() {
        return storeHrs;
    }

    public void setStoreHrs(String storeHrs) {
        this.storeHrs = storeHrs;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}
