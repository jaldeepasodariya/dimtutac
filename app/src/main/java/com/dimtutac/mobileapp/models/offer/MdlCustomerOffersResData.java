package com.dimtutac.mobileapp.models.offer;

import com.dimtutac.mobileapp.common.AppConstants;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Jalotsav on 1/31/2018.
 */

public class MdlCustomerOffersResData implements AppConstants {

    @SerializedName(KEY_PKID)
    private String pkId;
    @SerializedName(KEY_ASSIGNMENT_CODE)
    private String assignmentCode;
    @SerializedName(KEY_OFFER_ID)
    private String offerId;
    @SerializedName(KEY_BRANCH_ID)
    private String branchId;
    @SerializedName(KEY_USER_ID_FIRSTCHARCAPS)
    private String userId;
    @SerializedName(KEY_IS_REDEEMED)
    private boolean isRedeemed;
    @SerializedName(KEY_IS_DELETED)
    private boolean isDeleted;
    @SerializedName(KEY_OFFER_NAME)
    private String offerName;
    @SerializedName(KEY_USERNAME)
    private String userName;
    @SerializedName(KEY_BRANCH_NAME)
    private String branchName;
    @SerializedName(KEY_PHOTO)
    private String photo;
    @SerializedName(KEY_FROM_DATE)
    private String fromDate;
    @SerializedName(KEY_TO_DATE)
    private String toDate;
    @SerializedName(KEY_DISCOUNT)
    private String discount;
    @SerializedName(KEY_DESCRIPTION)
    private String description;
    @SerializedName(KEY_OFFER_TYPE)
    private String offerType;
    @SerializedName(KEY_TERMS_AND_CONDITIONS)
    private String termsAndConditions;
    @SerializedName(KEY_TERMS_AND_CONDITIONS_LIST)
    private String[] strArryTermsAndConditionsList;

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getAssignmentCode() {
        return assignmentCode;
    }

    public void setAssignmentCode(String assignmentCode) {
        this.assignmentCode = assignmentCode;
    }

    public String getOfferId() {
        return offerId;
    }

    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public boolean isRedeemed() {
        return isRedeemed;
    }

    public void setRedeemed(boolean redeemed) {
        isRedeemed = redeemed;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public String getOfferName() {
        return offerName;
    }

    public void setOfferName(String offerName) {
        this.offerName = offerName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOfferType() {
        return offerType;
    }

    public void setOfferType(String offerType) {
        this.offerType = offerType;
    }

    public String getTermsAndConditions() {
        return termsAndConditions;
    }

    public void setTermsAndConditions(String termsAndConditions) {
        this.termsAndConditions = termsAndConditions;
    }

    public String[] getStrArryTermsAndConditionsList() {
        return strArryTermsAndConditionsList;
    }

    public void setStrArryTermsAndConditionsList(String[] strArryTermsAndConditionsList) {
        this.strArryTermsAndConditionsList = strArryTermsAndConditionsList;
    }
}
