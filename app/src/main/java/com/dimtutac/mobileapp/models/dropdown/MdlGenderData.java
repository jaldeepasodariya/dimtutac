package com.dimtutac.mobileapp.models.dropdown;

import com.dimtutac.mobileapp.common.AppConstants;

/**
 * Created by Jalotsav on 9/15/2017.
 */

public class MdlGenderData implements AppConstants {

    private String genderText;

    public MdlGenderData(String genderText) {
        this.genderText = genderText;
    }

    public String getGenderText() {
        return genderText;
    }

    public void setGenderText(String genderText) {
        this.genderText = genderText;
    }
}
