package com.dimtutac.mobileapp.models.user;

import com.dimtutac.mobileapp.common.AppConstants;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Jalotsav on 1/22/2018.
 */

public class MdlCheckIfFacebookIdExistReq implements AppConstants {

    @SerializedName(KEY_FACEBOOK_ID)
    private String facebookId;

    public MdlCheckIfFacebookIdExistReq() {
    }

    public MdlCheckIfFacebookIdExistReq(String facebookId) {
        this.facebookId = facebookId;
    }

    public String getFacebookId() {
        return facebookId;
    }
}
