package com.dimtutac.mobileapp.models.contactus;

import com.dimtutac.mobileapp.common.AppConstants;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Jalotsav on 10/14/2017.
 */

public class MdlCntctusPostReq implements AppConstants {

    @SerializedName(KEY_TITLE)
    private String title;
    @SerializedName(KEY_NAME)
    private String name;
    @SerializedName(KEY_EMAIL)
    private String email;
    @SerializedName(KEY_TELEPHONE)
    private String telephone;
    @SerializedName(KEY_MESSAGE_SML)
    private String message;
    @SerializedName(KEY_BRANCH_ID)
    private String branchId;
    @SerializedName(KEY_IS_DELETED)
    private boolean isDeleted;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }
}
