package com.dimtutac.mobileapp.models.career;

import com.dimtutac.mobileapp.common.AppConstants;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Jalotsav on 10/18/2017.
 */

public class MdlPositionListResData implements AppConstants {

    @SerializedName(KEY_PKID)
    private String pkId;
    @SerializedName(KEY_BRANCH_ID)
    private String branchId;
    @SerializedName(KEY_POSITION_NAME)
    private String positionName;
    @SerializedName(KEY_DESCRIPTION)
    private String description;
    @SerializedName(KEY_IS_DELETED)
    private boolean isDeleted;
    @SerializedName(KEY_IS_ACTIVE)
    private boolean isActive;
    @SerializedName(KEY_BRANCH_NAME)
    private String branchName;
    @SerializedName(KEY_DESCRIPTION_LIST)
    private String[] strArryDescriptionList;

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String[] getStrArryDescriptionList() {
        return strArryDescriptionList;
    }

    public void setStrArryDescriptionList(String[] strArryDescriptionList) {
        this.strArryDescriptionList = strArryDescriptionList;
    }
}
