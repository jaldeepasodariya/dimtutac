package com.dimtutac.mobileapp.models.forgotpassword;

import com.dimtutac.mobileapp.common.AppConstants;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Jalotsav on 2/8/2018.
 */

public class MdlForgotPasswordReq implements AppConstants {

    @SerializedName(KEY_EMAIL_ID)
    private String emailId;

    public MdlForgotPasswordReq(String emailId) {
        this.emailId = emailId;
    }

    public String getEmailId() {
        return emailId;
    }
}
