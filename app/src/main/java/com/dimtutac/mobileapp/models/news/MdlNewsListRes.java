package com.dimtutac.mobileapp.models.news;

import com.dimtutac.mobileapp.common.AppConstants;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Jalotsav on 12/8/2017.
 */

public class MdlNewsListRes implements AppConstants {

    @SerializedName(KEY_SUCCESS_SML)
    private boolean success;
    @SerializedName(KEY_MESSAGE_SML)
    private String message;
    @SerializedName(KEY_DATA_SML)
    private ArrayList<MdlNewsListResData> arrylstMdlNewsListResData;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<MdlNewsListResData> getArrylstMdlNewsListResData() {
        return arrylstMdlNewsListResData;
    }

    public void setArrylstMdlNewsListResData(ArrayList<MdlNewsListResData> arrylstMdlNewsListResData) {
        this.arrylstMdlNewsListResData = arrylstMdlNewsListResData;
    }
}
