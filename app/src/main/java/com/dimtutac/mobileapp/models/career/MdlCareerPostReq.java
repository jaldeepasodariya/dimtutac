package com.dimtutac.mobileapp.models.career;

import com.dimtutac.mobileapp.common.AppConstants;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Jalotsav on 10/28/2017.
 */

public class MdlCareerPostReq implements AppConstants {

    @SerializedName(KEY_FULLNAME)
    private String fullName;
    @SerializedName(KEY_GENDER)
    private String gender;
    @SerializedName(KEY_DATEOFBIRTH)
    private String dateOfBirth;
    @SerializedName(KEY_ADDRESS)
    private String address;
    @SerializedName(KEY_CONTACT_NO)
    private String contactNo;
    @SerializedName(KEY_EMAIL)
    private String email;
    @SerializedName(KEY_EDUCATION)
    private String education;
    @SerializedName(KEY_POSITION_APPLIED_FOR)
    private String positionAppliedFor;
    @SerializedName(KEY_WORK_EXPERIENCE)
    private String workExperience;
    @SerializedName(KEY_POSITION_ID)
    private String positionId;
    @SerializedName(KEY_BRANCH_ID)
    private String branchId;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getPositionAppliedFor() {
        return positionAppliedFor;
    }

    public void setPositionAppliedFor(String positionAppliedFor) {
        this.positionAppliedFor = positionAppliedFor;
    }

    public String getWorkExperience() {
        return workExperience;
    }

    public void setWorkExperience(String workExperience) {
        this.workExperience = workExperience;
    }

    public String getPositionId() {
        return positionId;
    }

    public void setPositionId(String positionId) {
        this.positionId = positionId;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }
}
