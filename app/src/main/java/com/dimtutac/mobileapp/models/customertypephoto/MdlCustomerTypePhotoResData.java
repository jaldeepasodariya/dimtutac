package com.dimtutac.mobileapp.models.customertypephoto;

import com.dimtutac.mobileapp.common.AppConstants;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Jalotsav on 1/31/2018.
 */

public class MdlCustomerTypePhotoResData implements AppConstants {

    @SerializedName(KEY_PKID)
    private String pkId;
    @SerializedName(KEY_IMAGEPATH)
    private String imagePath;
    @SerializedName(KEY_CUSTOMER_TYPE_ID)
    private String customerTypeId;

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getCustomerTypeId() {
        return customerTypeId;
    }

    public void setCustomerTypeId(String customerTypeId) {
        this.customerTypeId = customerTypeId;
    }
}
