package com.dimtutac.mobileapp.models.menu;

import com.dimtutac.mobileapp.common.AppConstants;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Jalotsav on 11/3/2017.
 */

public class MdlNewMonthlyItemListRes implements AppConstants {

    @SerializedName(KEY_SUCCESS_SML)
    private boolean success;
    @SerializedName(KEY_MESSAGE_SML)
    private String message;
    @SerializedName(KEY_DATA_SML)
    private ArrayList<MdlNewMonthlyItemListResData> arrylstMdlNewMonthlyItemListData;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<MdlNewMonthlyItemListResData> getArrylstMdlNewMonthlyItemListData() {
        return arrylstMdlNewMonthlyItemListData;
    }

    public void setArrylstMdlNewMonthlyItemListData(ArrayList<MdlNewMonthlyItemListResData> arrylstMdlNewMonthlyItemListData) {
        this.arrylstMdlNewMonthlyItemListData = arrylstMdlNewMonthlyItemListData;
    }
}
