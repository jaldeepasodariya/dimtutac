package com.dimtutac.mobileapp.models.customertypephoto;

import com.dimtutac.mobileapp.common.AppConstants;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Jalotsav on 1/31/2018.
 */

public class MdlCustomerTypePhotoRes implements AppConstants {

    @SerializedName(KEY_SUCCESS_SML)
    private boolean success;
    @SerializedName(KEY_MESSAGE_SML)
    private String message;
    @SerializedName(KEY_DATA_SML)
    private MdlCustomerTypePhotoResData mdlCustomerTypePhotoResData;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public MdlCustomerTypePhotoResData getMdlCustomerTypePhotoResData() {
        return mdlCustomerTypePhotoResData;
    }

    public void setMdlCustomerTypePhotoResData(MdlCustomerTypePhotoResData mdlCustomerTypePhotoResData) {
        this.mdlCustomerTypePhotoResData = mdlCustomerTypePhotoResData;
    }
}
