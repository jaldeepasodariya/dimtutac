package com.dimtutac.mobileapp.models;

import com.google.gson.annotations.SerializedName;

import static com.dimtutac.mobileapp.common.AppConstants.KEY_PHOTOURL;

/**
 * Created by Jalotsav on 11/14/2017.
 */

public class MdlPhotoURL {

    @SerializedName(KEY_PHOTOURL)
    private String photoURL;

    public MdlPhotoURL(String photoURL) {
        this.photoURL = photoURL;
    }

    public String getPhotoURL() {
        return photoURL;
    }

    public void setPhotoURL(String photoURL) {
        this.photoURL = photoURL;
    }
}
