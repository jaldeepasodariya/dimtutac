package com.dimtutac.mobileapp.models.branch;

import com.dimtutac.mobileapp.common.AppConstants;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Jalotsav on 9/18/2017.
 */

public class MdlLocationListBusineshrs implements AppConstants {

    @SerializedName(KEY_PKID)
    private String pkId;
    @SerializedName(KEY_BRANCH_ID)
    private String branchId;
    @SerializedName(KEY_DAY)
    private String day;
    @SerializedName(KEY_OPENING_TIME)
    private String openingTime;
    @SerializedName(KEY_CLOSING_TIME)
    private String closingTime;
    @SerializedName(KEY_IS_DELETED)
    private boolean isDeleted;
    @SerializedName(KEY_IS_ACTIVE)
    private boolean isActive;

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getOpeningTime() {
        return openingTime;
    }

    public void setOpeningTime(String openingTime) {
        this.openingTime = openingTime;
    }

    public String getClosingTime() {
        return closingTime;
    }

    public void setClosingTime(String closingTime) {
        this.closingTime = closingTime;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }
}
