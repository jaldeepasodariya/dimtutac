package com.dimtutac.mobileapp;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.dimtutac.mobileapp.adapter.RcyclrAttchdImgsAdapter;
import com.dimtutac.mobileapp.common.AppConstants;
import com.dimtutac.mobileapp.common.GeneralFunctions;
import com.dimtutac.mobileapp.common.LogHelper;
import com.dimtutac.mobileapp.common.RecyclerViewEmptySupport;
import com.dimtutac.mobileapp.common.UserSessionManager;
import com.dimtutac.mobileapp.models.MdlPhotoURL;
import com.dimtutac.mobileapp.models.checkin.MdlCheckinDoReq;
import com.dimtutac.mobileapp.models.checkin.MdlCheckinDoRes;
import com.dimtutac.mobileapp.navgtndrawer.NavgtnDrwrMain;
import com.dimtutac.mobileapp.retrofitapi.APICheckin;
import com.dimtutac.mobileapp.retrofitapi.APIRetroBuilder;
import com.dimtutac.mobileapp.utils.ValidationUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Jalotsav on 11/11/2017.
 */

public class ActvtyCheckinDo extends AppCompatActivity {

    private static final String TAG = ActvtyCheckinDo.class.getSimpleName();

    @BindView(R.id.cordntrlyot_frgmnt_checkin_do) CoordinatorLayout mCrdntrlyot;
    @BindView(R.id.imgvw_frgmnt_checkin_do_image) ImageView mImgvwImage;
    @BindView(R.id.et_frgmnt_checkin_do_comments) EditText mEtComments;
    @BindView(R.id.lnrlyot_recyclremptyvw_appearhere) LinearLayout mLnrlyotAppearHere;
    @BindView(R.id.tv_recyclremptyvw_appearhere) TextView mTvAppearHere;
    @BindView(R.id.rcyclrvw_frgmnt_checkin_do) RecyclerViewEmptySupport mRecyclerView;
    @BindView(R.id.prgrsbr_frgmnt_checkin_do) ProgressBar mPrgrsbrMain;

    @BindString(R.string.no_intrnt_cnctn) String mNoInternetConnMsg;
    @BindString(R.string.server_problem_sml) String mServerPrblmMsg;
    @BindString(R.string.internal_problem_sml) String mInternalPrblmMsg;
    @BindString(R.string.attchd_imgs_appear_here) String mAttchdImgsAppearHere;
    @BindString(R.string.entr_comments_sml) String mEntrComments;

    @BindDrawable(R.mipmap.ic_logo_black) Drawable mDrwblDefault;

    String mLatitude = "", mLongitude = "";
    RecyclerView.LayoutManager mLayoutManager;
    RcyclrAttchdImgsAdapter mAdapter;
    public ArrayList<String> mArrylstSelectedImages = new ArrayList<>();
    String mBranchId, mBranchTitle, mRestaurantId, mBranchPhotoURL;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lo_actvty_checkin_do);
        ButterKnife.bind(this);

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) { e.printStackTrace(); }

        mBranchId = getIntent().getStringExtra(AppConstants.PUT_EXTRA_BRANCH_ID);
        mBranchTitle = getIntent().getStringExtra(AppConstants.PUT_EXTRA_BRANCH_NAME);
        mRestaurantId = getIntent().getStringExtra(AppConstants.PUT_EXTRA_RESTAURANT_ID);
        mBranchPhotoURL = getIntent().getStringExtra(AppConstants.PUT_EXTRA_PHOTO_URL);
        mLatitude = getIntent().getStringExtra(AppConstants.PUT_EXTRA_LATITUDE);
        mLongitude = getIntent().getStringExtra(AppConstants.PUT_EXTRA_LONGITUDE);

        // Set Branch Details
        if(!TextUtils.isEmpty(mBranchPhotoURL)) {
            Picasso.with(this)
                    .load(AppConstants.ATCHMNT_ROOT_URL.concat(mBranchPhotoURL))
                    .placeholder(mDrwblDefault)
                    .into(mImgvwImage);
        }

        mLayoutManager = new GridLayoutManager(this, 3);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setEmptyView(mLnrlyotAppearHere);

        mTvAppearHere.setText(mAttchdImgsAppearHere);

        setAttachedImages();
    }

    // Set Attached images on UI
    private void setAttachedImages() {

        mArrylstSelectedImages = new ArrayList<>();
        mArrylstSelectedImages.addAll(AppConstants.ARRYLST_ATTCHDIMGS_PATH);
        mAdapter = new RcyclrAttchdImgsAdapter(this, null, mArrylstSelectedImages, mDrwblDefault);
        mRecyclerView.setAdapter(mAdapter);
    }

    @OnClick({R.id.appcmptbtn_frgmnt_checkin_do_done})
    public void onClickView(View view) {

        switch (view.getId()) {
            case R.id.appcmptbtn_frgmnt_checkin_do_done:

                checkAllValidation();
                break;
        }
    }
    private void checkAllValidation() {

        if (!ValidationUtils.validateEmpty(this, mCrdntrlyot, mEtComments, mEntrComments))
            return;

        if(mPrgrsbrMain.getVisibility() != View.VISIBLE) {
            if (GeneralFunctions.isNetConnected(this))
                callDoCheckinAPI();
            else Snackbar.make(mCrdntrlyot, mNoInternetConnMsg, Snackbar.LENGTH_LONG).show();
        }
    }

    private void callDoCheckinAPI() {

        mPrgrsbrMain.setVisibility(View.VISIBLE);
        UserSessionManager session = new UserSessionManager(this);

        // Get + Convert attached image path to Base64
        ArrayList<MdlPhotoURL> arrylstMdlPhotoURL = new ArrayList<>();
        for(String slctdImgPath : mArrylstSelectedImages) {

            String strBase64 = GeneralFunctions.convertImageToBase64(slctdImgPath);
            arrylstMdlPhotoURL.add(new MdlPhotoURL(strBase64)); // "data:image/jpeg;base64," +
        }

        MdlCheckinDoReq objMdlCheckinDoReq = new MdlCheckinDoReq();
        objMdlCheckinDoReq.setUserId(session.getUserId());
        objMdlCheckinDoReq.setRestaurantId(mRestaurantId);
        objMdlCheckinDoReq.setBranchId(mBranchId);
        objMdlCheckinDoReq.setWritingStatus(mEtComments.getText().toString().trim());
        objMdlCheckinDoReq.setLatitude(mLatitude);
        objMdlCheckinDoReq.setLongitude(mLongitude);
        objMdlCheckinDoReq.setArrylstMdlPhotoURL(arrylstMdlPhotoURL);

        APICheckin objApiCheckin = APIRetroBuilder.getRetroBuilder(true).create(APICheckin.class);
        Call<MdlCheckinDoRes> callMdlCheckinDoRes = objApiCheckin.callDoCheckin(objMdlCheckinDoReq);
        callMdlCheckinDoRes.enqueue(new Callback<MdlCheckinDoRes>() {
            @Override
            public void onResponse(Call<MdlCheckinDoRes> call, Response<MdlCheckinDoRes> response) {
                mPrgrsbrMain.setVisibility(View.GONE);
                if(response.isSuccessful()) {

                    try {
                        if(response.body().isSuccess()) {

                            Toast.makeText(ActvtyCheckinDo.this, response.body().getData(), Toast.LENGTH_SHORT).show();

                            setResult(RESULT_OK);
                            onBackPressed();
                        } else
                            Snackbar.make(mCrdntrlyot, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                    } catch (Exception e) {

                        e.printStackTrace();
                        LogHelper.printLog(AppConstants.LOGTYPE_ERROR, TAG, e.getMessage());
                        Snackbar.make(mCrdntrlyot, mInternalPrblmMsg, Snackbar.LENGTH_LONG).show();
                    }
                } else
                    Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<MdlCheckinDoRes> call, Throwable t) {

                mPrgrsbrMain.setVisibility(View.GONE);
                Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
