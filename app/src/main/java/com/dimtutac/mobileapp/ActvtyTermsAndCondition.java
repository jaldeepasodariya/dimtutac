package com.dimtutac.mobileapp;

import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.dimtutac.mobileapp.common.AppConstants;
import com.dimtutac.mobileapp.common.LogHelper;
import com.dimtutac.mobileapp.common.UserSessionManager;
import com.dimtutac.mobileapp.models.offer.MdlRedeemOfferReq;
import com.dimtutac.mobileapp.models.offer.MdlRedeemOfferRes;
import com.dimtutac.mobileapp.retrofitapi.APIOffer;
import com.dimtutac.mobileapp.retrofitapi.APIRetroBuilder;
import com.squareup.picasso.Picasso;

import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Jalotsav on 1/31/2018.
 */

public class ActvtyTermsAndCondition extends AppCompatActivity implements AppConstants {

    private static final String TAG = ActvtyTermsAndCondition.class.getSimpleName();

    UserSessionManager session;

    @BindView(R.id.cordntrlyot_actvty_termscondtn) CoordinatorLayout mCrdntrlyot;
    @BindView(R.id.imgvw_actvty_termscondtn_image) ImageView mImgvwOfferImage;
    @BindView(R.id.tv_actvty_termscondtn_termscondtnslst) TextView mTvTermsAndCondtns;
    @BindView(R.id.prgrsbr_actvty_termscondtn) ProgressBar mPrgrsbrMain;

    @BindString(R.string.no_intrnt_cnctn) String mNoInternetConnMsg;
    @BindString(R.string.server_problem_sml) String mServerPrblmMsg;
    @BindString(R.string.internal_problem_sml) String mInternalPrblmMsg;

    @BindDrawable(R.mipmap.ic_logo_black) Drawable mDrwblDefault;

    String mOfferId, mAssignmntCode, mOfferPhotoURL, mstrTermsAndCondtnLst;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lo_actvty_termsandconditions);
        ButterKnife.bind(this);

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) { e.printStackTrace(); }

        session = new UserSessionManager(this);

        mOfferId = getIntent().getStringExtra(PUT_EXTRA_OFFER_ID);
        mAssignmntCode = getIntent().getStringExtra(PUT_EXTRA_ASSIGNMENT_CODE);
        mOfferPhotoURL = getIntent().getStringExtra(PUT_EXTRA_PHOTO_URL);
        mstrTermsAndCondtnLst = getIntent().getStringExtra(PUT_EXTRA_TERMSANDCONDTN_LIST_STR);

        mTvTermsAndCondtns.setText(mstrTermsAndCondtnLst);
        if(!TextUtils.isEmpty(mOfferPhotoURL)) {
            Picasso.with(this)
                    .load(AppConstants.ATCHMNT_ROOT_URL.concat(mOfferPhotoURL))
                    .placeholder(mDrwblDefault)
                    .into(mImgvwOfferImage);
        }
    }

    @OnClick({R.id.appcmptbtn_actvty_termscondtn_redeem})
    public void onClickView(View view) {

        switch (view.getId()) {
            case R.id.appcmptbtn_actvty_termscondtn_redeem:

                confirmRedeemAlertDialog();
                break;
        }
    }

    // Show AlertDialog for confirm to Redeem this offer
    private void confirmRedeemAlertDialog() {

        AlertDialog.Builder alrtDlg = new AlertDialog.Builder(this);
        alrtDlg.setTitle(getString(R.string.app_name));
        alrtDlg.setMessage(getString(R.string.redeem_alrtdlg_msg));
        alrtDlg.setNegativeButton(getString(R.string.no_sml).toUpperCase(), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        alrtDlg.setPositiveButton(getString(R.string.yes_sml).toUpperCase(), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                callRedeemOfferAPI();
            }
        });

        alrtDlg.show();
    }

    private void callRedeemOfferAPI() {

        mPrgrsbrMain.setVisibility(View.VISIBLE);
        MdlRedeemOfferReq objMdlLoginReq = new MdlRedeemOfferReq(mOfferId, mAssignmntCode, session.getUserId());

        APIOffer objApiOffer = APIRetroBuilder.getRetroBuilder(true).create(APIOffer.class);
        Call<MdlRedeemOfferRes> callMdlRedeemOfferRes = objApiOffer.callRedeemOffer(objMdlLoginReq);
        callMdlRedeemOfferRes.enqueue(new Callback<MdlRedeemOfferRes>() {
            @Override
            public void onResponse(Call<MdlRedeemOfferRes> call, Response<MdlRedeemOfferRes> response) {

                mPrgrsbrMain.setVisibility(View.GONE);
                if(response.isSuccessful()) {

                    try {

                        if (response.body().isSuccess()) {

                            Toast.makeText(ActvtyTermsAndCondition.this, response.body().getData(), Toast.LENGTH_LONG).show();
                            onBackPressed();
                        } else
                            Snackbar.make(mCrdntrlyot, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                    } catch (Exception e) {

                        e.printStackTrace();
                        LogHelper.printLog(AppConstants.LOGTYPE_ERROR, TAG, e.getMessage());
                        Snackbar.make(mCrdntrlyot, mInternalPrblmMsg, Snackbar.LENGTH_LONG).show();
                    }
                } else
                    Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<MdlRedeemOfferRes> call, Throwable t) {

                mPrgrsbrMain.setVisibility(View.GONE);
                Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
