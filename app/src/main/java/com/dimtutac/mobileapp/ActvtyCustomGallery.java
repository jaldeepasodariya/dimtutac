package com.dimtutac.mobileapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dimtutac.mobileapp.common.AppConstants;
import com.dimtutac.mobileapp.common.GeneralFunctions;
import com.dimtutac.mobileapp.common.UserSessionManager;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Jalotsav on 11/15/2017.
 */

public class ActvtyCustomGallery extends AppCompatActivity implements AppConstants {

    private static final String TAG = ActvtyCustomGallery.class.getSimpleName();

    UserSessionManager session;

    @BindView(R.id.grdvw_actvty_cstmgllry_imgdir) GridView mGrdvwImgDir;
    @BindView(R.id.prgrsbr_actvty_cstmgllry_imgdir) ProgressBar mPrgrsbrMain;
    TextView mTvSlctdImgCnt;

    @BindDrawable(R.mipmap.ic_logo_black) Drawable mDrwblDefault;

    GridViewImageAdapterImgDirctry mGrdvwImgAdapter;
    int mColmnWidth, mTotalSlctdImgCnt = 0;
    ArrayList<String> mArrylstDirctryPathContainsImg, mArrylstLastFileDirctry, mArrylstAlrdySlctdImgPath, mArrylstLastFileDirctryImgCnt;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lo_actvty_customgallery);
        ButterKnife.bind(this);

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        session = new UserSessionManager(this);

        mArrylstAlrdySlctdImgPath = new ArrayList<>();
        mArrylstAlrdySlctdImgPath.addAll(getIntent().getStringArrayListExtra(PUT_EXTRA_ARRAYLST_ALREADY_SELECTED_IMGS_PATHS));
        mTotalSlctdImgCnt = getIntent().getIntExtra(PUT_EXTRA_TOTAL_SELECTED_IMG_COUNT, 0);

        setSelectedImageCount();

        initializeGridLayout();

        mArrylstDirctryPathContainsImg = new ArrayList<>();
        mArrylstLastFileDirctry = new ArrayList<>();
        mArrylstLastFileDirctryImgCnt = new ArrayList<>();

        // Get Directory paths which are contain Image and set to GridView
        if (TextUtils.isEmpty(session.getDirctrypathsCntnsimgs())) {

            new GetDirctryPathCntnsImgAsync().execute();
        } else {

            mPrgrsbrMain.setVisibility(View.VISIBLE);

            getDirctrPathsLastImgPathFromShrdprfrnc();

            mGrdvwImgAdapter = new GridViewImageAdapterImgDirctry();
            mGrdvwImgDir.setAdapter(mGrdvwImgAdapter);

            mPrgrsbrMain.setVisibility(View.GONE);

            new GetDirctryPathCntnsImgAsyncRefreshGridview().execute();
        }
    }

    // Initialize GridView Layout
    private void initializeGridLayout() {

        float padding = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, GRID_PADDING_8, getResources().getDisplayMetrics());

        mColmnWidth = (int) ((GeneralFunctions.getScreenWidth(this) - ((NUM_OF_COLUMNS_2 + 1) * padding)) / NUM_OF_COLUMNS_2);

        mGrdvwImgDir.setNumColumns(NUM_OF_COLUMNS_2);
        mGrdvwImgDir.setColumnWidth(mColmnWidth);
        mGrdvwImgDir.setStretchMode(GridView.NO_STRETCH);
        mGrdvwImgDir.setPadding((int) padding, (int) padding, (int) padding, (int) padding);
        mGrdvwImgDir.setHorizontalSpacing((int) padding);
        mGrdvwImgDir.setVerticalSpacing((int) padding);
    }

    // Get Directory paths which are contain Image and set to GridView
    private class GetDirctryPathCntnsImgAsync extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mPrgrsbrMain.setVisibility(View.VISIBLE);

            mArrylstDirctryPathContainsImg = new ArrayList<String>();
            mArrylstLastFileDirctry = new ArrayList<String>();
            mArrylstLastFileDirctryImgCnt = new ArrayList<String>();
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {

                // Get all Directory name which contains images
                mArrylstDirctryPathContainsImg = getDirectoryPathContainsImg(PATH_EXTRNL_STRNG);

                // Get last file name path of given directory
                for (int i = 0; i < mArrylstDirctryPathContainsImg.size(); i++) {

                    getLastFileDrictryWithImgsCnt(new File(mArrylstDirctryPathContainsImg.get(i)));
                }

                storeDirctrPathsLastImgPathToShrdprfrnc();

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            mGrdvwImgAdapter = new GridViewImageAdapterImgDirctry();
            mGrdvwImgDir.setAdapter(mGrdvwImgAdapter);

            mPrgrsbrMain.setVisibility(View.GONE);
        }
    }

    // Get Directory paths which are contain Image and set to GridView
    private class GetDirctryPathCntnsImgAsyncRefreshGridview extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mArrylstDirctryPathContainsImg = new ArrayList<>();
            mArrylstLastFileDirctry = new ArrayList<>();
            mArrylstLastFileDirctryImgCnt = new ArrayList<>();
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {

                // Get all Directory name which contains images
                mArrylstDirctryPathContainsImg = getDirectoryPathContainsImg(PATH_EXTRNL_STRNG);

                // Get last file name path of given directory
                for (int i = 0; i < mArrylstDirctryPathContainsImg.size(); i++) {

                    getLastFileDrictryWithImgsCnt(new File(mArrylstDirctryPathContainsImg.get(i)));
                }

                storeDirctrPathsLastImgPathToShrdprfrnc();

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            mGrdvwImgAdapter.notifyDataSetChanged();
        }
    }

    // Get all Directory paths which contains IMAGES(png,jpg,jpeg)
    public ArrayList<String> getDirectoryPathContainsImg(File file) {

        String state = Environment.getExternalStorageState();
        if (state.contentEquals(Environment.MEDIA_MOUNTED) || state.contentEquals(Environment.MEDIA_MOUNTED_READ_ONLY)) {
            File[] directories = file.listFiles();
            if (directories != null) {

                for (File fileDirectory : directories) {

                    if (fileDirectory.isDirectory()) {

                        String dirctryName = fileDirectory.toString().substring(fileDirectory.toString().lastIndexOf("/") + 1).trim();
                        // Skip Directory which name is "Android" and starts with "."
                        if (!dirctryName.equals("Android") && !dirctryName.startsWith(".")) {

                            getDirectoryPathContainsImg(fileDirectory);
                        }
                    } else {

                        if (GeneralFunctions.isSupportedFile(fileDirectory.getAbsolutePath())) {

                            mArrylstDirctryPathContainsImg.add(fileDirectory.getParent());
                            break;
                        }
                    }
                }
            }
        } else {
            Log.e(TAG, "External Storage Unaccessible: " + state);
        }

        return mArrylstDirctryPathContainsImg;
    }

    // Reading file paths from SDCard
    public void getLastFileDrictryWithImgsCnt(File directory) {

        ArrayList<String> filePaths = new ArrayList<String>();

        if (directory.isDirectory()) {

            File[] listFiles = directory.listFiles();

            if (listFiles != null && listFiles.length > 0) {

                for (File objFile : listFiles) {

                    String filePath = objFile.getAbsolutePath();

                    if (GeneralFunctions.isSupportedFile(filePath)) {
                        filePaths.add(filePath);
                    }
                }
            }
        }

        if (filePaths.size() > 0) {
            mArrylstLastFileDirctry.add(filePaths.get(filePaths.size() - 1));
            mArrylstLastFileDirctryImgCnt.add(String.valueOf(filePaths.size()));
        } else {
            mArrylstLastFileDirctry.add("");
            mArrylstLastFileDirctryImgCnt.add("0");
        }
    }

    // Store Image contains directory paths and last image paths to SharedPreferences
    public void storeDirctrPathsLastImgPathToShrdprfrnc() {

        session.setDirctrypathsCntnsimgs(GeneralFunctions.convertArraylistToString(mArrylstDirctryPathContainsImg));
        session.setLastfilepathImgcntnsDirctry(GeneralFunctions.convertArraylistToString(mArrylstLastFileDirctry));
        session.setLastfilepathImgcntnsDirctryImgcnt(GeneralFunctions.convertArraylistToString(mArrylstLastFileDirctryImgCnt));
    }

    // Get Image contains directory paths and last image paths from SharedPreferences
    public void getDirctrPathsLastImgPathFromShrdprfrnc() {

        mArrylstDirctryPathContainsImg = GeneralFunctions.convertStringToArraylist(session.getDirctrypathsCntnsimgs());
        mArrylstLastFileDirctry = GeneralFunctions.convertStringToArraylist(session.getLastfilepathImgcntnsDirctry());
        mArrylstLastFileDirctryImgCnt = GeneralFunctions.convertStringToArraylist(session.getLastfilepathImgcntnsDirctryImgcnt());
    }

    private class GridViewImageAdapterImgDirctry extends BaseAdapter {

        private LayoutInflater mInflater;

        GridViewImageAdapterImgDirctry() {

            mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {

            return mArrylstLastFileDirctry.size();
        }

        @Override
        public Object getItem(int position) {

            return mArrylstLastFileDirctry.get(position);
        }

        @Override
        public long getItemId(int position) {

            return position;
        }

        class ViewHolder {
            ImageView imgThumb;
            TextView tvDirctryName;
            TextView tvImgsCnt;
            int id;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            final ViewHolder holder;
            if (convertView == null) {

                holder = new ViewHolder();
                convertView = mInflater.inflate(R.layout.lo_cstm_gllry_imgdirctry_item, null);
                holder.imgThumb = convertView.findViewById(R.id.imgvw_cstm_gllry_imgdirtctry_item_imgthumb);
                holder.tvDirctryName = convertView.findViewById(R.id.tv_cstm_gllry_imgdirtctry_item_dirctrynm);
                holder.tvImgsCnt = convertView.findViewById(R.id.tv_cstm_gllry_imgdirtctry_item_imgcnt);

                convertView.setTag(holder);
            } else {

                holder = (ViewHolder) convertView.getTag();
            }

            holder.imgThumb.setScaleType(ImageView.ScaleType.CENTER_CROP);
            holder.imgThumb.setLayoutParams(new RelativeLayout.LayoutParams(mColmnWidth, mColmnWidth));
            Picasso.with(ActvtyCustomGallery.this)
                    .load(new File(mArrylstLastFileDirctry.get(position)))
                    .placeholder(mDrwblDefault)
                    .into(holder.imgThumb);

            holder.imgThumb.setOnClickListener(new OnImageClickListener(position));

            String dirctryName = mArrylstDirctryPathContainsImg.get(position).substring(mArrylstDirctryPathContainsImg.get(position).lastIndexOf("/") + 1).trim();
            if (dirctryName.length() > 16) {

                dirctryName = dirctryName.substring(0, 13).concat("...");
                holder.tvDirctryName.setText(dirctryName);
            } else {

                holder.tvDirctryName.setText(dirctryName);
            }

            holder.tvImgsCnt.setText(mArrylstLastFileDirctryImgCnt.get(position));

            return convertView;
        }

        class OnImageClickListener implements View.OnClickListener {

            int _postion;

            OnImageClickListener(int position) {
                this._postion = position;
            }

            @Override
            public void onClick(View v) {

                Intent intnt_slctimgs = new Intent(ActvtyCustomGallery.this, ActvtyCustomGallerySelectImgs.class);
                intnt_slctimgs.putExtra(PUT_EXTRA_SELECTED_IMAGE_DIRECTORY_PATH, mArrylstDirctryPathContainsImg.get(_postion));
                intnt_slctimgs.putExtra(PUT_EXTRA_ARRAYLST_ALREADY_SELECTED_IMGS_PATHS, mArrylstAlrdySlctdImgPath);
                intnt_slctimgs.putExtra(PUT_EXTRA_TOTAL_SELECTED_IMG_COUNT, mTotalSlctdImgCnt);
                startActivityForResult(intnt_slctimgs, REQUEST_PICK_IMAGE);
            }
        }

        /*
         * Resizing image size
         */
        /*public Bitmap decodeFile(String filePath, int WIDTH, int HIGHT) {

            try {

                File f = new File(filePath);

                BitmapFactory.Options o = new BitmapFactory.Options();
                o.inJustDecodeBounds = true;
                BitmapFactory.decodeStream(new FileInputStream(f), null, o);

                final int REQUIRED_WIDTH = WIDTH;
                final int REQUIRED_HIGHT = HIGHT;
                int scale = 1;
                while (o.outWidth / scale / 2 >= REQUIRED_WIDTH
                        && o.outHeight / scale / 2 >= REQUIRED_HIGHT)
                    scale *= 2;

                BitmapFactory.Options o2 = new BitmapFactory.Options();
                o2.inSampleSize = scale;
                return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            return null;
        }*/
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_PICK_IMAGE) {

                mTotalSlctdImgCnt = data.getIntExtra(PUT_EXTRA_TOTAL_SELECTED_IMG_COUNT, 0);
                mArrylstAlrdySlctdImgPath = data.getStringArrayListExtra(PUT_EXTRA_ARRAYLST_ALREADY_SELECTED_IMGS_PATHS);
                setSelectedImageCount();

                // if user total selection of images is 10, then jump to directly MainScreen
                if (mTotalSlctdImgCnt == 10) {

                    Intent i = new Intent();
                    i.putExtra(PUT_EXTRA_ARRAYLST_ALREADY_SELECTED_IMGS_PATHS, mArrylstAlrdySlctdImgPath);
                    i.putExtra(PUT_EXTRA_TOTAL_SELECTED_IMG_COUNT, mTotalSlctdImgCnt);
                    setResult(Activity.RESULT_OK, i);
                    finish();
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_customgallery, menu);

        MenuItem item = menu.findItem(R.id.action_count);
        MenuItemCompat.setActionView(item, R.layout.lo_view_textvw_count);
        mTvSlctdImgCnt = (TextView) MenuItemCompat.getActionView(item);
        mTvSlctdImgCnt.setText(String.valueOf(mTotalSlctdImgCnt));

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                break;
            case R.id.action_done:

                if(mArrylstDirctryPathContainsImg.size() < 1){

                    Toast.makeText(this, getString(R.string.error_on_detectimgs_fromdevice), Toast.LENGTH_SHORT).show();
                }else if(mArrylstAlrdySlctdImgPath.size() < 1){

                    Toast.makeText(this, getString(R.string.slct_atleastone_image), Toast.LENGTH_SHORT).show();
                }else{

                    Intent i = new Intent();
                    i.putExtra(PUT_EXTRA_ARRAYLST_ALREADY_SELECTED_IMGS_PATHS, mArrylstAlrdySlctdImgPath);
                    i.putExtra(PUT_EXTRA_TOTAL_SELECTED_IMG_COUNT, mTotalSlctdImgCnt);
                    setResult(Activity.RESULT_OK, i);
                    finish();
                }
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    // Set Selected image count and update on ActionBar menu UI
    private void setSelectedImageCount() {

        invalidateOptionsMenu();
    }
}
