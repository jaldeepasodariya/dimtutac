package com.dimtutac.mobileapp;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.dimtutac.mobileapp.adapter.SpnrArryAdptrBranch;
import com.dimtutac.mobileapp.common.AppConstants;
import com.dimtutac.mobileapp.common.GeneralFunctions;
import com.dimtutac.mobileapp.common.LogHelper;
import com.dimtutac.mobileapp.common.UserSessionManager;
import com.dimtutac.mobileapp.models.branch.MdlBranchListRes;
import com.dimtutac.mobileapp.models.branch.MdlBranchListResData;
import com.dimtutac.mobileapp.models.reservation.MdlResrvtnDoReq;
import com.dimtutac.mobileapp.models.reservation.MdlResrvtnDoRes;
import com.dimtutac.mobileapp.retrofitapi.APIBranch;
import com.dimtutac.mobileapp.retrofitapi.APIReservation;
import com.dimtutac.mobileapp.retrofitapi.APIRetroBuilder;
import com.dimtutac.mobileapp.utils.ValidationUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Jalotsav on 29/8/17.
 */

public class ActvtyReservation extends AppCompatActivity {

    private static final String TAG = ActvtyReservation.class.getSimpleName();

    UserSessionManager session;

    @BindView(R.id.cordntrlyot_actvty_resrvtn) CoordinatorLayout mCrdntrlyot;
    @BindView(R.id.et_actvty_resrvtn_name) EditText mEtName;
    @BindView(R.id.et_actvty_resrvtn_phoneno) EditText mEtPhoneno;
    @BindView(R.id.et_actvty_resrvtn_email) EditText mEtEmail;
    @BindView(R.id.et_actvty_resrvtn_guests) EditText mEtGuests;
    @BindView(R.id.et_actvty_resrvtn_comments) EditText mEtComments;
    @BindView(R.id.tv_actvty_resrvtn_date) TextView mTvDate;
    @BindView(R.id.tv_actvty_resrvtn_time) TextView mTvTime;
    @BindView(R.id.prgrsbr_actvty_resrvtn) ProgressBar mPrgrsbrMain;
    @BindView(R.id.spnr_actvty_resrvtn_locations) Spinner mSpnrLocations;
    @BindView(R.id.appcmptbtn_actvty_resrvtn_cancelresrvtrn) AppCompatButton mAppcpmtbtnCancel;

    @BindString(R.string.no_intrnt_cnctn) String mNoInternetConnMsg;
    @BindString(R.string.server_problem_sml) String mServerPrblmMsg;
    @BindString(R.string.internal_problem_sml) String mInternalPrblmMsg;
    @BindString(R.string.entr_your_name_sml) String mEntrName;
    @BindString(R.string.entr_your_email_sml) String mEntrEmail;
    @BindString(R.string.entr_numberof_person_sml) String mEntrNoOfPerson;
    @BindString(R.string.entr_date_wanto_book_table_sml) String mSlctDateMsg;
    @BindString(R.string.select_time_from_avlb_slots_sml) String mSlctTimeMsg;
    @BindString(R.string.invalid_time_sml) String mInvalidTime;
    @BindString(R.string.entr_branch_wantto_book_table_sml) String mSlctLocationMsg;

    String mRestaurantId;
    ArrayList<MdlBranchListResData> mArrylstBranchData;
    SpnrArryAdptrBranch mAdptrSpnrBranch;
    Calendar mCalendar;
    long mSlctdDateTimestamp;
    int mSlctdFromHourOfDay, mSlctdFromMinute;
    String hourString, minuteString;
    boolean isDateSelected = false, isTimeSelected = false;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lo_actvty_reservation);
        ButterKnife.bind(this);

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) { e.printStackTrace(); }

        session = new UserSessionManager(this);

        mRestaurantId = getIntent().getStringExtra(AppConstants.PUT_EXTRA_RESTAURANT_ID);

        setUserSessionDataOnUI();

        mArrylstBranchData = new ArrayList<>();
        mArrylstBranchData.add(new MdlBranchListResData("0", mSlctLocationMsg));
        mAdptrSpnrBranch = new SpnrArryAdptrBranch(this, android.R.layout.simple_spinner_dropdown_item, mArrylstBranchData);
        mSpnrLocations.setAdapter(mAdptrSpnrBranch);

        if (GeneralFunctions.isNetConnected(this))
            getBranches();
        else Snackbar.make(mCrdntrlyot, mNoInternetConnMsg, Snackbar.LENGTH_LONG).show();
    }

    private void setUserSessionDataOnUI() {

        mEtName.setText(session.getFirstName().concat(" ").concat(session.getLastName()));
        mEtPhoneno.setText(session.getPhoneNo());
        mEtEmail.setText(session.getEmailId());

        mAppcpmtbtnCancel.setVisibility(View.GONE);
    }

    // Call Retrofit API
    private void getBranches() {

        mPrgrsbrMain.setVisibility(View.VISIBLE);

        APIBranch objApiBranch = APIRetroBuilder.getRetroBuilder(true).create(APIBranch.class);
        Call<MdlBranchListRes> callMdlBranchRes = objApiBranch.callGetBranchList();
        callMdlBranchRes.enqueue(new Callback<MdlBranchListRes>() {
            @Override
            public void onResponse(Call<MdlBranchListRes> call, Response<MdlBranchListRes> response) {

                mPrgrsbrMain.setVisibility(View.GONE);
                if(response.isSuccessful()) {

                    try {
                        if(response.body().isSuccess()) {

                            ArrayList<MdlBranchListResData> arrylstMdlBranchListData = response.body().getArrylstMdlBranchListData();

                            mArrylstBranchData = new ArrayList<>();
                            mArrylstBranchData.add(new MdlBranchListResData("0", mSlctLocationMsg));
                            mArrylstBranchData.addAll(arrylstMdlBranchListData);
                            mAdptrSpnrBranch = new SpnrArryAdptrBranch(ActvtyReservation.this, android.R.layout.simple_spinner_dropdown_item, mArrylstBranchData);
                            mSpnrLocations.setAdapter(mAdptrSpnrBranch);
                        } else
                            Snackbar.make(mCrdntrlyot, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                    } catch (Exception e) {

                        e.printStackTrace();
                        LogHelper.printLog(AppConstants.LOGTYPE_ERROR, TAG, e.getMessage());
                        Snackbar.make(mCrdntrlyot, mInternalPrblmMsg, Snackbar.LENGTH_LONG).show();
                    }
                } else
                    Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<MdlBranchListRes> call, Throwable t) {

                mPrgrsbrMain.setVisibility(View.GONE);
                Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    @OnClick({R.id.tv_actvty_resrvtn_date, R.id.tv_actvty_resrvtn_time, R.id.appcmptbtn_actvty_resrvtn_submit})
    public void onClickView(View view) {

        switch (view.getId()) {
            case R.id.tv_actvty_resrvtn_date:

                showDatePicker();
                break;
            case R.id.tv_actvty_resrvtn_time:

                showTimePicker();
                break;
            case R.id.appcmptbtn_actvty_resrvtn_submit:

                checkAllValidation();
                break;
        }
    }

    // Show DatePicker for select BookTableDate
    private void showDatePicker() {

        mCalendar = Calendar.getInstance();
        DatePickerDialog fromDatePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        /*monthOfYear = monthOfYear + 1;
                        String slctdPreviousDate = monthOfYear + "/" + dayOfMonth + "/" + year;*/

                        // Convert selected Date to Timestamp
                        mCalendar = new GregorianCalendar(year, monthOfYear, dayOfMonth);
                        isDateSelected = true;
                        validateDate();
                    }
                }, mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH), mCalendar.get(Calendar.DAY_OF_MONTH));

        fromDatePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000); // Disable past dates
        fromDatePickerDialog.show();
    }

    // Validate Table book Date
    private boolean validateDate() {

        if(!isDateSelected){

            if(mTvDate.getText().toString().equalsIgnoreCase(mSlctDateMsg)) {

                Snackbar.make(mCrdntrlyot, mSlctDateMsg, Snackbar.LENGTH_SHORT).show();
                isDateSelected = false;
                return false;
            }
        } else {

            mSlctdDateTimestamp = mCalendar.getTimeInMillis()/1000;
            mTvDate.setText(GeneralFunctions.getDateFromTimestamp(mSlctdDateTimestamp));
            isDateSelected = true;
            return true;
        }
        return false;
    }

    // Show TimePicker for select BookTableDate
    private void showTimePicker() {

        Calendar now = Calendar.getInstance();
        TimePickerDialog timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {

                mSlctdFromHourOfDay = hourOfDay;
                mSlctdFromMinute = minute;

                isTimeSelected = true;
                validateTime();
            }
        }, now.get(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE), false);
        timePickerDialog.show();
    }

    // Validate Table book Time
    private boolean validateTime() {

        if(!isTimeSelected){

            if(mTvTime.getText().toString().equalsIgnoreCase(mSlctTimeMsg)) {

                Snackbar.make(mCrdntrlyot, mSlctTimeMsg, Snackbar.LENGTH_SHORT).show();
                isTimeSelected = false;
                return false;
            }
        } else if(isDateSelected) {

            Calendar calndrObj = Calendar.getInstance();
            if(mSlctdDateTimestamp < calndrObj.getTimeInMillis()/1000) {

                if(mSlctdFromHourOfDay < calndrObj.get(Calendar.HOUR_OF_DAY)) {

                    Toast.makeText(ActvtyReservation.this, mInvalidTime, Toast.LENGTH_SHORT).show();
                    isTimeSelected = false;
                    mTvTime.setText(mSlctTimeMsg);
                } else if(mSlctdFromHourOfDay == calndrObj.get(Calendar.HOUR_OF_DAY)
                        && (mSlctdFromMinute < calndrObj.get(Calendar.MINUTE))) {

                    Toast.makeText(ActvtyReservation.this, mInvalidTime, Toast.LENGTH_SHORT).show();
                    isTimeSelected = false;
                    mTvTime.setText(mSlctTimeMsg);
                } else {

                    mTvTime.setText(convertTimeInFormat());
                    isTimeSelected= true;

                    return true;
                }
            } else {

                mTvTime.setText(convertTimeInFormat());
                isTimeSelected= true;

                return true;
            }
        } else {

            mTvTime.setText(convertTimeInFormat());
            isTimeSelected= true;

            return true;
        }
        return false;
    }

    // Convert Hours,Minute in proper format
    private String convertTimeInFormat() {

        hourString = mSlctdFromHourOfDay < 10 ? "0"+ mSlctdFromHourOfDay : ""+ mSlctdFromHourOfDay;
        minuteString = mSlctdFromMinute < 10 ? "0"+ mSlctdFromMinute : ""+ mSlctdFromMinute;
        return hourString.concat(":").concat(minuteString);
    }

    /*
        public String parseTime(String strTime) {

            String inputPattern = "HH:mm";
            String outputPattern = "hh:mm a";
            SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern, Locale.getDefault());
            SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern, Locale.getDefault());

            Date date;
            String str = "";

            try {
                date = inputFormat.parse(strTime);
                str = outputFormat.format(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return str;
        }
    */
    private void checkAllValidation() {

        if (!ValidationUtils.validateEmpty(this, mCrdntrlyot, mEtName, mEntrName))
            return;

        if(!validateDate())
            return;

        if(!validateTime())
            return;

        if (!ValidationUtils.validateMobile(this, mCrdntrlyot, mEtPhoneno))
            return;

        if (!ValidationUtils.validateEmpty(this, mCrdntrlyot, mEtEmail, mEntrEmail))
            return;

        if (!ValidationUtils.validateEmailFormat(this, mCrdntrlyot, mEtEmail))
            return;

        if(mSpnrLocations.getSelectedItemPosition() == 0) {
            Snackbar.make(mCrdntrlyot, mSlctLocationMsg, Snackbar.LENGTH_SHORT).show();
            return;
        }

        if (!ValidationUtils.validateEmpty(this, mCrdntrlyot, mEtGuests, mEntrNoOfPerson))
            return;

        if(mPrgrsbrMain.getVisibility() != View.VISIBLE) {
            if (GeneralFunctions.isNetConnected(this))
                callDoResrvtnAPI();
            else Snackbar.make(mCrdntrlyot, mNoInternetConnMsg, Snackbar.LENGTH_LONG).show();
        }
    }

    private void callDoResrvtnAPI() {

        mPrgrsbrMain.setVisibility(View.VISIBLE);
        UserSessionManager session = new UserSessionManager(this);

        MdlResrvtnDoReq objMdlResrvtnDoReq = new MdlResrvtnDoReq();
        objMdlResrvtnDoReq.setUserId(session.getUserId());
        objMdlResrvtnDoReq.setName(mEtName.getText().toString().trim());
        objMdlResrvtnDoReq.setDate(GeneralFunctions.getDateTimeFromTimestamp(mSlctdDateTimestamp));
        objMdlResrvtnDoReq.setFromTime(hourString.concat(":").concat(minuteString).concat(":00.000"));
        objMdlResrvtnDoReq.setToTime(hourString.concat(":").concat(minuteString).concat(":00.000"));
        objMdlResrvtnDoReq.setPhoneNumber(mEtPhoneno.getText().toString().trim());
        objMdlResrvtnDoReq.setEmailId(mEtEmail.getText().toString().trim());
        objMdlResrvtnDoReq.setRestaurantId(mRestaurantId);
        objMdlResrvtnDoReq.setBranchId(mAdptrSpnrBranch.getItem(mSpnrLocations.getSelectedItemPosition()).getBranchId());
        objMdlResrvtnDoReq.setNoOfGuest(Integer.parseInt(mEtGuests.getText().toString().trim()));
        objMdlResrvtnDoReq.setComments(TextUtils.isEmpty(mEtComments.getText().toString().trim())? "": mEtComments.getText().toString().trim());
        objMdlResrvtnDoReq.setStatus(AppConstants.STATIC_STATUS_ID_RESRVTN_NEW);

        APIReservation objApiResrvtn = APIRetroBuilder.getRetroBuilder(true).create(APIReservation.class);
        Call<MdlResrvtnDoRes> callMdlResrvynDoRes = objApiResrvtn.callDoReservation(objMdlResrvtnDoReq);
        callMdlResrvynDoRes.enqueue(new Callback<MdlResrvtnDoRes>() {
            @Override
            public void onResponse(Call<MdlResrvtnDoRes> call, Response<MdlResrvtnDoRes> response) {

                mPrgrsbrMain.setVisibility(View.GONE);
                if(response.isSuccessful()) {

                    try {
                        if(response.body().isSuccess()) {

                            Toast.makeText(ActvtyReservation.this, response.body().getData(), Toast.LENGTH_SHORT).show();
                            onBackPressed();
                        } else
                            Snackbar.make(mCrdntrlyot, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                    } catch (Exception e) {

                        e.printStackTrace();
                        LogHelper.printLog(AppConstants.LOGTYPE_ERROR, TAG, e.getMessage());
                        Snackbar.make(mCrdntrlyot, mInternalPrblmMsg, Snackbar.LENGTH_LONG).show();
                    }
                } else
                    Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<MdlResrvtnDoRes> call, Throwable t) {

                mPrgrsbrMain.setVisibility(View.GONE);
                Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
