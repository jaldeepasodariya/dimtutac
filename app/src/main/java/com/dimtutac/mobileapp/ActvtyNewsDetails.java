package com.dimtutac.mobileapp;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.dimtutac.mobileapp.common.AppConstants;
import com.squareup.picasso.Picasso;

import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Jalotsav on 12/8/2017.
 */

public class ActvtyNewsDetails extends AppCompatActivity {

    private static final String TAG = ActvtyNewsDetails.class.getSimpleName();

    @BindView(R.id.cordntrlyot_actvty_newsdtls)CoordinatorLayout mCrdntrlyot;
    @BindView(R.id.imgvw_actvty_newsdtls_image) ImageView mImgvwImage;
    @BindView(R.id.tv_actvty_newsdtls_title) TextView mTvTitle;
    @BindView(R.id.tv_actvty_newsdtls_date) TextView mTvFromDate;
    @BindView(R.id.tv_actvty_newsdtls_descrptn) TextView mTvDescrptn;

    @BindDrawable(R.mipmap.ic_logo_black) Drawable mDrwblDefault;

    String mNewsId, mTitle, mPhotoURL, mFromDate, mDescription;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lo_actvty_newsdtls);
        ButterKnife.bind(this);

        mNewsId = getIntent().getStringExtra(AppConstants.PUT_EXTRA_NEWS_ID);
        mTitle = getIntent().getStringExtra(AppConstants.PUT_EXTRA_TITLE);
        mPhotoURL = getIntent().getStringExtra(AppConstants.PUT_EXTRA_PHOTO_URL);
        mFromDate = getIntent().getStringExtra(AppConstants.PUT_EXTRA_FROM_DATE);
        mDescription = getIntent().getStringExtra(AppConstants.PUT_EXTRA_DESCRPTN);

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) { e.printStackTrace(); }

        // Set Branch Details
        if(!TextUtils.isEmpty(mPhotoURL)) {
            Picasso.with(this)
                    .load(AppConstants.ATCHMNT_ROOT_URL.concat(mPhotoURL))
                    .placeholder(mDrwblDefault)
                    .into(mImgvwImage);
        }

        mTvTitle.setText(mTitle);
        mTvFromDate.setText(mFromDate);
        mTvDescrptn.setText(mDescription);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
