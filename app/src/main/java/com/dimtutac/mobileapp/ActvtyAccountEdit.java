package com.dimtutac.mobileapp;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.dimtutac.mobileapp.common.AppConstants;
import com.dimtutac.mobileapp.common.GeneralFunctions;
import com.dimtutac.mobileapp.common.LogHelper;
import com.dimtutac.mobileapp.common.UserSessionManager;
import com.dimtutac.mobileapp.models.login.MdlLoginResData;
import com.dimtutac.mobileapp.retrofitapi.APIRetroBuilder;
import com.dimtutac.mobileapp.retrofitapi.APIUser;
import com.dimtutac.mobileapp.utils.ValidationUtils;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Jalotsav on 1/31/2018.
 */

public class ActvtyAccountEdit extends AppCompatActivity {

    private static final String TAG = ActvtyAccountEdit.class.getSimpleName();

    @BindView(R.id.cordntrlyot_actvty_edit_account) CoordinatorLayout mCrdntrlyot;
    @BindView(R.id.et_actvty_account_edit_firstname) EditText mEtFirstname;
    @BindView(R.id.et_actvty_account_edit_lastname) EditText mEtLastname;
    @BindView(R.id.et_actvty_account_edit_email) EditText mEtEmail;
    @BindView(R.id.et_actvty_account_edit_paswrd) EditText mEtPaswrd;
    @BindView(R.id.et_actvty_account_edit_phoneno) EditText mEtPhoneno;
    @BindView(R.id.et_actvty_account_edit_company) EditText mEtCompany;
    @BindView(R.id.spnr_actvty_account_edit_gender) Spinner mSpnrGender;
    @BindView(R.id.tv_actvty_account_edit_birthday) TextView mTvBirthday;
    @BindView(R.id.prgrsbr_actvty_edit_account) ProgressBar mPrgrsbrMain;

    @BindString(R.string.no_intrnt_cnctn) String mNoInternetConnMsg;
    @BindString(R.string.server_problem_sml) String mServerPrblmMsg;
    @BindString(R.string.internal_problem_sml) String mInternalPrblmMsg;
    @BindString(R.string.male_sml) String mStrMale;
    @BindString(R.string.female_sml) String mStrFemale;
    @BindString(R.string.entr_first_name_sml) String mEntrFirstname;
    @BindString(R.string.entr_last_name_sml) String mEntrLastname;
    @BindString(R.string.entr_email_sml) String mEntrEmail;
    @BindString(R.string.entr_password_sml) String mEntrPaswrd;
    @BindString(R.string.slct_gendr_sml) String mSlctGender;
    @BindString(R.string.birthday_sml) String mBirthdayStr;
    @BindString(R.string.slct_birthday_sml) String mSlctBirthday;
    @BindString(R.string.invalid_birthday_sml) String mInvalidBirthday;

    UserSessionManager session;
    Calendar mCalendar;
    long mSlctdDateTimestamp;
    int birthdayAgeCount;
    boolean isBirthDateSelected = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lo_actvty_account_edit);
        ButterKnife.bind(this);

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) { e.printStackTrace(); }

        session = new UserSessionManager(this);
        if(!session.isUserLoggedIn()) {

            startActivityForResult(new Intent(this, ActvtySignin.class), AppConstants.REQUEST_SIGNIN);
        } else {
            getUserData();
        }
    }

    private void getUserData() {

        mEtFirstname.setText(session.getFirstName());
        mEtLastname.setText(session.getLastName());
        mEtEmail.setText(session.getEmailId());
        mEtPaswrd.setText(session.getPassword());
        mEtPhoneno.setText(session.getPhoneNo());
        mEtCompany.setText(session.getCompanyName());

        if(session.getGender().equalsIgnoreCase(mStrMale))
            mSpnrGender.setSelection(1);
        else if(session.getGender().equalsIgnoreCase(mStrFemale))
            mSpnrGender.setSelection(2);
        else
            mSpnrGender.setSelection(0);

        try {
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault());
            Date objDate = formatter.parse(session.getDateOfBirth());
            mCalendar = Calendar.getInstance();
            mCalendar.setTime(objDate);
            isBirthDateSelected = true;
            calculateAge();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @OnClick({R.id.tv_actvty_account_edit_birthday, R.id.appcmptbtn_actvty_account_edit_submit})
    public void onClickView(View view) {

        switch (view.getId()) {
            case R.id.tv_actvty_account_edit_birthday:

                showDOBCalender();
                break;
            case R.id.appcmptbtn_actvty_account_edit_submit:

                checkAllValidation();
                break;
        }
    }

    private void checkAllValidation() {

        if (!ValidationUtils.validateEmpty(this, mCrdntrlyot, mEtFirstname, mEntrFirstname))
            return;

        if (!ValidationUtils.validateEmpty(this, mCrdntrlyot, mEtLastname, mEntrLastname))
            return;

        if (!ValidationUtils.validateEmpty(this, mCrdntrlyot, mEtEmail, mEntrEmail))
            return;

        if (!ValidationUtils.validateEmailFormat(this, mCrdntrlyot, mEtEmail))
            return;

        if (!ValidationUtils.validateEmpty(this, mCrdntrlyot, mEtPaswrd, mEntrPaswrd))
            return;

        if (!ValidationUtils.validateMobile(this, mCrdntrlyot, mEtPhoneno))
            return;

        if(mSpnrGender.getSelectedItemPosition() == 0) {
            Snackbar.make(mCrdntrlyot, getString(R.string.slct_gendr_sml), Snackbar.LENGTH_SHORT).show();
            return;
        }

        if(!validateBirthday())
            return;

        if(GeneralFunctions.isNetConnected(this))
            callUpdateUserAPI();
        else Snackbar.make(mCrdntrlyot, mNoInternetConnMsg, Snackbar.LENGTH_LONG).show();
    }

    // Show DatePicker for select BirthDate
    private void showDOBCalender() {

        mCalendar = Calendar.getInstance();
        DatePickerDialog fromDatePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        /*monthOfYear = monthOfYear + 1;
                        String slctdPreviousDate = monthOfYear + "/" + dayOfMonth + "/" + year;*/

                        // Convert selected Date to Timestamp
                        mCalendar = new GregorianCalendar(year, monthOfYear, dayOfMonth);
                        isBirthDateSelected = true;
                        calculateAge();
                    }
                }, mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH), mCalendar.get(Calendar.DAY_OF_MONTH));

        fromDatePickerDialog.show();
    }

    // Calculate Age from selected Date
    private void calculateAge() {

        Date dateOfBirth = new Date(mCalendar.getTimeInMillis());
        Calendar dob = Calendar.getInstance();
        dob.setTime(dateOfBirth);
        Calendar today = Calendar.getInstance();
        birthdayAgeCount = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
        if (today.get(Calendar.MONTH) < dob.get(Calendar.MONTH))
            birthdayAgeCount--;
        else if (today.get(Calendar.MONTH) == dob.get(Calendar.MONTH)
                && today.get(Calendar.DAY_OF_MONTH) < dob.get(Calendar.DAY_OF_MONTH))
            birthdayAgeCount--;

        validateBirthday();
    }

    // Validation for BirthDay
    private boolean validateBirthday() {

        if(!isBirthDateSelected){

            if(mTvBirthday.getText().toString().equalsIgnoreCase(mBirthdayStr)) {

//                mTvBirthday.setTextColor(ContextCompat.getColor(this, R.color.colorPrimaryOrange));
                Snackbar.make(mCrdntrlyot, mSlctBirthday, Snackbar.LENGTH_SHORT).show();
                isBirthDateSelected = false;

                return false;
            }
        } else if (birthdayAgeCount < 18) {

            mTvBirthday.setText(mBirthdayStr);
//            mTvBirthday.setTextColor(ContextCompat.getColor(this, R.color.grayA8));
            Snackbar.make(mCrdntrlyot, mInvalidBirthday, Snackbar.LENGTH_SHORT).show();
            isBirthDateSelected = false;

            return false;
        } else {

            mSlctdDateTimestamp = mCalendar.getTimeInMillis()/1000;
            mTvBirthday.setText(GeneralFunctions.getDateFromTimestamp(mSlctdDateTimestamp));
//            mTvBirthday.setTextColor(Color.BLACK);
            isBirthDateSelected = true;

            return true;
        }
        return false;
    }

    private void callUpdateUserAPI() {

        mPrgrsbrMain.setVisibility(View.VISIBLE);

        MdlLoginResData objMdlUserUpdateReq = new MdlLoginResData();
        objMdlUserUpdateReq.setPkId(session.getUserId());
        objMdlUserUpdateReq.setUserName(mEtEmail.getText().toString().trim());
        objMdlUserUpdateReq.setFirstName(mEtFirstname.getText().toString().trim());
        objMdlUserUpdateReq.setLastName(mEtLastname.getText().toString().trim());
        objMdlUserUpdateReq.setMiddleName(session.getMiddleName());
        objMdlUserUpdateReq.setUserTypeId(session.getUserTypeId());
        objMdlUserUpdateReq.setPassword(mEtPaswrd.getText().toString().trim());
        objMdlUserUpdateReq.setCompanyName(mEtCompany.getText().toString().trim());
        objMdlUserUpdateReq.setGender(mSpnrGender.getSelectedItem().toString());
        objMdlUserUpdateReq.setDateOfBirth(GeneralFunctions.getDateTimeFromTimestamp(mSlctdDateTimestamp));
        objMdlUserUpdateReq.setDefaultLanguageId(session.getDefaultLanguageId());
        objMdlUserUpdateReq.setRestaurantId(session.getRestaurantId());
        objMdlUserUpdateReq.setMainBranchId(session.getMainBranchId());
        objMdlUserUpdateReq.setAgreeTC(session.isAgreeTC());
        objMdlUserUpdateReq.setPromotionSend(session.isPromotionSend());
        objMdlUserUpdateReq.setFacebookLogin(session.isFacebookLogin());
        objMdlUserUpdateReq.setFacebookId(session.getFacebookId());
        objMdlUserUpdateReq.setAccountBarcode(session.getAccountBarcode());
        objMdlUserUpdateReq.setEmailId(mEtEmail.getText().toString().trim());
        objMdlUserUpdateReq.setPhoneNo(mEtPhoneno.getText().toString().trim());
        objMdlUserUpdateReq.setMobileNo(mEtPhoneno.getText().toString().trim());
        objMdlUserUpdateReq.setPhotoURL(session.getPhotoURL());
        objMdlUserUpdateReq.setAddressMasterId(session.getAddressMasterId());
        objMdlUserUpdateReq.setCustomerTypeId(AppConstants.STATIC_CUSTOMER_TYPE_ID);
        objMdlUserUpdateReq.setActive(session.isActive());

        APIUser objApiUser = APIRetroBuilder.getRetroBuilder(true).create(APIUser.class);
        Call<ResponseBody> callMdlUserUpdateRes = objApiUser.callUserUpdate(session.getUserId(), objMdlUserUpdateReq);
        callMdlUserUpdateRes.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                mPrgrsbrMain.setVisibility(View.GONE);
                if(response.isSuccessful()) {

                    try {

                        JSONObject jsnObjResponse = new JSONObject(response.body().string());
                        boolean isSuccess = jsnObjResponse.getBoolean(AppConstants.KEY_SUCCESS_SML);
                        String message = jsnObjResponse.getString(AppConstants.KEY_MESSAGE_SML);

                        if(isSuccess) {

                            JSONObject data = jsnObjResponse.getJSONObject(AppConstants.KEY_DATA_SML);
                            Gson objGson = new Gson();
                            MdlLoginResData objMdlLoginResData = objGson.fromJson(data.toString(), MdlLoginResData.class);
                            if(objMdlLoginResData != null) {
                                UserSessionManager session = new UserSessionManager(ActvtyAccountEdit.this);
                                session.setUserId(objMdlLoginResData.getPkId());
                                session.setUserName(objMdlLoginResData.getEmailId());
                                session.setFirstName(objMdlLoginResData.getFirstName());
                                session.setLastName(objMdlLoginResData.getLastName());
                                session.setMiddleName(objMdlLoginResData.getMiddleName());
                                session.setUserTypeId(objMdlLoginResData.getUserTypeId());
                                session.setPassword(objMdlLoginResData.getPassword());
                                session.setCompanyName(objMdlLoginResData.getCompanyName());
                                session.setGender(objMdlLoginResData.getGender());
                                session.setDateOfBirth(objMdlLoginResData.getDateOfBirth());
                                session.setDefaultLanguageId(objMdlLoginResData.getDefaultLanguageId());
                                session.setRestaurantId(objMdlLoginResData.getRestaurantId());
                                session.setMainBranchId(objMdlLoginResData.getMainBranchId());
                                session.setAgreeTC(objMdlLoginResData.isAgreeTC());
                                session.setPromotionSend(objMdlLoginResData.isPromotionSend());
                                session.setFacebookLogin(objMdlLoginResData.isFacebookLogin());
                                session.setFacebookId(objMdlLoginResData.getFacebookId());
                                session.setAccountBarcode(objMdlLoginResData.getAccountBarcode());
                                session.setEmailId(objMdlLoginResData.getEmailId());
                                session.setMobileNo(objMdlLoginResData.getMobileNo());
                                session.setPhoneNo(objMdlLoginResData.getPhoneNo());
                                session.setPhotoURL(objMdlLoginResData.getPhotoURL());
                                session.setActive(objMdlLoginResData.isActive());
                                session.setAddressMasterId(objMdlLoginResData.getAddressMasterId());
                                session.setAddressLine1(objMdlLoginResData.getAddressLine1());
                                session.setAddressLine2(objMdlLoginResData.getAddressLine2());
                                session.setCity(objMdlLoginResData.getCity());
                                session.setState(objMdlLoginResData.getState());
                                session.setCountry(objMdlLoginResData.getCountry());
                                session.setPincode(objMdlLoginResData.getPincode());
                                session.setCustomerType(objMdlLoginResData.getCustomerType());
                                session.setPasswordResetId(objMdlLoginResData.getPasswordResetId());
                                session.setDeviceId(objMdlLoginResData.getDeviceId());

                                Toast.makeText(ActvtyAccountEdit.this, message, Toast.LENGTH_SHORT).show();
                                onBackPressed();
                            }
                        } else
                            Snackbar.make(mCrdntrlyot, message, Snackbar.LENGTH_LONG).show();
                    } catch (Exception e) {

                        e.printStackTrace();
                        LogHelper.printLog(AppConstants.LOGTYPE_ERROR, TAG, e.getMessage());
                        Snackbar.make(mCrdntrlyot, mInternalPrblmMsg, Snackbar.LENGTH_LONG).show();
                    }
                } else
                    Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                mPrgrsbrMain.setVisibility(View.GONE);
                Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == Activity.RESULT_OK) {
            if(requestCode == AppConstants.REQUEST_SIGNIN) {

                getUserData();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
