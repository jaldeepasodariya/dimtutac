package com.dimtutac.mobileapp;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.dimtutac.mobileapp.adapter.SpnrArryAdptrBranch;
import com.dimtutac.mobileapp.common.AppConstants;
import com.dimtutac.mobileapp.common.GeneralFunctions;
import com.dimtutac.mobileapp.common.LogHelper;
import com.dimtutac.mobileapp.models.branch.MdlBranchListRes;
import com.dimtutac.mobileapp.models.branch.MdlBranchListResData;
import com.dimtutac.mobileapp.models.reservation.MdlResrvtnListResData;
import com.dimtutac.mobileapp.models.reservation.MdlResrvtnUpdateRes;
import com.dimtutac.mobileapp.retrofitapi.APIBranch;
import com.dimtutac.mobileapp.retrofitapi.APIReservation;
import com.dimtutac.mobileapp.retrofitapi.APIRetroBuilder;
import com.dimtutac.mobileapp.utils.ValidationUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Jalotsav on 10/3/2017.
 */

public class ActvtyReservationEdit extends AppCompatActivity {

    private static final String TAG = ActvtyReservationEdit.class.getSimpleName();

    @BindView(R.id.cordntrlyot_actvty_resrvtn) CoordinatorLayout mCrdntrlyot;
    @BindView(R.id.et_actvty_resrvtn_name) EditText mEtName;
    @BindView(R.id.et_actvty_resrvtn_phoneno) EditText mEtPhoneno;
    @BindView(R.id.et_actvty_resrvtn_email) EditText mEtEmail;
    @BindView(R.id.et_actvty_resrvtn_guests) EditText mEtGuests;
    @BindView(R.id.et_actvty_resrvtn_comments) EditText mEtComments;
    @BindView(R.id.tv_actvty_resrvtn_date) TextView mTvDate;
    @BindView(R.id.tv_actvty_resrvtn_time) TextView mTvTime;
    @BindView(R.id.prgrsbr_actvty_resrvtn) ProgressBar mPrgrsbrMain;
    @BindView(R.id.spnr_actvty_resrvtn_locations) Spinner mSpnrLocations;
    @BindView(R.id.lnrlyot_actvty_resrvtn_submitcancel) LinearLayout mLnrlyotSubmitCancel;
    @BindView(R.id.appcmptbtn_actvty_resrvtn_submit) AppCompatButton mAppcpmtbtnSubmit;

    @BindString(R.string.no_intrnt_cnctn) String mNoInternetConnMsg;
    @BindString(R.string.server_problem_sml) String mServerPrblmMsg;
    @BindString(R.string.internal_problem_sml) String mInternalPrblmMsg;
    @BindString(R.string.entr_your_name_sml) String mEntrName;
    @BindString(R.string.entr_your_email_sml) String mEntrEmail;
    @BindString(R.string.entr_numberof_person_sml) String mEntrNoOfPerson;
    @BindString(R.string.entr_date_wanto_book_table_sml) String mSlctDateMsg;
    @BindString(R.string.select_time_from_avlb_slots_sml) String mSlctTimeMsg;
    @BindString(R.string.invalid_time_sml) String mInvalidTime;
    @BindString(R.string.entr_branch_wantto_book_table_sml) String mSlctLocationMsg;

    String mRestaurantId;
    boolean mIsAllowUpdate, isDateSelected = false, isTimeSelected = false;
    ArrayList<MdlBranchListResData> mArrylstBranchData;
    MdlResrvtnListResData mMdlResrvtnListResData;
    SpnrArryAdptrBranch mAdptrSpnrBranch;
    Calendar mCalendar;
    long mSlctdDateTimestamp;
    int mSlctdFromHourOfDay, mSlctdFromMinute;
    String hourString, minuteString;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lo_actvty_reservation);
        ButterKnife.bind(this);

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) { e.printStackTrace(); }

        mIsAllowUpdate = getIntent().getBooleanExtra(AppConstants.PUT_EXTRA_IS_ALLOW_UPDATE, false);

        mArrylstBranchData = new ArrayList<>();
        mArrylstBranchData.add(new MdlBranchListResData("0", mSlctLocationMsg));
        mAdptrSpnrBranch = new SpnrArryAdptrBranch(this, android.R.layout.simple_spinner_dropdown_item, mArrylstBranchData);
        mSpnrLocations.setAdapter(mAdptrSpnrBranch);

        mAppcpmtbtnSubmit.setText(getString(R.string.update_resrvtn_sml));

        if(!mIsAllowUpdate) {
            mEtName.setEnabled(false);
            mTvDate.setEnabled(false);
            mTvTime.setEnabled(false);
            mEtPhoneno.setEnabled(false);
            mEtEmail.setEnabled(false);
            mSpnrLocations.setEnabled(false);
            mEtGuests.setEnabled(false);
            mEtComments.setEnabled(false);
            mLnrlyotSubmitCancel.setVisibility(View.GONE);
        }

        if (GeneralFunctions.isNetConnected(this))
            getBranches();
        else Snackbar.make(mCrdntrlyot, mNoInternetConnMsg, Snackbar.LENGTH_LONG).show();
    }

    // Call Retrofit API
    private void getBranches() {

        mPrgrsbrMain.setVisibility(View.VISIBLE);

        APIBranch objApiBranch = APIRetroBuilder.getRetroBuilder(true).create(APIBranch.class);
        Call<MdlBranchListRes> callMdlBranchRes = objApiBranch.callGetBranchList();
        callMdlBranchRes.enqueue(new Callback<MdlBranchListRes>() {
            @Override
            public void onResponse(Call<MdlBranchListRes> call, Response<MdlBranchListRes> response) {

                mPrgrsbrMain.setVisibility(View.GONE);
                if(response.isSuccessful()) {

                    try {
                        if(response.body().isSuccess()) {

                            ArrayList<MdlBranchListResData> arrylstMdlBranchListData = response.body().getArrylstMdlBranchListData();

                            mArrylstBranchData = new ArrayList<>();
                            mArrylstBranchData.add(new MdlBranchListResData("0", mSlctLocationMsg));
                            mArrylstBranchData.addAll(arrylstMdlBranchListData);
                            mAdptrSpnrBranch = new SpnrArryAdptrBranch(ActvtyReservationEdit.this, android.R.layout.simple_spinner_dropdown_item, mArrylstBranchData);
                            mSpnrLocations.setAdapter(mAdptrSpnrBranch);

                            setResrvtnDataUI();
                        } else
                            Snackbar.make(mCrdntrlyot, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                    } catch (Exception e) {

                        e.printStackTrace();
                        LogHelper.printLog(AppConstants.LOGTYPE_ERROR, TAG, e.getMessage());
                        Snackbar.make(mCrdntrlyot, mInternalPrblmMsg, Snackbar.LENGTH_LONG).show();
                    }
                } else
                    Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<MdlBranchListRes> call, Throwable t) {

                mPrgrsbrMain.setVisibility(View.GONE);
                Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    // Set Reservation data on UI, set from previous Reservation list screen
    private void setResrvtnDataUI() {

        if(AppConstants.ARRYLST_MDL_RESRVTNLIST_RESDATA.size() > 0) {
            mMdlResrvtnListResData = AppConstants.ARRYLST_MDL_RESRVTNLIST_RESDATA.get(0);
            mRestaurantId = mMdlResrvtnListResData.getRestaurantId();
            mEtName.setText(mMdlResrvtnListResData.getName());
            mEtPhoneno.setText(mMdlResrvtnListResData.getPhoneNumber());
            mEtEmail.setText(mMdlResrvtnListResData.getEmailId());
            mEtGuests.setText(String.valueOf(mMdlResrvtnListResData.getNoOfGuest()));
            mEtComments.setText(mMdlResrvtnListResData.getComments());

            // DATE
            try {
                DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault());
                Date objDate = formatter.parse(mMdlResrvtnListResData.getDate());
                mCalendar = Calendar.getInstance();
                mCalendar.setTime(objDate);
                isDateSelected = true;
                validateDate();
            } catch (ParseException e) {
                e.printStackTrace();
            }

            // TIME
            try {
                String[] strArryFromTime = mMdlResrvtnListResData.getFromTime().split(":");
                mSlctdFromHourOfDay = strArryFromTime[0].substring(0,1).equals("0")?
                        Integer.parseInt(strArryFromTime[0].substring(1)): Integer.parseInt(strArryFromTime[0]);
                mSlctdFromMinute = strArryFromTime[1].substring(0,1).equals("0")?
                        Integer.parseInt(strArryFromTime[1].substring(1)): Integer.parseInt(strArryFromTime[1]);
                isTimeSelected= true;
                validateTime();
            } catch (Exception e) {
                e.printStackTrace();
            }

            // LOCATION Spinner
            if(mArrylstBranchData.size() > 1) {
                for (int i=0; i<mArrylstBranchData.size(); i++) {
                    if(mArrylstBranchData.get(i).getBranchId().equals(mMdlResrvtnListResData.getBranchId())) {
                        mSpnrLocations.setSelection(i);
                    }
                }
            }
        } else {

            Toast.makeText(this, mServerPrblmMsg, Toast.LENGTH_SHORT).show();
            onBackPressed();
        }
    }

    @OnClick({R.id.tv_actvty_resrvtn_date, R.id.tv_actvty_resrvtn_time, R.id.appcmptbtn_actvty_resrvtn_submit, R.id.appcmptbtn_actvty_resrvtn_cancelresrvtrn})
    public void onClickView(View view) {

        switch (view.getId()) {
            case R.id.tv_actvty_resrvtn_date:

                showDatePicker();
                break;
            case R.id.tv_actvty_resrvtn_time:

                showTimePicker();
                break;
            case R.id.appcmptbtn_actvty_resrvtn_submit:

                checkAllValidation();
                break;
            case R.id.appcmptbtn_actvty_resrvtn_cancelresrvtrn:

                confirmCancelReservationAlertDialog();
                break;
        }
    }

    // Show DatePicker for select BookTableDate
    private void showDatePicker() {

        mCalendar = Calendar.getInstance();
        DatePickerDialog fromDatePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        /*monthOfYear = monthOfYear + 1;
                        String slctdPreviousDate = monthOfYear + "/" + dayOfMonth + "/" + year;*/

                        // Convert selected Date to Timestamp
                        mCalendar = new GregorianCalendar(year, monthOfYear, dayOfMonth);
                        isDateSelected = true;
                        validateDate();
                    }
                }, mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH), mCalendar.get(Calendar.DAY_OF_MONTH));

        fromDatePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000); // Disable past dates
        fromDatePickerDialog.show();
    }

    // Validate Table book Date
    private boolean validateDate() {

        if(!isDateSelected){

            if(mTvDate.getText().toString().equalsIgnoreCase(mSlctDateMsg)) {

                Snackbar.make(mCrdntrlyot, mSlctDateMsg, Snackbar.LENGTH_SHORT).show();
                isDateSelected = false;
                return false;
            }
        } else {

            mSlctdDateTimestamp = mCalendar.getTimeInMillis()/1000;
            mTvDate.setText(GeneralFunctions.getDateFromTimestamp(mSlctdDateTimestamp));
            isDateSelected = true;
            return true;
        }
        return false;
    }

    // Show TimePicker for select BookTableDate
    private void showTimePicker() {

        Calendar now = Calendar.getInstance();
        TimePickerDialog timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {

                mSlctdFromHourOfDay = hourOfDay;
                mSlctdFromMinute = minute;

                isTimeSelected = true;
                validateTime();
            }
        }, now.get(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE), false);
        timePickerDialog.show();
    }

    // Validate Table book Time
    private boolean validateTime() {

        if(!isTimeSelected){

            if(mTvTime.getText().toString().equalsIgnoreCase(mSlctTimeMsg)) {

                Snackbar.make(mCrdntrlyot, mSlctTimeMsg, Snackbar.LENGTH_SHORT).show();
                isTimeSelected = false;
                return false;
            }
        } else if(isDateSelected) {

            Calendar calndrObj = Calendar.getInstance();
            if(mSlctdDateTimestamp < calndrObj.getTimeInMillis()/1000) {

                if(mSlctdFromHourOfDay < calndrObj.get(Calendar.HOUR_OF_DAY)) {

                    Toast.makeText(ActvtyReservationEdit.this, mInvalidTime, Toast.LENGTH_SHORT).show();
                    isTimeSelected = false;
                    mTvTime.setText(mSlctTimeMsg);
                } else if(mSlctdFromHourOfDay == calndrObj.get(Calendar.HOUR_OF_DAY)
                        && (mSlctdFromMinute < calndrObj.get(Calendar.MINUTE))) {

                    Toast.makeText(ActvtyReservationEdit.this, mInvalidTime, Toast.LENGTH_SHORT).show();
                    isTimeSelected = false;
                    mTvTime.setText(mSlctTimeMsg);
                } else {

                    mTvTime.setText(convertTimeInFormat());
                    isTimeSelected= true;

                    return true;
                }
            } else {

                mTvTime.setText(convertTimeInFormat());
                isTimeSelected= true;

                return true;
            }
        } else {

            mTvTime.setText(convertTimeInFormat());
            isTimeSelected= true;

            return true;
        }
        return false;
    }

    // Convert Hours,Minute in proper format
    private String convertTimeInFormat() {

        hourString = mSlctdFromHourOfDay < 10 ? "0"+ mSlctdFromHourOfDay : ""+ mSlctdFromHourOfDay;
        minuteString = mSlctdFromMinute < 10 ? "0"+ mSlctdFromMinute : ""+ mSlctdFromMinute;
        return hourString.concat(":").concat(minuteString);
    }

    private void checkAllValidation() {

        if (!ValidationUtils.validateEmpty(this, mCrdntrlyot, mEtName, mEntrName))
            return;

        if(!validateDate())
            return;

        if(!validateTime())
            return;

        if (!ValidationUtils.validateMobile(this, mCrdntrlyot, mEtPhoneno))
            return;

        if (!ValidationUtils.validateEmpty(this, mCrdntrlyot, mEtEmail, mEntrEmail))
            return;

        if (!ValidationUtils.validateEmailFormat(this, mCrdntrlyot, mEtEmail))
            return;

        if(mSpnrLocations.getSelectedItemPosition() == 0) {
            Snackbar.make(mCrdntrlyot, mSlctLocationMsg, Snackbar.LENGTH_SHORT).show();
            return;
        }

        if (!ValidationUtils.validateEmpty(this, mCrdntrlyot, mEtGuests, mEntrNoOfPerson))
            return;

        if(mPrgrsbrMain.getVisibility() != View.VISIBLE) {
            if (GeneralFunctions.isNetConnected(this))
                callUpdateResrvtnAPI();
            else Snackbar.make(mCrdntrlyot, mNoInternetConnMsg, Snackbar.LENGTH_LONG).show();
        }
    }

    private void callUpdateResrvtnAPI() {

        mPrgrsbrMain.setVisibility(View.VISIBLE);

        mMdlResrvtnListResData.setName(mEtName.getText().toString().trim());
        mMdlResrvtnListResData.setDate(GeneralFunctions.getDateTimeFromTimestamp(mSlctdDateTimestamp));
        mMdlResrvtnListResData.setFromTime(hourString.concat(":").concat(minuteString).concat(":00.000"));
        mMdlResrvtnListResData.setToTime(hourString.concat(":").concat(minuteString).concat(":00.000"));
        mMdlResrvtnListResData.setPhoneNumber(mEtPhoneno.getText().toString().trim());
        mMdlResrvtnListResData.setEmailId(mEtEmail.getText().toString().trim());
        mMdlResrvtnListResData.setRestaurantId(mRestaurantId);
        mMdlResrvtnListResData.setBranchId(mAdptrSpnrBranch.getItem(mSpnrLocations.getSelectedItemPosition()).getBranchId());
        mMdlResrvtnListResData.setNoOfGuest(Integer.parseInt(mEtGuests.getText().toString().trim()));
        mMdlResrvtnListResData.setComments(TextUtils.isEmpty(mEtComments.getText().toString().trim())? "": mEtComments.getText().toString().trim());

        APIReservation objApiResrvtn = APIRetroBuilder.getRetroBuilder(true).create(APIReservation.class);
        Call<MdlResrvtnUpdateRes> callMdlResrvynUpdateRes = objApiResrvtn.callUpdateReservation(mMdlResrvtnListResData.getPkId(), mMdlResrvtnListResData);
        callMdlResrvynUpdateRes.enqueue(new Callback<MdlResrvtnUpdateRes>() {
            @Override
            public void onResponse(Call<MdlResrvtnUpdateRes> call, Response<MdlResrvtnUpdateRes> response) {

                mPrgrsbrMain.setVisibility(View.GONE);
                if(response.isSuccessful()) {

                    try {
                        if(response.body().isSuccess()) {

                            Toast.makeText(ActvtyReservationEdit.this, response.body().getData(), Toast.LENGTH_SHORT).show();
                            AppConstants.ARRYLST_MDL_RESRVTNLIST_RESDATA.clear();
                            onBackPressed();
                        } else
                            Snackbar.make(mCrdntrlyot, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                    } catch (Exception e) {

                        e.printStackTrace();
                        LogHelper.printLog(AppConstants.LOGTYPE_ERROR, TAG, e.getMessage());
                        Snackbar.make(mCrdntrlyot, mInternalPrblmMsg, Snackbar.LENGTH_LONG).show();
                    }
                } else
                    Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<MdlResrvtnUpdateRes> call, Throwable t) {

                mPrgrsbrMain.setVisibility(View.GONE);
                Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    // Show AlertDialog for confirm to Logout
    private void confirmCancelReservationAlertDialog() {

        AlertDialog.Builder alrtDlg = new AlertDialog.Builder(this);
        alrtDlg.setTitle(getString(R.string.cancel_sml));
        alrtDlg.setMessage(getString(R.string.resrvtn_cancel_alrtdlg_msg));
        alrtDlg.setNegativeButton(getString(R.string.no_sml).toUpperCase(), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        alrtDlg.setPositiveButton(getString(R.string.yes_sml).toUpperCase(), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                if(AppConstants.ARRYLST_MDL_RESRVTNLIST_RESDATA.size() > 0 && mMdlResrvtnListResData != null)
                    cancelReservation();
            }
        });

        alrtDlg.show();
    }

    public void cancelReservation() {

        if (mPrgrsbrMain.getVisibility() != View.VISIBLE) {
            if (GeneralFunctions.isNetConnected(this))
                callResrvtnCancelAPI();
            else Snackbar.make(mCrdntrlyot, mNoInternetConnMsg, Snackbar.LENGTH_LONG).show();
        }
    }

    // Call Reservation Cancel API
    private void callResrvtnCancelAPI() {

        mPrgrsbrMain.setVisibility(View.VISIBLE);

        mMdlResrvtnListResData.setStatus(AppConstants.STATIC_STATUS_ID_RESRVTN_CANCEL);

        APIReservation objApiResrvtn = APIRetroBuilder.getRetroBuilder(true).create(APIReservation.class);
        Call<MdlResrvtnUpdateRes> callMdlResrvynUpdateRes = objApiResrvtn.callUpdateReservation(mMdlResrvtnListResData.getPkId(), mMdlResrvtnListResData);
        callMdlResrvynUpdateRes.enqueue(new Callback<MdlResrvtnUpdateRes>() {
            @Override
            public void onResponse(Call<MdlResrvtnUpdateRes> call, Response<MdlResrvtnUpdateRes> response) {

                mPrgrsbrMain.setVisibility(View.GONE);
                if (response.isSuccessful()) {

                    try {
                        if (response.body().isSuccess()) {

                            Toast.makeText(ActvtyReservationEdit.this, response.body().getData(), Toast.LENGTH_SHORT).show();
                            onBackPressed();
                        } else
                            Snackbar.make(mCrdntrlyot, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                    } catch (Exception e) {

                        e.printStackTrace();
                        LogHelper.printLog(AppConstants.LOGTYPE_ERROR, TAG, e.getMessage());
                        Snackbar.make(mCrdntrlyot, mInternalPrblmMsg, Snackbar.LENGTH_LONG).show();
                    }
                } else
                    Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<MdlResrvtnUpdateRes> call, Throwable t) {

                mPrgrsbrMain.setVisibility(View.GONE);
                Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
