package com.dimtutac.mobileapp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.dimtutac.mobileapp.common.AppConstants;
import com.dimtutac.mobileapp.common.GeneralFunctions;
import com.dimtutac.mobileapp.common.LogHelper;
import com.dimtutac.mobileapp.common.UserSessionManager;
import com.dimtutac.mobileapp.models.branch.MdlBranchListRes;
import com.dimtutac.mobileapp.models.branch.MdlBranchListResData;
import com.dimtutac.mobileapp.models.career.MdlCareerPostReq;
import com.dimtutac.mobileapp.models.career.MdlCareerPostRes;
import com.dimtutac.mobileapp.retrofitapi.APIBranch;
import com.dimtutac.mobileapp.retrofitapi.APICareer;
import com.dimtutac.mobileapp.retrofitapi.APIRetroBuilder;
import com.dimtutac.mobileapp.utils.ValidationUtils;

import java.util.ArrayList;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dimtutac.mobileapp.common.AppConstants.PUT_EXTRA_BRANCH_ID;
import static com.dimtutac.mobileapp.common.AppConstants.PUT_EXTRA_BRANCH_NAME;
import static com.dimtutac.mobileapp.common.AppConstants.PUT_EXTRA_DESCRPTN_LIST_STR;
import static com.dimtutac.mobileapp.common.AppConstants.PUT_EXTRA_POSITION_ID;
import static com.dimtutac.mobileapp.common.AppConstants.PUT_EXTRA_POSITION_NAME;

/**
 * Created by Jalotsav on 10/26/2017.
 */

public class ActvtyCareerApply extends AppCompatActivity {

    private static final String TAG = ActvtyCareerApply.class.getSimpleName();

    @BindView(R.id.cordntrlyot_actvty_career_apply) CoordinatorLayout mCrdntrlyot;
    @BindView(R.id.tv_actvty_career_apply_branchname) TextView mTvBranchName;
    @BindView(R.id.tv_actvty_career_apply_postnname) TextView mTvPostnName;
    @BindView(R.id.tv_actvty_career_apply_postndescrptns) TextView mTvPostnDescrptns;
    @BindView(R.id.et_actvty_career_apply_fullname) EditText mEtFullName;
    @BindView(R.id.et_actvty_career_apply_email) EditText mEtEmail;
    @BindView(R.id.et_actvty_career_apply_phoneno) EditText mEtPhoneno;
    @BindView(R.id.et_actvty_career_apply_position) EditText mEtPostonApldFor;
    @BindView(R.id.et_actvty_career_apply_experience) EditText mEtExprnce;
    @BindView(R.id.prgrsbr_actvty_career_apply) ProgressBar mPrgrsbrMain;

    @BindString(R.string.no_intrnt_cnctn) String mNoInternetConnMsg;
    @BindString(R.string.server_problem_sml) String mServerPrblmMsg;
    @BindString(R.string.internal_problem_sml) String mInternalPrblmMsg;
    @BindString(R.string.entr_your_full_name_sml) String mEntrFullName;
    @BindString(R.string.entr_your_email_sml) String mEntrEmail;
    @BindString(R.string.entr_position_sml) String mEntrPosition;
    @BindString(R.string.entr_experience_sml) String mEntrExperience;

    String mSlctdPostnId, mslctdBranchId, mSlctdBranchName, mSlctdPostnName, mstrDescptnLst;
    ArrayList<MdlBranchListResData> mArrylstBranchData;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lo_actvty_career_apply);
        ButterKnife.bind(this);

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) { e.printStackTrace(); }

        mSlctdPostnId = getIntent().getStringExtra(PUT_EXTRA_POSITION_ID);
        mslctdBranchId = getIntent().getStringExtra(PUT_EXTRA_BRANCH_ID);
        mSlctdBranchName = getIntent().getStringExtra(PUT_EXTRA_BRANCH_NAME);
        mSlctdPostnName = getIntent().getStringExtra(PUT_EXTRA_POSITION_NAME);
        mstrDescptnLst = getIntent().getStringExtra(PUT_EXTRA_DESCRPTN_LIST_STR);

        mTvBranchName.setText(mSlctdBranchName);
        mTvPostnName.setText(mSlctdPostnName);
        mTvPostnDescrptns.setText(mstrDescptnLst);

        mArrylstBranchData = new ArrayList<>();
    }

    @OnClick({R.id.appcmptbtn_actvty_career_apply_send})
    public void onClickView(View view) {

        switch (view.getId()) {
            case R.id.appcmptbtn_actvty_career_apply_send:

                checkAllValidation();
                break;
        }
    }

    private void checkAllValidation() {

        if (!ValidationUtils.validateEmpty(this, mCrdntrlyot, mEtFullName, mEntrFullName))
            return;

        if (!ValidationUtils.validateEmpty(this, mCrdntrlyot, mEtEmail, mEntrEmail))
            return;

        if (!ValidationUtils.validateEmailFormat(this, mCrdntrlyot, mEtEmail))
            return;

        if (!ValidationUtils.validateMobile(this, mCrdntrlyot, mEtPhoneno))
            return;

        if (!ValidationUtils.validateEmpty(this, mCrdntrlyot, mEtPostonApldFor, mEntrPosition))
            return;

        if (!ValidationUtils.validateEmpty(this, mCrdntrlyot, mEtExprnce, mEntrExperience))
            return;

        if(mPrgrsbrMain.getVisibility() != View.VISIBLE) {
            if (GeneralFunctions.isNetConnected(this))
                callCareerPostAPI();
            else Snackbar.make(mCrdntrlyot, mNoInternetConnMsg, Snackbar.LENGTH_LONG).show();
        }
    }

    private void callCareerPostAPI() {

        mPrgrsbrMain.setVisibility(View.VISIBLE);
        UserSessionManager session = new UserSessionManager(this);

        MdlCareerPostReq objMdlCareerPostReq = new MdlCareerPostReq();
        objMdlCareerPostReq.setFullName(mEtFullName.getText().toString().trim());
        objMdlCareerPostReq.setGender(session.getGender());
        objMdlCareerPostReq.setDateOfBirth(session.getDateOfBirth());
        objMdlCareerPostReq.setAddress(session.getAddressLine1());
        objMdlCareerPostReq.setContactNo(mEtPhoneno.getText().toString().trim());
        objMdlCareerPostReq.setEmail(mEtEmail.getText().toString().trim());
        objMdlCareerPostReq.setEducation("");
        objMdlCareerPostReq.setPositionAppliedFor(mEtPostonApldFor.getText().toString().trim());
        objMdlCareerPostReq.setWorkExperience(mEtExprnce.getText().toString().trim());
        objMdlCareerPostReq.setPositionId(mSlctdPostnId);
        objMdlCareerPostReq.setBranchId(mslctdBranchId);

        APICareer objApiCareer = APIRetroBuilder.getRetroBuilder(true).create(APICareer.class);
        Call<MdlCareerPostRes> callMdlCareerPostRes = objApiCareer.callCareerPost(objMdlCareerPostReq);
        callMdlCareerPostRes.enqueue(new Callback<MdlCareerPostRes>() {
            @Override
            public void onResponse(Call<MdlCareerPostRes> call, Response<MdlCareerPostRes> response) {

                mPrgrsbrMain.setVisibility(View.GONE);
                if(response.isSuccessful()) {

                    try {
                        if(response.body().isSuccess()) {

                            Toast.makeText(ActvtyCareerApply.this, response.body().getData(), Toast.LENGTH_SHORT).show();
                            onBackPressed();
                        } else
                            Snackbar.make(mCrdntrlyot, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                    } catch (Exception e) {

                        e.printStackTrace();
                        LogHelper.printLog(AppConstants.LOGTYPE_ERROR, TAG, e.getMessage());
                        Snackbar.make(mCrdntrlyot, mInternalPrblmMsg, Snackbar.LENGTH_LONG).show();
                    }
                } else
                    Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<MdlCareerPostRes> call, Throwable t) {

                mPrgrsbrMain.setVisibility(View.GONE);
                Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
