package com.dimtutac.mobileapp.common;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.dimtutac.mobileapp.ActvtyMain;
import com.dimtutac.mobileapp.ActvtySignin;

/**
 * Created by Jalotsav on 9/5/2017.
 */

public class UserSessionManager implements AppConstants {

    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private Context mContext;

    // Shared Preferences Keys
    private static final String IS_USER_LOGIN = "isUserLoggedIn";
    private static final String IS_REMEMBER_LOGIN = "isRememberLogin";

    // Constructor
    public UserSessionManager(Context context) {

        this.mContext = context;
        String PREFER_NAME = mContext.getPackageName() + "_shrdprfrnc";
        int PRIVATE_MODE = 0;
        pref = mContext.getSharedPreferences(PREFER_NAME, PRIVATE_MODE);
        editor = pref.edit();
        editor.apply();
    }

    // Create login session
    // Get-Set UserId to SharedPreferences
    public String getUserId() {

        return pref.getString(KEY_USER_ID, "");
    }

    public void setUserId(String userId) {

        // Storing login value as TRUE
        editor.putBoolean(IS_USER_LOGIN, true);
        editor.putString(KEY_USER_ID, userId);
        editor.commit();
    }

    // Get UserLoggedIn from SharedPreferences
    public boolean isUserLoggedIn() {

        return pref.getBoolean(IS_USER_LOGIN, false);
    }

    // Get-Set RememberLogin to SharedPreferences
    public boolean isRememberLogin() {

        return pref.getBoolean(IS_REMEMBER_LOGIN, false);
    }

    public void setRememberLogin(boolean isRememberLogin) {

        editor.putBoolean(IS_REMEMBER_LOGIN, isRememberLogin);
        editor.commit();
    }

    // Get-Set UserName to SharedPreferences
    public String getUserName() {

        return pref.getString(KEY_USERNAME, "");
    }

    public void setUserName(String userName) {

        editor.putString(KEY_USERNAME, userName);
        editor.commit();
    }

    // Get-Set FirstName to SharedPreferences
    public String getFirstName() {

        return pref.getString(KEY_FIRSTNAME, "");
    }

    public void setFirstName(String firstName) {

        editor.putString(KEY_FIRSTNAME, firstName);
        editor.commit();
    }

    // Get-Set MiddleName to SharedPreferences
    public String getMiddleName() {

        return pref.getString(KEY_MIDDLENAME, "");
    }

    public void setMiddleName(String middleName) {

        editor.putString(KEY_MIDDLENAME, middleName);
        editor.commit();
    }

    // Get-Set LastName to SharedPreferences
    public String getLastName() {

        return pref.getString(KEY_LASTNAME, "");
    }

    public void setLastName(String lastName) {

        editor.putString(KEY_LASTNAME, lastName);
        editor.commit();
    }

    // Get-Set UserTypeId to SharedPreferences
    public String getUserTypeId() {

        return pref.getString(KEY_USERTYPE_ID, "");
    }

    public void setUserTypeId(String userTypeId) {

        editor.putString(KEY_USERTYPE_ID, userTypeId);
        editor.commit();
    }

    // Get-Set Password to SharedPreferences
    public String getPassword() {

        return pref.getString(KEY_PASSWORD, "");
    }

    public void setPassword(String password) {

        editor.putString(KEY_PASSWORD, password);
        editor.commit();
    }

    // Get-Set CompanyName to SharedPreferences
    public String getCompanyName() {

        return pref.getString(KEY_COMPANY_NAME, "");
    }

    public void setCompanyName(String companyName) {

        editor.putString(KEY_COMPANY_NAME, companyName);
        editor.commit();
    }

    // Get-Set Gender to SharedPreferences
    public String getGender() {

        return pref.getString(KEY_GENDER, "");
    }

    public void setGender(String gender) {

        editor.putString(KEY_GENDER, gender);
        editor.commit();
    }

    // Get-Set DateOfBirth to SharedPreferences
    public String getDateOfBirth() {

        return pref.getString(KEY_DATEOFBIRTH, "");
    }

    public void setDateOfBirth(String dateOfBirth) {

        editor.putString(KEY_DATEOFBIRTH, dateOfBirth);
        editor.commit();
    }

    // Get-Set DefaultLanguageId to SharedPreferences
    public String getDefaultLanguageId() {

        return pref.getString(KEY_DEFAULT_LANGUAGE_ID, "");
    }

    public void setDefaultLanguageId(String defaultLanguageId) {

        editor.putString(KEY_DEFAULT_LANGUAGE_ID, defaultLanguageId);
        editor.commit();
    }

    // Get-Set RestaurantId to SharedPreferences
    public String getRestaurantId() {

        return pref.getString(KEY_RESTAURANT_ID, "");
    }

    public void setRestaurantId(String restaurantId) {

        editor.putString(KEY_RESTAURANT_ID, restaurantId);
        editor.commit();
    }

    // Get-Set MainBranchId to SharedPreferences
    public String getMainBranchId() {

        return pref.getString(KEY_MAIN_BRANCH_ID, "");
    }

    public void setMainBranchId(String mainBranchId) {

        editor.putString(KEY_RESTAURANT_ID, mainBranchId);
        editor.commit();
    }

    // Get-Set IsAgreeTC to SharedPreferences
    public boolean isAgreeTC() {

        return pref.getBoolean(KEY_IS_AGREE_TC, false);
    }

    public void setAgreeTC(boolean agreeTC) {

        editor.putBoolean(KEY_IS_AGREE_TC, agreeTC);
        editor.commit();
    }

    // Get-Set IsPromotionSend to SharedPreferences
    public boolean isPromotionSend() {

        return pref.getBoolean(KEY_IS_PROMOTION_SEND, false);
    }

    public void setPromotionSend(boolean promotionSend) {

        editor.putBoolean(KEY_IS_PROMOTION_SEND, promotionSend);
        editor.commit();
    }

    // Get-Set IsFacebookLogin to SharedPreferences
    public boolean isFacebookLogin() {

        return pref.getBoolean(KEY_IS_FACEBOOK_LOGIN, false);
    }

    public void setFacebookLogin(boolean facebookLogin) {

        editor.putBoolean(KEY_IS_FACEBOOK_LOGIN, facebookLogin);
        editor.commit();
    }

    // Get-Set FacebookId to SharedPreferences
    public String getFacebookId() {

        return pref.getString(KEY_FACEBOOK_ID, "");
    }

    public void setFacebookId(String facebookId) {

        editor.putString(KEY_FACEBOOK_ID, facebookId);
        editor.commit();
    }

    // Get-Set AccountBarcode to SharedPreferences
    public String getAccountBarcode() {

        return pref.getString(KEY_ACCOUNT_BARCODE, "");
    }

    public void setAccountBarcode(String accountBarcode) {

        editor.putString(KEY_ACCOUNT_BARCODE, accountBarcode);
        editor.commit();
    }

    // Get-Set Email to SharedPreferences
    public String getEmailId() {

        return pref.getString(KEY_EMAIL_ID, "");
    }

    public void setEmailId(String emailId) {

        editor.putString(KEY_EMAIL_ID, emailId);
        editor.commit();
    }

    // Get-Set MobileNo to SharedPreferences
    public String getMobileNo() {

        return pref.getString(KEY_MOBILENO, "");
    }

    public void setMobileNo(String mobileNo) {

        editor.putString(KEY_MOBILENO, mobileNo);
        editor.commit();
    }

    // Get-Set PhoneNo to SharedPreferences
    public String getPhoneNo() {

        return pref.getString(KEY_PHONENO, "");
    }

    public void setPhoneNo(String phoneNo) {

        editor.putString(KEY_PHONENO, phoneNo);
        editor.commit();
    }

    // Get-Set PhotoURL to SharedPreferences
    public String getPhotoURL() {

        return pref.getString(KEY_PHOTOURL, "");
    }

    public void setPhotoURL(String photoURL) {

        editor.putString(KEY_PHOTOURL, photoURL);
        editor.commit();
    }

    // Get-Set IsActive to SharedPreferences
    public boolean isActive() {

        return pref.getBoolean(KEY_IS_ACTIVE, false);
    }

    public void setActive(boolean active) {

        editor.putBoolean(KEY_IS_ACTIVE, active);
        editor.commit();
    }

    // Get-Set AddressMasterId to SharedPreferences
    public String getAddressMasterId() {

        return pref.getString(KEY_ADDRESS_MASTER_ID, "");
    }

    public void setAddressMasterId(String addressMasterId) {

        editor.putString(KEY_ADDRESS_MASTER_ID, addressMasterId);
        editor.commit();
    }

    // Get-Set AddressLine1 to SharedPreferences
    public String getAddressLine1() {

        return pref.getString(KEY_ADDRESS_LINE1, "");
    }

    public void setAddressLine1(String addressLine1) {

        editor.putString(KEY_ADDRESS_LINE1, addressLine1);
        editor.commit();
    }

    // Get-Set AddressLine2 to SharedPreferences
    public String getAddressLine2() {

        return pref.getString(KEY_ADDRESS_LINE2, "");
    }

    public void setAddressLine2(String addressLine2) {

        editor.putString(KEY_ADDRESS_LINE2, addressLine2);
        editor.commit();
    }

    // Get-Set City to SharedPreferences
    public String getCity() {

        return pref.getString(KEY_CITY, "");
    }

    public void setCity(String city) {

        editor.putString(KEY_CITY, city);
        editor.commit();
    }

    // Get-Set State to SharedPreferences
    public String getState() {

        return pref.getString(KEY_STATE, "");
    }

    public void setState(String state) {

        editor.putString(KEY_STATE, state);
        editor.commit();
    }

    // Get-Set Country to SharedPreferences
    public String getCountry() {

        return pref.getString(KEY_COUNTRY, "");
    }

    public void setCountry(String country) {

        editor.putString(KEY_COUNTRY, country);
        editor.commit();
    }

    // Get-Set Pincode to SharedPreferences
    public String getPincode() {

        return pref.getString(KEY_PINCODE, "");
    }

    public void setPincode(String pincode) {

        editor.putString(KEY_PINCODE, pincode);
        editor.commit();
    }

    // Get-Set CustomerType to SharedPreferences
    public String getCustomerType() {

        return pref.getString(KEY_CUSTOMER_TYPE, "");
    }

    public void setCustomerType(String customerType) {

        editor.putString(KEY_CUSTOMER_TYPE, customerType);
        editor.commit();
    }

    // Get-Set PasswordResetId to SharedPreferences
    public String getPasswordResetId() {

        return pref.getString(KEY_PASSWORD_RESET_ID, "");
    }

    public void setPasswordResetId(String passwordResetId) {

        editor.putString(KEY_PASSWORD_RESET_ID, passwordResetId);
        editor.commit();
    }

    // Get-Set DeviceId to SharedPreferences
    public String getDeviceId() {

        return pref.getString(KEY_DEVICE_ID, "");
    }

    public void setDeviceId(String deviceId) {

        editor.putString(KEY_DEVICE_ID, deviceId);
        editor.commit();
    }

    // Get-Set Image contain Directory path to SharedPreferences
    public String getDirctrypathsCntnsimgs(){

        return pref.getString(KEY_DIRCTRYPATHS_CNTNSIMGS, "");
    }

    public void setDirctrypathsCntnsimgs(String dirctrypaths_cntnsimgs_withcommasprt){

        editor.putString(KEY_DIRCTRYPATHS_CNTNSIMGS, dirctrypaths_cntnsimgs_withcommasprt);
        editor.commit();
    }

    // Get-Set Last File path of Image contain Directory to SharedPreferences
    public String getLastfilepathImgcntnsDirctry(){

        return pref.getString(KEY_LASTFILEPATH_IMGCNTNS_DIRCTRY, "");
    }

    public void setLastfilepathImgcntnsDirctry(String lastfilepath_imgcntns_dirctry_withcommasprt){

        editor.putString(KEY_LASTFILEPATH_IMGCNTNS_DIRCTRY, lastfilepath_imgcntns_dirctry_withcommasprt);
        editor.commit();
    }

    // Get-Set Image count of Last File path of Image contain Directory to SharedPreferences
    public String getLastfilepathImgcntnsDirctryImgcnt(){

        return pref.getString(KEY_LASTFILEPATH_IMGCNTNS_DIRCTRY_IMGCNT, "");
    }

    public void setLastfilepathImgcntnsDirctryImgcnt(String lastfilepath_imgcntns_dirctry_imgcnt_withcommasprt){

        editor.putString(KEY_LASTFILEPATH_IMGCNTNS_DIRCTRY_IMGCNT, lastfilepath_imgcntns_dirctry_imgcnt_withcommasprt);
        editor.commit();
    }

    /**
     * Clear session details and/or start app from 1st screen
     */
    public void logoutUser(boolean isOnlyClearData) {

        // Clearing all user data from Shared Preferences
        editor.clear();
        editor.commit();

        if(!isOnlyClearData) {
            // After clear data, Redirect user to SignIn Activity
            Intent i = new Intent(mContext, ActvtyMain.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            mContext.startActivity(i);
        }
    }
}
