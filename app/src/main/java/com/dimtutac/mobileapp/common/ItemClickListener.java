package com.dimtutac.mobileapp.common;

import android.view.View;

/**
 * Created by Jalotsav on 2/14/2018.
 */

public interface ItemClickListener {
    void onClick(View view, int position);
}
