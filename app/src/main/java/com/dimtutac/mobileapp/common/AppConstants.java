package com.dimtutac.mobileapp.common;

import android.os.Environment;

import com.dimtutac.mobileapp.models.reservation.MdlResrvtnListResData;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Jalotsav on 22/8/17.
 */

public interface AppConstants {

    // Build Type
    String DEBUG_BUILD_TYPE = "debug";

    // Log Type
    int LOGTYPE_VERBOSE = 1;
    int LOGTYPE_DEBUG = 2;
    int LOGTYPE_INFO = 3;
    int LOGTYPE_WARN = 4;
    int LOGTYPE_ERROR = 5;

    // API URLs
    String CONTENT_TYPE_JSON = "Content-Type: application/json";
//    String API_ROOT_URL = "http://ok-restaurant.projects.cybervon.com/api/";
    String API_ROOT_URL = "http://ok-restaurant.fuzzylogictechnology.com/api/";
//    String ATCHMNT_ROOT_URL = "http://booking.projects.cybervon.com/Attachment/";
    String ATCHMNT_ROOT_URL = "http://dimtutac.fuzzylogictechnology.com/attachment/";
    String API_LOGIN_LOGIN = "Login/Login";
    String API_USER_CREATE = "User/Create";
    String API_USER_UPDATE = "User/Update"; // ?UserId={UserId}
    String API_USER_CHECKIFFACEBOOKIDEXIST = "User/chekIfFacebookIdExist"; // ?FacebookId={FacebookId}
    String API_DASHBOARD_DASHBOARDDATA = "Dashboard/GetDashBoardData";
    String API_BRANCH_LOCATIONLIST = "Branch/LocationList?RestaurantId=A898A857-2E39-4106-8AE0-A88EDE2FA098";
    String API_BRANCH_BRANCHLIST = "Branch/BranchList?RestaurantId=A898A857-2E39-4106-8AE0-A88EDE2FA098";
    String API_RESRVTN_RESRVTNLIST = "Reservation/ReservationList"; // ?UserId=e47f9cfb-1a7a-4843-8621-3455a43d0f95
    String API_RESRVTN_DORESRVTN = "Reservation/DoReservation";
    String API_RESRVTN_UPDATERESRVTN = "Reservation/UpdateReservation"; // ?reservationId=A898A857-2E39-4106-8AE0-A88EDE2FA098
    String API_CONTACTUS_POST = "contactus/post";
    String API_CAREER_GETPOSITIONLIST = "career/GetPositionList";
    String API_CAREER_POST = "career/post";
    String API_MENU_GETNEWMONTHLYITEM = "Menu/GetNewMonthlyItem"; // ?BranchId=97A34D18-9937-469B-9391-14971885DADC
    String API_CHECKIN_DOCHECKIN = "checkin/DoCheckIn";
    String API_MENU_GETMENU = "Menu/GetMenu";
    String API_MENU_GETSUBMENU= "Menu/GetSubMenu"; // ?menuId=f3404593-ffac-4bbd-af3e-4e4d3f749450
    String API_NEWS_GETNEWSLIST = "news/GetNewsList?restaurantId=A898A857-2E39-4106-8AE0-A88EDE2FA098"; // 6d1d899e-5c9d-4def-b8f0-6e61d11948ed
    String API_RESTAURANTPHOTO_RESTAURANTPHOTOLIST = "RestaurantPhoto/RestaurantPhotoList"; //  ?RestaurantId=B8B5FB41-2ACF-49ED-8F89-D40F6EA27744 (value is BranchID)
    String API_ADMINCONFIGURATIONS_GETSPASHSCREENLOGO = "AdminConfigurations/GetSpashScreenLogo";
    String API_CHECKIN_CHECKINLIST = "CheckIn/CheckInList"; // ?UserId=e47f9cfb-1a7a-4843-8621-3455a43d0f95
    String API_CUSTOMERTYPEPHOTO_GETCUSTOMERTYPEPHOTO = "CustomerTypePhoto/GetCustomerTypePhoto"; // ?customerTypeId={userId}
    String API_OFFER_GETCUSTOMEROFFERS = "Offer/GetCustomerOffers"; // ?customerId=e47f9cfb-1a7a-4843-8621-3455a43d0f95
    String API_OFFER_REDEEMOFFER = "Offer/RedeemOffer";
    String API_FORGOTPASSWORD_INDEX = "ForgotPassword/Index";
    String API_LOGIN_GETTERMSANDCONDITIONS = "Login/GetTermsAndConditions";

    // Web-Service Keys
    String KEY_USERNAME = "UserName";
    String KEY_PASSWORD = "Password";
    String KEY_USER_ID = "user_id";
    String KEY_SUCCESS_SML = "Success";
    String KEY_MESSAGE_SML = "Message";
    String KEY_DATA_SML = "Data";
    String KEY_PKID = "PKId";
    String KEY_FIRSTNAME = "FirstName";
    String KEY_LASTNAME = "LastName";
    String KEY_MIDDLENAME = "MiddleName";
    String KEY_USERTYPE_ID = "UserTypeId";
    String KEY_COMPANY_NAME = "CompanyName";
    String KEY_GENDER = "Gender";
    String KEY_DATEOFBIRTH = "DateOfBirth";
    String KEY_DEFAULT_LANGUAGE_ID = "DefaultLanguageId";
    String KEY_RESTAURANT_ID = "RestaurantId";
    String KEY_MAIN_BRANCH_ID = "MainBranchId";
    String KEY_IS_AGREE_TC = "IsAgreeTC";
    String KEY_IS_PROMOTION_SEND = "IsPromotionSend";
    String KEY_IS_FACEBOOK_LOGIN = "IsFacebookLogin";
    String KEY_FACEBOOK_ID = "FacebookId";
    String KEY_ACCOUNT_BARCODE = "AccountBarcode";
    String KEY_EMAIL_ID = "EmailId";
    String KEY_MOBILENO = "MobileNo";
    String KEY_PHONENO = "PhoneNo";
    String KEY_PHOTOURL = "PhotoURL";
    String KEY_ADDRESS_MASTER_ID = "AddressMasterId";
    String KEY_IS_ACTIVE = "IsActive";
    String KEY_ADDRESS_LINE1 = "AddressLine1";
    String KEY_ADDRESS_LINE2 = "AddressLine2";
    String KEY_CITY = "City";
    String KEY_STATE = "State";
    String KEY_COUNTRY = "Country";
    String KEY_PINCODE = "Pincode";
    String KEY_CUSTOMER_TYPE = "CustomerType";
    String KEY_PASSWORD_RESET_ID = "PasswordResetId";
    String KEY_DEVICE_ID = "DeviceId";
    String KEY_MALE = "Male";
    String KEY_FEMALE = "Female";
    String KEY_SELECTED = "Selected";
    String KEY_TEXT = "Text";
    String KEY_VALUE = "Value";
    String KEY_BRANCH_NAME = "BranchName";
    String KEY_CODE = "Code";
    String KEY_ADDRESS_ID = "AddressId";
    String KEY_TELEPHONE_NO = "TelephoneNo";
    String KEY_BUSINESS_HOURS = "Businesshours";
    String KEY_BUSINESS_HOURS_LIST = "BusinessHoursList";
    String KEY_BRANCH_ID = "BranchId";
    String KEY_DAY = "Day";
    String KEY_OPENING_TIME = "OpeningTime";
    String KEY_CLOSING_TIME = "ClosingTime";
    String KEY_IS_DELETED = "IsDeleted";
    String KEY_LATITUDE_PREFIX = "Lat";
    String KEY_LONGITUDE_PREFIX = "Long";
    String KEY_TITLE1 = "Title1";
    String KEY_TITLE2 = "Title2";
    String KEY_TITLE3 = "Title3";
    String KEY_WITHTEXT = "WithText";
    String KEY_USER_ID_FIRSTCHARCAPS = "UserId";
    String KEY_NAME = "Name";
    String KEY_DATE = "Date";
    String KEY_FROM_TIME = "FromTime";
    String KEY_TO_TIME = "ToTime";
    String KEY_PHONE_NUMBER = "PhoneNumber";
    String KEY_NO_OF_GUEST = "NoOfGuest";
    String KEY_COMMENTS = "Comments";
    String KEY_STATUS_NAME = "StatusName";
    String KEY_STATUS = "Status";
    String KEY_RESERVATION_ID_SML = "reservationId";
    String KEY_TITLE = "Title";
    String KEY_EMAIL = "Email";
    String KEY_TELEPHONE = "Telephone";
    String KEY_POSITION_NAME = "PositionName";
    String KEY_DESCRIPTION = "Description";
    String KEY_DESCRIPTION_LIST = "DescriptionList";
    String KEY_FULLNAME = "FullName";
    String KEY_ADDRESS = "Address";
    String KEY_CONTACT_NO = "ContactNo";
    String KEY_EDUCATION = "Education";
    String KEY_POSITION_APPLIED_FOR = "PositionAppliedFor";
    String KEY_WORK_EXPERIENCE = "WorkExperience";
    String KEY_POSITION_ID = "PositionId";
    String KEY_MENU_NAME = "MenuName";
    String KEY_PRICE = "Price";
    String KEY_CURRENCY = "Currency";
    String KEY_WRITING_STATUS = "WritingStatus";
    String KEY_CHECKIN_PHOTO_MASTER = "CheckinPhotoMaster";
    String KEY_MENU_ID = "menuId";
    String KEY_CATEGORY_NAME = "CategoryName";
    String KEY_CATEGORY_ID = "CategoryId";
    String KEY_SUBCATEGORY_NAME = "SubCategoryName";
    String KEY_FROM_DATE = "FromDate";
    String KEY_TO_DATE = "ToDate";
    String KEY_VIDEOURL = "VideoURL";
    String KEY_PUBLISH_TIME = "PublishTime";
    String KEY_THUMB_IMAGEPATH = "ThumbImagePath";
    String KEY_FDATE = "FDate";
    String KEY_TDATE = "TDate";
    String KEY_IS_NEW = "IsNew";
    String KEY_RESTAURANT_NAME = "RestaurantName";
    String KEY_IMAGEPATH = "ImagePath";
    String KEY_KEY_SML = "Key";
    String KEY_IS_ALLOW_UPDATE = "IsAllowUpdate";
    String KEY_CHECKIN_DATETIME = "CheckInDateTime";
    String KEY_BRANCH_PHOTOURL = "BranchPhotoURL";
    String KEY_CUSTOMER_TYPE_ID = "customerTypeId";
    String KEY_CUSTOMER_ID = "customerId";
    String KEY_ASSIGNMENT_CODE = "AssignmentCode";
    String KEY_OFFER_ID = "OfferId";
    String KEY_IS_REDEEMED = "IsRedeemed";
    String KEY_OFFER_NAME = "OfferName";
    String KEY_PHOTO = "Photo";
    String KEY_DISCOUNT = "Discount";
    String KEY_OFFER_TYPE = "OfferType";
    String KEY_TERMS_AND_CONDITIONS = "TermsAndConditions";
    String KEY_TERMS_AND_CONDITIONS_LIST = "TermsAndConditionList";

    // Web-Service Values

    // User session manager Keys
    String KEY_DIRCTRYPATHS_CNTNSIMGS = "dirctrypathsCntnsimgs"; // Directory Path which are contain images
    String KEY_LASTFILEPATH_IMGCNTNS_DIRCTRY = "lastfilepathImgcntnsDirctry"; // Last file path of Image contain directory
    String KEY_LASTFILEPATH_IMGCNTNS_DIRCTRY_IMGCNT = "lastfilepathIimgcntnsDirctryImgcnt"; // Image count of Last file path of Image contain directory

    // PutExtra Keys
    String PUT_EXTRA_COME_FROM = "comeFrom";
    String PUT_EXTRA_NAVDRWER_POSTN = "navdrawerPosition";
    String PUT_EXTRA_RESTAURANT_ID = "restaurantId";
    String PUT_EXTRA_BRANCH_ID = "branchId";
    String PUT_EXTRA_BRANCH_NAME = "branchName";
    String PUT_EXTRA_TITLE = "title";
    String PUT_EXTRA_ADDRESS = "address";
    String PUT_EXTRA_PHONENO = "phoneno";
    String PUT_EXTRA_POSITION_ID = "positionId";
    String PUT_EXTRA_POSITION_NAME = "positionName";
    String PUT_EXTRA_DESCRPTN_LIST_STR = "descriptionListString";
    String PUT_EXTRA_IMAGE_PATH = "imagePath";
    String PUT_EXTRA_IMAGE_PATH_TYPE = "imagePathType";
    String PUT_EXTRA_PHOTO_URL = "photoURL";
    String PUT_EXTRA_SELECTED_IMAGE_DIRECTORY_PATH = "selectedImageDirectoryPath";
    String PUT_EXTRA_ARRAYLST_ALREADY_SELECTED_IMGS_PATHS = "arrylstAlrdySlctdImgPath";
    String PUT_EXTRA_TOTAL_SELECTED_IMG_COUNT = "totalSlctdImgCnt";
    String PUT_EXTRA_LATITUDE = "latitude";
    String PUT_EXTRA_LONGITUDE = "longitude";
    String PUT_EXTRA_MENU_ID = "menuId";
    String PUT_EXTRA_CATEGORY_NAME = "categoryName";
    String PUT_EXTRA_NEWS_ID = "newsId";
    String PUT_EXTRA_FROM_DATE = "fromDate";
    String PUT_EXTRA_DESCRPTN = "description";
    String PUT_EXTRA_EMAILID = "emailId";
    String PUT_EXTRA_STOREHRS = "storeHrs";
    String PUT_EXTRA_ISFACEBOOKSIGNIN = "isFacebookSignIn";
    String PUT_EXTRA_FACEBOOK_ID = "facebookId";
    String PUT_EXTRA_FACEBOOK_NAME = "facebookName";
    String PUT_EXTRA_FACEBOOK_EMAIL = "facebookEmail";
    String PUT_EXTRA_FACEBOOK_GENDER = "facebookGender";
    String PUT_EXTRA_FACEBOOK_BIRTHDAY = "facebookBirthday";
    String PUT_EXTRA_IS_ALLOW_UPDATE = "isAllowUpdate";
    String PUT_EXTRA_ASSIGNMENT_CODE = "AssignmentCode";
    String PUT_EXTRA_OFFER_ID = "OfferId";
    String PUT_EXTRA_TERMSANDCONDTN_LIST_STR = "TermsAndConditionList";

    // PutExtra Values
    int IMAGE_PATH_TYPE_FILE = 11;
    int IMAGE_PATH_TYPE_SERVER = 12;

    // Navigation Drawer MenuItem position check for direct open that fragment
    int NAVDRWER_WHATDOWEHAVE = 1;
    int NAVDRWER_HOME = 2;
    int NAVDRWER_ACCOUNT = 3;
    int NAVDRWER_LOCATIONS = 4;
    int NAVDRWER_NEW_MONTHLY_ITEMS = 5;
    int NAVDRWER_MENU = 6;
    int NAVDRWER_MORE = 7;
    int NAVDRWER_NEWS = 8;
    int NAVDRWER_RESERVATION= 9;
    int NAVDRWER_CHECKIN = 10;
    int NAVDRWER_CAREERS = 11;

    // Request Keys
    int REQUEST_SIGNIN = 101;
    int REQUEST_APP_PERMISSION_CAMERASTORAGE = 102;
    int REQUEST_APP_PERMISSION_LOCATION = 103;
    int REQUEST_OPEN_CAMERA = 104;
    int REQUEST_PICK_IMAGE = 105;
    int REQUEST_PICK_IMAGE_MULTIPLE = 106;
    int REQUEST_ENABLE_LOCATION = 107;
    int REQUEST_SIGNUP = 108;
    int REQUEST_CHECKIN_DO = 109;

    // Others
    String ID_SML = "id";
    String BASE64_PART = "base64Part";
    String EXTENSION_JPG = "jpg";

    // Static Ids
    String STATIC_USER_TYPE_ID = "43CE341F-6522-4669-BFA0-874F877CA8C1";
    String STATIC_CUSTOMER_TYPE_ID = "D7598496-8609-4E21-AFAB-7AE72D453DA8";
    String STATIC_STATUS_ID_RESRVTN_NEW = "1305900F-D476-4356-A087-69C09E0922D0";
    String STATIC_STATUS_ID_RESRVTN_COMPLETE = "E9A6A150-8FE0-4316-9957-77E44897E5FB";
    String STATIC_STATUS_ID_RESRVTN_CANCEL = "5AF82CF1-94D9-489A-A626-018749C4FCD3";

    /** Custom Gallery **/
    int NUM_OF_COLUMNS_2 = 2;
    int NUM_OF_COLUMNS_3 = 3;
    int GRID_PADDING_5 = 5; // in dp
    int GRID_PADDING_8 = 8; // in dp

    // supported file formats
    List<String> FILE_EXTN = Arrays.asList("jpg", "jpeg", "png");

    // File & Path of External storage
    String EXTRNL_STRG_PATH_STRING = Environment.getExternalStorageDirectory().getAbsolutePath();
    File PATH_EXTRNL_STRNG = new File(EXTRNL_STRG_PATH_STRING);

    ArrayList<MdlResrvtnListResData> ARRYLST_MDL_RESRVTNLIST_RESDATA = new ArrayList<>();
    ArrayList<String> ARRYLST_ATTCHDIMGS_PATH = new ArrayList<>();
}
