package com.dimtutac.mobileapp.common;

import android.util.Log;

import com.dimtutac.mobileapp.BuildConfig;

/**
 * Created by Jalotsav on 9/5/2017.
 */

public class LogHelper implements AppConstants {

    private static boolean LOGGING_ENABLED = false;

    static {

        if (BuildConfig.BUILD_TYPE.equals(DEBUG_BUILD_TYPE))
            LOGGING_ENABLED = true;
    }

    public static void printLog(int logType, String logTag, String logMessage) {

        if (LOGGING_ENABLED) {

            switch (logType) {

                case LOGTYPE_VERBOSE:

                    if (Log.isLoggable(logTag, Log.VERBOSE))
                        Log.v(logTag, logMessage);
                    break;
                case LOGTYPE_DEBUG:

                    if (Log.isLoggable(logTag, Log.DEBUG))
                        Log.d(logTag, logMessage);
                    break;
                case LOGTYPE_INFO:

                    Log.i(logTag, logMessage);
                    break;
                case LOGTYPE_WARN:

                    Log.w(logTag, logMessage);
                    break;
                case LOGTYPE_ERROR:

                    Log.e(logTag, logMessage);
                    break;
                default:

                    Log.i(logTag, logMessage);
                    break;
            }
        }
    }
}
