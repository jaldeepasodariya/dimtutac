package com.dimtutac.mobileapp.common;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import android.widget.Toast;

import com.dimtutac.mobileapp.models.dropdown.MdlGenderData;
import com.dimtutac.mobileapp.models.dropdown.MdlLanguageData;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.dimtutac.mobileapp.common.AppConstants.FILE_EXTN;

/**
 * Created by Jalotsav on 27/8/17.
 */

public class GeneralFunctions {

    private static final String TAG = GeneralFunctions.class.getSimpleName();

    /***
     * Check Internet Connection
     * Mobile device is connect with Internet or not?
     * ***/
    public static boolean isNetConnected(Context context){

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        //This for Wifi.
        NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected())
            return true;

        //This for Mobile Network.
        NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected())
            return true;

        //This for Return true else false for Current status.
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();

    }

    /**
     *  Show/Cancel Toast
     **/
    private static Toast TOAST = null;

    public static void showToastSingle(Context context, String message, int toastLength) {

        if(TOAST != null) TOAST.cancel();
        switch (toastLength) {
            case Toast.LENGTH_SHORT:

                TOAST = Toast.makeText(context, message, Toast.LENGTH_SHORT);
                TOAST.show();
                break;
            case Toast.LENGTH_LONG:

                TOAST = Toast.makeText(context, message, Toast.LENGTH_LONG);
                TOAST.show();
                break;
            default:

                TOAST = Toast.makeText(context, message, Toast.LENGTH_SHORT);
                TOAST.show();
                break;
        }
    }

    /***
     * Open native call dialer with given number
     * ***/
    public static void openDialerToCall(Context context, String mobileNo) {

        try {
            context.startActivity(
                    new Intent(Intent.ACTION_DIAL).setData(Uri.parse("tel:".concat(mobileNo)))
            );
        } catch (Exception e) {e.printStackTrace();}
    }

    /***
     * Check Email Address Format is valid or not
     * ***/
    public static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    /***
     * Check Mobile Format is valid or not
     * ***/
    public static boolean isValidMobile(String target) {

//        String numberRegExp = "^\\(?([0-9]{3})\\)?[-.]?([0-9]{3})[-.]?([0-9]{4})$";
        /*String numberRegExp = "^[0123456789]\\d{9}$"; // Prefix any from 0123456789
        Matcher matcherEmail = Pattern.compile(numberRegExp).matcher(target);
        return matcherEmail.matches();*/
        return target.length() > 9; // Min 10 digit compulsory (Max 15 is set in XML layout design)
    }

    /***
     * Check Email Format is valid or not
     * ***/
    public static boolean isValidEmail(String str) {

        String reg_Email = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Matcher matcherEmail = Pattern.compile(reg_Email).matcher(str);
        return matcherEmail.matches();
    }

    /***
     * Convert Timestamp to dd-MMM-yyyy Date format
     * ***/
    public static String getDateFromTimestamp(long timestamp) {

        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(timestamp * 1000);
        return DateFormat.format("dd-MMM-yyyy", cal).toString();
    }

    /***
     * Convert Timestamp to dd-MMM-yyyy Date format
     * ***/
    public static String getDateTimeFromTimestamp(long timestamp) {

        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(timestamp * 1000);
        return DateFormat.format("yyyy-MM-dd HH:mm:ss.sss", cal).toString();
    }

    /***
     * Get Current TimeStamp
     * ***/
    public static String getCurrentTimestamp() {

        return String.valueOf(System.currentTimeMillis() / 1000);
    }

    /***
     * Get Gender Data
     * ***/
    public static ArrayList<MdlGenderData> getGenderData() {

        ArrayList<MdlGenderData> arrylstMdlGenderData = new ArrayList<>();
        arrylstMdlGenderData.add(new MdlGenderData("Male"));
        arrylstMdlGenderData.add(new MdlGenderData("Female"));

        return arrylstMdlGenderData;
    }

    /***
     * Get Language Data
     * ***/
    public static ArrayList<MdlLanguageData> getLanguageData() {

        ArrayList<MdlLanguageData> arrylstMdlLanguageData = new ArrayList<>();
        arrylstMdlLanguageData.add(new MdlLanguageData(false, "English", "ce48ae32-5d88-46b0-8312-f65e1b00b14a"));
        arrylstMdlLanguageData.add(new MdlLanguageData(false, "Chienese", "ce35b67f-8ef5-4a98-8968-bef80468f9e9"));
        arrylstMdlLanguageData.add(new MdlLanguageData(false, "Vietnamese", "5e8c6786-fcee-4112-b17a-4c3fe13025cb"));

        return arrylstMdlLanguageData;
    }

    /***
     * Convert image to Base64 from file path
     * ***/
    public static String convertImageToBase64(String path) {

        Bitmap btmp = BitmapFactory.decodeFile(path);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        btmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] byteArrayImage = baos.toByteArray();

        return Base64.encodeToString(byteArrayImage, Base64.DEFAULT);
    }

    /**
     *	Getting screen width
     **/
    @SuppressWarnings("deprecation")
    public static int getScreenWidth(Context context) {

        int screenWidth;
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();

        final Point point = new Point();
        try {
            display.getSize(point);
        } catch (java.lang.NoSuchMethodError ignore) { // Older device
            point.x = display.getWidth();
            point.y = display.getHeight();
        }
        screenWidth = point.x;
        return screenWidth;
    }

    // Check supported file extensions like .png,.jpg,.jpeg
    public static boolean isSupportedFile(String filePath) {

        String ext = filePath.substring((filePath.lastIndexOf(".") + 1),
                filePath.length());

        return FILE_EXTN.contains(ext.toLowerCase(Locale.getDefault()));
    }

    /**
     *  Convert ArrayList to String
     **/
    public static String convertArraylistToString(ArrayList<String> arrylst) {

        StringBuilder strbuildr = new StringBuilder();
        String str_coma = "";
        for (String s : arrylst)
        {
            strbuildr.append(str_coma);
            strbuildr.append(s);
            str_coma = ",";
        }
        return strbuildr.toString();
    }

    /**
     *  Convert String to ArrayList
     **/
    public static ArrayList<String> convertStringToArraylist(String str) {

        return new ArrayList<>(Arrays.asList(str.split(",")));
    }

    /**
     *	Get all images file paths from given directory
     **/
    public static ArrayList<String> getAllImgFilePaths(File file) {

        ArrayList<String> arrylst_allimgfilepaths = new ArrayList<String>();

        String state = Environment.getExternalStorageState();
        if(state.contentEquals(Environment.MEDIA_MOUNTED) || state.contentEquals(Environment.MEDIA_MOUNTED_READ_ONLY))
        {
            File[] directories = file.listFiles();
            if(directories != null){

                for (File directory : directories) {

                    if (isSupportedFile(directory.getAbsolutePath())) {

                        arrylst_allimgfilepaths.add(directory.getAbsolutePath());
                    }
                }
            }
        }
        else
            Log.e(TAG, "External Storage Unaccessible: " + state);

        return arrylst_allimgfilepaths;
    }

    /**
     *	Generate Development Key Hashes (Used in Facebook Developer page)
     **/
    /*public void printHashKey(Context pContext) {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
                Log.i(TAG, "printHashKey() Hash Key: " + hashKey);
            }
        } catch (NoSuchAlgorithmException e) {
            Log.e(TAG, "printHashKey()", e);
        } catch (Exception e) {
            Log.e(TAG, "printHashKey()", e);
        }
    }*/
}
