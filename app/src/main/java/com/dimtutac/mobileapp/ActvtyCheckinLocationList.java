package com.dimtutac.mobileapp;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.dimtutac.mobileapp.adapter.RcyclrCheckinLocationlstAdapter;
import com.dimtutac.mobileapp.common.AppConstants;
import com.dimtutac.mobileapp.common.GeneralFunctions;
import com.dimtutac.mobileapp.common.LogHelper;
import com.dimtutac.mobileapp.common.RecyclerViewEmptySupport;
import com.dimtutac.mobileapp.models.branch.MdlLocationListRes;
import com.dimtutac.mobileapp.models.branch.MdlLocationListResData;
import com.dimtutac.mobileapp.retrofitapi.APIBranch;
import com.dimtutac.mobileapp.retrofitapi.APIRetroBuilder;

import java.util.ArrayList;

import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dimtutac.mobileapp.common.AppConstants.PUT_EXTRA_LATITUDE;
import static com.dimtutac.mobileapp.common.AppConstants.PUT_EXTRA_LONGITUDE;

/**
 * Created by Jalotsav on 11/10/2017.
 */

public class ActvtyCheckinLocationList extends AppCompatActivity {

    private static final String TAG = ActvtyCheckinLocationList.class.getSimpleName();

    @BindView(R.id.cordntrlyot_actvty_checkin_locationlst)CoordinatorLayout mCrdntrlyot;
    @BindView(R.id.lnrlyot_recyclremptyvw_appearhere)LinearLayout mLnrlyotAppearHere;
    @BindView(R.id.tv_recyclremptyvw_appearhere)TextView mTvAppearHere;
    @BindView(R.id.rcyclrvw_actvty_checkin_locationlst) RecyclerViewEmptySupport mRecyclerView;
    @BindView(R.id.prgrsbr_actvty_checkin_locationlst) ProgressBar mPrgrsbrMain;

    @BindString(R.string.no_intrnt_cnctn) String mNoInternetConnMsg;
    @BindString(R.string.server_problem_sml) String mServerPrblmMsg;
    @BindString(R.string.internal_problem_sml) String mInternalPrblmMsg;
    @BindString(R.string.locationslist_appear_here) String mLocationslstAppearHere;

    @BindDrawable(R.mipmap.ic_logo_black) Drawable mDrwblDefault;

    RecyclerView.LayoutManager mLayoutManager;
    RcyclrCheckinLocationlstAdapter mAdapter;
    ArrayList<MdlLocationListResData> mArrylstMdlLocationslst;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lo_actvty_checkin_locationlst);
        ButterKnife.bind(this);

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) { e.printStackTrace(); }

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setEmptyView(mLnrlyotAppearHere);

        mTvAppearHere.setText(mLocationslstAppearHere);

        mArrylstMdlLocationslst = new ArrayList<>();
        mAdapter = new RcyclrCheckinLocationlstAdapter(this, mArrylstMdlLocationslst, mDrwblDefault);
        mRecyclerView.setAdapter(mAdapter);

        getLocationsList();
    }

    private void getLocationsList() {

        if (GeneralFunctions.isNetConnected(this))
            getLocationsListAPI();
        else Snackbar.make(mCrdntrlyot, mNoInternetConnMsg, Snackbar.LENGTH_LONG).show();
    }

    // call API for Get Location List
    private void getLocationsListAPI() {

        mPrgrsbrMain.setVisibility(View.VISIBLE);

        APIBranch objApiBranch = APIRetroBuilder.getRetroBuilder(true).create(APIBranch.class);
        Call<MdlLocationListRes> callMdlLocationRes = objApiBranch.callGetLocationList();
        callMdlLocationRes.enqueue(new Callback<MdlLocationListRes>() {
            @Override
            public void onResponse(Call<MdlLocationListRes> call, Response<MdlLocationListRes> response) {

                mPrgrsbrMain.setVisibility(View.GONE);
                if(response.isSuccessful()) {

                    try {
                        if(response.body().isSuccess()) {

                            ArrayList<MdlLocationListResData> arrylstMdlLocationListData = response.body().getArrylstMdlLocationListData();
                            if (arrylstMdlLocationListData.size() > 0) {
                                mArrylstMdlLocationslst.addAll(arrylstMdlLocationListData);
                                mAdapter.notifyDataSetChanged();
                            }
                        } else
                            Snackbar.make(mCrdntrlyot, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                    } catch (Exception e) {

                        e.printStackTrace();
                        LogHelper.printLog(AppConstants.LOGTYPE_ERROR, TAG, e.getMessage());
                        Snackbar.make(mCrdntrlyot, mInternalPrblmMsg, Snackbar.LENGTH_LONG).show();
                    }
                } else
                    Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<MdlLocationListRes> call, Throwable t) {

                mPrgrsbrMain.setVisibility(View.GONE);
                Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == AppConstants.REQUEST_CHECKIN_DO) {
                setResult(RESULT_OK);
                onBackPressed();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
