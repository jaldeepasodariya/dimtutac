package com.dimtutac.mobileapp;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.dimtutac.mobileapp.adapter.RcyclrStoreDetailsPhotosAdapter;
import com.dimtutac.mobileapp.adapter.VwpgrSliderImagesViewAdapter;
import com.dimtutac.mobileapp.common.AppConstants;
import com.dimtutac.mobileapp.common.GeneralFunctions;
import com.dimtutac.mobileapp.common.LogHelper;
import com.dimtutac.mobileapp.common.RecyclerViewEmptySupport;
import com.dimtutac.mobileapp.models.contactus.MdlCntctusPostReq;
import com.dimtutac.mobileapp.models.contactus.MdlCntctusPostRes;
import com.dimtutac.mobileapp.models.restaurantphoto.MdlRestaurantPhotoListRes;
import com.dimtutac.mobileapp.models.restaurantphoto.MdlRestaurantPhotoListResData;
import com.dimtutac.mobileapp.retrofitapi.APIContactus;
import com.dimtutac.mobileapp.retrofitapi.APIRestaurantPhoto;
import com.dimtutac.mobileapp.retrofitapi.APIRetroBuilder;
import com.dimtutac.mobileapp.utils.ValidationUtils;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Jalotsav on 1/6/2018.
 */

public class ActvtyStoreDetails extends AppCompatActivity {

    private static final String TAG = ActvtyStoreDetails.class.getSimpleName();

    @BindView(R.id.cordntrlyot_actvty_storedtls) CoordinatorLayout mCrdntrlyot;
    @BindView(R.id.tv_actvty_storedtls_title) TextView mTvTitle;
    @BindView(R.id.tv_actvty_storedtls_addrs) TextView mTvAddress;
    @BindView(R.id.tv_actvty_storedtls_phone) TextView mTvPhoneNo;
    @BindView(R.id.tv_actvty_storedtls_email) TextView mTvEmail;
    @BindView(R.id.tv_actvty_storedtls_storehrs) TextView mTvStoreHrs;
    @BindView(R.id.lnrlyot_actvty_storedtls_storetime) LinearLayout mLnrlyotStoreTime;
    @BindView(R.id.mapvw_actvty_storedtls_location) MapView mMapvwLocation;
    @BindView(R.id.lnrlyot_recyclremptyvw_appearhere) LinearLayout mLnrlyotAppearHere;
    @BindView(R.id.tv_recyclremptyvw_appearhere) TextView mTvAppearHere;
    @BindView(R.id.rcyclrvw_actvty_storedtls_photos) RecyclerViewEmptySupport mRecyclerView;
    @BindView(R.id.et_actvty_contact_title) EditText mEtTitle;
    @BindView(R.id.et_actvty_contact_name) EditText mEtName;
    @BindView(R.id.et_actvty_contact_phoneno) EditText mEtPhoneno;
    @BindView(R.id.et_actvty_contact_email) EditText mEtEmail;
    @BindView(R.id.et_actvty_contact_message) EditText mEtMessage;
    @BindView(R.id.prgrsbr_actvty_storedtls) ProgressBar mPrgrsbrMain;

    @BindString(R.string.no_intrnt_cnctn) String mNoInternetConnMsg;
    @BindString(R.string.server_problem_sml) String mServerPrblmMsg;
    @BindString(R.string.internal_problem_sml) String mInternalPrblmMsg;
    @BindString(R.string.photos_appear_here) String mPhotosAppearHere;
    @BindString(R.string.entr_the_title_sml) String mEntrTitle;
    @BindString(R.string.entr_your_name_sml) String mEntrName;
    @BindString(R.string.entr_your_email_sml) String mEntrEmail;

    @BindDrawable(R.mipmap.ic_logo_black) Drawable mDrwblDefault;

    String mBranchId, mRestaurantId, mBranchTitle, mBranchAddress, mBranchPhoneNO, mBranchEmailID, mBranchPhotoURL, mBranchStoreHrs, mBranchLatitude, mBranchLongitude;
    RecyclerView.LayoutManager mLayoutManager;
    RcyclrStoreDetailsPhotosAdapter mAdapter;
    public ArrayList<String> mArrylstRestaurantPhoto;
    Dialog mDialogSliderImages;
    ViewPager mvwpgrSliderImages;
    VwpgrSliderImagesViewAdapter mAdptrVwpgr;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lo_actvty_store_details);
        ButterKnife.bind(this);

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            // To avoid automatically appear android keyboard when activity start
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        } catch (Exception e) {
            e.printStackTrace();
        }

        mBranchId = getIntent().getStringExtra(AppConstants.PUT_EXTRA_BRANCH_ID);
        mRestaurantId = getIntent().getStringExtra(AppConstants.PUT_EXTRA_RESTAURANT_ID);
        mBranchTitle = getIntent().getStringExtra(AppConstants.PUT_EXTRA_BRANCH_NAME);
        mBranchAddress = getIntent().getStringExtra(AppConstants.PUT_EXTRA_ADDRESS);
        mBranchPhoneNO = getIntent().getStringExtra(AppConstants.PUT_EXTRA_PHONENO);
        mBranchEmailID = getIntent().getStringExtra(AppConstants.PUT_EXTRA_EMAILID);
        mBranchPhotoURL = getIntent().getStringExtra(AppConstants.PUT_EXTRA_PHOTO_URL);
        mBranchStoreHrs = getIntent().getStringExtra(AppConstants.PUT_EXTRA_STOREHRS);
        mBranchLatitude = getIntent().getStringExtra(AppConstants.PUT_EXTRA_LATITUDE);
        mBranchLongitude = getIntent().getStringExtra(AppConstants.PUT_EXTRA_LONGITUDE);

        mLayoutManager = new GridLayoutManager(this, 3);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setEmptyView(mLnrlyotAppearHere);

        mTvAppearHere.setText(mPhotosAppearHere);

        mArrylstRestaurantPhoto = new ArrayList<>();
        mAdapter = new RcyclrStoreDetailsPhotosAdapter(this, mArrylstRestaurantPhoto, mDrwblDefault);
        mRecyclerView.setAdapter(mAdapter);

        setIntentDataOnUI();

        initMenuDetailsDialog();

        getRestaurantPhotoList();

        mMapvwLocation.onCreate(savedInstanceState);
        mMapvwLocation.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {

                Log.i(TAG, "onMapReady");
                if(!TextUtils.isEmpty(mBranchLatitude) && !TextUtils.isEmpty(mBranchLongitude)) {

                    try {

                        LatLng mPosition = new LatLng(Double.parseDouble(mBranchLatitude), Double.parseDouble(mBranchLongitude));
                        googleMap.addMarker(new MarkerOptions().position(mPosition).title(mBranchTitle));
                        CameraUpdate mCameraUpdate = CameraUpdateFactory.newLatLngZoom(mPosition, 16);
                        googleMap.animateCamera(mCameraUpdate);
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.e(TAG, "onMapReady: TryCatch" + e.getMessage());
                    }
                }
            }
        });
    }

    // Set data from Intent of previous screen on UI
    private void setIntentDataOnUI() {

        mTvTitle.setText(mBranchTitle);
        mTvAddress.setText(mBranchAddress);
        mTvPhoneNo.setText(mBranchPhoneNO);
        mTvEmail.setText(mBranchEmailID);
        if(TextUtils.isEmpty(mBranchStoreHrs))
            mLnrlyotStoreTime.setVisibility(View.GONE);
        else {
            mLnrlyotStoreTime.setVisibility(View.VISIBLE);
            mTvStoreHrs.setText(mBranchStoreHrs);
        }
    }

    // Initialization of Menu Details dialog
    private void initMenuDetailsDialog() {

        mDialogSliderImages = new Dialog(this);
        mDialogSliderImages.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialogSliderImages.setContentView(R.layout.lo_dialog_branch_newmonthlyitems_dtls);
        mDialogSliderImages.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        mvwpgrSliderImages = mDialogSliderImages.findViewById(R.id.vwpgr_dialog_branch_newmonthlyitems_dtls);
        ImageView mImgvwPrevious = mDialogSliderImages.findViewById(R.id.imgvw_dialog_branch_newmonthlyitems_dtls_previous);
        ImageView mImgvwNext = mDialogSliderImages.findViewById(R.id.imgvw_dialog_branch_newmonthlyitems_dtls_next);
        mImgvwPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mAdptrVwpgr != null)
                    mvwpgrSliderImages.setCurrentItem(mvwpgrSliderImages.getCurrentItem()-1,true);
            }
        });
        mImgvwNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mAdptrVwpgr != null)
                    mvwpgrSliderImages.setCurrentItem(mvwpgrSliderImages.getCurrentItem()+1,true);
            }
        });
    }

    @OnClick({R.id.appcmptbtn_actvty_storedtls_resrvtn, R.id.appcmptbtn_actvty_storedtls_checkin, R.id.appcmptbtn_actvty_contact_send})
    public void onClickView(View view) {

        switch (view.getId()) {
            case R.id.appcmptbtn_actvty_storedtls_resrvtn:

                startActivity(new Intent(this, ActvtyReservation.class)
                        .putExtra(AppConstants.PUT_EXTRA_RESTAURANT_ID, mRestaurantId));
                break;
            case R.id.appcmptbtn_actvty_storedtls_checkin:

                startActivity(new Intent(this, ActvtyCheckin.class)
                        .putExtra(AppConstants.PUT_EXTRA_BRANCH_ID, mBranchId)
                        .putExtra(AppConstants.PUT_EXTRA_BRANCH_NAME, mBranchTitle)
                        .putExtra(AppConstants.PUT_EXTRA_RESTAURANT_ID, mRestaurantId)
                        .putExtra(AppConstants.PUT_EXTRA_PHOTO_URL, mBranchPhotoURL)
                        .putExtra(AppConstants.PUT_EXTRA_LATITUDE, mBranchLatitude)
                        .putExtra(AppConstants.PUT_EXTRA_LONGITUDE, mBranchLongitude));
                break;
            case R.id.appcmptbtn_actvty_contact_send:

                if(mPrgrsbrMain.getVisibility() != View.VISIBLE)
                    checkAllValidation();
                break;
        }
    }

    private void getRestaurantPhotoList() {

        if (GeneralFunctions.isNetConnected(this))
            getRestaurantPhotoListAPI();
        else Snackbar.make(mCrdntrlyot, mNoInternetConnMsg, Snackbar.LENGTH_LONG).show();
    }

    // call API for Get Restaurant Photo List
    private void getRestaurantPhotoListAPI() {

        mPrgrsbrMain.setVisibility(View.VISIBLE);

        APIRestaurantPhoto objApiRestaurantPhoto = APIRetroBuilder.getRetroBuilder(true).create(APIRestaurantPhoto.class);
        Call<MdlRestaurantPhotoListRes> callMdlRestaurantPhotoRes = objApiRestaurantPhoto.callGetRestaurantPhotoList(mBranchId);
        callMdlRestaurantPhotoRes.enqueue(new Callback<MdlRestaurantPhotoListRes>() {
            @Override
            public void onResponse(Call<MdlRestaurantPhotoListRes> call, Response<MdlRestaurantPhotoListRes> response) {

                mPrgrsbrMain.setVisibility(View.GONE);
                if(response.isSuccessful()) {

                    try {
                        if(response.body().isSuccess()) {

                            ArrayList<MdlRestaurantPhotoListResData> arrylstMdlRestaurantPhotoListData = response.body().getArrylstMdlRestaurantPhotoListData();
                            if (arrylstMdlRestaurantPhotoListData.size() > 0) {

                                for(MdlRestaurantPhotoListResData objMdlRestaurantPhoto : arrylstMdlRestaurantPhotoListData) {

                                    mAdapter.addItem(objMdlRestaurantPhoto.getImagePath());
                                }

                                mAdptrVwpgr = new VwpgrSliderImagesViewAdapter(ActvtyStoreDetails.this, mAdapter.getAllItems(), mDrwblDefault);
                                mvwpgrSliderImages.setAdapter(mAdptrVwpgr);
                            }
                        } else
                            Snackbar.make(mCrdntrlyot, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                    } catch (Exception e) {

                        e.printStackTrace();
                        LogHelper.printLog(AppConstants.LOGTYPE_ERROR, TAG, e.getMessage());
                        Snackbar.make(mCrdntrlyot, mInternalPrblmMsg, Snackbar.LENGTH_LONG).show();
                    }
                } else
                    Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<MdlRestaurantPhotoListRes> call, Throwable t) {

                mPrgrsbrMain.setVisibility(View.GONE);
                Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    // Set current position of Menu Details dialog view pager
    public void setMenuDtlsDialogCurrentItem(int position) {

        if(!mDialogSliderImages.isShowing()) {

            mDialogSliderImages.show();
            if (mAdptrVwpgr != null)
                mvwpgrSliderImages.setCurrentItem(position);
        }
    }

    private void checkAllValidation() {

        if (!ValidationUtils.validateEmpty(this, mCrdntrlyot, mEtTitle, mEntrTitle))
            return;

        if (!ValidationUtils.validateEmpty(this, mCrdntrlyot, mEtName, mEntrName))
            return;

        if (!ValidationUtils.validateMobile(this, mCrdntrlyot, mEtPhoneno))
            return;

        if (!ValidationUtils.validateEmpty(this, mCrdntrlyot, mEtEmail, mEntrEmail))
            return;

        if (!ValidationUtils.validateEmailFormat(this, mCrdntrlyot, mEtEmail))
            return;

        if (GeneralFunctions.isNetConnected(this))
            callContactusPostAPI();
        else Snackbar.make(mCrdntrlyot, mNoInternetConnMsg, Snackbar.LENGTH_LONG).show();
    }

    private void callContactusPostAPI() {

        mPrgrsbrMain.setVisibility(View.VISIBLE);

        MdlCntctusPostReq objMdlCntctusPostReq = new MdlCntctusPostReq();
        objMdlCntctusPostReq.setTitle(mEtTitle.getText().toString().trim());
        objMdlCntctusPostReq.setName(mEtName.getText().toString().trim());
        objMdlCntctusPostReq.setTelephone(mEtPhoneno.getText().toString().trim());
        objMdlCntctusPostReq.setEmail(mEtEmail.getText().toString().trim());
        objMdlCntctusPostReq.setMessage(TextUtils.isEmpty(mEtMessage.getText().toString().trim())? "": mEtMessage.getText().toString().trim());
        objMdlCntctusPostReq.setBranchId(mBranchId);

        APIContactus objApiCntctus = APIRetroBuilder.getRetroBuilder(true).create(APIContactus.class);
        Call<MdlCntctusPostRes> callMdlCntctusPostRes = objApiCntctus.callContactusPost(objMdlCntctusPostReq);
        callMdlCntctusPostRes.enqueue(new Callback<MdlCntctusPostRes>() {
            @Override
            public void onResponse(Call<MdlCntctusPostRes> call, Response<MdlCntctusPostRes> response) {

                mPrgrsbrMain.setVisibility(View.GONE);
                if(response.isSuccessful()) {

                    try {
                        if(response.body().isSuccess()) {

                            Toast.makeText(ActvtyStoreDetails.this, response.body().getData(), Toast.LENGTH_SHORT).show();
                            clearContactUI();
                        } else
                            Snackbar.make(mCrdntrlyot, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                    } catch (Exception e) {

                        e.printStackTrace();
                        LogHelper.printLog(AppConstants.LOGTYPE_ERROR, TAG, e.getMessage());
                        Snackbar.make(mCrdntrlyot, mInternalPrblmMsg, Snackbar.LENGTH_LONG).show();
                    }
                } else
                    Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<MdlCntctusPostRes> call, Throwable t) {

                mPrgrsbrMain.setVisibility(View.GONE);
                Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    // Clear/Blank EditText field of Contact form
    private void clearContactUI() {

        mEtTitle.setText("");
        mEtName.setText("");
        mEtPhoneno.setText("");
        mEtEmail.setText("");
        mEtMessage.setText("");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        mMapvwLocation.onResume();
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapvwLocation.onDestroy();
    }
}
