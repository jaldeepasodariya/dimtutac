package com.dimtutac.mobileapp;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.dimtutac.mobileapp.adapter.RcyclrBranchNewMonthlyItemsAdapter;
import com.dimtutac.mobileapp.adapter.VwpgrBranchNewMonthlyDialogAdapter;
import com.dimtutac.mobileapp.common.AppConstants;
import com.dimtutac.mobileapp.common.GeneralFunctions;
import com.dimtutac.mobileapp.common.LogHelper;
import com.dimtutac.mobileapp.common.RecyclerViewEmptySupport;
import com.dimtutac.mobileapp.models.menu.MdlNewMonthlyItemListRes;
import com.dimtutac.mobileapp.models.menu.MdlNewMonthlyItemListResData;
import com.dimtutac.mobileapp.retrofitapi.APIMenu;
import com.dimtutac.mobileapp.retrofitapi.APIRetroBuilder;

import java.util.ArrayList;

import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Jalotsav on 11/3/2017.
 */

public class ActvtyBranchNewMonthlyItems extends AppCompatActivity implements AppConstants {

    private static final String TAG = ActvtyBranchNewMonthlyItems.class.getSimpleName();

    @BindView(R.id.cordntrlyot_actvty_branch_newmonthlyitems)CoordinatorLayout mCrdntrlyot;
    @BindView(R.id.lnrlyot_recyclremptyvw_appearhere)LinearLayout mLnrlyotAppearHere;
    @BindView(R.id.tv_recyclremptyvw_appearhere)TextView mTvAppearHere;
    @BindView(R.id.rcyclrvw_actvty_branch_newmonthlyitems) RecyclerViewEmptySupport mRecyclerView;
    @BindView(R.id.prgrsbr_actvty_branch_newmonthlyitems) ProgressBar mPrgrsbrMain;

    @BindString(R.string.no_intrnt_cnctn) String mNoInternetConnMsg;
    @BindString(R.string.server_problem_sml) String mServerPrblmMsg;
    @BindString(R.string.internal_problem_sml) String mInternalPrblmMsg;
    @BindString(R.string.newmonthlyitemslist_appear_here) String mNewMonthlyItemslstAppearHere;

    @BindDrawable(R.mipmap.ic_logo_black) Drawable mDrwblDefault;

    RecyclerView.LayoutManager mLayoutManager;
    RcyclrBranchNewMonthlyItemsAdapter mAdapter;
    ArrayList<MdlNewMonthlyItemListResData> mArrylstMdlNewMonthlyItems;
    Dialog mDialogMenuDetails;
    ViewPager mvwpgrMenuDtls;
    VwpgrBranchNewMonthlyDialogAdapter mAdptrVwpgr;

    String mBranchId, mBranchName;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lo_actvty_branch_newmonthlyitems);
        ButterKnife.bind(this);

        mBranchId = getIntent().getStringExtra(AppConstants.PUT_EXTRA_BRANCH_ID);
        mBranchName = getIntent().getStringExtra(AppConstants.PUT_EXTRA_BRANCH_NAME);

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            setTitle(mBranchName);
        } catch (Exception e) { e.printStackTrace(); }

        mLayoutManager = new GridLayoutManager(this, 2);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setEmptyView(mLnrlyotAppearHere);

        mTvAppearHere.setText(mNewMonthlyItemslstAppearHere);

        mArrylstMdlNewMonthlyItems = new ArrayList<>();
        mAdapter = new RcyclrBranchNewMonthlyItemsAdapter(this, mArrylstMdlNewMonthlyItems, mDrwblDefault);
        mRecyclerView.setAdapter(mAdapter);

        initMenuDetailsDialog();

        getBranchNewMonthlyItemList();
    }

    // Initialization of Menu Details dialog
    private void initMenuDetailsDialog() {

        mDialogMenuDetails = new Dialog(this);
        mDialogMenuDetails.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialogMenuDetails.setContentView(R.layout.lo_dialog_branch_newmonthlyitems_dtls);
        mDialogMenuDetails.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        mvwpgrMenuDtls = mDialogMenuDetails.findViewById(R.id.vwpgr_dialog_branch_newmonthlyitems_dtls);
        ImageView mImgvwPrevious = mDialogMenuDetails.findViewById(R.id.imgvw_dialog_branch_newmonthlyitems_dtls_previous);
        ImageView mImgvwNext = mDialogMenuDetails.findViewById(R.id.imgvw_dialog_branch_newmonthlyitems_dtls_next);
        mImgvwPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mAdptrVwpgr != null)
                    mvwpgrMenuDtls.setCurrentItem(mvwpgrMenuDtls.getCurrentItem()-1,true);
            }
        });
        mImgvwNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mAdptrVwpgr != null)
                    mvwpgrMenuDtls.setCurrentItem(mvwpgrMenuDtls.getCurrentItem()+1,true);
            }
        });
    }

    private void getBranchNewMonthlyItemList() {

        if (GeneralFunctions.isNetConnected(this))
            getBranchNewMonthlyItemListAPI();
        else Snackbar.make(mCrdntrlyot, mNoInternetConnMsg, Snackbar.LENGTH_LONG).show();
    }

    // call API for Get New Monthly Items List of Branch
    private void getBranchNewMonthlyItemListAPI() {

        mPrgrsbrMain.setVisibility(View.VISIBLE);

        APIMenu objApiMenu = APIRetroBuilder.getRetroBuilder(true).create(APIMenu.class);
        Call<MdlNewMonthlyItemListRes> callMdlNewMonthlyItemListRes = objApiMenu.callGetNewMonthlyItem(mBranchId);
        callMdlNewMonthlyItemListRes.enqueue(new Callback<MdlNewMonthlyItemListRes>() {
            @Override
            public void onResponse(Call<MdlNewMonthlyItemListRes> call, Response<MdlNewMonthlyItemListRes> response) {

                mPrgrsbrMain.setVisibility(View.GONE);
                if(response.isSuccessful()) {

                    try {
                        if(response.body().isSuccess()) {

                            ArrayList<MdlNewMonthlyItemListResData> arrylstMdlLocationListData = response.body().getArrylstMdlNewMonthlyItemListData();
                            if (arrylstMdlLocationListData.size() > 0) {
                                mArrylstMdlNewMonthlyItems.addAll(arrylstMdlLocationListData);
                                mAdapter.notifyDataSetChanged();

                                mAdptrVwpgr = new VwpgrBranchNewMonthlyDialogAdapter(ActvtyBranchNewMonthlyItems.this, mArrylstMdlNewMonthlyItems, mDrwblDefault);
                                mvwpgrMenuDtls.setAdapter(mAdptrVwpgr);
                            }
                        } else
                            Snackbar.make(mCrdntrlyot, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                    } catch (Exception e) {

                        e.printStackTrace();
                        LogHelper.printLog(AppConstants.LOGTYPE_ERROR, TAG, e.getMessage());
                        Snackbar.make(mCrdntrlyot, mInternalPrblmMsg, Snackbar.LENGTH_LONG).show();
                    }
                } else
                    Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<MdlNewMonthlyItemListRes> call, Throwable t) {

                mPrgrsbrMain.setVisibility(View.GONE);
                Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    // Set current position of Menu Details dialog view pager
    public void setMenuDtlsDialogCurrentItem(int position) {

        if(!mDialogMenuDetails.isShowing()) {

            mDialogMenuDetails.show();
            if (mAdptrVwpgr != null)
                mvwpgrMenuDtls.setCurrentItem(position);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
