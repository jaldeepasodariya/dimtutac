package com.dimtutac.mobileapp;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.ImageView;

import com.dimtutac.mobileapp.common.AppConstants;
import com.squareup.picasso.Picasso;

import java.io.File;

import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Jalotsav on 11/10/2017.
 */

public class ActvtyPreviewImage extends AppCompatActivity {

    @BindView(R.id.imgvw_actvty_prevwimage_preview) ImageView mImgvwPrevwImg;

    @BindDrawable(R.mipmap.ic_logo_black) Drawable mDrwblDefault;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lo_actvty_previewimage);
        ButterKnife.bind(this);

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) { e.printStackTrace(); }

        String imgPath = getIntent().getStringExtra(AppConstants.PUT_EXTRA_IMAGE_PATH);
        int imgPathType = getIntent().getIntExtra(AppConstants.PUT_EXTRA_IMAGE_PATH_TYPE, 0);
        switch (imgPathType) {
            case AppConstants.IMAGE_PATH_TYPE_FILE:
                Picasso.with(this)
                        .load(new File(imgPath))
                        .placeholder(mDrwblDefault)
                        .into(mImgvwPrevwImg);
                break;
            case AppConstants.IMAGE_PATH_TYPE_SERVER:
                Picasso.with(this)
                        .load(AppConstants.ATCHMNT_ROOT_URL.concat(imgPath))
                        .placeholder(mDrwblDefault)
                        .into(mImgvwPrevwImg);
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
