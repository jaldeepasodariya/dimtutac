package com.dimtutac.mobileapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.dimtutac.mobileapp.common.AppConstants;
import com.dimtutac.mobileapp.common.GeneralFunctions;
import com.dimtutac.mobileapp.common.LogHelper;
import com.dimtutac.mobileapp.common.UserSessionManager;
import com.dimtutac.mobileapp.models.forgotpassword.MdlForgotPasswordReq;
import com.dimtutac.mobileapp.models.forgotpassword.MdlForgotPasswordRes;
import com.dimtutac.mobileapp.models.login.MdlLoginReq;
import com.dimtutac.mobileapp.models.login.MdlLoginResData;
import com.dimtutac.mobileapp.models.user.MdlCheckIfFacebookIdExistReq;
import com.dimtutac.mobileapp.retrofitapi.APIForgotPassword;
import com.dimtutac.mobileapp.retrofitapi.APILogin;
import com.dimtutac.mobileapp.retrofitapi.APIRetroBuilder;
import com.dimtutac.mobileapp.retrofitapi.APIUser;
import com.dimtutac.mobileapp.utils.ValidationUtils;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Jalotsav on 25/8/17.
 */

public class ActvtySignin extends AppCompatActivity implements AppConstants {

    private static final String TAG = ActvtySignin.class.getSimpleName();

    @BindView(R.id.cordntrlyot_actvty_signin) CoordinatorLayout mCrdntrlyot;
    @BindView(R.id.et_actvty_signin_email) EditText mEtEmail;
    @BindView(R.id.et_actvty_signin_paswrd) EditText mEtPaswrd;
    @BindView(R.id.chckbx_actvty_signin_rememberme) CheckBox mChckbxRembrme;
    @BindView(R.id.tv_actvty_signin_forgotpaswrd) TextView mTvForgtPswrd;
    @BindView(R.id.prgrsbr_actvty_signin) ProgressBar mPrgrsbrMain;

    @BindString(R.string.no_intrnt_cnctn) String mNoInternetConnMsg;
    @BindString(R.string.server_problem_sml) String mServerPrblmMsg;
    @BindString(R.string.internal_problem_sml) String mInternalPrblmMsg;
    @BindString(R.string.entr_email_sml) String mEntrEmail;
    @BindString(R.string.entr_password_sml) String mEntrPaswrd;

    UserSessionManager session;
    CallbackManager mCallbackManager;
    String mFacebookId = "", mFacebookName = "", mFacebookEmail = "", mFacebookGender = "", mFacebookBirthday = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lo_actvty_signin);
        ButterKnife.bind(this);

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) { e.printStackTrace(); }

        session = new UserSessionManager(this);
        mCallbackManager = CallbackManager.Factory.create();

//        boolean loggedIn = AccessToken.getCurrentAccessToken() == null;
//        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile"));
    }

    @OnClick({R.id.tv_actvty_signin_forgotpaswrd, R.id.appcmptbtn_actvty_signin_signin,
            R.id.appcmptbtn_actvty_signin_createnewaccount, R.id.appcmptbtn_actvty_signin_signfacebook})
    public void onClickView(View view) {

        switch (view.getId()) {
            case R.id.tv_actvty_signin_forgotpaswrd:

                if(mPrgrsbrMain.getVisibility() != View.VISIBLE)
                    checkForgotPasswordValidation();
                break;
            case R.id.appcmptbtn_actvty_signin_signin:

                checkAllValidation();
                break;
            case R.id.appcmptbtn_actvty_signin_createnewaccount:

                startSignupActivty(false);
                break;
            case R.id.appcmptbtn_actvty_signin_signfacebook:

                LoginManager.getInstance().logInWithReadPermissions(this,
                        Arrays.asList("public_profile", "email", "user_birthday"));
                facebookLoginCallback();
                break;
        }
    }

    private void checkForgotPasswordValidation() {

        if (!ValidationUtils.validateEmpty(this, mCrdntrlyot, mEtEmail, mEntrEmail))
            return;

        if (!ValidationUtils.validateEmailFormat(this, mCrdntrlyot, mEtEmail))
            return;

        if(GeneralFunctions.isNetConnected(this))
            callForgotPasswordAPI();
        else Toast.makeText(this, mNoInternetConnMsg, Toast.LENGTH_LONG).show();
    }

    private void callForgotPasswordAPI() {

        mPrgrsbrMain.setVisibility(View.VISIBLE);
        MdlForgotPasswordReq objMdlForgotPasswordReq = new MdlForgotPasswordReq(mEtEmail.getText().toString().trim());

        APIForgotPassword objApiForgtPaswrd = APIRetroBuilder.getRetroBuilder(true).create(APIForgotPassword.class);
        Call<MdlForgotPasswordRes> callMdlForgtPaswrdRes = objApiForgtPaswrd.callForgotPassword(objMdlForgotPasswordReq);
        callMdlForgtPaswrdRes.enqueue(new Callback<MdlForgotPasswordRes>() {
            @Override
            public void onResponse(Call<MdlForgotPasswordRes> call, Response<MdlForgotPasswordRes> response) {

                mPrgrsbrMain.setVisibility(View.GONE);
                if(response.isSuccessful()) {

                    try {

                        if (response.body().isSuccess())
                            Toast.makeText(ActvtySignin.this, response.body().getData(), Toast.LENGTH_LONG).show();
                        else
                            Snackbar.make(mCrdntrlyot, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                    } catch (Exception e) {

                        e.printStackTrace();
                        LogHelper.printLog(AppConstants.LOGTYPE_ERROR, TAG, e.getMessage());
                        Snackbar.make(mCrdntrlyot, mInternalPrblmMsg, Snackbar.LENGTH_LONG).show();
                    }
                } else
                    Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<MdlForgotPasswordRes> call, Throwable t) {

                mPrgrsbrMain.setVisibility(View.GONE);
                Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    private void checkAllValidation() {

        if (!ValidationUtils.validateEmpty(this, mCrdntrlyot, mEtEmail, mEntrEmail))
            return;

        if (!ValidationUtils.validateEmailFormat(this, mCrdntrlyot, mEtEmail))
            return;

        if (!ValidationUtils.validateEmpty(this, mCrdntrlyot, mEtPaswrd, mEntrPaswrd))
            return;

        if(GeneralFunctions.isNetConnected(this))
            callSigninAPI();
        else Snackbar.make(mCrdntrlyot, mNoInternetConnMsg, Snackbar.LENGTH_LONG).show();
    }

    private void callSigninAPI() {

        mPrgrsbrMain.setVisibility(View.VISIBLE);
        MdlLoginReq objMdlLoginReq = new MdlLoginReq(mEtEmail.getText().toString().trim(), mEtPaswrd.getText().toString().trim());

        APILogin objApiLogin = APIRetroBuilder.getRetroBuilder(true).create(APILogin.class);
        Call<ResponseBody> callMdlLoginRes = objApiLogin.callLogin(objMdlLoginReq);
        callMdlLoginRes.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                mPrgrsbrMain.setVisibility(View.GONE);
                if(response.isSuccessful()) {

                    try {

                        JSONObject jsnObjResponse = new JSONObject(response.body().string());
                        boolean isSuccess = jsnObjResponse.getBoolean(KEY_SUCCESS_SML);
                        String message = jsnObjResponse.getString(KEY_MESSAGE_SML);

                        if(isSuccess)
                            storeUserDataToLocal(jsnObjResponse);
                        else
                            Snackbar.make(mCrdntrlyot, message, Snackbar.LENGTH_LONG).show();
                    } catch (Exception e) {

                        e.printStackTrace();
                        LogHelper.printLog(AppConstants.LOGTYPE_ERROR, TAG, e.getMessage());
                        Snackbar.make(mCrdntrlyot, mInternalPrblmMsg, Snackbar.LENGTH_LONG).show();
                    }
                } else
                    Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                mPrgrsbrMain.setVisibility(View.GONE);
                Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    // Store user all data in to SharedPreference
    private void storeUserDataToLocal(JSONObject jsnObjResponse) {

        try {
            JSONObject data = jsnObjResponse.getJSONObject(KEY_DATA_SML);
            Gson objGson = new Gson();
            MdlLoginResData objMdlLoginResData = objGson.fromJson(data.toString(), MdlLoginResData.class);
            if(objMdlLoginResData != null) {
                UserSessionManager session = new UserSessionManager(ActvtySignin.this);
                session.setRememberLogin(mChckbxRembrme.isChecked());
                session.setUserId(objMdlLoginResData.getPkId());
                session.setUserName(objMdlLoginResData.getEmailId());
                session.setFirstName(objMdlLoginResData.getFirstName());
                session.setLastName(objMdlLoginResData.getLastName());
                session.setMiddleName(objMdlLoginResData.getMiddleName());
                session.setUserTypeId(objMdlLoginResData.getUserTypeId());
                session.setPassword(objMdlLoginResData.getPassword());
                session.setCompanyName(objMdlLoginResData.getCompanyName());
                session.setGender(objMdlLoginResData.getGender());
                session.setDateOfBirth(objMdlLoginResData.getDateOfBirth());
                session.setDefaultLanguageId(objMdlLoginResData.getDefaultLanguageId());
                session.setRestaurantId(objMdlLoginResData.getRestaurantId());
                session.setMainBranchId(objMdlLoginResData.getMainBranchId());
                session.setAgreeTC(objMdlLoginResData.isAgreeTC());
                session.setPromotionSend(objMdlLoginResData.isPromotionSend());
                session.setFacebookLogin(objMdlLoginResData.isFacebookLogin());
                session.setFacebookId(objMdlLoginResData.getFacebookId());
                session.setAccountBarcode(objMdlLoginResData.getAccountBarcode());
                session.setEmailId(objMdlLoginResData.getEmailId());
                session.setMobileNo(objMdlLoginResData.getMobileNo());
                session.setPhoneNo(objMdlLoginResData.getPhoneNo());
                session.setPhotoURL(objMdlLoginResData.getPhotoURL());
                session.setActive(objMdlLoginResData.isActive());
                session.setAddressMasterId(objMdlLoginResData.getAddressMasterId());
                session.setAddressLine1(objMdlLoginResData.getAddressLine1());
                session.setAddressLine2(objMdlLoginResData.getAddressLine2());
                session.setCity(objMdlLoginResData.getCity());
                session.setState(objMdlLoginResData.getState());
                session.setCountry(objMdlLoginResData.getCountry());
                session.setPincode(objMdlLoginResData.getPincode());
                session.setCustomerType(objMdlLoginResData.getCustomerType());
                session.setPasswordResetId(objMdlLoginResData.getPasswordResetId());
                session.setDeviceId(objMdlLoginResData.getDeviceId());

                setResult(RESULT_OK);
                finish();
            }
        } catch (Exception e) {

            e.printStackTrace();
            LogHelper.printLog(AppConstants.LOGTYPE_ERROR, TAG, e.getMessage());
            Snackbar.make(mCrdntrlyot, mInternalPrblmMsg, Snackbar.LENGTH_LONG).show();
        }
    }

    private void facebookLoginCallback() {

        LoginManager.getInstance().registerCallback(mCallbackManager,
            new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {

                    Log.i(TAG, "FacebookCallback-onSuccess: AccessToken - " + loginResult.getAccessToken().getToken());
                    GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {

                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                Log.i(TAG, "GraphJSONObjectCallback: Object - " + object.toString());
                                try {
                                    mFacebookId = object.getString("id");
                                    mFacebookName = object.getString("name");
                                    if(object.has("email"))
                                        mFacebookEmail = object.getString("email");
                                    if(object.has("gender"))
                                        mFacebookGender = object.getString("gender");
                                    if(object.has("birthday"))
                                        mFacebookBirthday = object.getString("birthday");

                                    if(mPrgrsbrMain.getVisibility() != View.VISIBLE) {
                                        if (GeneralFunctions.isNetConnected(ActvtySignin.this))
                                            callCheckIfFacebookIdExistAPI();
                                        else Snackbar.make(mCrdntrlyot, mNoInternetConnMsg, Snackbar.LENGTH_LONG).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    LogHelper.printLog(AppConstants.LOGTYPE_ERROR, TAG, e.getMessage());
                                    Snackbar.make(mCrdntrlyot, mInternalPrblmMsg, Snackbar.LENGTH_LONG).show();
                                }
                            }
                        });

                    Bundle parameters = new Bundle();
                    parameters.putString("fields", "id,name,email,gender,birthday");
                    request.setParameters(parameters);
                    request.executeAsync();
                }

                @Override
                public void onCancel() {
                }

                @Override
                public void onError(FacebookException exception) {
                }
            });
    }

    // call API for Check FacebookId is exist in server
    private void callCheckIfFacebookIdExistAPI() {

        mPrgrsbrMain.setVisibility(View.VISIBLE);

        APIUser objApiUser = APIRetroBuilder.getRetroBuilder(true).create(APIUser.class);
        Call<ResponseBody> callFacebookIdExistRes = objApiUser.callCheckIfFacebookIdExist(new MdlCheckIfFacebookIdExistReq(mFacebookId));
        callFacebookIdExistRes.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                mPrgrsbrMain.setVisibility(View.GONE);
                if(response.isSuccessful()) {

                    try {

                        JSONObject jsnObjResponse = new JSONObject(response.body().string());
                        boolean isSuccess = jsnObjResponse.getBoolean(KEY_SUCCESS_SML);

                        if(isSuccess)
                            storeUserDataToLocal(jsnObjResponse);
                        else
                            startSignupActivty(true);
                    } catch (Exception e) {

                        e.printStackTrace();
                        LogHelper.printLog(AppConstants.LOGTYPE_ERROR, TAG, e.getMessage());
                        Snackbar.make(mCrdntrlyot, mInternalPrblmMsg, Snackbar.LENGTH_LONG).show();
                    }
                } else
                    Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                mPrgrsbrMain.setVisibility(View.GONE);
                Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    private void startSignupActivty(boolean isFacebookSignin) {

        Intent intntSignup = new Intent(this, ActvtySignup.class);
        intntSignup.putExtra(PUT_EXTRA_ISFACEBOOKSIGNIN, isFacebookSignin);
        intntSignup.putExtra(PUT_EXTRA_FACEBOOK_ID, mFacebookId);
        intntSignup.putExtra(PUT_EXTRA_FACEBOOK_NAME, mFacebookName);
        intntSignup.putExtra(PUT_EXTRA_FACEBOOK_EMAIL, mFacebookEmail);
        intntSignup.putExtra(PUT_EXTRA_FACEBOOK_GENDER, mFacebookGender);
        intntSignup.putExtra(PUT_EXTRA_FACEBOOK_BIRTHDAY, mFacebookBirthday);
        startActivityForResult(intntSignup, REQUEST_SIGNUP);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REQUEST_SIGNUP) {
            if(resultCode == RESULT_OK) {
                if(data.getBooleanExtra(PUT_EXTRA_ISFACEBOOKSIGNIN, false)){

                    setResult(RESULT_OK);
                    finish();
                }
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
