package com.dimtutac.mobileapp.navgtndrawer;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.dimtutac.mobileapp.ActvtySignin;
import com.dimtutac.mobileapp.R;
import com.dimtutac.mobileapp.adapter.RcyclrNavgtnDrwrAdapter;
import com.dimtutac.mobileapp.common.AppConstants;
import com.dimtutac.mobileapp.common.ItemClickListener;
import com.dimtutac.mobileapp.common.UserSessionManager;
import com.dimtutac.mobileapp.models.MdlNavgtnDrwr;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Jalotsav on 22/8/17.
 */

public class NavgtnDrwrMain extends AppCompatActivity {

    private static final String TAG = NavgtnDrwrMain.class.getSimpleName();

    UserSessionManager session;

    @BindView(R.id.toolbar_drwrlyot_appbar_main) Toolbar mToolbar;
    @BindView(R.id.vw_drwrlyot_appbar_toolbarview) View mVwToolbarView;
    @BindView(R.id.drwrlyot_nvgtndrwr_main) DrawerLayout mDrwrlyot;
    @BindView(R.id.rcyclrvw_nvgtndrwr_main)
    RecyclerView mRecyclerView;

    ArrayList<MdlNavgtnDrwr> mArrylstMdlNavgtnDrwr;
    RcyclrNavgtnDrwrAdapter mAdapter;
    RecyclerView.LayoutManager mLayoutManager;
    boolean mIsHome = true;
    Bundle mBundle;
    int currentNavgtnSelectedPosition;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lo_actvty_nvgtndrwr_main);
        ButterKnife.bind(this);

        setSupportActionBar(mToolbar);

        session = new UserSessionManager(this);

        setTitle("");

        // for solve issue when Set app:elevation = 0dp to AppBarLayout --> hamburgermenu not showing to toolbar
        findViewById(R.id.appbarlyot_drwrlyot_main).bringToFront();

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrwrlyot, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrwrlyot.addDrawerListener(toggle);
        toggle.syncState();

        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mArrylstMdlNavgtnDrwr = new ArrayList<>();
        setDrawerMenuItemsData();

        mAdapter = new RcyclrNavgtnDrwrAdapter(this, mArrylstMdlNavgtnDrwr, new ItemClickListener() {
            @Override
            public void onClick(View view, int position) {

                currentNavgtnSelectedPosition = position;
                displayFragmntView(position);
            }
        });
        mRecyclerView.setAdapter(mAdapter);

        setHeaderName();

        currentNavgtnSelectedPosition = getIntent().getIntExtra(AppConstants.PUT_EXTRA_NAVDRWER_POSTN, AppConstants.NAVDRWER_HOME);
        displayFragmntView(currentNavgtnSelectedPosition);
    }

    private void setDrawerMenuItemsData() {

        mArrylstMdlNavgtnDrwr.add(new MdlNavgtnDrwr(getString(R.string.what_do_we_have_quemark), R.mipmap.ic_home_white)); // INVISIBLE in Drawer
        mArrylstMdlNavgtnDrwr.add(new MdlNavgtnDrwr(getString(R.string.what_do_we_have_quemark), R.mipmap.ic_home_white)); // INVISIBLE in Drawer
        mArrylstMdlNavgtnDrwr.add(new MdlNavgtnDrwr(getString(R.string.home_caps), R.mipmap.ic_home_white));
        mArrylstMdlNavgtnDrwr.add(new MdlNavgtnDrwr(getString(R.string.account_caps), R.mipmap.ic_account_white));
        mArrylstMdlNavgtnDrwr.add(new MdlNavgtnDrwr(getString(R.string.locations_caps), R.mipmap.ic_locations_white));
        mArrylstMdlNavgtnDrwr.add(new MdlNavgtnDrwr(getString(R.string.new_monthly_items_caps), R.mipmap.ic_new_monthly_item_white));
        mArrylstMdlNavgtnDrwr.add(new MdlNavgtnDrwr(getString(R.string.menu_caps), R.mipmap.ic_menu_white));
        mArrylstMdlNavgtnDrwr.add(new MdlNavgtnDrwr(getString(R.string.more_sml), R.mipmap.ic_home_white)); // INVISIBLE in Drawer
        mArrylstMdlNavgtnDrwr.add(new MdlNavgtnDrwr(getString(R.string.news_caps), R.mipmap.ic_news_sml));
        mArrylstMdlNavgtnDrwr.add(new MdlNavgtnDrwr(getString(R.string.reservation_caps), R.mipmap.ic_reservation_white));
        mArrylstMdlNavgtnDrwr.add(new MdlNavgtnDrwr(getString(R.string.checkin_caps), R.mipmap.ic_checkin_white));
        mArrylstMdlNavgtnDrwr.add(new MdlNavgtnDrwr(getString(R.string.careers_caps), R.mipmap.ic_careers_white));
    }

    // Set Header Name in NavigationView
    public void setHeaderName() {

        try {
            // Add/Remove LOGOUT menu
            if (session.isUserLoggedIn()) {

                if (mArrylstMdlNavgtnDrwr.size() == 12)
                    mAdapter.addItem(new MdlNavgtnDrwr(getString(R.string.logout_caps), R.mipmap.ic_account_white));
            } else {

                if (mArrylstMdlNavgtnDrwr.size() == 13)
                    mAdapter.removeAt(mArrylstMdlNavgtnDrwr.size());
            }
            mAdapter.notifyDataSetChanged(); // for update HEADER view
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void displayFragmntView(int position) {

        Fragment fragment = null;
        if(mBundle == null) {

            mBundle = new Bundle();
            mBundle.putInt(AppConstants.PUT_EXTRA_COME_FROM, 0);
        }
        switch (position) {
            case 0:
                mIsHome = false;
                break;
            case 1:
                mIsHome = false;
                break;
            case 2:
                fragment = new FrgmntHome();
                mIsHome = true;
                break;
            case 3:
                fragment = new FrgmntAccount();
                mIsHome = false;
                break;
            case 4:
                fragment = new FrgmntLocations();
                mIsHome = false;
                break;
            case 5:
                fragment = new FrgmntNewMonthlyItems();
                mIsHome = false;
                break;
            case 6:
                fragment = new FrgmntMenu();
                mIsHome = false;
                break;
            case 7:
                mIsHome = false;
                break;
            case 8:
                fragment = new FrgmntNews();
                mIsHome = false;
                break;
            case 9:
                fragment = new FrgmntResrvtnLst();
                mIsHome = false;
                break;
            case 10:
                fragment = new FrgmntCheckinList();
                mIsHome = false;
                break;
            case 11:
                fragment = new FrgmntCareer();
                mIsHome = false;
                break;
            case 12:
                confirmLogoutAlertDialog();
                mIsHome = false;
                break;

            default:
                break;
        }

        mDrwrlyot.closeDrawer(GravityCompat.START);

        if (fragment != null) {

            // Menu = HOME --> Actionbar background color = Transparent otherwise ColorPrimary
            try {
                ActionBar mActionBar = getSupportActionBar();
                if(position == AppConstants.NAVDRWER_HOME) {

                    mVwToolbarView.setVisibility(View.GONE);
                    mActionBar.setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(this, R.color.transparent)));
//                mActionBar.setDisplayShowTitleEnabled(false);
                } else {

                    mVwToolbarView.setVisibility(View.VISIBLE);
                    mActionBar.setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(this, R.color.colorPrimary)));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            if(position== AppConstants.NAVDRWER_ACCOUNT && !session.isUserLoggedIn()) {
                startActivityForResult(new Intent(NavgtnDrwrMain.this, ActvtySignin.class), AppConstants.REQUEST_SIGNIN);
            } else {
                fragment.setArguments(mBundle);
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.framlyot_drwrlyot_contnt_container, fragment).commit();

                setTitle(position != AppConstants.NAVDRWER_HOME ? mArrylstMdlNavgtnDrwr.get(position).getTitle() : "");
            }
        } else {
            Log.e(TAG, "Error in creating fragment"); // error in creating fragment
        }
    }

    // Show AlertDialog for confirm to Logout
    private void confirmLogoutAlertDialog() {

        AlertDialog.Builder alrtDlg = new AlertDialog.Builder(this);
        alrtDlg.setTitle(getString(R.string.logout_caps));
        alrtDlg.setMessage(getString(R.string.logout_alrtdlg_msg));
        alrtDlg.setNegativeButton(getString(R.string.no_sml).toUpperCase(), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        alrtDlg.setPositiveButton(getString(R.string.logout_caps).toUpperCase(), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                dialogInterface.dismiss();
                session.logoutUser(true); // Only Clear session data
                setHeaderName();
                currentNavgtnSelectedPosition = AppConstants.NAVDRWER_HOME;
                displayFragmntView(currentNavgtnSelectedPosition);
            }
        });

        alrtDlg.show();
    }

    @Override
    public void setTitle(CharSequence title) {

        try {

            if(currentNavgtnSelectedPosition == AppConstants.NAVDRWER_HOME)
                getSupportActionBar().setDisplayShowTitleEnabled(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        mToolbar.setTitle(title);
    }

    // For Handle onActivityResult and send that to respective Fragment
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.framlyot_drwrlyot_contnt_container);
        fragment.onActivityResult(requestCode, resultCode, data);

        if(resultCode == Activity.RESULT_OK) {
            if(requestCode == AppConstants.REQUEST_SIGNIN) {

                if(currentNavgtnSelectedPosition == AppConstants.NAVDRWER_ACCOUNT) {
                    setHeaderName();
                    currentNavgtnSelectedPosition = AppConstants.NAVDRWER_HOME;
                    displayFragmntView(currentNavgtnSelectedPosition);
                }
            }
        }
    }

    @Override
    public void onBackPressed() {

        if (mDrwrlyot.isDrawerOpen(GravityCompat.START))
            mDrwrlyot.closeDrawer(GravityCompat.START);
        else if(!mIsHome) {

            currentNavgtnSelectedPosition = AppConstants.NAVDRWER_HOME;
            displayFragmntView(currentNavgtnSelectedPosition);
            mAdapter.notifyDataSetChanged();
        } else
            super.onBackPressed();
    }
}
