package com.dimtutac.mobileapp.navgtndrawer;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.dimtutac.mobileapp.ActvtySignin;
import com.dimtutac.mobileapp.R;
import com.dimtutac.mobileapp.adapter.RcyclrPositionsAdapter;
import com.dimtutac.mobileapp.common.AppConstants;
import com.dimtutac.mobileapp.common.GeneralFunctions;
import com.dimtutac.mobileapp.common.LogHelper;
import com.dimtutac.mobileapp.common.RecyclerViewEmptySupport;
import com.dimtutac.mobileapp.common.UserSessionManager;
import com.dimtutac.mobileapp.models.career.MdlPositionListRes;
import com.dimtutac.mobileapp.models.career.MdlPositionListResData;
import com.dimtutac.mobileapp.retrofitapi.APICareer;
import com.dimtutac.mobileapp.retrofitapi.APIRetroBuilder;

import java.util.ArrayList;

import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Jalotsav on 10/18/2017.
 */

public class FrgmntCareer extends Fragment {

    private static final String TAG = FrgmntCareer.class.getSimpleName();

    UserSessionManager session;

    @BindView(R.id.cordntrlyot_frgmnt_career) CoordinatorLayout mCrdntrlyot;
    @BindView(R.id.lnrlyot_recyclremptyvw_appearhere) LinearLayout mLnrlyotAppearHere;
    @BindView(R.id.tv_recyclremptyvw_appearhere) TextView mTvAppearHere;
    @BindView(R.id.rcyclrvw_frgmnt_career) RecyclerViewEmptySupport mRecyclerView;
    @BindView(R.id.prgrsbr_frgmnt_career) ProgressBar mPrgrsbrMain;

    @BindString(R.string.no_intrnt_cnctn) String mNoInternetConnMsg;
    @BindString(R.string.server_problem_sml) String mServerPrblmMsg;
    @BindString(R.string.internal_problem_sml) String mInternalPrblmMsg;
    @BindString(R.string.positionlist_appear_here) String mLocationslstAppearHere;

    @BindDrawable(R.mipmap.ic_logo_black) Drawable mDrwblDefault;

    RecyclerView.LayoutManager mLayoutManager;
    RcyclrPositionsAdapter mAdapter;
    ArrayList<MdlPositionListResData> mArrylstMdlPositions;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.lo_frgmnt_career, container, false);
        ButterKnife.bind(this, rootView);

        session = new UserSessionManager(getActivity());

        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setEmptyView(mLnrlyotAppearHere);

        mTvAppearHere.setText(mLocationslstAppearHere);

        mArrylstMdlPositions = new ArrayList<>();
        mAdapter = new RcyclrPositionsAdapter(getActivity(), mArrylstMdlPositions, mDrwblDefault);
        mRecyclerView.setAdapter(mAdapter);

        if (!session.isUserLoggedIn()) {

            startActivityForResult(new Intent(getActivity(), ActvtySignin.class), AppConstants.REQUEST_SIGNIN);
        }

        return rootView;
    }

    private void getPositionList() {

        if (GeneralFunctions.isNetConnected(getActivity()))
            callPositionListAPI();
        else Snackbar.make(mCrdntrlyot, mNoInternetConnMsg, Snackbar.LENGTH_LONG).show();
    }

    // call API for Get Location List
    private void callPositionListAPI() {

        mPrgrsbrMain.setVisibility(View.VISIBLE);

        APICareer objApiCareer = APIRetroBuilder.getRetroBuilder(true).create(APICareer.class);
        Call<MdlPositionListRes> callMdlPostnRes = objApiCareer.callGetPositionList();
        callMdlPostnRes.enqueue(new Callback<MdlPositionListRes>() {
            @Override
            public void onResponse(Call<MdlPositionListRes> call, Response<MdlPositionListRes> response) {

                mPrgrsbrMain.setVisibility(View.GONE);
                if(response.isSuccessful()) {

                    try {
                        if(response.body().isSuccess()) {

                            ArrayList<MdlPositionListResData> arrylstMdlLocationListData = response.body().getArrylstMdlPositionListData();
                            mArrylstMdlPositions.addAll(arrylstMdlLocationListData);
                            mAdapter.notifyDataSetChanged();
                        } else
                            Snackbar.make(mCrdntrlyot, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                    } catch (Exception e) {

                        e.printStackTrace();
                        LogHelper.printLog(AppConstants.LOGTYPE_ERROR, TAG, e.getMessage());
                        Snackbar.make(mCrdntrlyot, mInternalPrblmMsg, Snackbar.LENGTH_LONG).show();
                    }
                } else
                    Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<MdlPositionListRes> call, Throwable t) {

                mPrgrsbrMain.setVisibility(View.GONE);
                Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if (resultCode == Activity.RESULT_OK) {
                if (requestCode == AppConstants.REQUEST_SIGNIN) {

                    ((NavgtnDrwrMain) getActivity()).setHeaderName();
                }
            } else
                ((NavgtnDrwrMain) getActivity()).displayFragmntView(AppConstants.NAVDRWER_HOME);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (session.isUserLoggedIn()) {

            mArrylstMdlPositions.clear();
            getPositionList();
        }
    }
}
