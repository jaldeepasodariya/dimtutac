package com.dimtutac.mobileapp.navgtndrawer;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.dimtutac.mobileapp.ActvtySignin;
import com.dimtutac.mobileapp.R;
import com.dimtutac.mobileapp.adapter.RcyclrCheckinListAdapter;
import com.dimtutac.mobileapp.common.AppConstants;
import com.dimtutac.mobileapp.common.GeneralFunctions;
import com.dimtutac.mobileapp.common.LogHelper;
import com.dimtutac.mobileapp.common.RecyclerViewEmptySupport;
import com.dimtutac.mobileapp.common.UserSessionManager;
import com.dimtutac.mobileapp.models.checkin.MdlCheckinListRes;
import com.dimtutac.mobileapp.models.checkin.MdlCheckinListResData;
import com.dimtutac.mobileapp.retrofitapi.APICheckin;
import com.dimtutac.mobileapp.retrofitapi.APIRetroBuilder;

import java.util.ArrayList;

import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Jalotsav on 1/25/2018.
 */

public class FrgmntCheckinList extends Fragment {

    private static final String TAG = FrgmntCheckinList.class.getSimpleName();

    UserSessionManager session;

    @BindView(R.id.cordntrlyot_frgmnt_checkinlst) CoordinatorLayout mCrdntrlyot;
    @BindView(R.id.lnrlyot_recyclremptyvw_appearhere) LinearLayout mLnrlyotAppearHere;
    @BindView(R.id.tv_recyclremptyvw_appearhere) TextView mTvAppearHere;
    @BindView(R.id.rcyclrvw_frgmnt_checkinlst) RecyclerViewEmptySupport mRecyclerView;
    @BindView(R.id.prgrsbr_frgmnt_checkinlst) ProgressBar mPrgrsbrMain;

    @BindString(R.string.no_intrnt_cnctn) String mNoInternetConnMsg;
    @BindString(R.string.server_problem_sml) String mServerPrblmMsg;
    @BindString(R.string.internal_problem_sml) String mInternalPrblmMsg;
    @BindString(R.string.checkinlist_appear_here) String mCheckinAppearHere;

    @BindDrawable(R.mipmap.ic_logo_black) Drawable mDrwblDefault;

    RecyclerView.LayoutManager mLayoutManager;
    RcyclrCheckinListAdapter mAdapter;
    ArrayList<MdlCheckinListResData> mArrylstMdlCheckinlst;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.lo_frgmnt_checkinlst, container, false);
        ButterKnife.bind(this, rootView);

        session = new UserSessionManager(getActivity());

        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setEmptyView(mLnrlyotAppearHere);

        mTvAppearHere.setText(mCheckinAppearHere);

        mArrylstMdlCheckinlst = new ArrayList<>();
        mAdapter = new RcyclrCheckinListAdapter(getActivity(), mArrylstMdlCheckinlst, mDrwblDefault);
        mRecyclerView.setAdapter(mAdapter);

        if (!session.isUserLoggedIn()) {

            startActivityForResult(new Intent(getActivity(), ActvtySignin.class), AppConstants.REQUEST_SIGNIN);
        }

        return rootView;
    }

    private void getCheckinList() {

        if (GeneralFunctions.isNetConnected(getActivity()))
            callCheckinListAPI();
        else Snackbar.make(mCrdntrlyot, mNoInternetConnMsg, Snackbar.LENGTH_LONG).show();
    }

    // Call Reservation list Get API
    private void callCheckinListAPI() {

        mPrgrsbrMain.setVisibility(View.VISIBLE);

        APICheckin objApiCheckin = APIRetroBuilder.getRetroBuilder(true).create(APICheckin.class);
        Call<MdlCheckinListRes> callMdlCheckinListRes = objApiCheckin.callGetCheckinList(session.getUserId());
        callMdlCheckinListRes.enqueue(new Callback<MdlCheckinListRes>() {
            @Override
            public void onResponse(Call<MdlCheckinListRes> call, Response<MdlCheckinListRes> response) {

                mPrgrsbrMain.setVisibility(View.GONE);
                if (response.isSuccessful()) {

                    try {
                        if (response.body().isSuccess()) {

                            ArrayList<MdlCheckinListResData> arrylstMdlCheckinListData = response.body().getArrylstMdlCheckinListData();
                            if (arrylstMdlCheckinListData.size() > 0) {
                                mArrylstMdlCheckinlst.addAll(arrylstMdlCheckinListData);
                                mAdapter.notifyDataSetChanged();
                            }
                        } else
                            Snackbar.make(mCrdntrlyot, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                    } catch (Exception e) {

                        e.printStackTrace();
                        LogHelper.printLog(AppConstants.LOGTYPE_ERROR, TAG, e.getMessage());
                        Snackbar.make(mCrdntrlyot, mInternalPrblmMsg, Snackbar.LENGTH_LONG).show();
                    }
                } else
                    Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<MdlCheckinListRes> call, Throwable t) {

                mPrgrsbrMain.setVisibility(View.GONE);
                Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if (resultCode == Activity.RESULT_OK) {
                if (requestCode == AppConstants.REQUEST_SIGNIN) {

                    ((NavgtnDrwrMain) getActivity()).setHeaderName();
                }
            } else
                ((NavgtnDrwrMain) getActivity()).displayFragmntView(AppConstants.NAVDRWER_HOME);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (session.isUserLoggedIn()) {

            mArrylstMdlCheckinlst.clear();
            getCheckinList();
        }
    }
}
