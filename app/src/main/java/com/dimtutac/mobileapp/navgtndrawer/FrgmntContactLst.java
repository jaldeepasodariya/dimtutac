package com.dimtutac.mobileapp.navgtndrawer;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.dimtutac.mobileapp.R;
import com.dimtutac.mobileapp.adapter.RcyclrContactListAdapter;
import com.dimtutac.mobileapp.common.AppConstants;
import com.dimtutac.mobileapp.common.GeneralFunctions;
import com.dimtutac.mobileapp.common.LogHelper;
import com.dimtutac.mobileapp.common.RecyclerViewEmptySupport;
import com.dimtutac.mobileapp.models.branch.MdlLocationListRes;
import com.dimtutac.mobileapp.models.branch.MdlLocationListResData;
import com.dimtutac.mobileapp.models.branch.MdlLocations;
import com.dimtutac.mobileapp.retrofitapi.APIBranch;
import com.dimtutac.mobileapp.retrofitapi.APIRetroBuilder;

import java.util.ArrayList;

import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Jalotsav on 10/14/2017.
 */

public class FrgmntContactLst extends Fragment {

    private static final String TAG = FrgmntContactLst.class.getSimpleName();

    @BindView(R.id.cordntrlyot_frgmnt_contact) CoordinatorLayout mCrdntrlyot;
    @BindView(R.id.lnrlyot_recyclremptyvw_appearhere) LinearLayout mLnrlyotAppearHere;
    @BindView(R.id.tv_recyclremptyvw_appearhere) TextView mTvAppearHere;
    @BindView(R.id.rcyclrvw_frgmnt_contact) RecyclerViewEmptySupport mRecyclerView;
    @BindView(R.id.prgrsbr_frgmnt_contact) ProgressBar mPrgrsbrMain;

    @BindString(R.string.no_intrnt_cnctn) String mNoInternetConnMsg;
    @BindString(R.string.server_problem_sml) String mServerPrblmMsg;
    @BindString(R.string.internal_problem_sml) String mInternalPrblmMsg;
    @BindString(R.string.contactlist_appear_here) String mContactlstAppearHere;

    @BindDrawable(R.mipmap.ic_logo_black) Drawable mDrwblDefault;

    RecyclerView.LayoutManager mLayoutManager;
    RcyclrContactListAdapter mAdapter;
    ArrayList<MdlLocations> mArrylstMdlLocations;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.lo_frgmnt_contactlst, container, false);
        ButterKnife.bind(this, rootView);

        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setEmptyView(mLnrlyotAppearHere);

        mTvAppearHere.setText(mContactlstAppearHere);

        mArrylstMdlLocations = new ArrayList<>();
        mAdapter = new RcyclrContactListAdapter(getActivity(), mArrylstMdlLocations, mDrwblDefault);
        mRecyclerView.setAdapter(mAdapter);

        getContactList();

        return rootView;
    }

    private void getContactList() {

        if (GeneralFunctions.isNetConnected(getActivity()))
            callContactListAPI();
        else Snackbar.make(mCrdntrlyot, mNoInternetConnMsg, Snackbar.LENGTH_LONG).show();
    }

    // call API for Get Contact List
    private void callContactListAPI() {

        mPrgrsbrMain.setVisibility(View.VISIBLE);

        APIBranch objApiBranch = APIRetroBuilder.getRetroBuilder(true).create(APIBranch.class);
        Call<MdlLocationListRes> callMdlLoginRes = objApiBranch.callGetLocationList();
        callMdlLoginRes.enqueue(new Callback<MdlLocationListRes>() {
            @Override
            public void onResponse(Call<MdlLocationListRes> call, Response<MdlLocationListRes> response) {

                mPrgrsbrMain.setVisibility(View.GONE);
                if(response.isSuccessful()) {

                    try {
                        if(response.body().isSuccess()) {

                            ArrayList<MdlLocationListResData> arrylstMdlLocationListData = response.body().getArrylstMdlLocationListData();
                            MdlLocations objMdlLocations;
                            for(MdlLocationListResData objLoctnlstData: arrylstMdlLocationListData) {

                                String storeHrs = "";
                                try {
                                    // Generate TermsAndCondition list String
                                    StringBuilder stringBusinsHrsLst = new StringBuilder();
                                    if(objLoctnlstData.getArrylstMdlLoctnLstBusineshrs() != null) {
                                        for (String str : objLoctnlstData.getStrArryBusineshrs()) {

                                            if (str.length() > 1) // to avoid "." at last element
                                                stringBusinsHrsLst.append(str).append("\n");
                                        }
                                    }
                                    storeHrs = stringBusinsHrsLst.toString();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                objMdlLocations = new MdlLocations(objLoctnlstData.getPkId(), objLoctnlstData.getRestaurantId(), objLoctnlstData.getBranchName(),
                                        objLoctnlstData.getAddressLine1(), objLoctnlstData.getMobileNo(), AppConstants.ATCHMNT_ROOT_URL.concat(objLoctnlstData.getPhotoURL()),
                                        storeHrs);

                                mArrylstMdlLocations.add(objMdlLocations);
                            }

                            mAdapter.notifyDataSetChanged();
                        } else
                            Snackbar.make(mCrdntrlyot, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                    } catch (Exception e) {

                        e.printStackTrace();
                        LogHelper.printLog(AppConstants.LOGTYPE_ERROR, TAG, e.getMessage());
                        Snackbar.make(mCrdntrlyot, mInternalPrblmMsg, Snackbar.LENGTH_LONG).show();
                    }
                } else
                    Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<MdlLocationListRes> call, Throwable t) {

                mPrgrsbrMain.setVisibility(View.GONE);
                Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if (resultCode == Activity.RESULT_OK) {
                if (requestCode == AppConstants.REQUEST_SIGNIN) {

                    ((NavgtnDrwrMain) getActivity()).setHeaderName();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
