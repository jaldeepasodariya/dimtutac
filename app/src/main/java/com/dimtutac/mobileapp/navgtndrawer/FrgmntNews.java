package com.dimtutac.mobileapp.navgtndrawer;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.dimtutac.mobileapp.R;
import com.dimtutac.mobileapp.adapter.RcyclrNewsAdapter;
import com.dimtutac.mobileapp.common.AppConstants;
import com.dimtutac.mobileapp.common.GeneralFunctions;
import com.dimtutac.mobileapp.common.LogHelper;
import com.dimtutac.mobileapp.common.RecyclerViewEmptySupport;
import com.dimtutac.mobileapp.models.news.MdlNewsListRes;
import com.dimtutac.mobileapp.models.news.MdlNewsListResData;
import com.dimtutac.mobileapp.retrofitapi.APINews;
import com.dimtutac.mobileapp.retrofitapi.APIRetroBuilder;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Jalotsav on 12/8/2017.
 */

public class FrgmntNews extends Fragment {

    private static final String TAG = FrgmntNews.class.getSimpleName();

    @BindView(R.id.cordntrlyot_frgmnt_news)CoordinatorLayout mCrdntrlyot;
    @BindView(R.id.lnrlyot_recyclremptyvw_appearhere)LinearLayout mLnrlyotAppearHere;
    @BindView(R.id.tv_recyclremptyvw_appearhere)TextView mTvAppearHere;
    @BindView(R.id.rcyclrvw_frgmnt_news) RecyclerViewEmptySupport mRecyclerView;
    @BindView(R.id.prgrsbr_frgmnt_news) ProgressBar mPrgrsbrMain;

    @BindString(R.string.no_intrnt_cnctn) String mNoInternetConnMsg;
    @BindString(R.string.server_problem_sml) String mServerPrblmMsg;
    @BindString(R.string.internal_problem_sml) String mInternalPrblmMsg;
    @BindString(R.string.newslist_appear_here) String mNewslstAppearHere;
    @BindString(R.string.allow_permtn_sharenews) String mAllowPermsnMsg;

    @BindDrawable(R.mipmap.ic_logo_black) Drawable mDrwblDefault;

    RecyclerView.LayoutManager mLayoutManager;
    RcyclrNewsAdapter mAdapter;
    ArrayList<MdlNewsListResData> mArrylstMdlNewslst;
    MdlNewsListResData mObjMdlNewsSelected;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.lo_frgmnt_news, container, false);
        ButterKnife.bind(this, rootView);

        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setEmptyView(mLnrlyotAppearHere);

        mTvAppearHere.setText(mNewslstAppearHere);

        mArrylstMdlNewslst = new ArrayList<>();
        mAdapter = new RcyclrNewsAdapter(getActivity(), FrgmntNews.this, mArrylstMdlNewslst, mDrwblDefault);
        mRecyclerView.setAdapter(mAdapter);

        getNewsList();

        return rootView;
    }

    private void getNewsList() {

        if (GeneralFunctions.isNetConnected(getActivity()))
            callNewsListAPI();
        else Snackbar.make(mCrdntrlyot, mNoInternetConnMsg, Snackbar.LENGTH_LONG).show();
    }

    // call API for Get News List
    private void callNewsListAPI() {

        mPrgrsbrMain.setVisibility(View.VISIBLE);

        APINews objApiNews = APIRetroBuilder.getRetroBuilder(true).create(APINews.class);
        Call<MdlNewsListRes> callMdlNewsRes = objApiNews.callGetNews();
        callMdlNewsRes.enqueue(new Callback<MdlNewsListRes>() {
            @Override
            public void onResponse(Call<MdlNewsListRes> call, Response<MdlNewsListRes> response) {

                mPrgrsbrMain.setVisibility(View.GONE);
                if(response.isSuccessful()) {

                    try {
                        if(response.body().isSuccess()) {

                            ArrayList<MdlNewsListResData> arrylstMdlMenuListData = response.body().getArrylstMdlNewsListResData();
                            if (arrylstMdlMenuListData.size() > 0) {
                                mArrylstMdlNewslst.addAll(arrylstMdlMenuListData);
                                mAdapter.notifyDataSetChanged();
                            }
                        } else
                            Snackbar.make(mCrdntrlyot, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                    } catch (Exception e) {

                        e.printStackTrace();
                        LogHelper.printLog(AppConstants.LOGTYPE_ERROR, TAG, e.getMessage());
                        Snackbar.make(mCrdntrlyot, mInternalPrblmMsg, Snackbar.LENGTH_LONG).show();
                    }
                } else
                    Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<MdlNewsListRes> call, Throwable t) {

                mPrgrsbrMain.setVisibility(View.GONE);
                Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    // Check Storage permission for use
    public void checkAppPermission(MdlNewsListResData objMdlNews) {

        mObjMdlNewsSelected = objMdlNews;
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (!isCheckSelfPermission())
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, AppConstants.REQUEST_APP_PERMISSION_CAMERASTORAGE);
            else {
                shareNews();
            }
        } else {
            shareNews();
        }
    }

    private boolean isCheckSelfPermission(){

        int selfPermsnStorage = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return  selfPermsnStorage == PackageManager.PERMISSION_GRANTED;
    }

    private void shareNews() {

        try {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            mObjMdlNewsSelected.getPhotoBitmap().compress(Bitmap.CompressFormat.JPEG, 100, bytes);
            String storedImagePath = MediaStore.Images.Media.insertImage(getActivity().getContentResolver(),
                    mObjMdlNewsSelected.getPhotoBitmap(), mObjMdlNewsSelected.getTitle(), null);

            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.setType("image/jpeg");
            shareIntent.putExtra(Intent.EXTRA_TEXT, mObjMdlNewsSelected.getTitle());
            shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(storedImagePath));
            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivity(Intent.createChooser(shareIntent, getString(R.string.share_appname_news_3dots, getString(R.string.app_name))));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if(requestCode == AppConstants.REQUEST_APP_PERMISSION_CAMERASTORAGE) {

            if(grantResults.length > 0) {

                if(grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    shareNews();
                } else
                    Snackbar.make(mCrdntrlyot, mAllowPermsnMsg, Snackbar.LENGTH_LONG).show();
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
