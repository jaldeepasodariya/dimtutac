package com.dimtutac.mobileapp.navgtndrawer;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.dimtutac.mobileapp.R;
import com.dimtutac.mobileapp.adapter.RcyclrMenuAdapter;
import com.dimtutac.mobileapp.common.AppConstants;
import com.dimtutac.mobileapp.common.GeneralFunctions;
import com.dimtutac.mobileapp.common.LogHelper;
import com.dimtutac.mobileapp.common.RecyclerViewEmptySupport;
import com.dimtutac.mobileapp.models.menu.MdlMenuListRes;
import com.dimtutac.mobileapp.models.menu.MdlMenuListResData;
import com.dimtutac.mobileapp.retrofitapi.APIMenu;
import com.dimtutac.mobileapp.retrofitapi.APIRetroBuilder;

import java.util.ArrayList;

import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Jalotsav on 20/11/17.
 */

public class FrgmntMenu extends Fragment {

    private static final String TAG = FrgmntMenu.class.getSimpleName();

    @BindView(R.id.cordntrlyot_frgmnt_menu)CoordinatorLayout mCrdntrlyot;
    @BindView(R.id.lnrlyot_recyclremptyvw_appearhere)LinearLayout mLnrlyotAppearHere;
    @BindView(R.id.tv_recyclremptyvw_appearhere)TextView mTvAppearHere;
    @BindView(R.id.rcyclrvw_frgmnt_menu) RecyclerViewEmptySupport mRecyclerView;
    @BindView(R.id.prgrsbr_frgmnt_menu) ProgressBar mPrgrsbrMain;

    @BindString(R.string.no_intrnt_cnctn) String mNoInternetConnMsg;
    @BindString(R.string.server_problem_sml) String mServerPrblmMsg;
    @BindString(R.string.internal_problem_sml) String mInternalPrblmMsg;
    @BindString(R.string.menulist_appear_here) String mMenulstAppearHere;

    @BindDrawable(R.mipmap.ic_logo_black) Drawable mDrwblDefault;

    RecyclerView.LayoutManager mLayoutManager;
    RcyclrMenuAdapter mAdapter;
    ArrayList<MdlMenuListResData> mArrylstMdlMenulst;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.lo_frgmnt_menu, container, false);
        ButterKnife.bind(this, rootView);

        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setEmptyView(mLnrlyotAppearHere);

        mTvAppearHere.setText(mMenulstAppearHere);

        mArrylstMdlMenulst = new ArrayList<>();
        mAdapter = new RcyclrMenuAdapter(getActivity(), mArrylstMdlMenulst, mDrwblDefault);
        mRecyclerView.setAdapter(mAdapter);

        getMenuList();

        return rootView;
    }

    private void getMenuList() {

        if (GeneralFunctions.isNetConnected(getActivity()))
            callMenuListAPI();
        else Snackbar.make(mCrdntrlyot, mNoInternetConnMsg, Snackbar.LENGTH_LONG).show();
    }

    // call API for Get Menu List
    private void callMenuListAPI() {

        mPrgrsbrMain.setVisibility(View.VISIBLE);

        APIMenu objApiMenu = APIRetroBuilder.getRetroBuilder(true).create(APIMenu.class);
        Call<MdlMenuListRes> callMdlMenuRes = objApiMenu.callGetMenu();
        callMdlMenuRes.enqueue(new Callback<MdlMenuListRes>() {
            @Override
            public void onResponse(Call<MdlMenuListRes> call, Response<MdlMenuListRes> response) {

                mPrgrsbrMain.setVisibility(View.GONE);
                if(response.isSuccessful()) {

                    try {
                        if(response.body().isSuccess()) {

                            ArrayList<MdlMenuListResData> arrylstMdlMenuListData = response.body().getArrylstMdlMenuListResData();
                            if (arrylstMdlMenuListData.size() > 0) {
                                mArrylstMdlMenulst.addAll(arrylstMdlMenuListData);
                                mAdapter.notifyDataSetChanged();
                            }
                        } else
                            Snackbar.make(mCrdntrlyot, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                    } catch (Exception e) {

                        e.printStackTrace();
                        LogHelper.printLog(AppConstants.LOGTYPE_ERROR, TAG, e.getMessage());
                        Snackbar.make(mCrdntrlyot, mInternalPrblmMsg, Snackbar.LENGTH_LONG).show();
                    }
                } else
                    Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<MdlMenuListRes> call, Throwable t) {

                mPrgrsbrMain.setVisibility(View.GONE);
                Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_SHORT).show();
            }
        });
    }
}
