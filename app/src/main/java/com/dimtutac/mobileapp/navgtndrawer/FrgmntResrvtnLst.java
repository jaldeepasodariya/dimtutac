package com.dimtutac.mobileapp.navgtndrawer;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.dimtutac.mobileapp.ActvtyReservation;
import com.dimtutac.mobileapp.ActvtySignin;
import com.dimtutac.mobileapp.R;
import com.dimtutac.mobileapp.adapter.RcyclrResrvtnListAdapter;
import com.dimtutac.mobileapp.common.AppConstants;
import com.dimtutac.mobileapp.common.GeneralFunctions;
import com.dimtutac.mobileapp.common.LogHelper;
import com.dimtutac.mobileapp.common.RecyclerViewEmptySupport;
import com.dimtutac.mobileapp.common.UserSessionManager;
import com.dimtutac.mobileapp.models.reservation.MdlResrvtnListRes;
import com.dimtutac.mobileapp.models.reservation.MdlResrvtnListResData;
import com.dimtutac.mobileapp.models.reservation.MdlResrvtnUpdateRes;
import com.dimtutac.mobileapp.retrofitapi.APIReservation;
import com.dimtutac.mobileapp.retrofitapi.APIRetroBuilder;

import java.util.ArrayList;

import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Jalotsav on 30/8/17.
 */

public class FrgmntResrvtnLst extends Fragment {

    private static final String TAG = FrgmntLocations.class.getSimpleName();

    UserSessionManager session;

    @BindView(R.id.cordntrlyot_frgmnt_resrvtnlst) CoordinatorLayout mCrdntrlyot;
    @BindView(R.id.lnrlyot_recyclremptyvw_appearhere) LinearLayout mLnrlyotAppearHere;
    @BindView(R.id.tv_recyclremptyvw_appearhere) TextView mTvAppearHere;
    @BindView(R.id.rcyclrvw_frgmnt_resrvtnlst) RecyclerViewEmptySupport mRecyclerView;
    @BindView(R.id.prgrsbr_frgmnt_resrvtnlst) ProgressBar mPrgrsbrMain;

    @BindString(R.string.no_intrnt_cnctn) String mNoInternetConnMsg;
    @BindString(R.string.server_problem_sml) String mServerPrblmMsg;
    @BindString(R.string.internal_problem_sml) String mInternalPrblmMsg;
    @BindString(R.string.resrvtnlist_appear_here) String mResrvtnAppearHere;

    @BindDrawable(R.mipmap.ic_logo_black) Drawable mDrwblDefault;

    RecyclerView.LayoutManager mLayoutManager;
    RcyclrResrvtnListAdapter mAdapter;
    ArrayList<MdlResrvtnListResData> mArrylstMdlResrvtnlst;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.lo_frgmnt_reservationlst, container, false);
        ButterKnife.bind(this, rootView);

        session = new UserSessionManager(getActivity());

        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setEmptyView(mLnrlyotAppearHere);

        mTvAppearHere.setText(mResrvtnAppearHere);

        mArrylstMdlResrvtnlst = new ArrayList<>();
        mAdapter = new RcyclrResrvtnListAdapter(getActivity(), mArrylstMdlResrvtnlst, mDrwblDefault, FrgmntResrvtnLst.this);
        mRecyclerView.setAdapter(mAdapter);

        if (!session.isUserLoggedIn()) {

            startActivityForResult(new Intent(getActivity(), ActvtySignin.class), AppConstants.REQUEST_SIGNIN);
        }

        return rootView;
    }

    @OnClick({R.id.appcmptbtn_frgmnt_resrvtnlst_makeanewresrvtn})
    public void onClickView(View view) {

        switch (view.getId()) {
            case R.id.appcmptbtn_frgmnt_resrvtnlst_makeanewresrvtn:
                if (!session.isUserLoggedIn()) {

                    startActivityForResult(new Intent(getActivity(), ActvtySignin.class), AppConstants.REQUEST_SIGNIN);
                } else {
                    startActivity(new Intent(getActivity(), ActvtyReservation.class)
                            .putExtra(AppConstants.PUT_EXTRA_RESTAURANT_ID, "0"));
                }
                break;
        }
    }

    private void getReservationList() {

        if (GeneralFunctions.isNetConnected(getActivity()))
            getResrvtnListAPI();
        else Snackbar.make(mCrdntrlyot, mNoInternetConnMsg, Snackbar.LENGTH_LONG).show();
    }

    // Call Reservation list Get API
    private void getResrvtnListAPI() {

        mPrgrsbrMain.setVisibility(View.VISIBLE);

        APIReservation objApiResrvtn = APIRetroBuilder.getRetroBuilder(true).create(APIReservation.class);
        Call<MdlResrvtnListRes> callMdlResrvtnRes = objApiResrvtn.callGetReservationList(session.getUserId());
        callMdlResrvtnRes.enqueue(new Callback<MdlResrvtnListRes>() {
            @Override
            public void onResponse(Call<MdlResrvtnListRes> call, Response<MdlResrvtnListRes> response) {

                mPrgrsbrMain.setVisibility(View.GONE);
                if (response.isSuccessful()) {

                    try {
                        if (response.body().isSuccess()) {

                            ArrayList<MdlResrvtnListResData> arrylstMdlLocationListData = response.body().getArrylstMdlResrvtnListData();
                            if (arrylstMdlLocationListData.size() > 0) {

                                for(MdlResrvtnListResData objMdlResrvtnlstData : arrylstMdlLocationListData) {

                                    // CANCEL status data not include in to List
                                    if(!objMdlResrvtnlstData.getStatus().equalsIgnoreCase(AppConstants.STATIC_STATUS_ID_RESRVTN_CANCEL)) {
                                        mArrylstMdlResrvtnlst.add(objMdlResrvtnlstData);
                                    }
                                }
                                mAdapter.notifyDataSetChanged();
                            }
                        } else
                            Snackbar.make(mCrdntrlyot, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                    } catch (Exception e) {

                        e.printStackTrace();
                        LogHelper.printLog(AppConstants.LOGTYPE_ERROR, TAG, e.getMessage());
                        Snackbar.make(mCrdntrlyot, mInternalPrblmMsg, Snackbar.LENGTH_LONG).show();
                    }
                } else
                    Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<MdlResrvtnListRes> call, Throwable t) {

                mPrgrsbrMain.setVisibility(View.GONE);
                Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    public void cancelReservation(MdlResrvtnListResData objMdlResrvtnlstData) {

        if (mPrgrsbrMain.getVisibility() != View.VISIBLE) {
            if (GeneralFunctions.isNetConnected(getActivity()))
                callResrvtnCancelAPI(objMdlResrvtnlstData);
            else Snackbar.make(mCrdntrlyot, mNoInternetConnMsg, Snackbar.LENGTH_LONG).show();
        }
    }

    // Call Reservation Cancel API
    private void callResrvtnCancelAPI(MdlResrvtnListResData objMdlResrvtnlstData) {

        mPrgrsbrMain.setVisibility(View.VISIBLE);

        objMdlResrvtnlstData.setStatus(AppConstants.STATIC_STATUS_ID_RESRVTN_CANCEL);

        APIReservation objApiResrvtn = APIRetroBuilder.getRetroBuilder(true).create(APIReservation.class);
        Call<MdlResrvtnUpdateRes> callMdlResrvynUpdateRes = objApiResrvtn.callUpdateReservation(objMdlResrvtnlstData.getPkId(), objMdlResrvtnlstData);
        callMdlResrvynUpdateRes.enqueue(new Callback<MdlResrvtnUpdateRes>() {
            @Override
            public void onResponse(Call<MdlResrvtnUpdateRes> call, Response<MdlResrvtnUpdateRes> response) {

                mPrgrsbrMain.setVisibility(View.GONE);
                if (response.isSuccessful()) {

                    try {
                        if (response.body().isSuccess()) {

                            Toast.makeText(getActivity(), response.body().getData(), Toast.LENGTH_SHORT).show();
                            mArrylstMdlResrvtnlst.clear();
                            getReservationList(); // Refresh data after cancel reservation
                        } else
                            Snackbar.make(mCrdntrlyot, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                    } catch (Exception e) {

                        e.printStackTrace();
                        LogHelper.printLog(AppConstants.LOGTYPE_ERROR, TAG, e.getMessage());
                        Snackbar.make(mCrdntrlyot, mInternalPrblmMsg, Snackbar.LENGTH_LONG).show();
                    }
                } else
                    Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<MdlResrvtnUpdateRes> call, Throwable t) {

                mPrgrsbrMain.setVisibility(View.GONE);
                Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if (resultCode == Activity.RESULT_OK) {
                if (requestCode == AppConstants.REQUEST_SIGNIN) {

                    ((NavgtnDrwrMain) getActivity()).setHeaderName();
                }
            } else
                ((NavgtnDrwrMain) getActivity()).displayFragmntView(AppConstants.NAVDRWER_HOME);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (session.isUserLoggedIn()) {

            mArrylstMdlResrvtnlst.clear();
            getReservationList();
        }
    }
}
