package com.dimtutac.mobileapp.navgtndrawer;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.dimtutac.mobileapp.R;
import com.dimtutac.mobileapp.adapter.SliderViewpagerAdptr;
import com.dimtutac.mobileapp.common.AppConstants;
import com.dimtutac.mobileapp.common.GeneralFunctions;
import com.dimtutac.mobileapp.common.LogHelper;
import com.dimtutac.mobileapp.models.dashboard.MdlDashbrdDataRes;
import com.dimtutac.mobileapp.models.dashboard.MdlDashbrdDataResData;
import com.dimtutac.mobileapp.retrofitapi.APIDashboard;
import com.dimtutac.mobileapp.retrofitapi.APIRetroBuilder;

import java.util.ArrayList;

import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Jalotsav on 22/8/17.
 */

public class FrgmntHome extends Fragment {

    private static final String TAG = FrgmntHome.class.getSimpleName();

    @BindView(R.id.cordntrlyot_frgmnt_home) CoordinatorLayout mCrdntrlyot;
    @BindView(R.id.vwpgr_frgmnt_home) ViewPager mViewPager;
    @BindView(R.id.vwpgr_frgmnt_home_slidrindictr) CircleIndicator mSliderIndictr;
    @BindView(R.id.prgrsbr_frgmnt_home) ProgressBar mPrgrsbrMain;

    @BindString(R.string.no_intrnt_cnctn) String mNoInternetConnMsg;
    @BindString(R.string.server_problem_sml) String mServerPrblmMsg;
    @BindString(R.string.internal_problem_sml) String mInternalPrblmMsg;

    @BindDrawable(R.drawable.img_home_slider1) Drawable mDrwblDefault;

    SliderViewpagerAdptr mSliderAdapter;
    ArrayList<MdlDashbrdDataResData> mArrylstMdlDashbrdResData;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.lo_frgmnt_home, container, false);
        ButterKnife.bind(this, rootView);

        mArrylstMdlDashbrdResData = new ArrayList<>();
        mArrylstMdlDashbrdResData.add(new MdlDashbrdDataResData(null, null, null, null, null, false, false));

        mSliderAdapter = new SliderViewpagerAdptr(getActivity(), mArrylstMdlDashbrdResData, mDrwblDefault);
        mViewPager.setAdapter(mSliderAdapter);
        mSliderIndictr.setViewPager(mViewPager);

        getDashboardData();

        return rootView;
    }

    private void getDashboardData() {

        if (GeneralFunctions.isNetConnected(getActivity()))
            callDashboardDataAPI();
        else Snackbar.make(mCrdntrlyot, mNoInternetConnMsg, Snackbar.LENGTH_LONG).show();
    }

    private void callDashboardDataAPI() {

        mPrgrsbrMain.setVisibility(View.VISIBLE);

        APIDashboard objApiBranch = APIRetroBuilder.getRetroBuilder(true).create(APIDashboard.class);
        Call<MdlDashbrdDataRes> callMdlDashbrdDataRes = objApiBranch.callGetDashboardData();
        callMdlDashbrdDataRes.enqueue(new Callback<MdlDashbrdDataRes>() {
            @Override
            public void onResponse(Call<MdlDashbrdDataRes> call, Response<MdlDashbrdDataRes> response) {

                mPrgrsbrMain.setVisibility(View.GONE);
                if(response.isSuccessful()) {

                    try {
                        if(response.body().isSuccess()) {

                            if(response.body().getArrylstDashbrdDataResData().size() > 0) {
                                mArrylstMdlDashbrdResData = new ArrayList<>();
                                mArrylstMdlDashbrdResData = response.body().getArrylstDashbrdDataResData();

                                mSliderAdapter = new SliderViewpagerAdptr(getActivity(), mArrylstMdlDashbrdResData, mDrwblDefault);
                                mViewPager.setAdapter(mSliderAdapter);
                                mSliderIndictr.setViewPager(mViewPager);
                            }
                        } else
                            Snackbar.make(mCrdntrlyot, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                    } catch (Exception e) {

                        e.printStackTrace();
                        LogHelper.printLog(AppConstants.LOGTYPE_ERROR, TAG, e.getMessage());
                        Snackbar.make(mCrdntrlyot, mInternalPrblmMsg, Snackbar.LENGTH_LONG).show();
                    }
                } else
                    Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<MdlDashbrdDataRes> call, Throwable t) {

                mPrgrsbrMain.setVisibility(View.GONE);
                Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    @OnClick({R.id.tv_frgmnt_home_btmcheckin, R.id.tv_frgmnt_home_btmresrvtn, R.id.tv_frgmnt_home_btmmenu})
    public void onClickView(View view) {

        int navDrwrPostn;
        switch (view.getId()) {
            case R.id.tv_frgmnt_home_btmcheckin:

                navDrwrPostn = AppConstants.NAVDRWER_CHECKIN;
                break;
            case R.id.tv_frgmnt_home_btmresrvtn:

                navDrwrPostn = AppConstants.NAVDRWER_RESERVATION;
                break;
            case R.id.tv_frgmnt_home_btmmenu:

                navDrwrPostn = AppConstants.NAVDRWER_MENU;
                break;
            default:

                navDrwrPostn = AppConstants.NAVDRWER_HOME;
                break;
        }
        try {
            ((NavgtnDrwrMain) getActivity()).displayFragmntView(navDrwrPostn);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
