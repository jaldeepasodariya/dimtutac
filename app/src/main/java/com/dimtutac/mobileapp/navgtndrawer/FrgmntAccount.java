package com.dimtutac.mobileapp.navgtndrawer;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.dimtutac.mobileapp.ActvtyAccountEdit;
import com.dimtutac.mobileapp.ActvtySignin;
import com.dimtutac.mobileapp.R;
import com.dimtutac.mobileapp.adapter.RcyclrMyOffersAdapter;
import com.dimtutac.mobileapp.common.AppConstants;
import com.dimtutac.mobileapp.common.GeneralFunctions;
import com.dimtutac.mobileapp.common.LogHelper;
import com.dimtutac.mobileapp.common.RecyclerViewEmptySupport;
import com.dimtutac.mobileapp.common.UserSessionManager;
import com.dimtutac.mobileapp.models.customertypephoto.MdlCustomerTypePhotoRes;
import com.dimtutac.mobileapp.models.offer.MdlCustomerOffersRes;
import com.dimtutac.mobileapp.models.offer.MdlCustomerOffersResData;
import com.dimtutac.mobileapp.retrofitapi.APICustomerTypePhoto;
import com.dimtutac.mobileapp.retrofitapi.APIOffer;
import com.dimtutac.mobileapp.retrofitapi.APIRetroBuilder;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Jalotsav on 22/8/17.
 */

public class FrgmntAccount extends Fragment {

    private static final String TAG = FrgmntAccount.class.getSimpleName();

    @BindView(R.id.cordntrlyot_frgmnt_account) CoordinatorLayout mCrdntrlyot;
    @BindView(R.id.imgvw_frgmnt_account_customertypephoto) ImageView mImgvwCustomerTypePhoto;
    @BindView(R.id.tv_frgmnt_account_firstlastname) TextView mTvFirstLastName;
    @BindView(R.id.lnrlyot_recyclremptyvw_appearhere)LinearLayout mLnrlyotAppearHere;
    @BindView(R.id.tv_recyclremptyvw_appearhere)TextView mTvAppearHere;
    @BindView(R.id.rcyclrvw_frgmnt_account_myoffers) RecyclerViewEmptySupport mRecyclerView;
    @BindView(R.id.prgrsbr_frgmnt_account) ProgressBar mPrgrsbrMain;

    @BindString(R.string.no_intrnt_cnctn) String mNoInternetConnMsg;
    @BindString(R.string.server_problem_sml) String mServerPrblmMsg;
    @BindString(R.string.internal_problem_sml) String mInternalPrblmMsg;
    @BindString(R.string.myoffers_appear_here) String mMyOffersAppearHere;

    @BindDrawable(R.mipmap.ic_logo_black) Drawable mDrwblDefault;

    RecyclerView.LayoutManager mLayoutManager;
    RcyclrMyOffersAdapter mAdapter;
    ArrayList<MdlCustomerOffersResData> mArrylstMdlCustomerOffers;

    UserSessionManager session;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.lo_frgmnt_account, container, false);
        ButterKnife.bind(this, rootView);

        session = new UserSessionManager(getActivity());

        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setEmptyView(mLnrlyotAppearHere);

        mTvAppearHere.setText(mMyOffersAppearHere);

        mArrylstMdlCustomerOffers = new ArrayList<>();
        mAdapter = new RcyclrMyOffersAdapter(getActivity(), mArrylstMdlCustomerOffers, mDrwblDefault);
        mRecyclerView.setAdapter(mAdapter);

        if(!session.isUserLoggedIn()) {

            startActivityForResult(new Intent(getActivity(), ActvtySignin.class), AppConstants.REQUEST_SIGNIN);
        }
        return rootView;
    }

    private void getCustomerTypePhotoAndOffers() {

        if (GeneralFunctions.isNetConnected(getActivity())) {
            callCustomerTypePhotoAPI();
            callMyOffersAPI();
        } else Snackbar.make(mCrdntrlyot, mNoInternetConnMsg, Snackbar.LENGTH_LONG).show();
    }

    private void callCustomerTypePhotoAPI() {

        APICustomerTypePhoto objApiCustomerTypePhoto = APIRetroBuilder.getRetroBuilder(true).create(APICustomerTypePhoto.class);
        Call<MdlCustomerTypePhotoRes> callMdlCustmrTypePhotoRes = objApiCustomerTypePhoto.callGetCustomerTypePhoto(session.getUserId());
        callMdlCustmrTypePhotoRes.enqueue(new Callback<MdlCustomerTypePhotoRes>() {
            @Override
            public void onResponse(Call<MdlCustomerTypePhotoRes> call, Response<MdlCustomerTypePhotoRes> response) {

                if(response.isSuccessful()) {

                    try {
                        if(response.body().isSuccess()) {

                            if(response.body().getMdlCustomerTypePhotoResData() != null) {
                                String CustomerTypeImageName = response.body().getMdlCustomerTypePhotoResData().getImagePath();
                                if (!TextUtils.isEmpty(CustomerTypeImageName)) {

                                    mTvFirstLastName.setText(session.getFirstName().concat(" ").concat(session.getLastName()));
                                    Picasso.with(getActivity())
                                            .load(AppConstants.ATCHMNT_ROOT_URL.concat(CustomerTypeImageName))
                                            .placeholder(R.drawable.drwbl_bg_black)
                                            .into(mImgvwCustomerTypePhoto);
                                }
                            }
                        } else
                            Snackbar.make(mCrdntrlyot, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                    } catch (Exception e) {

                        e.printStackTrace();
                        LogHelper.printLog(AppConstants.LOGTYPE_ERROR, TAG, e.getMessage());
                        Snackbar.make(mCrdntrlyot, mInternalPrblmMsg, Snackbar.LENGTH_LONG).show();
                    }
                } else
                    Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<MdlCustomerTypePhotoRes> call, Throwable t) {

                Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    private void callMyOffersAPI() {

        mPrgrsbrMain.setVisibility(View.VISIBLE);

        APIOffer objApiOffers = APIRetroBuilder.getRetroBuilder(true).create(APIOffer.class);
        Call<MdlCustomerOffersRes> callMdlCustomerOffersRes = objApiOffers.callGetCustomerOffers(session.getUserId());
        callMdlCustomerOffersRes.enqueue(new Callback<MdlCustomerOffersRes>() {
            @Override
            public void onResponse(Call<MdlCustomerOffersRes> call, Response<MdlCustomerOffersRes> response) {

                mPrgrsbrMain.setVisibility(View.GONE);
                if (response.isSuccessful()) {

                    try {
                        if (response.body().isSuccess()) {

                            ArrayList<MdlCustomerOffersResData> arrylstMdlCheckinListData = response.body().getArrylstMdlCustomerOffersResData();
                            if (arrylstMdlCheckinListData.size() > 0) {
                                mArrylstMdlCustomerOffers.addAll(arrylstMdlCheckinListData);
                                mAdapter.notifyDataSetChanged();
                            }
                        } else
                            Snackbar.make(mCrdntrlyot, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                    } catch (Exception e) {

                        e.printStackTrace();
                        LogHelper.printLog(AppConstants.LOGTYPE_ERROR, TAG, e.getMessage());
                        Snackbar.make(mCrdntrlyot, mInternalPrblmMsg, Snackbar.LENGTH_LONG).show();
                    }
                } else
                    Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<MdlCustomerOffersRes> call, Throwable t) {

                mPrgrsbrMain.setVisibility(View.GONE);
                Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    @OnClick({R.id.tv_frgmnt_account_edit})
    public void onClickView(View view) {

        switch (view.getId()) {
            case R.id.tv_frgmnt_account_edit:

                startActivity(new Intent(getActivity(), ActvtyAccountEdit.class));
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if (resultCode == Activity.RESULT_OK) {
                if (requestCode == AppConstants.REQUEST_SIGNIN) {

                    ((NavgtnDrwrMain) getActivity()).setHeaderName();
                    getCustomerTypePhotoAndOffers();
                }
            } else
                ((NavgtnDrwrMain) getActivity()).displayFragmntView(AppConstants.NAVDRWER_HOME);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (session.isUserLoggedIn()) {

            mArrylstMdlCustomerOffers.clear();
            getCustomerTypePhotoAndOffers();
        }
    }
}
