package com.dimtutac.mobileapp.navgtndrawer;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dimtutac.mobileapp.R;

/**
 * Created by Jalotsav on 26/8/17.
 */

public class FrgmntInDevlpmnt extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.lo_actvty_indevlpmnt, container, false);
    }
}
