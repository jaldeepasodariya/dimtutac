package com.dimtutac.mobileapp.retrofitapi;

import com.dimtutac.mobileapp.common.AppConstants;
import com.dimtutac.mobileapp.models.news.MdlNewsListRes;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Jalotsav on 12/8/2017.
 */

public interface APINews {

    @GET(AppConstants.API_NEWS_GETNEWSLIST)
    Call<MdlNewsListRes> callGetNews();
}
