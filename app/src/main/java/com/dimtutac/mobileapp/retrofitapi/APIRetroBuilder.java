package com.dimtutac.mobileapp.retrofitapi;

import android.content.Context;

import com.dimtutac.mobileapp.BuildConfig;
import com.dimtutac.mobileapp.common.AppConstants;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Jalotsav on 9/5/2017.
 */

public class APIRetroBuilder {

    private static Retrofit OBJ_RETROFIT = null;

    public static Retrofit getRetroBuilder(boolean enableConnectTimeout) {

        if (OBJ_RETROFIT == null) {

            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();
            clientBuilder.addInterceptor(loggingInterceptor); // print OkHTTP log
            if (enableConnectTimeout) {
                clientBuilder.connectTimeout(5, TimeUnit.MINUTES);
                clientBuilder.readTimeout(5, TimeUnit.MINUTES);
            }

            OBJ_RETROFIT = new Retrofit.Builder()
                    .baseUrl(AppConstants.API_ROOT_URL)
                    .client(clientBuilder.build())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return OBJ_RETROFIT;
    }
}
