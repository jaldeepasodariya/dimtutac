package com.dimtutac.mobileapp.retrofitapi;

import com.dimtutac.mobileapp.common.AppConstants;
import com.dimtutac.mobileapp.models.contactus.MdlCntctusPostReq;
import com.dimtutac.mobileapp.models.contactus.MdlCntctusPostRes;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by Jalotsav on 10/14/2017.
 */

public interface APIContactus {

    @Headers(AppConstants.CONTENT_TYPE_JSON)
    @POST(AppConstants.API_CONTACTUS_POST)
    Call<MdlCntctusPostRes> callContactusPost(@Body MdlCntctusPostReq objMdlCntctusPostReq);
}
