package com.dimtutac.mobileapp.retrofitapi;

import com.dimtutac.mobileapp.common.AppConstants;
import com.dimtutac.mobileapp.models.branch.MdlBranchListRes;
import com.dimtutac.mobileapp.models.branch.MdlLocationListRes;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Jalotsav on 9/18/2017.
 */

public interface APIBranch {

    @GET(AppConstants.API_BRANCH_LOCATIONLIST)
    Call<MdlLocationListRes> callGetLocationList();

    @GET(AppConstants.API_BRANCH_BRANCHLIST)
    Call<MdlBranchListRes> callGetBranchList();
}
