package com.dimtutac.mobileapp.retrofitapi;

import com.dimtutac.mobileapp.common.AppConstants;
import com.dimtutac.mobileapp.models.career.MdlCareerPostReq;
import com.dimtutac.mobileapp.models.career.MdlCareerPostRes;
import com.dimtutac.mobileapp.models.career.MdlPositionListRes;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by Jalotsav on 10/18/2017.
 */

public interface APICareer {

    @GET(AppConstants.API_CAREER_GETPOSITIONLIST)
    Call<MdlPositionListRes> callGetPositionList();

    @Headers(AppConstants.CONTENT_TYPE_JSON)
    @POST(AppConstants.API_CAREER_POST)
    Call<MdlCareerPostRes> callCareerPost(@Body MdlCareerPostReq objMdlResrvtnDoReq);
}
