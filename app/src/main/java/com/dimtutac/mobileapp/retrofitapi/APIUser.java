package com.dimtutac.mobileapp.retrofitapi;

import com.dimtutac.mobileapp.common.AppConstants;
import com.dimtutac.mobileapp.models.login.MdlLoginResData;
import com.dimtutac.mobileapp.models.user.MdlCheckIfFacebookIdExistReq;
import com.dimtutac.mobileapp.models.user.MdlUserCreateReq;
import com.dimtutac.mobileapp.models.user.MdlUserCreateRes;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Jalotsav on 9/16/2017.
 */

public interface APIUser {

    @Headers(AppConstants.CONTENT_TYPE_JSON)
    @POST(AppConstants.API_USER_CREATE)
    Call<MdlUserCreateRes> callUserCreate(@Body MdlUserCreateReq objMdlLoginReq);

    @Headers(AppConstants.CONTENT_TYPE_JSON)
    @POST(AppConstants.API_USER_UPDATE)
    Call<ResponseBody> callUserUpdate(@Query(AppConstants.KEY_USER_ID_FIRSTCHARCAPS) String userId,
                                      @Body MdlLoginResData objMdlLoginResData);

    @Headers(AppConstants.CONTENT_TYPE_JSON)
    @POST(AppConstants.API_USER_CHECKIFFACEBOOKIDEXIST)
    Call<ResponseBody> callCheckIfFacebookIdExist(@Body MdlCheckIfFacebookIdExistReq objMdlCheckIfFacebookIdExistReq);
}
