package com.dimtutac.mobileapp.retrofitapi;

import com.dimtutac.mobileapp.common.AppConstants;
import com.dimtutac.mobileapp.models.menu.MdlMenuListRes;
import com.dimtutac.mobileapp.models.menu.MdlNewMonthlyItemListRes;
import com.dimtutac.mobileapp.models.menu.MdlSubMenuListRes;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Jalotsav on 11/3/2017.
 */

public interface APIMenu {

    @GET(AppConstants.API_MENU_GETNEWMONTHLYITEM)
    Call<MdlNewMonthlyItemListRes> callGetNewMonthlyItem(@Query(AppConstants.KEY_BRANCH_ID) String branchId);

    @GET(AppConstants.API_MENU_GETMENU)
    Call<MdlMenuListRes> callGetMenu();

    @GET(AppConstants.API_MENU_GETSUBMENU)
    Call<MdlSubMenuListRes> callGetSubMenu(@Query(AppConstants.KEY_MENU_ID) String menuId);
}
