package com.dimtutac.mobileapp.retrofitapi;

import com.dimtutac.mobileapp.common.AppConstants;
import com.dimtutac.mobileapp.models.forgotpassword.MdlForgotPasswordReq;
import com.dimtutac.mobileapp.models.forgotpassword.MdlForgotPasswordRes;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by Jalotsav on 2/8/2018.
 */

public interface APIForgotPassword {

    @Headers(AppConstants.CONTENT_TYPE_JSON)
    @POST(AppConstants.API_FORGOTPASSWORD_INDEX)
    Call<MdlForgotPasswordRes> callForgotPassword(@Body MdlForgotPasswordReq objMdlForgotPasswordReq);
}
