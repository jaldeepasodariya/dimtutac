package com.dimtutac.mobileapp.retrofitapi;

import com.dimtutac.mobileapp.common.AppConstants;
import com.dimtutac.mobileapp.models.login.MdlLoginReq;
import com.dimtutac.mobileapp.models.login.MdlLoginRes;
import com.dimtutac.mobileapp.models.login.MdlTermsAndConditionsRes;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by Jalotsav on 9/5/2017.
 */

public interface APILogin {

    @Headers(AppConstants.CONTENT_TYPE_JSON)
    @POST(AppConstants.API_LOGIN_LOGIN)
    Call<ResponseBody> callLogin(@Body MdlLoginReq objMdlLoginReq);

    @GET(AppConstants.API_LOGIN_GETTERMSANDCONDITIONS)
    Call<MdlTermsAndConditionsRes> callGetTermsAndConditions();
}
