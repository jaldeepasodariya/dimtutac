package com.dimtutac.mobileapp.retrofitapi;

import com.dimtutac.mobileapp.common.AppConstants;
import com.dimtutac.mobileapp.models.offer.MdlCustomerOffersRes;
import com.dimtutac.mobileapp.models.offer.MdlRedeemOfferReq;
import com.dimtutac.mobileapp.models.offer.MdlRedeemOfferRes;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Jalotsav on 1/31/2018.
 */

public interface APIOffer {

    @GET(AppConstants.API_OFFER_GETCUSTOMEROFFERS)
    Call<MdlCustomerOffersRes> callGetCustomerOffers(@Query(AppConstants.KEY_CUSTOMER_ID) String customerId);

    @Headers(AppConstants.CONTENT_TYPE_JSON)
    @POST(AppConstants.API_OFFER_REDEEMOFFER)
    Call<MdlRedeemOfferRes> callRedeemOffer(@Body MdlRedeemOfferReq objMdlRedeemOfferReq);
}
