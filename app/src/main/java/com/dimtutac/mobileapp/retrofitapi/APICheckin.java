package com.dimtutac.mobileapp.retrofitapi;

import com.dimtutac.mobileapp.common.AppConstants;
import com.dimtutac.mobileapp.models.checkin.MdlCheckinDoReq;
import com.dimtutac.mobileapp.models.checkin.MdlCheckinDoRes;
import com.dimtutac.mobileapp.models.checkin.MdlCheckinListRes;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Jalotsav on 11/14/2017.
 */

public interface APICheckin {

    @Headers(AppConstants.CONTENT_TYPE_JSON)
    @POST(AppConstants.API_CHECKIN_DOCHECKIN)
    Call<MdlCheckinDoRes> callDoCheckin(@Body MdlCheckinDoReq objMdlCheckinDoReq);

    @GET(AppConstants.API_CHECKIN_CHECKINLIST)
    Call<MdlCheckinListRes> callGetCheckinList(@Query(AppConstants.KEY_USER_ID_FIRSTCHARCAPS) String userId);
}
