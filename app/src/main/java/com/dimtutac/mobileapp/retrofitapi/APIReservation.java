package com.dimtutac.mobileapp.retrofitapi;

import com.dimtutac.mobileapp.common.AppConstants;
import com.dimtutac.mobileapp.models.reservation.MdlResrvtnDoReq;
import com.dimtutac.mobileapp.models.reservation.MdlResrvtnDoRes;
import com.dimtutac.mobileapp.models.reservation.MdlResrvtnListRes;
import com.dimtutac.mobileapp.models.reservation.MdlResrvtnListResData;
import com.dimtutac.mobileapp.models.reservation.MdlResrvtnUpdateRes;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Jalotsav on 9/27/2017.
 */

public interface APIReservation {

    @GET(AppConstants.API_RESRVTN_RESRVTNLIST)
    Call<MdlResrvtnListRes> callGetReservationList(@Query(AppConstants.KEY_USER_ID_FIRSTCHARCAPS) String userId);


    @Headers(AppConstants.CONTENT_TYPE_JSON)
    @POST(AppConstants.API_RESRVTN_DORESRVTN)
    Call<MdlResrvtnDoRes> callDoReservation(@Body MdlResrvtnDoReq objMdlResrvtnDoReq);

    @Headers(AppConstants.CONTENT_TYPE_JSON)
    @POST(AppConstants.API_RESRVTN_UPDATERESRVTN)
    Call<MdlResrvtnUpdateRes> callUpdateReservation(@Query(AppConstants.KEY_RESERVATION_ID_SML) String reservationId,
                                                    @Body MdlResrvtnListResData objMdlResrvtnUpdateReq);
}
