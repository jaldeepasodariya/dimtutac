package com.dimtutac.mobileapp.retrofitapi;

import com.dimtutac.mobileapp.common.AppConstants;
import com.dimtutac.mobileapp.models.dashboard.MdlDashbrdDataRes;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Jalotsav on 9/21/2017.
 */

public interface APIDashboard {

    @GET(AppConstants.API_DASHBOARD_DASHBOARDDATA)
    Call<MdlDashbrdDataRes> callGetDashboardData();
}
