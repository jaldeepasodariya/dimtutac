package com.dimtutac.mobileapp.retrofitapi;

import com.dimtutac.mobileapp.common.AppConstants;
import com.dimtutac.mobileapp.models.customertypephoto.MdlCustomerTypePhotoRes;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Jalotsav on 1/31/2018.
 */

public interface APICustomerTypePhoto {

    @GET(AppConstants.API_CUSTOMERTYPEPHOTO_GETCUSTOMERTYPEPHOTO)
    Call<MdlCustomerTypePhotoRes> callGetCustomerTypePhoto(@Query(AppConstants.KEY_CUSTOMER_TYPE_ID) String customerTypeId);
}
