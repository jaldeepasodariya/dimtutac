package com.dimtutac.mobileapp.retrofitapi;

import com.dimtutac.mobileapp.common.AppConstants;
import com.dimtutac.mobileapp.models.restaurantphoto.MdlRestaurantPhotoListRes;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Jalotsav on 1/8/2018.
 */

public interface APIRestaurantPhoto {

    @GET(AppConstants.API_RESTAURANTPHOTO_RESTAURANTPHOTOLIST)
    Call<MdlRestaurantPhotoListRes> callGetRestaurantPhotoList(@Query(AppConstants.KEY_RESTAURANT_ID) String restaurantId); // Pass BranchId as a value
}
