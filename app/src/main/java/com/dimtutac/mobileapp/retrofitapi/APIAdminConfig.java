package com.dimtutac.mobileapp.retrofitapi;

import com.dimtutac.mobileapp.common.AppConstants;
import com.dimtutac.mobileapp.models.adminconfigurations.MdlAdminConfigRes;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Jalotsav on 1/10/2018.
 */

public interface APIAdminConfig {

    @GET(AppConstants.API_ADMINCONFIGURATIONS_GETSPASHSCREENLOGO)
    Call<MdlAdminConfigRes> callGetSpashScreenLogo();
}
