package com.dimtutac.mobileapp;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.dimtutac.mobileapp.adapter.SpnrArryAdptrLanguage;
import com.dimtutac.mobileapp.common.AppConstants;
import com.dimtutac.mobileapp.common.GeneralFunctions;
import com.dimtutac.mobileapp.common.LogHelper;
import com.dimtutac.mobileapp.common.UserSessionManager;
import com.dimtutac.mobileapp.models.dropdown.MdlLanguageData;
import com.dimtutac.mobileapp.models.login.MdlLoginResData;
import com.dimtutac.mobileapp.models.login.MdlTermsAndConditionsRes;
import com.dimtutac.mobileapp.models.user.MdlCheckIfFacebookIdExistReq;
import com.dimtutac.mobileapp.models.user.MdlUserCreateReq;
import com.dimtutac.mobileapp.models.user.MdlUserCreateRes;
import com.dimtutac.mobileapp.retrofitapi.APILogin;
import com.dimtutac.mobileapp.retrofitapi.APIRetroBuilder;
import com.dimtutac.mobileapp.retrofitapi.APIUser;
import com.dimtutac.mobileapp.utils.ValidationUtils;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dimtutac.mobileapp.common.AppConstants.KEY_DATA_SML;
import static com.dimtutac.mobileapp.common.AppConstants.KEY_SUCCESS_SML;

/**
 * Created by Jalotsav on 25/8/17.
 */

public class ActvtySignup extends AppCompatActivity {

    private static final String TAG = ActvtySignup.class.getSimpleName();

    @BindView(R.id.cordntrlyot_actvty_signup) CoordinatorLayout mCrdntrlyot;
    @BindView(R.id.et_actvty_signup_firstname) EditText mEtFirstname;
    @BindView(R.id.et_actvty_signup_lastname) EditText mEtLastname;
    @BindView(R.id.et_actvty_signup_email) EditText mEtEmail;
    @BindView(R.id.et_actvty_signup_paswrd) EditText mEtPaswrd;
    @BindView(R.id.et_actvty_signup_confrmpaswrd) EditText mEtConfrmPaswrd;
    @BindView(R.id.et_actvty_signup_phoneno) EditText mEtPhoneno;
    @BindView(R.id.et_actvty_signup_company) EditText mEtCompany;
    @BindView(R.id.spnr_actvty_signup_gender) Spinner mSpnrGender;
    @BindView(R.id.spnr_actvty_signup_language) Spinner mSpnrLanguage;
    @BindView(R.id.tv_actvty_signup_birthday) TextView mTvBirthday;
    @BindView(R.id.chckbx_actvty_signup_agreeterms) CheckBox mChckbxAgreeTerms;
    @BindView(R.id.chckbx_actvty_signup_sendmepromtnupdate) CheckBox mChckbxSendPromtn;
    @BindView(R.id.prgrsbr_actvty_signup) ProgressBar mPrgrsbrMain;

    @BindString(R.string.no_intrnt_cnctn) String mNoInternetConnMsg;
    @BindString(R.string.server_problem_sml) String mServerPrblmMsg;
    @BindString(R.string.internal_problem_sml) String mInternalPrblmMsg;
    @BindString(R.string.entr_first_name_sml) String mEntrFirstname;
    @BindString(R.string.entr_last_name_sml) String mEntrLastname;
    @BindString(R.string.entr_email_sml) String mEntrEmail;
    @BindString(R.string.entr_password_sml) String mEntrPaswrd;
    @BindString(R.string.entr_confrm_passwrd_sml) String mEntrConfrmPaswrd;
    @BindString(R.string.slct_gendr_sml) String mSlctGender;
    @BindString(R.string.birthday_sml) String mBirthdayStr;
    @BindString(R.string.slct_birthday_sml) String mSlctBirthday;
    @BindString(R.string.invalid_birthday_sml) String mInvalidBirthday;
    @BindString(R.string.slct_language_sml) String mSlctLanguage;
    @BindString(R.string.choose_lang_star) String mStrChooseLanguageStar;
    @BindString(R.string.slct_agree_termscondtn_sml) String mSlctAgreeTermsCondtn;

    UserSessionManager session;
    CallbackManager mCallbackManager;
    boolean mIsFacbookSignin;
    String mFacebookId = "", mFacebookName = "", mFacebookEmail = "", mFacebookGender = "", mFacebookBirthday = "", mTermsAndConditions = "";
    Calendar mCalendar;
    long mSlctdDateTimestamp;
    int birthdayAgeCount;
    boolean isBirthDateSelected = false;
    ArrayList<MdlLanguageData> mArrylstMdlLanguageData;
    AlertDialog mAlrtDlgTermsCondtns;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lo_actvty_signup);
        ButterKnife.bind(this);

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) { e.printStackTrace(); }

        session = new UserSessionManager(this);
        mCallbackManager = CallbackManager.Factory.create();

        mIsFacbookSignin = getIntent().getBooleanExtra(AppConstants.PUT_EXTRA_ISFACEBOOKSIGNIN, false);

        mArrylstMdlLanguageData = new ArrayList<>();
        mArrylstMdlLanguageData.add(new MdlLanguageData(false, mStrChooseLanguageStar, "0"));
        mArrylstMdlLanguageData.addAll(GeneralFunctions.getLanguageData());
        SpnrArryAdptrLanguage adapterLanguage = new SpnrArryAdptrLanguage(
                this,
                android.R.layout.simple_spinner_dropdown_item,
                mArrylstMdlLanguageData
        );
        mSpnrLanguage.setAdapter(adapterLanguage);

        if(mIsFacbookSignin) {
            mFacebookId = getIntent().getStringExtra(AppConstants.PUT_EXTRA_FACEBOOK_ID);
            mFacebookName = getIntent().getStringExtra(AppConstants.PUT_EXTRA_FACEBOOK_NAME);
            mFacebookEmail = getIntent().getStringExtra(AppConstants.PUT_EXTRA_FACEBOOK_EMAIL);
            mFacebookGender = getIntent().getStringExtra(AppConstants.PUT_EXTRA_FACEBOOK_GENDER);
            mFacebookBirthday = getIntent().getStringExtra(AppConstants.PUT_EXTRA_FACEBOOK_BIRTHDAY);

            setFacebookDataOnUI();
        }

        mAlrtDlgTermsCondtns = new AlertDialog.Builder(this).create();
        mChckbxAgreeTerms.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {

                if(isChecked) {

                    if(!TextUtils.isEmpty(mTermsAndConditions) && !mAlrtDlgTermsCondtns.isShowing())
                        showTermsAndConditionsAlertDialog();
                    else {

                        mChckbxAgreeTerms.setChecked(false);
                        getTermsAndConditions();
                    }
                }
            }
        });
    }

    // Set user all Facebook data on UI in proper format
    private void setFacebookDataOnUI() {

        if(!TextUtils.isEmpty(mFacebookName)) { // First name, Last name
            if(mFacebookName.contains(" ")) {
                mEtFirstname.setText(mFacebookName.split(" ")[0]);
                mEtLastname.setText(mFacebookName.split(" ")[1]);
            } else
                mEtFirstname.setText(mFacebookName);
        }
        if(!TextUtils.isEmpty(mFacebookEmail)) // Email
            mEtEmail.setText(mFacebookEmail);
        if(!TextUtils.isEmpty(mFacebookGender)) { // Gender
            if(mFacebookGender.equalsIgnoreCase(AppConstants.KEY_MALE))
                mSpnrGender.setSelection(1);
            else if(mFacebookGender.equalsIgnoreCase(AppConstants.KEY_FEMALE))
                mSpnrGender.setSelection(2);
        }
        if(!TextUtils.isEmpty(mFacebookBirthday)) { // Birthday

            try {
                SimpleDateFormat sd = new SimpleDateFormat("mm/dd/yyyy", Locale.getDefault());
                Date dateFacebookBirthday = sd.parse(mFacebookBirthday);
                mCalendar = Calendar.getInstance();
                mCalendar.setTime(dateFacebookBirthday);
                int year = mCalendar.get(Calendar.YEAR);
                int monthOfYear = mCalendar.get(Calendar.MONTH);
                int dayOfMonth = mCalendar.get(Calendar.DAY_OF_MONTH);

                mCalendar = new GregorianCalendar(year, monthOfYear, dayOfMonth);
                isBirthDateSelected = true;
                calculateAge();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        mEtPaswrd.setVisibility(View.GONE);
        mEtConfrmPaswrd.setVisibility(View.GONE);
    }

    @OnClick({R.id.tv_actvty_signup_birthday, R.id.appcmptbtn_actvty_signup_signup, R.id.appcmptbtn_actvty_signup_signfacebook})
    public void onClickView(View view) {

        switch (view.getId()) {
            case R.id.tv_actvty_signup_birthday:

                showDOBCalender();
                break;
            case R.id.appcmptbtn_actvty_signup_signup:

                checkAllValidation();
                break;
            case R.id.appcmptbtn_actvty_signup_signfacebook:

                LoginManager.getInstance().logInWithReadPermissions(this,
                        Arrays.asList("public_profile", "email", "user_birthday"));
                facebookLoginCallback();
                break;
        }
    }

    private void facebookLoginCallback() {

        LoginManager.getInstance().registerCallback(mCallbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {

                        Log.i(TAG, "FacebookCallback-onSuccess: AccessToken - " + loginResult.getAccessToken().getToken());
                        GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {

                                    @Override
                                    public void onCompleted(JSONObject object, GraphResponse response) {
                                        Log.i(TAG, "GraphJSONObjectCallback: Object - " + object.toString());
                                        try {
                                            mFacebookId = object.getString("id");
                                            mFacebookName = object.getString("name");
                                            if(object.has("email"))
                                                mFacebookEmail = object.getString("email");
                                            if(object.has("gender"))
                                                mFacebookGender = object.getString("gender");
                                            if(object.has("birthday"))
                                                mFacebookBirthday = object.getString("birthday");

                                            if(mPrgrsbrMain.getVisibility() != View.VISIBLE) {
                                                if (GeneralFunctions.isNetConnected(ActvtySignup.this))
                                                    callCheckIfFacebookIdExistAPI();
                                                else Snackbar.make(mCrdntrlyot, mNoInternetConnMsg, Snackbar.LENGTH_LONG).show();
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });

                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,email,gender,birthday");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                    }

                    @Override
                    public void onError(FacebookException exception) {
                    }
                });
    }

    // call API for Check FacebookId is exist in server
    private void callCheckIfFacebookIdExistAPI() {

        mPrgrsbrMain.setVisibility(View.VISIBLE);

        APIUser objApiUser = APIRetroBuilder.getRetroBuilder(true).create(APIUser.class);
        Call<ResponseBody> callFacebookIdExistRes = objApiUser.callCheckIfFacebookIdExist(new MdlCheckIfFacebookIdExistReq(mFacebookId));
        callFacebookIdExistRes.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                mPrgrsbrMain.setVisibility(View.GONE);
                if(response.isSuccessful()) {

                    try {

                        JSONObject jsnObjResponse = new JSONObject(response.body().string());
                        boolean isSuccess = jsnObjResponse.getBoolean(KEY_SUCCESS_SML);

                        if(isSuccess)
                            storeUserDataToLocal(jsnObjResponse);
                        else
                            setFacebookDataOnUI();
                    } catch (Exception e) {

                        e.printStackTrace();
                        LogHelper.printLog(AppConstants.LOGTYPE_ERROR, TAG, e.getMessage());
                        Snackbar.make(mCrdntrlyot, mInternalPrblmMsg, Snackbar.LENGTH_LONG).show();
                    }
                } else
                    Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                mPrgrsbrMain.setVisibility(View.GONE);
                Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    // Store user all data in to SharedPreference
    private void storeUserDataToLocal(JSONObject jsnObjResponse) {

        try {
            JSONObject data = jsnObjResponse.getJSONObject(KEY_DATA_SML);
            Gson objGson = new Gson();
            MdlLoginResData objMdlLoginResData = objGson.fromJson(data.toString(), MdlLoginResData.class);
            if(objMdlLoginResData != null) {
                UserSessionManager session = new UserSessionManager(ActvtySignup.this);
                session.setRememberLogin(false);
                session.setUserId(objMdlLoginResData.getPkId());
                session.setUserName(objMdlLoginResData.getEmailId());
                session.setFirstName(objMdlLoginResData.getFirstName());
                session.setLastName(objMdlLoginResData.getLastName());
                session.setMiddleName(objMdlLoginResData.getMiddleName());
                session.setUserTypeId(objMdlLoginResData.getUserTypeId());
                session.setPassword(objMdlLoginResData.getPassword());
                session.setCompanyName(objMdlLoginResData.getCompanyName());
                session.setGender(objMdlLoginResData.getGender());
                session.setDateOfBirth(objMdlLoginResData.getDateOfBirth());
                session.setDefaultLanguageId(objMdlLoginResData.getDefaultLanguageId());
                session.setRestaurantId(objMdlLoginResData.getRestaurantId());
                session.setMainBranchId(objMdlLoginResData.getMainBranchId());
                session.setAgreeTC(objMdlLoginResData.isAgreeTC());
                session.setPromotionSend(objMdlLoginResData.isPromotionSend());
                session.setFacebookLogin(true);
                session.setFacebookId(objMdlLoginResData.getFacebookId());
                session.setAccountBarcode(objMdlLoginResData.getAccountBarcode());
                session.setEmailId(objMdlLoginResData.getEmailId());
                session.setMobileNo(objMdlLoginResData.getMobileNo());
                session.setPhoneNo(objMdlLoginResData.getPhoneNo());
                session.setPhotoURL(objMdlLoginResData.getPhotoURL());
                session.setActive(objMdlLoginResData.isActive());
                session.setAddressMasterId(objMdlLoginResData.getAddressMasterId());
                session.setAddressLine1(objMdlLoginResData.getAddressLine1());
                session.setAddressLine2(objMdlLoginResData.getAddressLine2());
                session.setCity(objMdlLoginResData.getCity());
                session.setState(objMdlLoginResData.getState());
                session.setCountry(objMdlLoginResData.getCountry());
                session.setPincode(objMdlLoginResData.getPincode());
                session.setCustomerType(objMdlLoginResData.getCustomerType());
                session.setPasswordResetId(objMdlLoginResData.getPasswordResetId());
                session.setDeviceId(objMdlLoginResData.getDeviceId());

                mIsFacbookSignin = true;

                Intent resultIntent = new Intent();
                resultIntent.putExtra(AppConstants.PUT_EXTRA_ISFACEBOOKSIGNIN, mIsFacbookSignin);
                setResult(RESULT_OK, resultIntent);
                finish();
            }
        } catch (Exception e) {

            e.printStackTrace();
            LogHelper.printLog(AppConstants.LOGTYPE_ERROR, TAG, e.getMessage());
            Snackbar.make(mCrdntrlyot, mInternalPrblmMsg, Snackbar.LENGTH_LONG).show();
        }
    }

    private void checkAllValidation() {

        if (!ValidationUtils.validateEmpty(this, mCrdntrlyot, mEtFirstname, mEntrFirstname))
            return;

        if (!ValidationUtils.validateEmpty(this, mCrdntrlyot, mEtLastname, mEntrLastname))
            return;

        if (!ValidationUtils.validateEmpty(this, mCrdntrlyot, mEtEmail, mEntrEmail))
            return;

        if (!ValidationUtils.validateEmailFormat(this, mCrdntrlyot, mEtEmail))
            return;

        if (!ValidationUtils.validateEmpty(this, mCrdntrlyot, mEtPaswrd, mEntrPaswrd))
            return;

        if (!ValidationUtils.validateEmpty(this, mCrdntrlyot, mEtConfrmPaswrd, mEntrConfrmPaswrd))
            return;

        if(!validateMatchPassword())
            return;

        if (!ValidationUtils.validateMobile(this, mCrdntrlyot, mEtPhoneno))
            return;

        if(mSpnrGender.getSelectedItemPosition() == 0) {
            Snackbar.make(mCrdntrlyot, getString(R.string.slct_gendr_sml), Snackbar.LENGTH_SHORT).show();
            return;
        }

        if(!validateBirthday())
            return;

        if(mSpnrLanguage.getSelectedItemPosition() == 0) {
            Snackbar.make(mCrdntrlyot, getString(R.string.slct_language_sml), Snackbar.LENGTH_SHORT).show();
            return;
        }

        if(!mChckbxAgreeTerms.isChecked()) {
            Snackbar.make(mCrdntrlyot, mSlctAgreeTermsCondtn, Snackbar.LENGTH_SHORT).show();
            return;
        }

        if(GeneralFunctions.isNetConnected(this))
            callSignupAPI();
        else Snackbar.make(mCrdntrlyot, mNoInternetConnMsg, Snackbar.LENGTH_LONG).show();
    }

    // Match Password and Confirm Password validation for field
    private boolean validateMatchPassword() {

        if (!mEtPaswrd.getText().toString().trim().equals(mEtConfrmPaswrd.getText().toString().trim())) {
            Snackbar.make(mCrdntrlyot, getString(R.string.passwrd_not_matched_sml), Snackbar.LENGTH_SHORT).show();
            ValidationUtils.requestFocus(this, mEtConfrmPaswrd);
            return false;
        } else {
            return true;
        }
    }

    // Show DatePicker for select BirthDate
    private void showDOBCalender() {

        mCalendar = Calendar.getInstance();
        DatePickerDialog fromDatePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        /*monthOfYear = monthOfYear + 1;
                        String slctdPreviousDate = monthOfYear + "/" + dayOfMonth + "/" + year;*/

                        // Convert selected Date to Timestamp
                        mCalendar = new GregorianCalendar(year, monthOfYear, dayOfMonth);
                        isBirthDateSelected = true;
                        calculateAge();
                    }
                }, mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH), mCalendar.get(Calendar.DAY_OF_MONTH));

        fromDatePickerDialog.show();
    }

    // Calculate Age from selected Date
    private void calculateAge() {

        Date dateOfBirth = new Date(mCalendar.getTimeInMillis());
        Calendar dob = Calendar.getInstance();
        dob.setTime(dateOfBirth);
        Calendar today = Calendar.getInstance();
        birthdayAgeCount = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
        if (today.get(Calendar.MONTH) < dob.get(Calendar.MONTH))
            birthdayAgeCount--;
        else if (today.get(Calendar.MONTH) == dob.get(Calendar.MONTH)
                && today.get(Calendar.DAY_OF_MONTH) < dob.get(Calendar.DAY_OF_MONTH))
            birthdayAgeCount--;

        validateBirthday();
    }

    // Validation for BirthDay
    private boolean validateBirthday() {

        if(!isBirthDateSelected){

            if(mTvBirthday.getText().toString().equalsIgnoreCase(mBirthdayStr)) {

//                mTvBirthday.setTextColor(ContextCompat.getColor(this, R.color.colorPrimaryOrange));
                Snackbar.make(mCrdntrlyot, mSlctBirthday, Snackbar.LENGTH_SHORT).show();
                isBirthDateSelected = false;

                return false;
            }
        } else if (birthdayAgeCount < 18) {

            mTvBirthday.setText(mBirthdayStr);
//            mTvBirthday.setTextColor(ContextCompat.getColor(this, R.color.grayA8));
            Snackbar.make(mCrdntrlyot, mInvalidBirthday, Snackbar.LENGTH_SHORT).show();
            isBirthDateSelected = false;

            return false;
        } else {

            mSlctdDateTimestamp = mCalendar.getTimeInMillis()/1000;
            mTvBirthday.setText(GeneralFunctions.getDateFromTimestamp(mSlctdDateTimestamp));
//            mTvBirthday.setTextColor(Color.BLACK);
            isBirthDateSelected = true;

            return true;
        }
        return false;
    }

    // Show AlertDialog for AGREE to Terms And Conditions
    private void showTermsAndConditionsAlertDialog() {

        mAlrtDlgTermsCondtns.setTitle(getString(R.string.termsandcondition_sml));
        mAlrtDlgTermsCondtns.setMessage(mTermsAndConditions);
        mAlrtDlgTermsCondtns.setCancelable(false);
        mAlrtDlgTermsCondtns.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.agree_sml).toUpperCase(), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                dialogInterface.dismiss();
                mChckbxAgreeTerms.setChecked(true);
            }
        });

        if(!mAlrtDlgTermsCondtns.isShowing())
            mAlrtDlgTermsCondtns.show();
    }

    private void getTermsAndConditions() {

        if(mPrgrsbrMain.getVisibility() != View.VISIBLE) {
            if (GeneralFunctions.isNetConnected(this))
                callTermsAndConditionsAPI();
            else Snackbar.make(mCrdntrlyot, mNoInternetConnMsg, Snackbar.LENGTH_LONG).show();
        }
    }

    private void callTermsAndConditionsAPI() {

        mPrgrsbrMain.setVisibility(View.VISIBLE);

        APILogin objApiLogin = APIRetroBuilder.getRetroBuilder(true).create(APILogin.class);
        Call<MdlTermsAndConditionsRes> callMdlTermsCondtnRes = objApiLogin.callGetTermsAndConditions();
        callMdlTermsCondtnRes.enqueue(new Callback<MdlTermsAndConditionsRes>() {
            @Override
            public void onResponse(Call<MdlTermsAndConditionsRes> call, Response<MdlTermsAndConditionsRes> response) {

                mPrgrsbrMain.setVisibility(View.GONE);
                if(response.isSuccessful()) {

                    try {
                        if(response.body().isSuccess()) {

                            mTermsAndConditions = "";
                            try {
                                // Generate BusinessHours list String
                                StringBuilder stringBusinsHrsLst = new StringBuilder();
                                if(response.body().getStrArryData() != null) {
                                    for (String str : response.body().getStrArryData()) {

                                        if (str.length() > 1) // to avoid "." at last element
                                            stringBusinsHrsLst.append(str).append("\n");
                                    }
                                }
                                mTermsAndConditions = stringBusinsHrsLst.toString();

                                showTermsAndConditionsAlertDialog();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else
                            Snackbar.make(mCrdntrlyot, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                    } catch (Exception e) {

                        e.printStackTrace();
                        LogHelper.printLog(AppConstants.LOGTYPE_ERROR, TAG, e.getMessage());
                        Snackbar.make(mCrdntrlyot, mInternalPrblmMsg, Snackbar.LENGTH_LONG).show();
                    }
                } else
                    Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<MdlTermsAndConditionsRes> call, Throwable t) {

                mPrgrsbrMain.setVisibility(View.GONE);
                Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    private void callSignupAPI() {

        mPrgrsbrMain.setVisibility(View.VISIBLE);

        MdlUserCreateReq objMdlUserCreateReq = new MdlUserCreateReq();
        objMdlUserCreateReq.setFirstName(mEtFirstname.getText().toString().trim());
        objMdlUserCreateReq.setLastName(mEtLastname.getText().toString().trim());
        objMdlUserCreateReq.setEmailId(mEtEmail.getText().toString().trim());
        objMdlUserCreateReq.setUserName(mEtEmail.getText().toString().trim());
        objMdlUserCreateReq.setPassword(mEtPaswrd.getText().toString().trim());
        objMdlUserCreateReq.setPhoneNo(mEtPhoneno.getText().toString().trim());
        objMdlUserCreateReq.setCompanyName(mEtCompany.getText().toString().trim());
        objMdlUserCreateReq.setGender(mSpnrGender.getSelectedItem().toString());
        objMdlUserCreateReq.setDateOfBirth(GeneralFunctions.getDateTimeFromTimestamp(mSlctdDateTimestamp));
        objMdlUserCreateReq.setDefaultLanguageId(mArrylstMdlLanguageData.get(mSpnrLanguage.getSelectedItemPosition()).getValue());
        objMdlUserCreateReq.setFacebookLogin(!TextUtils.isEmpty(mFacebookId));
        objMdlUserCreateReq.setFacebookId(mFacebookId);
        objMdlUserCreateReq.setMobileNo(mEtPhoneno.getText().toString().trim());
        objMdlUserCreateReq.setActive(true);
        objMdlUserCreateReq.setUserTypeId(AppConstants.STATIC_USER_TYPE_ID);
        objMdlUserCreateReq.setCustomerTypeId(AppConstants.STATIC_CUSTOMER_TYPE_ID);
        objMdlUserCreateReq.setAgreeTC(mChckbxAgreeTerms.isChecked());

        APIUser objApiUser = APIRetroBuilder.getRetroBuilder(true).create(APIUser.class);
        Call<MdlUserCreateRes> callMdlUserCreateRes = objApiUser.callUserCreate(objMdlUserCreateReq);
        callMdlUserCreateRes.enqueue(new Callback<MdlUserCreateRes>() {
            @Override
            public void onResponse(Call<MdlUserCreateRes> call, Response<MdlUserCreateRes> response) {

                mPrgrsbrMain.setVisibility(View.GONE);
                if(response.isSuccessful()) {

                    try {
                        if(response.body().isSuccess()) {

                            Toast.makeText(ActvtySignup.this, response.body().getData(), Toast.LENGTH_SHORT).show();

                            mIsFacbookSignin = false;

                            Intent resultIntent = new Intent();
                            resultIntent.putExtra(AppConstants.PUT_EXTRA_ISFACEBOOKSIGNIN, mIsFacbookSignin);
                            setResult(RESULT_OK, resultIntent);
                            finish();
                        } else
                            Snackbar.make(mCrdntrlyot, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                    } catch (Exception e) {

                        e.printStackTrace();
                        LogHelper.printLog(AppConstants.LOGTYPE_ERROR, TAG, e.getMessage());
                        Snackbar.make(mCrdntrlyot, mInternalPrblmMsg, Snackbar.LENGTH_LONG).show();
                    }
                } else
                    Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<MdlUserCreateRes> call, Throwable t) {

                mPrgrsbrMain.setVisibility(View.GONE);
                Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setResult(RESULT_CANCELED);
    }
}
