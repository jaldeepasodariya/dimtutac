package com.dimtutac.mobileapp;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.dimtutac.mobileapp.common.AppConstants;
import com.dimtutac.mobileapp.common.GeneralFunctions;
import com.dimtutac.mobileapp.common.LogHelper;
import com.dimtutac.mobileapp.models.contactus.MdlCntctusPostReq;
import com.dimtutac.mobileapp.models.contactus.MdlCntctusPostRes;
import com.dimtutac.mobileapp.retrofitapi.APIContactus;
import com.dimtutac.mobileapp.retrofitapi.APIRetroBuilder;
import com.dimtutac.mobileapp.utils.ValidationUtils;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Jalotsav on 10/14/2017.
 */

public class ActvtyContact extends AppCompatActivity {

    private static final String TAG = ActvtyContact.class.getSimpleName();

    @BindView(R.id.cordntrlyot_actvty_contact) CoordinatorLayout mCrdntrlyot;
    @BindView(R.id.tv_actvty_contact_branch_title) TextView mTvBranchTitle;
    @BindView(R.id.tv_actvty_contact_branch_addrs) TextView mTvBranchAddress;
    @BindView(R.id.tv_actvty_contact_branch_phoneno) TextView mTvBranchPhoneNo;
    @BindView(R.id.et_actvty_contact_title) EditText mEtTitle;
    @BindView(R.id.et_actvty_contact_name) EditText mEtName;
    @BindView(R.id.et_actvty_contact_phoneno) EditText mEtPhoneno;
    @BindView(R.id.et_actvty_contact_email) EditText mEtEmail;
    @BindView(R.id.et_actvty_contact_message) EditText mEtMessage;
    @BindView(R.id.prgrsbr_actvty_contact) ProgressBar mPrgrsbrMain;

    @BindString(R.string.no_intrnt_cnctn) String mNoInternetConnMsg;
    @BindString(R.string.server_problem_sml) String mServerPrblmMsg;
    @BindString(R.string.internal_problem_sml) String mInternalPrblmMsg;
    @BindString(R.string.invalid_phone_number_sml) String mInvldPhoneNo;
    @BindString(R.string.entr_the_title_sml) String mEntrTitle;
    @BindString(R.string.entr_your_name_sml) String mEntrName;
    @BindString(R.string.entr_your_email_sml) String mEntrEmail;

    String mBranchId, mBranchTitle, mBranchAddress, mBranchPhoneNo;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lo_actvty_contact);
        ButterKnife.bind(this);

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) { e.printStackTrace(); }

        mBranchId = getIntent().getStringExtra(AppConstants.PUT_EXTRA_BRANCH_ID);
        mBranchTitle = getIntent().getStringExtra(AppConstants.PUT_EXTRA_TITLE);
        mBranchAddress = getIntent().getStringExtra(AppConstants.PUT_EXTRA_ADDRESS);
        mBranchPhoneNo = getIntent().getStringExtra(AppConstants.PUT_EXTRA_PHONENO);

        // Set Branch Details
        mTvBranchTitle.setText(mBranchTitle);
        mTvBranchAddress.setText(mBranchAddress);
        mTvBranchPhoneNo.setText(mBranchPhoneNo);
    }

    @OnClick({R.id.appcmptbtn_actvty_contact_branch_call, R.id.appcmptbtn_actvty_contact_send})
    public void onClickView(View view) {

        switch (view.getId()) {
            case R.id.appcmptbtn_actvty_contact_branch_call:

                if(TextUtils.isEmpty(mBranchPhoneNo))
                    Snackbar.make(mCrdntrlyot, mInvldPhoneNo, Snackbar.LENGTH_LONG).show();
                else
                    GeneralFunctions.openDialerToCall(this, mBranchPhoneNo);
                break;
            case R.id.appcmptbtn_actvty_contact_send:

                checkAllValidation();
                break;
        }
    }

    private void checkAllValidation() {

        if (!ValidationUtils.validateEmpty(this, mCrdntrlyot, mEtTitle, mEntrTitle))
            return;

        if (!ValidationUtils.validateEmpty(this, mCrdntrlyot, mEtName, mEntrName))
            return;

        if (!ValidationUtils.validateMobile(this, mCrdntrlyot, mEtPhoneno))
            return;

        if (!ValidationUtils.validateEmpty(this, mCrdntrlyot, mEtEmail, mEntrEmail))
            return;

        if (!ValidationUtils.validateEmailFormat(this, mCrdntrlyot, mEtEmail))
            return;

        if (GeneralFunctions.isNetConnected(this))
            callContactusPostAPI();
        else Snackbar.make(mCrdntrlyot, mNoInternetConnMsg, Snackbar.LENGTH_LONG).show();
    }

    private void callContactusPostAPI() {

        mPrgrsbrMain.setVisibility(View.VISIBLE);

        MdlCntctusPostReq objMdlCntctusPostReq = new MdlCntctusPostReq();
        objMdlCntctusPostReq.setTitle(mEtTitle.getText().toString().trim());
        objMdlCntctusPostReq.setName(mEtName.getText().toString().trim());
        objMdlCntctusPostReq.setTelephone(mEtPhoneno.getText().toString().trim());
        objMdlCntctusPostReq.setEmail(mEtEmail.getText().toString().trim());
        objMdlCntctusPostReq.setMessage(TextUtils.isEmpty(mEtMessage.getText().toString().trim())? "": mEtMessage.getText().toString().trim());
        objMdlCntctusPostReq.setBranchId(mBranchId);

        APIContactus objApiCntctus = APIRetroBuilder.getRetroBuilder(true).create(APIContactus.class);
        Call<MdlCntctusPostRes> callMdlCntctusPostRes = objApiCntctus.callContactusPost(objMdlCntctusPostReq);
        callMdlCntctusPostRes.enqueue(new Callback<MdlCntctusPostRes>() {
            @Override
            public void onResponse(Call<MdlCntctusPostRes> call, Response<MdlCntctusPostRes> response) {

                mPrgrsbrMain.setVisibility(View.GONE);
                if(response.isSuccessful()) {

                    try {
                        if(response.body().isSuccess()) {

                            Toast.makeText(ActvtyContact.this, response.body().getData(), Toast.LENGTH_SHORT).show();
                            onBackPressed();
                        } else
                            Snackbar.make(mCrdntrlyot, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                    } catch (Exception e) {

                        e.printStackTrace();
                        LogHelper.printLog(AppConstants.LOGTYPE_ERROR, TAG, e.getMessage());
                        Snackbar.make(mCrdntrlyot, mInternalPrblmMsg, Snackbar.LENGTH_LONG).show();
                    }
                } else
                    Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<MdlCntctusPostRes> call, Throwable t) {

                mPrgrsbrMain.setVisibility(View.GONE);
                Snackbar.make(mCrdntrlyot, mServerPrblmMsg, Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
