package com.dimtutac.mobileapp.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.view.PagerAdapter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dimtutac.mobileapp.R;
import com.dimtutac.mobileapp.common.AppConstants;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Jalotsav on 1/24/2018.
 */

public class VwpgrSliderImagesViewAdapter extends PagerAdapter {

    private Context mContext;
    private ArrayList<String> mArrylstImagePath;
    private Drawable mDrwblDefault;

    public VwpgrSliderImagesViewAdapter(Context context, ArrayList<String> arrylstImagePath, Drawable drwblDefault) {

        this.mContext = context;
        this.mArrylstImagePath = arrylstImagePath;
        mDrwblDefault = drwblDefault;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View itemView = inflater.inflate(R.layout.lo_vwpgr_item_branch_newmonthlyitems_dialog, container, false);
        ImageView mImgvwImage = itemView.findViewById(R.id.imgvw_vwpgr_item_branch_newmonthlyitems_dialog_image);
        TextView mTvMenuDtls = itemView.findViewById(R.id.tv_vwpgr_item_branch_newmonthlyitems_dialog_menudtls);

        String imagePath = mArrylstImagePath.get(position);
        if (!TextUtils.isEmpty(imagePath)) {
            Picasso.with(mContext)
                    .load(AppConstants.ATCHMNT_ROOT_URL.concat(imagePath))
                    .placeholder(mDrwblDefault)
                    .into(mImgvwImage);
        }
        mTvMenuDtls.setVisibility(View.GONE);
        container.addView(itemView);

        return itemView;
    }

    @Override
    public int getCount() {
        return mArrylstImagePath.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
