package com.dimtutac.mobileapp.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dimtutac.mobileapp.R;
import com.dimtutac.mobileapp.common.AppConstants;
import com.dimtutac.mobileapp.models.MdlPhotoURL;
import com.dimtutac.mobileapp.models.checkin.MdlCheckinListResData;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Jalotsav on 1/25/2018.
 */

public class RcyclrCheckinListAdapter extends RecyclerView.Adapter<RcyclrCheckinListAdapter.ViewHolder> {

    private Context mContext;
    private ArrayList<MdlCheckinListResData> mArrylstMdlCheckinLstData;
    private ArrayList<String> mArrylstCheckinPhotos;
    private Drawable mDrwblDefault;
    private RecyclerView.LayoutManager mLayoutManager;
    private RcyclrPhotosAdapter mAdapter;

    public RcyclrCheckinListAdapter(Context context, ArrayList<MdlCheckinListResData> arrylstMdlCheckin, Drawable drwblDefault) {
        super();

        this.mContext = context;
        this.mArrylstMdlCheckinLstData = arrylstMdlCheckin;
        this.mDrwblDefault = drwblDefault;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.lo_recyclritem_checkinlst, viewGroup, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {

        final MdlCheckinListResData objMdlCheckinlstData = mArrylstMdlCheckinLstData.get(i);

        viewHolder.tvCheckinDateTime.setText(objMdlCheckinlstData.getCheckInDateTime());
        viewHolder.tvWritingStatus.setText(objMdlCheckinlstData.getWritingStatus());
        viewHolder.tvBranchName.setText(objMdlCheckinlstData.getBranchName());
        if(!TextUtils.isEmpty(objMdlCheckinlstData.getBranchPhotoURL())) {
            Picasso.with(mContext)
                    .load(AppConstants.ATCHMNT_ROOT_URL.concat(objMdlCheckinlstData.getBranchPhotoURL()))
                    .placeholder(mDrwblDefault)
                    .into(viewHolder.mImgvwImage);
        }

        mLayoutManager = new GridLayoutManager(mContext, 3);
        viewHolder.mRecyclerView.setHasFixedSize(true);
        viewHolder.mRecyclerView.setLayoutManager(mLayoutManager);

        mArrylstCheckinPhotos = new ArrayList<>();
        for(MdlPhotoURL objMdlPhotoUrl : objMdlCheckinlstData.getArrylstMdlPhotoURL()) {
            mArrylstCheckinPhotos.add(objMdlPhotoUrl.getPhotoURL());
        }

        mAdapter = new RcyclrPhotosAdapter(mContext, false, false, AppConstants.IMAGE_PATH_TYPE_SERVER, mArrylstCheckinPhotos, mDrwblDefault);
        viewHolder.mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public int getItemCount() {
        return mArrylstMdlCheckinLstData.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        ImageView mImgvwImage;
        TextView tvCheckinDateTime, tvWritingStatus, tvBranchName;
        RecyclerView mRecyclerView;

        ViewHolder(View itemView) {
            super(itemView);

            mImgvwImage = itemView.findViewById(R.id.imgvw_recylrvw_item_checkinlst_image);
            tvCheckinDateTime = itemView.findViewById(R.id.tv_recylrvw_item_checkinlst_checkindatetime);
            tvWritingStatus = itemView.findViewById(R.id.tv_recylrvw_item_checkinlst_writingstatus);
            tvBranchName = itemView.findViewById(R.id.tv_recylrvw_item_checkinlst_branchname);
            mRecyclerView = itemView.findViewById(R.id.rcyclrvw_recylrvw_item_checkinlst_checkinphotos);
        }
    }
}
