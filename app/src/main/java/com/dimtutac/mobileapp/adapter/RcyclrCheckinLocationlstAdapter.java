package com.dimtutac.mobileapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dimtutac.mobileapp.ActvtyCheckinDo;
import com.dimtutac.mobileapp.R;
import com.dimtutac.mobileapp.common.AppConstants;
import com.dimtutac.mobileapp.models.branch.MdlLocationListResData;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Jalotsav on 11/10/2017.
 */

public class RcyclrCheckinLocationlstAdapter extends RecyclerView.Adapter<RcyclrCheckinLocationlstAdapter.ViewHolder> {

    private Context mContext;
    private ArrayList<MdlLocationListResData> mArrylstMdlLocations;
    private Drawable mDrwblDefault;

    public RcyclrCheckinLocationlstAdapter(Context context, ArrayList<MdlLocationListResData> arrylstMdlLocations, Drawable drwblDefault) {
        super();

        this.mContext = context;
        this.mArrylstMdlLocations = arrylstMdlLocations;
        this.mDrwblDefault = drwblDefault;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.lo_recyclritem_checkin_locationlst, viewGroup, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {

        final MdlLocationListResData objMdlLocations = mArrylstMdlLocations.get(i);

        viewHolder.mTvTitle.setText(objMdlLocations.getBranchName());
        viewHolder.mTvAddress.setText(objMdlLocations.getAddressLine1());

        if(!TextUtils.isEmpty(objMdlLocations.getPhotoURL())) {
            Picasso.with(mContext)
                    .load(AppConstants.ATCHMNT_ROOT_URL.concat(objMdlLocations.getPhotoURL()))
                    .placeholder(mDrwblDefault)
                    .into(viewHolder.mImgvwImage);
        }

        viewHolder.mLnrlyotContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intntCheckinDo = new Intent(mContext, ActvtyCheckinDo.class)
                        .putExtra(AppConstants.PUT_EXTRA_BRANCH_ID, objMdlLocations.getPkId())
                        .putExtra(AppConstants.PUT_EXTRA_BRANCH_NAME, objMdlLocations.getBranchName())
                        .putExtra(AppConstants.PUT_EXTRA_RESTAURANT_ID, objMdlLocations.getRestaurantId())
                        .putExtra(AppConstants.PUT_EXTRA_PHOTO_URL, objMdlLocations.getPhotoURL())
                        .putExtra(AppConstants.PUT_EXTRA_LATITUDE, objMdlLocations.getLatitude())
                        .putExtra(AppConstants.PUT_EXTRA_LONGITUDE, objMdlLocations.getLongitude());
                ((Activity) mContext).startActivityForResult(intntCheckinDo, AppConstants.REQUEST_CHECKIN_DO);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mArrylstMdlLocations.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        LinearLayout mLnrlyotContent;
        ImageView mImgvwImage;
        TextView mTvTitle, mTvAddress;

        ViewHolder(View itemView) {
            super(itemView);

            mLnrlyotContent = itemView.findViewById(R.id.lnrlyot_recylrvw_item_checkinloctnlst_content);
            mImgvwImage = itemView.findViewById(R.id.imgvw_recylrvw_item_checkinloctnlst_image);
            mTvTitle = itemView.findViewById(R.id.tv_recylrvw_item_checkinloctnlst_branchname);
            mTvAddress = itemView.findViewById(R.id.tv_recylrvw_item_checkinloctnlst_address);
        }
    }
}
