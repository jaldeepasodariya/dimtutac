package com.dimtutac.mobileapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dimtutac.mobileapp.ActvtyCareerApply;
import com.dimtutac.mobileapp.R;
import com.dimtutac.mobileapp.common.AppConstants;
import com.dimtutac.mobileapp.models.career.MdlPositionListResData;

import java.util.ArrayList;

/**
 * Created by Jalotsav on 10/18/2017.
 */

public class RcyclrPositionsAdapter extends RecyclerView.Adapter<RcyclrPositionsAdapter.ViewHolder> {

    private Context mContext;
    private ArrayList<MdlPositionListResData> mArrylstMdlPostns;
    private Drawable mDrwblDefault;

    public RcyclrPositionsAdapter(Context context, ArrayList<MdlPositionListResData> arrylstMdlPositions, Drawable drwblDefault) {
        super();

        this.mContext = context;
        this.mArrylstMdlPostns = arrylstMdlPositions;
        mDrwblDefault = drwblDefault;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.lo_recyclritem_positionlst, viewGroup, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {

        final MdlPositionListResData objMdlLocations = mArrylstMdlPostns.get(i);

        viewHolder.mTvPostnName.setText(objMdlLocations.getPositionName());
        viewHolder.mTvDescrptn.setText(objMdlLocations.getDescription());

        /*Picasso.with(mContext)
                .load(objMdlLocations.getImagePath())
                .placeholder(mDrwblDefault)
                .into(viewHolder.mImgvwImage);*/

        viewHolder.mAppcpmtbtnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!TextUtils.isEmpty(objMdlLocations.getPkId()) && !TextUtils.isEmpty(objMdlLocations.getBranchId())) {

                    // Generate Description list String
                    StringBuilder stringDescptnLst = new StringBuilder();
                    for(String str : objMdlLocations.getStrArryDescriptionList()) {

                        if(str.length() > 1) // to avoid "." at last element
                            stringDescptnLst.append("- ").append(str).append("\n");
                    }

                    mContext.startActivity(new Intent(mContext, ActvtyCareerApply.class)
                            .putExtra(AppConstants.PUT_EXTRA_POSITION_ID, objMdlLocations.getPkId())
                            .putExtra(AppConstants.PUT_EXTRA_BRANCH_ID, objMdlLocations.getBranchId())
                            .putExtra(AppConstants.PUT_EXTRA_BRANCH_NAME, objMdlLocations.getBranchName())
                            .putExtra(AppConstants.PUT_EXTRA_POSITION_NAME, objMdlLocations.getPositionName())
                            .putExtra(AppConstants.PUT_EXTRA_DESCRPTN_LIST_STR, stringDescptnLst.toString()));
                } else
                    Toast.makeText(mContext, mContext.getString(R.string.no_sfcntdata_avlbl_sml), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mArrylstMdlPostns.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ImageView mImgvwImage;
        TextView mTvPostnName, mTvDescrptn;
        AppCompatButton mAppcpmtbtnApply;

        ViewHolder(View itemView) {
            super(itemView);

            mImgvwImage = itemView.findViewById(R.id.imgvw_recylrvw_item_postnlst_image);
            mTvPostnName = itemView.findViewById(R.id.tv_recylrvw_item_postnlst_name);
            mTvDescrptn = itemView.findViewById(R.id.tv_recylrvw_item_postnlst_descrptn);
            mAppcpmtbtnApply = itemView.findViewById(R.id.appcmptbtn_recylrvw_item_postnlst_apply);
        }
    }
}
