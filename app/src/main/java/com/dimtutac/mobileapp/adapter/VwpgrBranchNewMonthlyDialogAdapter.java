package com.dimtutac.mobileapp.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.view.PagerAdapter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dimtutac.mobileapp.R;
import com.dimtutac.mobileapp.common.AppConstants;
import com.dimtutac.mobileapp.models.menu.MdlNewMonthlyItemListResData;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Jalotsav on 11/6/2017.
 */

public class VwpgrBranchNewMonthlyDialogAdapter extends PagerAdapter {

    private Context mContext;
    private ArrayList<MdlNewMonthlyItemListResData> mArrylstMdlNewMonthlyItems;
    private Drawable mDrwblDefault;

    public VwpgrBranchNewMonthlyDialogAdapter(Context context, ArrayList<MdlNewMonthlyItemListResData> arrylstMdlNewMonthlyItemLists, Drawable drwblDefault) {

        this.mContext = context;
        this.mArrylstMdlNewMonthlyItems = arrylstMdlNewMonthlyItemLists;
        mDrwblDefault = drwblDefault;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View itemView = inflater.inflate(R.layout.lo_vwpgr_item_branch_newmonthlyitems_dialog, container, false);
        ImageView mImgvwImage = itemView.findViewById(R.id.imgvw_vwpgr_item_branch_newmonthlyitems_dialog_image);
        TextView mTvMenuDtls = itemView.findViewById(R.id.tv_vwpgr_item_branch_newmonthlyitems_dialog_menudtls);

        MdlNewMonthlyItemListResData objMdlNewMonthlyItem = mArrylstMdlNewMonthlyItems.get(position);
        if (!TextUtils.isEmpty(objMdlNewMonthlyItem.getPhotoURL())) {
            Picasso.with(mContext)
                    .load(AppConstants.ATCHMNT_ROOT_URL.concat(objMdlNewMonthlyItem.getPhotoURL()))
                    .placeholder(mDrwblDefault)
                    .into(mImgvwImage);
        }
        if(TextUtils.isEmpty(objMdlNewMonthlyItem.getPrice()))
            objMdlNewMonthlyItem.setPrice("");
        if(TextUtils.isEmpty(objMdlNewMonthlyItem.getCurrency()))
            objMdlNewMonthlyItem.setCurrency("");
        mTvMenuDtls.setText(mContext.getResources().getString(R.string.menunm_price_curncy_vals,
                objMdlNewMonthlyItem.getMenuName(),
                objMdlNewMonthlyItem.getPrice(),
                objMdlNewMonthlyItem.getCurrency()));
        container.addView(itemView);

        return itemView;
    }

    @Override
    public int getCount() {
        return mArrylstMdlNewMonthlyItems.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
