package com.dimtutac.mobileapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dimtutac.mobileapp.ActvtyNewsDetails;
import com.dimtutac.mobileapp.R;
import com.dimtutac.mobileapp.common.AppConstants;
import com.dimtutac.mobileapp.models.news.MdlNewsListResData;
import com.dimtutac.mobileapp.navgtndrawer.FrgmntNews;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;

/**
 * Created by Jalotsav on 12/8/2017.
 */

public class RcyclrNewsAdapter  extends RecyclerView.Adapter<RcyclrNewsAdapter.ViewHolder> {

    private Context mContext;
    private FrgmntNews mFrgmntNews;
    private ArrayList<MdlNewsListResData> mArrylstMdlNews;
    private Drawable mDrwblDefault;

    public RcyclrNewsAdapter(Context context, FrgmntNews frgmntNews, ArrayList<MdlNewsListResData> arrylstMdlNews, Drawable drwblDefault) {
        super();

        this.mContext = context;
        this.mFrgmntNews = frgmntNews;
        this.mArrylstMdlNews = arrylstMdlNews;
        mDrwblDefault = drwblDefault;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.lo_recyclritem_news, viewGroup, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int i) {

        final MdlNewsListResData objMdlNews = mArrylstMdlNews.get(i);

        viewHolder.mTvTitle.setText(objMdlNews.getTitle());
        viewHolder.mTvDate.setText(objMdlNews.getfDate());

        if(!TextUtils.isEmpty(objMdlNews.getPhotoURL())) {
            Picasso.with(mContext)
                    .load(AppConstants.ATCHMNT_ROOT_URL.concat(objMdlNews.getPhotoURL()))
                    .placeholder(mDrwblDefault)
                    .into(viewHolder.mImgvwImage);
        }

        viewHolder.mRltvlyotContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mContext.startActivity(new Intent(mContext, ActvtyNewsDetails.class)
                        .putExtra(AppConstants.PUT_EXTRA_NEWS_ID, objMdlNews.getPkId())
                        .putExtra(AppConstants.PUT_EXTRA_TITLE, objMdlNews.getTitle())
                        .putExtra(AppConstants.PUT_EXTRA_PHOTO_URL, objMdlNews.getPhotoURL())
                        .putExtra(AppConstants.PUT_EXTRA_FROM_DATE, objMdlNews.getfDate())
                        .putExtra(AppConstants.PUT_EXTRA_DESCRPTN, objMdlNews.getDescription()));
            }
        });
        viewHolder.mImgvwShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!TextUtils.isEmpty(objMdlNews.getPhotoURL())) {
                    Picasso.with(mContext)
                        .load(AppConstants.ATCHMNT_ROOT_URL.concat(objMdlNews.getPhotoURL()))
                        .placeholder(mDrwblDefault)
                        .into(new Target() {
                            @Override
                            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                                objMdlNews.setPhotoBitmap(bitmap); // Set Bitmap to Model class object
                            }

                            @Override
                            public void onBitmapFailed(Drawable errorDrawable) {

                            }

                            @Override
                            public void onPrepareLoad(Drawable placeHolderDrawable) {

                            }
                        });
                }

                // Check Storage permission and Share news
                mFrgmntNews.checkAppPermission(objMdlNews);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mArrylstMdlNews.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        RelativeLayout mRltvlyotContent;
        ImageView mImgvwImage, mImgvwShare;
        TextView mTvTitle, mTvDate;

        ViewHolder(View itemView) {
            super(itemView);

            mRltvlyotContent = itemView.findViewById(R.id.rltvlyot_recylrvw_item_news_content);
            mImgvwImage = itemView.findViewById(R.id.imgvw_recylrvw_item_news_image);
            mImgvwShare = itemView.findViewById(R.id.imgvw_recylrvw_item_news_share);
            mTvTitle = itemView.findViewById(R.id.tv_recylrvw_item_news_title);
            mTvDate = itemView.findViewById(R.id.tv_recylrvw_item_news_date);
        }
    }
}
