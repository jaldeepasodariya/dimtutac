package com.dimtutac.mobileapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dimtutac.mobileapp.ActvtyBranchNewMonthlyItems;
import com.dimtutac.mobileapp.R;
import com.dimtutac.mobileapp.common.AppConstants;
import com.dimtutac.mobileapp.common.UserSessionManager;
import com.dimtutac.mobileapp.models.branch.MdlLocationListResData;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Jalotsav on 11/2/2017.
 */

public class RcyclrNewMonthlyItemsAdapter extends RecyclerView.Adapter<RcyclrNewMonthlyItemsAdapter.ViewHolder> {

    private Context mContext;
    private ArrayList<MdlLocationListResData> mArrylstMdlLocations;
    private Drawable mDrwblDefault;

    public RcyclrNewMonthlyItemsAdapter(Context context, ArrayList<MdlLocationListResData> arrylstMdlLocations, Drawable drwblDefault) {
        super();

        this.mContext = context;
        this.mArrylstMdlLocations = arrylstMdlLocations;
        mDrwblDefault = drwblDefault;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.lo_recyclritem_newmonthlyitems, viewGroup, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {

        final MdlLocationListResData objMdlLocations = mArrylstMdlLocations.get(i);

        viewHolder.mTvTitle.setText(objMdlLocations.getBranchName());

        if(!TextUtils.isEmpty(objMdlLocations.getPhotoURL())) {
            Picasso.with(mContext)
                    .load(AppConstants.ATCHMNT_ROOT_URL.concat(objMdlLocations.getPhotoURL()))
                    .placeholder(mDrwblDefault)
                    .into(viewHolder.mImgvwImage);
        }

        viewHolder.mRltvlyotContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mContext.startActivity(new Intent(mContext, ActvtyBranchNewMonthlyItems.class)
                        .putExtra(AppConstants.PUT_EXTRA_BRANCH_ID, objMdlLocations.getPkId())
                        .putExtra(AppConstants.PUT_EXTRA_BRANCH_NAME, objMdlLocations.getBranchName()));
            }
        });
    }

    @Override
    public int getItemCount() {
        return mArrylstMdlLocations.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        RelativeLayout mRltvlyotContent;
        ImageView mImgvwImage;
        TextView mTvTitle;

        ViewHolder(View itemView) {
            super(itemView);

            mRltvlyotContent = itemView.findViewById(R.id.rltvlyot_recylrvw_item_newmonthlyitems_content);
            mImgvwImage = itemView.findViewById(R.id.imgvw_recylrvw_item_newmonthlyitems_image);
            mTvTitle = itemView.findViewById(R.id.tv_recylrvw_item_newmonthlyitems_branchname);
        }
    }
}
