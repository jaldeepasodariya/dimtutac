package com.dimtutac.mobileapp.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dimtutac.mobileapp.ActvtyReservationEdit;
import com.dimtutac.mobileapp.R;
import com.dimtutac.mobileapp.common.AppConstants;
import com.dimtutac.mobileapp.models.reservation.MdlResrvtnListResData;
import com.dimtutac.mobileapp.navgtndrawer.FrgmntResrvtnLst;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Jalotsav on 30/8/17.
 */

public class RcyclrResrvtnListAdapter extends RecyclerView.Adapter<RcyclrResrvtnListAdapter.ViewHolder> {

    private Context mContext;
    private ArrayList<MdlResrvtnListResData> mArrylstMdlLocations;
    private Drawable mDrwblDefault;
    private FrgmntResrvtnLst mFrgmntResrvtnlst;

    public RcyclrResrvtnListAdapter(Context context, ArrayList<MdlResrvtnListResData> arrylstMdlLocations, Drawable drwblDefault, FrgmntResrvtnLst frgmntResrvtnLst) {
        super();

        this.mContext = context;
        this.mArrylstMdlLocations = arrylstMdlLocations;
        this.mDrwblDefault = drwblDefault;
        this.mFrgmntResrvtnlst = frgmntResrvtnLst;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.lo_recyclritem_resrvtnlst, viewGroup, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {

        final MdlResrvtnListResData objMdlResrvtnlstData = mArrylstMdlLocations.get(i);

        viewHolder.tvTitle.setText(objMdlResrvtnlstData.getBranchName());
        viewHolder.tvCustmrInfo.setText(objMdlResrvtnlstData.getName());
        viewHolder.tvDateTime.setText(parseDate(objMdlResrvtnlstData.getDate()).concat(" ").concat(objMdlResrvtnlstData.getFromTime()));
        viewHolder.tvGuest.setText(String.valueOf(objMdlResrvtnlstData.getNoOfGuest()));
        if(!TextUtils.isEmpty(objMdlResrvtnlstData.getPhotoURL())) {
            Picasso.with(mContext)
                    .load(AppConstants.ATCHMNT_ROOT_URL.concat(objMdlResrvtnlstData.getPhotoURL()))
                    .placeholder(mDrwblDefault)
                    .into(viewHolder.mImgvwImage);
        }
        viewHolder.tvStatus.setText(objMdlResrvtnlstData.getStatusName());
        viewHolder.tvStatus.setTextColor(getStatusColorResId(objMdlResrvtnlstData.getStatus()));

        // Currently "Cancel Reservation" button is HIDE from UI (already set from layout file)
        /*if(!objMdlResrvtnlstData.isAllowUpdate())
            viewHolder.mAppcpmtbtnCancel.setVisibility(View.GONE);*/

        viewHolder.mAppcpmtbtnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AppConstants.ARRYLST_MDL_RESRVTNLIST_RESDATA.clear();
                AppConstants.ARRYLST_MDL_RESRVTNLIST_RESDATA.add(objMdlResrvtnlstData);

                // Status=NEW then only EDIT Reservation is allow.
                boolean isAllowUpdate = objMdlResrvtnlstData.getStatus().equalsIgnoreCase(AppConstants.STATIC_STATUS_ID_RESRVTN_NEW);
                mContext.startActivity(new Intent(mContext, ActvtyReservationEdit.class)
                        .putExtra(AppConstants.PUT_EXTRA_IS_ALLOW_UPDATE, isAllowUpdate));
            }
        });
        viewHolder.mAppcpmtbtnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                confirmCancelReservationAlertDialog(objMdlResrvtnlstData);
            }
        });
    }

    // Show AlertDialog for confirm to Logout
    private void confirmCancelReservationAlertDialog(final MdlResrvtnListResData objMdlResrvtnlstData) {

        AlertDialog.Builder alrtDlg = new AlertDialog.Builder(mContext);
        alrtDlg.setTitle(mContext.getString(R.string.cancel_sml));
        alrtDlg.setMessage(mContext.getString(R.string.resrvtn_cancel_alrtdlg_msg));
        alrtDlg.setNegativeButton(mContext.getString(R.string.no_sml).toUpperCase(), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        alrtDlg.setPositiveButton(mContext.getString(R.string.yes_sml).toUpperCase(), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                mFrgmntResrvtnlst.cancelReservation(objMdlResrvtnlstData);
            }
        });

        alrtDlg.show();
    }

    // Get color resourceId as per statusId
    private int getStatusColorResId(String statusId) {

        if(statusId.equalsIgnoreCase(AppConstants.STATIC_STATUS_ID_RESRVTN_NEW))
            return ContextCompat.getColor(mContext, R.color.colorPrimaryBlue);
        else if(statusId.equalsIgnoreCase(AppConstants.STATIC_STATUS_ID_RESRVTN_COMPLETE))
            return ContextCompat.getColor(mContext, R.color.colorPrimaryGreen);
        else if(statusId.equalsIgnoreCase(AppConstants.STATIC_STATUS_ID_RESRVTN_CANCEL))
            return ContextCompat.getColor(mContext, R.color.colorPrimaryRed);
        else
            return ContextCompat.getColor(mContext, R.color.colorPrimaryBlue);
    }

    @Override
    public int getItemCount() {
        return mArrylstMdlLocations.size();
    }

    private String parseDate(String strDate) {
        String inputPattern = "yyyy-MM-dd'T'HH:mm:ss";
        String outputPattern = "dd MMM yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern, Locale.getDefault());
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern, Locale.getDefault());

        Date date;
        String str = "";

        try {
            date = inputFormat.parse(strDate);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        ImageView mImgvwImage;
        TextView tvTitle, tvCustmrInfo, tvDateTime, tvGuest, tvStatus;
        AppCompatButton mAppcpmtbtnEdit, mAppcpmtbtnCancel;

        ViewHolder(View itemView) {
            super(itemView);

            mImgvwImage = itemView.findViewById(R.id.imgvw_recylrvw_item_resrvtnlst_image);
            tvTitle = itemView.findViewById(R.id.tv_recylrvw_item_resrvtnlst_title);
            tvCustmrInfo = itemView.findViewById(R.id.tv_recylrvw_item_resrvtnlst_custmrinfo);
            tvDateTime = itemView.findViewById(R.id.tv_recylrvw_item_resrvtnlst_datetime);
            tvGuest = itemView.findViewById(R.id.tv_recylrvw_item_resrvtnlst_guest);
            tvStatus = itemView.findViewById(R.id.tv_recylrvw_item_resrvtnlst_status);
            mAppcpmtbtnEdit = itemView.findViewById(R.id.appcmptbtn_recylrvw_item_resrvtnlst_edit);
            mAppcpmtbtnCancel = itemView.findViewById(R.id.appcmptbtn_recylrvw_item_resrvtnlst_cancel);
        }
    }
}
