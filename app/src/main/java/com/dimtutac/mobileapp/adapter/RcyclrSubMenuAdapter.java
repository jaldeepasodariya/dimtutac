package com.dimtutac.mobileapp.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.dimtutac.mobileapp.ActvtySubMenu;
import com.dimtutac.mobileapp.R;
import com.dimtutac.mobileapp.common.AppConstants;
import com.dimtutac.mobileapp.models.menu.MdlSubMenuListResData;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Jalotsav on 20/11/17.
 */

public class RcyclrSubMenuAdapter extends RecyclerView.Adapter<RcyclrSubMenuAdapter.ViewHolder> {

    private Context mContext;
    private ArrayList<MdlSubMenuListResData> mArrylstMdlSubMenu;
    private Drawable mDrwblDefault;

    public RcyclrSubMenuAdapter(Context context, ArrayList<MdlSubMenuListResData> arrylstMdlSubMenuList, Drawable drwblDefault) {
        super();

        this.mContext = context;
        this.mArrylstMdlSubMenu = arrylstMdlSubMenuList;
        mDrwblDefault = drwblDefault;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.lo_recyclritem_submenu, viewGroup, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        final MdlSubMenuListResData objMdlNewMonthlyItem = mArrylstMdlSubMenu.get(position);

        if(!TextUtils.isEmpty(objMdlNewMonthlyItem.getPhotoURL())) {
            Picasso.with(mContext)
                    .load(AppConstants.ATCHMNT_ROOT_URL.concat(objMdlNewMonthlyItem.getPhotoURL()))
                    .placeholder(mDrwblDefault)
                    .into(viewHolder.mImgvwImage);
        }

        viewHolder.mImgvwImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ((ActvtySubMenu) mContext).setMenuDtlsDialogCurrentItem(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mArrylstMdlSubMenu.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        ImageView mImgvwImage;

        ViewHolder(View itemView) {
            super(itemView);

            mImgvwImage = itemView.findViewById(R.id.imgvw_recylrvw_item_submenu_image);
        }
    }
}
