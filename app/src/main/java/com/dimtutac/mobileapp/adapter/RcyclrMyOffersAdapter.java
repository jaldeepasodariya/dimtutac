package com.dimtutac.mobileapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.dimtutac.mobileapp.ActvtyTermsAndCondition;
import com.dimtutac.mobileapp.R;
import com.dimtutac.mobileapp.common.AppConstants;
import com.dimtutac.mobileapp.models.offer.MdlCustomerOffersResData;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Jalotsav on 1/31/2018.
 */

public class RcyclrMyOffersAdapter  extends RecyclerView.Adapter<RcyclrMyOffersAdapter.ViewHolder> {

    private Context mContext;
    private ArrayList<MdlCustomerOffersResData> mArrylstMdlCustomerOffers;
    private Drawable mDrwblDefault;

    public RcyclrMyOffersAdapter(Context context, ArrayList<MdlCustomerOffersResData> arrylstMdlCustomerOffers, Drawable drwblDefault) {
        super();

        this.mContext = context;
        this.mArrylstMdlCustomerOffers = arrylstMdlCustomerOffers;
        mDrwblDefault = drwblDefault;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.lo_recyclritem_account_myoffers, viewGroup, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        final MdlCustomerOffersResData objMdlCustomerOffers = mArrylstMdlCustomerOffers.get(position);

        if(!TextUtils.isEmpty(objMdlCustomerOffers.getPhoto())) {
            Picasso.with(mContext)
                    .load(AppConstants.ATCHMNT_ROOT_URL.concat(objMdlCustomerOffers.getPhoto()))
                    .placeholder(mDrwblDefault)
                    .into(viewHolder.mImgvwImage);
        }

        viewHolder.mImgvwImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    // Generate TermsAndCondition list String
                    StringBuilder stringTermsAndConditionLst = new StringBuilder();
                    if(objMdlCustomerOffers.getStrArryTermsAndConditionsList() != null) {
                        for (String str : objMdlCustomerOffers.getStrArryTermsAndConditionsList()) {

                            if (str.length() > 1) // to avoid "." at last element
                                stringTermsAndConditionLst.append("- ").append(str).append("\n");
                        }
                    }

                    mContext.startActivity(new Intent(mContext, ActvtyTermsAndCondition.class)
                            .putExtra(AppConstants.PUT_EXTRA_OFFER_ID, objMdlCustomerOffers.getOfferId())
                            .putExtra(AppConstants.PUT_EXTRA_ASSIGNMENT_CODE, objMdlCustomerOffers.getAssignmentCode())
                            .putExtra(AppConstants.PUT_EXTRA_PHOTO_URL, objMdlCustomerOffers.getPhoto())
                            .putExtra(AppConstants.PUT_EXTRA_TERMSANDCONDTN_LIST_STR, stringTermsAndConditionLst.toString()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mArrylstMdlCustomerOffers.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        ImageView mImgvwImage;

        ViewHolder(View itemView) {
            super(itemView);

            mImgvwImage = itemView.findViewById(R.id.imgvw_recylrvw_item_account_myoffers_image);
        }
    }
}
