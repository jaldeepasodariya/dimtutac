package com.dimtutac.mobileapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dimtutac.mobileapp.ActvtyContact;
import com.dimtutac.mobileapp.ActvtySignin;
import com.dimtutac.mobileapp.R;
import com.dimtutac.mobileapp.common.AppConstants;
import com.dimtutac.mobileapp.common.UserSessionManager;
import com.dimtutac.mobileapp.models.branch.MdlLocations;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Jalotsav on 10/14/2017.
 */

public class RcyclrContactListAdapter extends RecyclerView.Adapter<RcyclrContactListAdapter.ViewHolder> {

    private Context mContext;
    private UserSessionManager session;
    private ArrayList<MdlLocations> mArrylstMdlLocations;
    private Drawable mDrwblDefault;

    public RcyclrContactListAdapter(Context context, ArrayList<MdlLocations> arrylstMdlLocations, Drawable drwblDefault) {
        super();

        this.mContext = context;
        session = new UserSessionManager(mContext);
        this.mArrylstMdlLocations = arrylstMdlLocations;
        mDrwblDefault = drwblDefault;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.lo_recyclritem_contactlst, viewGroup, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {

        final MdlLocations objMdlLocations = mArrylstMdlLocations.get(i);

        viewHolder.mTvTitle.setText(objMdlLocations.getTitle());
        viewHolder.mTvAddress.setText(objMdlLocations.getAddress());
        viewHolder.mTvPhoneNo.setText(objMdlLocations.getPhonenumber());
        if(TextUtils.isEmpty(objMdlLocations.getStoreHrs()))
            viewHolder.mTvStoreTimeLbl.setVisibility(View.GONE);
        else {
            viewHolder.mTvStoreTimeLbl.setVisibility(View.VISIBLE);
            viewHolder.mTvStoreTime.setText(objMdlLocations.getStoreHrs());
        }

        if(!TextUtils.isEmpty(objMdlLocations.getImagePath())) {
            Picasso.with(mContext)
                    .load(objMdlLocations.getImagePath())
                    .placeholder(mDrwblDefault)
                    .into(viewHolder.mImgvwImage);
        }

        viewHolder.mLnrlyotMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!session.isUserLoggedIn()) {

                    ((Activity) mContext).startActivityForResult(new Intent(mContext, ActvtySignin.class), AppConstants.REQUEST_SIGNIN);
                } else
                    mContext.startActivity(new Intent(mContext, ActvtyContact.class)
                            .putExtra(AppConstants.PUT_EXTRA_BRANCH_ID, objMdlLocations.getPkId())
                            .putExtra(AppConstants.PUT_EXTRA_TITLE, objMdlLocations.getTitle())
                            .putExtra(AppConstants.PUT_EXTRA_ADDRESS, objMdlLocations.getAddress())
                            .putExtra(AppConstants.PUT_EXTRA_PHONENO, objMdlLocations.getPhonenumber()));
            }
        });
    }

    @Override
    public int getItemCount() {
        return mArrylstMdlLocations.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        LinearLayout mLnrlyotMain;
        ImageView mImgvwImage;
        TextView mTvTitle, mTvAddress, mTvPhoneNo, mTvStoreTimeLbl, mTvStoreTime;

        ViewHolder(View itemView) {
            super(itemView);

            mLnrlyotMain = itemView.findViewById(R.id.lnrlyot_recylrvw_item_main);
            mImgvwImage = itemView.findViewById(R.id.imgvw_recylrvw_item_contactlst_image);
            mTvTitle = itemView.findViewById(R.id.tv_recylrvw_item_contactlst_title);
            mTvAddress = itemView.findViewById(R.id.tv_recylrvw_item_contactlst_addrs);
            mTvPhoneNo = itemView.findViewById(R.id.tv_recylrvw_item_contactlst_phone);
            mTvStoreTimeLbl = itemView.findViewById(R.id.tv_recylrvw_item_contactlst_storetimelbl);
            mTvStoreTime = itemView.findViewById(R.id.tv_recylrvw_item_contactlst_storetime);
        }
    }
}
