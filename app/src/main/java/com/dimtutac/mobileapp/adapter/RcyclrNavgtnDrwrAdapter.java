package com.dimtutac.mobileapp.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dimtutac.mobileapp.R;
import com.dimtutac.mobileapp.common.AppConstants;
import com.dimtutac.mobileapp.common.ItemClickListener;
import com.dimtutac.mobileapp.common.UserSessionManager;
import com.dimtutac.mobileapp.models.MdlNavgtnDrwr;

import java.util.ArrayList;

/**
 * Created by Jalotsav on 2/14/2018.
 */

public class RcyclrNavgtnDrwrAdapter  extends RecyclerView.Adapter<RcyclrNavgtnDrwrAdapter.ViewHolder> {

    private Context mCntext;
    private UserSessionManager session;

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;

    private ArrayList<MdlNavgtnDrwr> mArrylstNavgtnDrwr;
    private ItemClickListener mListener;

    public RcyclrNavgtnDrwrAdapter(Context context, ArrayList<MdlNavgtnDrwr> arrylstMdlNavgtnDrwr, ItemClickListener mListener) {
        mCntext = context;
        session = new UserSessionManager(mCntext);
        mArrylstNavgtnDrwr = new ArrayList<>();
        mArrylstNavgtnDrwr.addAll(arrylstMdlNavgtnDrwr);
        this.mListener = mListener;
    }

    @Override
    public RcyclrNavgtnDrwrAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == TYPE_ITEM) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.lo_recyclritem_drwerlyot_row, parent, false);

            return new ViewHolder(v, viewType);
        } else if (viewType == TYPE_HEADER) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.lo_drwrlyot_header_main, parent, false);

            return new ViewHolder(v, viewType);
        }
        return null;

    }

    @Override
    public void onBindViewHolder(final RcyclrNavgtnDrwrAdapter.ViewHolder holder, int position) {

        if (holder.Holderid == 1) {
            MdlNavgtnDrwr objMdlNavgtnDrwr = mArrylstNavgtnDrwr.get(position);
            if(position == AppConstants.NAVDRWER_WHATDOWEHAVE || position == AppConstants.NAVDRWER_MORE){

                holder.rltvlyotMain.setBackgroundColor(mCntext.getResources().getColor(R.color.colorPrimaryDark));
                holder.imgvwItemIcon.setVisibility(View.INVISIBLE);
            }else{

                holder.rltvlyotMain.setBackgroundColor(mCntext.getResources().getColor(R.color.transparent));
                holder.imgvwItemIcon.setVisibility(View.VISIBLE);
                holder.imgvwItemIcon.setImageDrawable(ContextCompat.getDrawable(mCntext, objMdlNavgtnDrwr.getIconDrawableId()));
            }
            holder.tvItemTitle.setText(objMdlNavgtnDrwr.getTitle());
            holder.rltvlyotItemContent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.onClick(view, holder.getAdapterPosition());
                }
            });
        } else {

            if(session.isUserLoggedIn()) {
                holder.tvHeaderFullName.setText(mCntext.getString(R.string.welcome_val,
                        session.getFirstName().concat(" ").concat(session.getLastName())));
            } else
                holder.tvHeaderFullName.setText(mCntext.getString(R.string.welcome_val, mCntext.getString(R.string.signin_signup_sml)));
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        int Holderid;

        RelativeLayout rltvlyotMain, rltvlyotItemContent;
        TextView tvItemTitle, tvHeaderFullName;
        ImageView imgvwItemIcon;

        public ViewHolder(View itemView, int ViewType) {
            super(itemView);

            if (ViewType == TYPE_ITEM) {
                rltvlyotMain = itemView.findViewById(R.id.rltvlyot_recylrvw_item_drwerlyot_main);
                rltvlyotItemContent = itemView.findViewById(R.id.rltvlyot_recylrvw_item_drwerlyot_content);
                tvItemTitle = itemView.findViewById(R.id.tv_recylrvw_item_drwerlyot_title);
                imgvwItemIcon = itemView.findViewById(R.id.imgvw_recylrvw_item_drwerlyot_icon);
                Holderid = 1;
            } else {

                tvHeaderFullName = itemView.findViewById(R.id.tv_drwrlyot_header_fullname);
                Holderid = 0;
            }
        }
    }

    @Override
    public int getItemCount() {
        return mArrylstNavgtnDrwr.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position))
            return TYPE_HEADER;

        return TYPE_ITEM;
    }

    // Remove item at given position
    public void removeAt(int position) {

        mArrylstNavgtnDrwr.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mArrylstNavgtnDrwr.size());
        notifyDataSetChanged();
    }

    // Add new Item at last position
    public void addItem(MdlNavgtnDrwr objMdlNavgtnDrwr) {

        mArrylstNavgtnDrwr.add(objMdlNavgtnDrwr);
        notifyItemInserted(mArrylstNavgtnDrwr.size() - 1);
        notifyDataSetChanged();
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }
}
