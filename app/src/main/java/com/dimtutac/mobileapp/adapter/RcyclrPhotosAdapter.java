package com.dimtutac.mobileapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.dimtutac.mobileapp.ActvtyPreviewImage;
import com.dimtutac.mobileapp.R;
import com.dimtutac.mobileapp.common.AppConstants;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Jalotsav on 1/8/2018.
 */

public class RcyclrPhotosAdapter extends RecyclerView.Adapter<RcyclrPhotosAdapter.ViewHolder> {

    private Context mContext;
    private boolean mIsWithPreview, mIsWithRemove;
    private int mImagePathType;
    private ArrayList<String> mArrylstImgFilePath;
    private Drawable mDrwblDefault;

    public RcyclrPhotosAdapter(Context context, boolean isWithPreview, boolean isWithRemove, int imagePathType, ArrayList<String> arrylstImgFilePath, Drawable drwblDefault) {

        mContext = context;
        mIsWithPreview = isWithPreview;
        mIsWithRemove = isWithRemove;
        mImagePathType = imagePathType;
        mArrylstImgFilePath = new ArrayList<>();
        mArrylstImgFilePath.addAll(arrylstImgFilePath);
        mDrwblDefault = drwblDefault;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.lo_recyclritem_checkin_attchdimgs, parent, false);

        return new ViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        String imageFilePath = mArrylstImgFilePath.get(position);

        switch (mImagePathType) {
            case AppConstants.IMAGE_PATH_TYPE_FILE:
                Picasso.with(mContext)
                        .load(new File(imageFilePath))
                        .placeholder(mDrwblDefault)
                        .into(holder.imgvwAttchdImg);
                break;
            case AppConstants.IMAGE_PATH_TYPE_SERVER:
                Picasso.with(mContext)
                        .load(AppConstants.ATCHMNT_ROOT_URL.concat(imageFilePath))
                        .placeholder(mDrwblDefault)
                        .into(holder.imgvwAttchdImg);
                break;
        }

        holder.imgvwAttchdImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int holderAdapterPosition = holder.getAdapterPosition();
                showPreviewiewRemovePopupmenu(view, holderAdapterPosition);
            }
        });
    }

    // Show pop-up for preview or remove
    private void showPreviewiewRemovePopupmenu(View view, final int holderAdapterPosition) {

        PopupMenu mPopupmenu = new PopupMenu(mContext, view);
        mPopupmenu.getMenuInflater().inflate(R.menu.menu_popup_previewremove, mPopupmenu.getMenu());

        // Hide Remove options
        if(!mIsWithPreview)
            mPopupmenu.getMenu().findItem(R.id.action_preview).setVisible(false);
        if(!mIsWithRemove)
            mPopupmenu.getMenu().findItem(R.id.action_remove).setVisible(false);

        mPopupmenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.action_preview:

                        mContext.startActivity(
                                new Intent(mContext, ActvtyPreviewImage.class)
                                        .putExtra(AppConstants.PUT_EXTRA_IMAGE_PATH, mArrylstImgFilePath.get(holderAdapterPosition))
                                        .putExtra(AppConstants.PUT_EXTRA_IMAGE_PATH_TYPE, mImagePathType));
                        break;
                    case R.id.action_remove:

                        removeAt(holderAdapterPosition);
                        break;
                }
                return false;
            }
        });
        mPopupmenu.show();
    }

    @Override
    public int getItemCount() {
        return mArrylstImgFilePath.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        ImageView imgvwAttchdImg;

        ViewHolder(View itemView) {
            super(itemView);

            imgvwAttchdImg = itemView.findViewById(R.id.imgvw_recylrvw_item_checkin_attchdimgs_image);
        }
    }

    // Remove item at given position
    private void removeAt(int position) {

        mArrylstImgFilePath.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mArrylstImgFilePath.size());
        notifyDataSetChanged();
    }

    // Add new Item at last position
    public void addItem(String filePath) {

        mArrylstImgFilePath.add(filePath);
        notifyItemInserted(mArrylstImgFilePath.size() - 1);
        notifyDataSetChanged();
    }

    // Get All Items
    public ArrayList<String> getAllItems() {
        return this.mArrylstImgFilePath;
    }

    // Set Filter and Notify
    public void setFilter(ArrayList<String> arrylstImgFilePath) {

        mArrylstImgFilePath = new ArrayList<>();
        mArrylstImgFilePath.addAll(arrylstImgFilePath);
        notifyDataSetChanged();
    }
}
