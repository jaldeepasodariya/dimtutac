package com.dimtutac.mobileapp.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.dimtutac.mobileapp.models.dropdown.MdlGenderData;

import java.util.ArrayList;

/**
 * Created by Jalotsav on 9/16/2017.
 */

public class SpnrArryAdptrGender extends ArrayAdapter<MdlGenderData> {

    private ArrayList<MdlGenderData> mArrylstMdlGenderData;

    public SpnrArryAdptrGender(Context context, int resource, ArrayList<MdlGenderData> objects) {
        super(context, resource, resource, objects);

        mArrylstMdlGenderData = objects;
    }

    public int getCount() {
        return mArrylstMdlGenderData.size();
    }

    public MdlGenderData getItem(int position) {
        return mArrylstMdlGenderData.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {

        TextView tvDisplayText = (TextView) super.getDropDownView(position, convertView, parent);
        tvDisplayText.setText(mArrylstMdlGenderData.get(position).getGenderText());
        return tvDisplayText;
//            return super.getDropDownView(position, convertView, parent);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {

        TextView tvDisplayText = (TextView) super.getView(position, convertView, parent);
        tvDisplayText.setText(mArrylstMdlGenderData.get(position).getGenderText());
        return tvDisplayText;
//            return super.getView(position, convertView, parent);
    }
}
