package com.dimtutac.mobileapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dimtutac.mobileapp.ActvtyReservation;
import com.dimtutac.mobileapp.ActvtySignin;
import com.dimtutac.mobileapp.ActvtyStoreDetails;
import com.dimtutac.mobileapp.R;
import com.dimtutac.mobileapp.common.AppConstants;
import com.dimtutac.mobileapp.common.UserSessionManager;
import com.dimtutac.mobileapp.models.branch.MdlLocations;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Jalotsav on 27/8/17.
 */

public class RcyclrLocationsAdapter extends RecyclerView.Adapter<RcyclrLocationsAdapter.ViewHolder> {

    private Context mContext;
    private UserSessionManager session;
    private ArrayList<MdlLocations> mArrylstMdlLocations;
    private Drawable mDrwblDefault;

    public RcyclrLocationsAdapter(Context context, ArrayList<MdlLocations> arrylstMdlLocations, Drawable drwblDefault) {
        super();

        this.mContext = context;
        session = new UserSessionManager(mContext);
        this.mArrylstMdlLocations = arrylstMdlLocations;
        mDrwblDefault = drwblDefault;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.lo_recyclritem_locations, viewGroup, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int i) {

        final MdlLocations objMdlLocations = mArrylstMdlLocations.get(i);

        viewHolder.mTvTitle.setText(objMdlLocations.getTitle());
        viewHolder.mTvAddress.setText(objMdlLocations.getAddress());
        viewHolder.mTvPhoneNo.setText(objMdlLocations.getPhonenumber());
        if(TextUtils.isEmpty(objMdlLocations.getStoreHrs()))
            viewHolder.mLnrlyotStoreHrs.setVisibility(View.GONE);
        else {
            viewHolder.mLnrlyotStoreHrs.setVisibility(View.VISIBLE);
            viewHolder.mTvStoreHrs.setText(objMdlLocations.getStoreHrs());
        }

        if(!TextUtils.isEmpty(objMdlLocations.getImagePath())) {
            Picasso.with(mContext)
                    .load(objMdlLocations.getImagePath())
                    .placeholder(mDrwblDefault)
                    .into(viewHolder.mImgvwImage);
        }

        viewHolder.mAppcpmtbtnResrvtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!session.isUserLoggedIn()) {

                    ((Activity) mContext).startActivityForResult(new Intent(mContext, ActvtySignin.class), AppConstants.REQUEST_SIGNIN);
                } else {

                    mContext.startActivity(new Intent(mContext, ActvtyReservation.class)
                            .putExtra(AppConstants.PUT_EXTRA_RESTAURANT_ID, objMdlLocations.getRestaurantId()));
                }
            }
        });

        viewHolder.mAppcpmtbtnCheckin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!session.isUserLoggedIn()) {

                    ((Activity) mContext).startActivityForResult(new Intent(mContext, ActvtySignin.class), AppConstants.REQUEST_SIGNIN);
                } else
                    mContext.startActivity(new Intent(mContext, ActvtyStoreDetails.class)
                            .putExtra(AppConstants.PUT_EXTRA_BRANCH_ID, objMdlLocations.getPkId())
                            .putExtra(AppConstants.PUT_EXTRA_RESTAURANT_ID, objMdlLocations.getRestaurantId())
                            .putExtra(AppConstants.PUT_EXTRA_BRANCH_NAME, objMdlLocations.getTitle())
                            .putExtra(AppConstants.PUT_EXTRA_ADDRESS, objMdlLocations.getAddress())
                            .putExtra(AppConstants.PUT_EXTRA_PHONENO, objMdlLocations.getPhonenumber())
                            .putExtra(AppConstants.PUT_EXTRA_EMAILID, objMdlLocations.getEmailId())
                            .putExtra(AppConstants.PUT_EXTRA_PHOTO_URL, objMdlLocations.getImagePath())
                            .putExtra(AppConstants.PUT_EXTRA_STOREHRS, objMdlLocations.getStoreHrs())
                            .putExtra(AppConstants.PUT_EXTRA_LATITUDE, objMdlLocations.getLatitude())
                            .putExtra(AppConstants.PUT_EXTRA_LONGITUDE, objMdlLocations.getLongitude()));
            }
        });

        viewHolder.mLnrlyotMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                viewHolder.mAppcpmtbtnCheckin.performClick();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mArrylstMdlLocations.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        LinearLayout mLnrlyotMain;
        ImageView mImgvwImage;
        TextView mTvTitle, mTvAddress, mTvPhoneNo, mTvStoreHrs;
        LinearLayout mLnrlyotStoreHrs;
        AppCompatButton mAppcpmtbtnResrvtn, mAppcpmtbtnCheckin;

        ViewHolder(View itemView) {
            super(itemView);

            mLnrlyotMain = itemView.findViewById(R.id.lnrlyot_recylrvw_item_locations_main);
            mImgvwImage = itemView.findViewById(R.id.imgvw_recylrvw_item_locations_image);
            mTvTitle = itemView.findViewById(R.id.tv_recylrvw_item_locations_title);
            mTvAddress = itemView.findViewById(R.id.tv_recylrvw_item_locations_addrs);
            mTvPhoneNo = itemView.findViewById(R.id.tv_recylrvw_item_locations_phone);
            mTvStoreHrs = itemView.findViewById(R.id.tv_recylrvw_item_locations_storehrs);
            mLnrlyotStoreHrs = itemView.findViewById(R.id.lnrlyot_recylrvw_item_locations_storehrs);
            mAppcpmtbtnResrvtn = itemView.findViewById(R.id.appcmptbtn_recylrvw_item_locations_resrvtn);
            mAppcpmtbtnCheckin = itemView.findViewById(R.id.appcmptbtn_recylrvw_item_locations_checkin);
        }
    }
}
