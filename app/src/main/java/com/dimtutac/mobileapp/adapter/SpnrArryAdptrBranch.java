package com.dimtutac.mobileapp.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.dimtutac.mobileapp.models.branch.MdlBranchListResData;

import java.util.ArrayList;

/**
 * Created by Jalotsav on 1/10/17.
 */

public class SpnrArryAdptrBranch extends ArrayAdapter<MdlBranchListResData> {

    private Context mContext;
    private ArrayList<MdlBranchListResData> mArrylstMdlBranchData;

    public SpnrArryAdptrBranch(Context context, int resource, ArrayList<MdlBranchListResData> objects) {
        super(context, resource, resource, objects);

        mContext = context;
        mArrylstMdlBranchData = objects;
    }

    public int getCount() {
        return mArrylstMdlBranchData.size();
    }

    public MdlBranchListResData getItem(int position) {
        return mArrylstMdlBranchData.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(android.R.layout.simple_spinner_dropdown_item, parent, false);
        }
//        TextView tvDisplayText = (TextView) super.getDropDownView(position, convertView, parent);
//        tvDisplayText.setText(mArrylstMdlBranchData.get(position).getBranchName());
        ((TextView) convertView).setText(mArrylstMdlBranchData.get(position).getBranchName());
        return convertView;
//            return super.getDropDownView(position, convertView, parent);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {

        TextView tvDisplayText = (TextView) View.inflate(mContext, android.R.layout.simple_spinner_item, null);
//        TextView tvDisplayText = (TextView) super.getView(position, convertView, parent);
        tvDisplayText.setText(mArrylstMdlBranchData.get(position).getBranchName());
        return tvDisplayText;
//            return super.getView(position, convertView, parent);
    }
}
