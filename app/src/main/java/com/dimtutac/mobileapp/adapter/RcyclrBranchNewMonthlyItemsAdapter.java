package com.dimtutac.mobileapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.dimtutac.mobileapp.ActvtyBranchNewMonthlyItems;
import com.dimtutac.mobileapp.R;
import com.dimtutac.mobileapp.common.AppConstants;
import com.dimtutac.mobileapp.models.menu.MdlNewMonthlyItemListResData;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Jalotsav on 11/3/2017.
 */

public class RcyclrBranchNewMonthlyItemsAdapter extends RecyclerView.Adapter<RcyclrBranchNewMonthlyItemsAdapter.ViewHolder> {

    private Context mContext;
    private ArrayList<MdlNewMonthlyItemListResData> mArrylstMdlNewMonthlyItems;
    private Drawable mDrwblDefault;

    public RcyclrBranchNewMonthlyItemsAdapter(Context context, ArrayList<MdlNewMonthlyItemListResData> arrylstMdlNewMonthlyItemLists, Drawable drwblDefault) {
        super();

        this.mContext = context;
        this.mArrylstMdlNewMonthlyItems = arrylstMdlNewMonthlyItemLists;
        mDrwblDefault = drwblDefault;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.lo_recyclritem_branch_newmonthlyitems, viewGroup, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        final MdlNewMonthlyItemListResData objMdlNewMonthlyItem = mArrylstMdlNewMonthlyItems.get(position);

        if(!TextUtils.isEmpty(objMdlNewMonthlyItem.getPhotoURL())) {
            Picasso.with(mContext)
                    .load(AppConstants.ATCHMNT_ROOT_URL.concat(objMdlNewMonthlyItem.getPhotoURL()))
                    .placeholder(mDrwblDefault)
                    .into(viewHolder.mImgvwImage);
        }

        viewHolder.mImgvwImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ((ActvtyBranchNewMonthlyItems) mContext).setMenuDtlsDialogCurrentItem(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mArrylstMdlNewMonthlyItems.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        ImageView mImgvwImage;

        ViewHolder(View itemView) {
            super(itemView);

            mImgvwImage = itemView.findViewById(R.id.imgvw_recylrvw_item_branch_newmonthlyitems_image);
        }
    }
}
