package com.dimtutac.mobileapp.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.dimtutac.mobileapp.models.dropdown.MdlLanguageData;

import java.util.ArrayList;

/**
 * Created by Jalotsav on 9/16/2017.
 */

public class SpnrArryAdptrLanguage extends ArrayAdapter<MdlLanguageData> {

    private Context mContext;
    private ArrayList<MdlLanguageData> mArrylstMdlLanguageData;

    public SpnrArryAdptrLanguage(Context context, int resource, ArrayList<MdlLanguageData> objects) {
        super(context, resource, resource, objects);

        mContext = context;
        mArrylstMdlLanguageData = objects;
    }

    public int getCount() {
        return mArrylstMdlLanguageData.size();
    }

    public MdlLanguageData getItem(int position) {
        return mArrylstMdlLanguageData.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(android.R.layout.simple_spinner_dropdown_item, parent, false);
        }
        ((TextView) convertView).setText(mArrylstMdlLanguageData.get(position).getText());
        return convertView;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {

        TextView tvDisplayText = (TextView) View.inflate(mContext, android.R.layout.simple_spinner_item, null);
        tvDisplayText.setText(mArrylstMdlLanguageData.get(position).getText());
        return tvDisplayText;
    }
}
