package com.dimtutac.mobileapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dimtutac.mobileapp.ActvtySubMenu;
import com.dimtutac.mobileapp.R;
import com.dimtutac.mobileapp.common.AppConstants;
import com.dimtutac.mobileapp.models.menu.MdlMenuListResData;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Jalotsav on 20/11/17.
 */

public class RcyclrMenuAdapter extends RecyclerView.Adapter<RcyclrMenuAdapter.ViewHolder> {

    private Context mContext;
    private ArrayList<MdlMenuListResData> mArrylstMdlMenu;
    private Drawable mDrwblDefault;

    public RcyclrMenuAdapter(Context context, ArrayList<MdlMenuListResData> arrylstMdlLocations, Drawable drwblDefault) {
        super();

        this.mContext = context;
        this.mArrylstMdlMenu = arrylstMdlLocations;
        mDrwblDefault = drwblDefault;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.lo_recyclritem_menu, viewGroup, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {

        final MdlMenuListResData objMdlMenu = mArrylstMdlMenu.get(i);

        viewHolder.mTvTitle.setText(objMdlMenu.getCategoryName());

        if(!TextUtils.isEmpty(objMdlMenu.getPhotoURL())) {
            Picasso.with(mContext)
                    .load(AppConstants.ATCHMNT_ROOT_URL.concat(objMdlMenu.getPhotoURL()))
                    .placeholder(mDrwblDefault)
                    .into(viewHolder.mImgvwImage);
        }

        viewHolder.mRltvlyotContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mContext.startActivity(new Intent(mContext, ActvtySubMenu.class)
                        .putExtra(AppConstants.PUT_EXTRA_MENU_ID, objMdlMenu.getPkId())
                        .putExtra(AppConstants.PUT_EXTRA_CATEGORY_NAME, objMdlMenu.getCategoryName()));
            }
        });
    }

    @Override
    public int getItemCount() {
        return mArrylstMdlMenu.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        RelativeLayout mRltvlyotContent;
        ImageView mImgvwImage;
        TextView mTvTitle;

        ViewHolder(View itemView) {
            super(itemView);

            mRltvlyotContent = itemView.findViewById(R.id.rltvlyot_recylrvw_item_menu_content);
            mImgvwImage = itemView.findViewById(R.id.imgvw_recylrvw_item_menu_image);
            mTvTitle = itemView.findViewById(R.id.tv_recylrvw_item_menu_categoryname);
        }
    }
}
