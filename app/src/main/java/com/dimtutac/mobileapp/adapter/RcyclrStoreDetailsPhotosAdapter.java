package com.dimtutac.mobileapp.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.dimtutac.mobileapp.ActvtyStoreDetails;
import com.dimtutac.mobileapp.R;
import com.dimtutac.mobileapp.common.AppConstants;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Jalotsav on 1/24/2018.
 */

public class RcyclrStoreDetailsPhotosAdapter extends RecyclerView.Adapter<RcyclrStoreDetailsPhotosAdapter.ViewHolder> {

    private Context mContext;
    private ArrayList<String> mArrylstImagePath;
    private Drawable mDrwblDefault;

    public RcyclrStoreDetailsPhotosAdapter(Context context, ArrayList<String> arrylstImagePath, Drawable drwblDefault) {

        mContext = context;
        mArrylstImagePath = new ArrayList<>();
        mArrylstImagePath.addAll(arrylstImagePath);
        mDrwblDefault = drwblDefault;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.lo_recyclritem_checkin_attchdimgs, parent, false);

        return new ViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        String imagePath = mArrylstImagePath.get(position);
        if (!TextUtils.isEmpty(imagePath)) {
            Picasso.with(mContext)
                    .load(AppConstants.ATCHMNT_ROOT_URL.concat(imagePath))
                    .placeholder(mDrwblDefault)
                    .into(holder.imgvwAttchdImg);
        }

        holder.imgvwAttchdImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ((ActvtyStoreDetails) mContext).setMenuDtlsDialogCurrentItem(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mArrylstImagePath.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        ImageView imgvwAttchdImg;

        ViewHolder(View itemView) {
            super(itemView);

            imgvwAttchdImg = itemView.findViewById(R.id.imgvw_recylrvw_item_checkin_attchdimgs_image);
        }
    }

    // Remove item at given position
    private void removeAt(int position) {

        mArrylstImagePath.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mArrylstImagePath.size());
        notifyDataSetChanged();
    }

    // Add new Item at last position
    public void addItem(String filePath) {

        mArrylstImagePath.add(filePath);
        notifyItemInserted(mArrylstImagePath.size() - 1);
        notifyDataSetChanged();
    }

    // Get All Items
    public ArrayList<String> getAllItems() {
        return this.mArrylstImagePath;
    }

    // Set Filter and Notify
    public void setFilter(ArrayList<String> arrylstImgFilePath) {

        mArrylstImagePath = new ArrayList<>();
        mArrylstImagePath.addAll(arrylstImgFilePath);
        notifyDataSetChanged();
    }
}
