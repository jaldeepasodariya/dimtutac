package com.dimtutac.mobileapp.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.view.PagerAdapter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dimtutac.mobileapp.R;
import com.dimtutac.mobileapp.common.AppConstants;
import com.dimtutac.mobileapp.models.dashboard.MdlDashbrdDataResData;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Jalotsav on 26/8/17.
 */

public class SliderViewpagerAdptr extends PagerAdapter {

    private Context mContext;
    private ArrayList<MdlDashbrdDataResData> mArrylstMdlDashbrdRedData;
    private Drawable mDrwblDefault;

    public SliderViewpagerAdptr(Context context, ArrayList<MdlDashbrdDataResData> arrylstMdlDashbrdResData, Drawable drwblDefault) {

        this.mContext = context;
        this.mArrylstMdlDashbrdRedData = arrylstMdlDashbrdResData;
        mDrwblDefault = drwblDefault;
    }

    @Override
    public int getCount() {
        return this.mArrylstMdlDashbrdRedData.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = inflater.inflate(R.layout.lo_vwpgr_imagevw, container, false);

        TextView mTvTitle1 = viewLayout.findViewById(R.id.tv_frgmnt_home_title1);
        TextView mTvTitle2 = viewLayout.findViewById(R.id.tv_frgmnt_home_title2);
        TextView mTvTitle3 = viewLayout.findViewById(R.id.tv_frgmnt_home_title3);
        ImageView mImgvwImage = viewLayout.findViewById(R.id.imgvw_frgmnt_home_vwpgr_image);

        try {

            MdlDashbrdDataResData objMdlDashbrdData = mArrylstMdlDashbrdRedData.get(position);

            if(objMdlDashbrdData.isWithText()) {
                mTvTitle1.setText(objMdlDashbrdData.getTitle1());
                mTvTitle2.setText(objMdlDashbrdData.getTitle2());
                mTvTitle3.setText(objMdlDashbrdData.getTitle3());
            }

            if(!TextUtils.isEmpty(objMdlDashbrdData.getPhotoURL())) {
                Picasso.with(mContext)
                        .load(AppConstants.ATCHMNT_ROOT_URL.concat(objMdlDashbrdData.getPhotoURL()))
                        .placeholder(mDrwblDefault)
                        .into(mImgvwImage);
            }
        } catch (Exception e) {

            e.printStackTrace();
        }

        container.addView(viewLayout);

        return viewLayout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        container.removeView((RelativeLayout) object);
    }
}
