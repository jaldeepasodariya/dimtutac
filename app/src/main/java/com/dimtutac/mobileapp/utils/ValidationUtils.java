package com.dimtutac.mobileapp.utils;

import android.app.Activity;
import android.content.Context;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;

import com.dimtutac.mobileapp.R;
import com.dimtutac.mobileapp.common.GeneralFunctions;

/**
 * Created by Jalotsav on 9/5/2017.
 */

public class ValidationUtils {

    // Check Empty validation for field
    public static boolean validateEmpty(Context context, View mView, EditText mEt, String errorMsg) {

        if (mEt.getText().toString().trim().isEmpty()) {
            Snackbar.make(mView, errorMsg, Snackbar.LENGTH_SHORT).show();
            requestFocus(context, mEt);
            return false;
        } else {
            return true;
        }
    }

    // Check validation for Mobile
    public static boolean validateMobile(Context context, View mView, EditText mEtMobile) {

        if (mEtMobile.getText().toString().trim().isEmpty()) {
            Snackbar.make(mView, context.getString(R.string.entr_phone_number_sml), Snackbar.LENGTH_SHORT).show();
            requestFocus(context, mEtMobile);
            return false;
        }/* else if (!GeneralFunctions.isValidMobile(mEtMobile.getText().toString().trim())) {
            Snackbar.make(mView, context.getString(R.string.invalid_phone_number_sml), Snackbar.LENGTH_SHORT).show();
            requestFocus(context, mEtMobile);
            return false;
        }*/ else {
            return true;
        }
    }

    // Check validation for Password
    public static boolean validatePassword(Context context, View mView, EditText mEtPaswrd) {

        if (mEtPaswrd.getText().toString().trim().isEmpty()) {
            Snackbar.make(mView, context.getString(R.string.entr_password_sml), Snackbar.LENGTH_SHORT).show();
            requestFocus(context, mEtPaswrd);
            return false;
        }/* else if (mEtPaswrd.getText().toString().trim().length() < 6) {
            mTxtinptlyotPaswrd.setErrorEnabled(true);
            mTxtinptlyotPaswrd.setError(context.getString(R.string.password_6chars_long));
            requestFocus(context, mEtPaswrd);
            return false;
        }*/ else {
            return true;
        }
    }

    // Check validation for Email
    public static boolean validateEmailFormat(Context context, View mView, EditText mEtEmail) {

        if (!GeneralFunctions.isValidEmail(mEtEmail.getText().toString().trim())) {

            Snackbar.make(mView, context.getString(R.string.invalid_email), Snackbar.LENGTH_SHORT).show();
            requestFocus(context, mEtEmail);
            return false;
        } else {
            return true;
        }
    }

    public static void requestFocus(Context context, View view) {
        if (view.requestFocus()) {
            ((Activity) context).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }
}
