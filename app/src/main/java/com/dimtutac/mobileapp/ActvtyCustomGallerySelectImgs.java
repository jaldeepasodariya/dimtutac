package com.dimtutac.mobileapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dimtutac.mobileapp.common.AppConstants;
import com.dimtutac.mobileapp.common.GeneralFunctions;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Jalotsav on 11/16/2017.
 */

public class ActvtyCustomGallerySelectImgs extends AppCompatActivity implements AppConstants {

    private static final String TAG = ActvtyCustomGallerySelectImgs.class.getSimpleName();

    @BindView(R.id.grdvw_actvty_cstmgllry_imgdir_slctimgs) GridView mGrdvwslctImgs;
    TextView mTvSlctdImgCnt;

    @BindDrawable(R.mipmap.ic_logo_black) Drawable mDrwblDefault;

    GridViewImageAdapterSlctImgs mGrdvwImgAdapter;
    int mColmnWidth, mTotalSlctdImgCnt = 0;
    ArrayList<String> mArrylstAllImgFilePaths;
    private boolean[] mThumbnailsSelection;
    ArrayList<String> mArrylstAlrdySlctdImgPath;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lo_actvty_customgallery_slctimgs);
        ButterKnife.bind(this);

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        mArrylstAlrdySlctdImgPath = new ArrayList<>();

        String selected_image_directory_path = getIntent().getStringExtra(PUT_EXTRA_SELECTED_IMAGE_DIRECTORY_PATH);
        mArrylstAlrdySlctdImgPath.addAll(getIntent().getStringArrayListExtra(PUT_EXTRA_ARRAYLST_ALREADY_SELECTED_IMGS_PATHS));
        mTotalSlctdImgCnt = getIntent().getIntExtra(PUT_EXTRA_TOTAL_SELECTED_IMG_COUNT, 0);

        // Get only directory name from selected directory path and substring to 0-25 character
        String slctd_img_dirname = selected_image_directory_path.substring(selected_image_directory_path.lastIndexOf("/") + 1).trim();
        if(slctd_img_dirname.length() > 25)
            slctd_img_dirname = slctd_img_dirname.substring(0,25) + "...";
        setTitle(slctd_img_dirname);

        setSelectedImageCount();

        initializeGridLayout();

        mArrylstAllImgFilePaths = new ArrayList<String>();
        mArrylstAllImgFilePaths = GeneralFunctions.getAllImgFilePaths(new File(selected_image_directory_path));
        Collections.sort(mArrylstAllImgFilePaths, Collections.reverseOrder());

        this.mThumbnailsSelection = new boolean[mArrylstAllImgFilePaths.size()];

        for(int i = 0; i< mArrylstAllImgFilePaths.size(); i++){

            for(int j = 0; j< mArrylstAlrdySlctdImgPath.size(); j++){
                if(mArrylstAlrdySlctdImgPath.get(j).equals(mArrylstAllImgFilePaths.get(i))){
                    mThumbnailsSelection[i] = true;
                }
            }
        }

        mGrdvwImgAdapter = new GridViewImageAdapterSlctImgs();
        mGrdvwslctImgs.setAdapter(mGrdvwImgAdapter);
    }

    // Initialize GridView Layout
    private void initializeGridLayout() {

        float padding = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, GRID_PADDING_5, getResources().getDisplayMetrics());

        mColmnWidth = (int) ((GeneralFunctions.getScreenWidth(this) - ((NUM_OF_COLUMNS_3 + 1) * padding)) / NUM_OF_COLUMNS_3);

        mGrdvwslctImgs.setNumColumns(NUM_OF_COLUMNS_3);
        mGrdvwslctImgs.setColumnWidth(mColmnWidth);
        mGrdvwslctImgs.setStretchMode(GridView.NO_STRETCH);
        mGrdvwslctImgs.setPadding((int) padding, (int) padding, (int) padding, (int) padding);
        mGrdvwslctImgs.setHorizontalSpacing((int) padding);
        mGrdvwslctImgs.setVerticalSpacing((int) padding);
    }

    private class GridViewImageAdapterSlctImgs extends BaseAdapter {

        private LayoutInflater mInflater;

        GridViewImageAdapterSlctImgs() {
            mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        public int getCount() {
            return mArrylstAllImgFilePaths.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {

            final ViewHolder holder;
            if (convertView == null) {

                holder = new ViewHolder();
                convertView = mInflater.inflate(R.layout.lo_cstm_gllry_imgdirctry_slctimgs_item, null);
                holder.imgThumb = convertView.findViewById(R.id.imgvw_cstm_gllry_imgdirtctry_scltimg_item_imgthumb);
                holder.chkImage = convertView.findViewById(R.id.chkbx_cstm_gllry_imgdirtctry_scltimg_item_chkimage);

                convertView.setTag(holder);
            } else {

                holder = (ViewHolder) convertView.getTag();
            }

            holder.vwPosition = position;

            holder.imgThumb.setScaleType(ImageView.ScaleType.CENTER_CROP);
            holder.imgThumb.setLayoutParams(new RelativeLayout.LayoutParams(mColmnWidth, mColmnWidth));
            Picasso.with(ActvtyCustomGallerySelectImgs.this)
                    .load(new File(mArrylstAllImgFilePaths.get(holder.vwPosition)))
                    .placeholder(mDrwblDefault)
                    .into(holder.imgThumb);

            holder.chkImage.setChecked(mThumbnailsSelection[position]);

            holder.chkImage.setId(position);
            holder.imgThumb.setId(position);
            holder.chkImage.setOnClickListener(new View.OnClickListener() {

                public void onClick(View v) {

                    CheckBox cb = (CheckBox) v;
                    int id = cb.getId();
                    if (mThumbnailsSelection[id]) {

                        cb.setChecked(false);
                        mThumbnailsSelection[id] = false;
                        mTotalSlctdImgCnt--;
                        setSelectedImageCount();

                        for(int i = 0; i< mArrylstAlrdySlctdImgPath.size(); i++){
                            if(mArrylstAlrdySlctdImgPath.get(i).equals(mArrylstAllImgFilePaths.get(id))){
                                mArrylstAlrdySlctdImgPath.remove(i);
                                break;
                            }
                        }
                    } else {

                        if(mTotalSlctdImgCnt > 9) {

                            cb.setChecked(false);
                            Toast.makeText(ActvtyCustomGallerySelectImgs.this, getString(R.string.cant_add_morethan_10_items), Toast.LENGTH_SHORT).show();
                        }else {

                            cb.setChecked(true);
                            mThumbnailsSelection[id] = true;
                            mTotalSlctdImgCnt++;
                            setSelectedImageCount();

                            mArrylstAlrdySlctdImgPath.add(mArrylstAllImgFilePaths.get(id));
                        }
                    }
                }
            });
            holder.imgThumb.setOnClickListener(new View.OnClickListener() {

                public void onClick(View v) {

                    int id = holder.chkImage.getId();
                    if (mThumbnailsSelection[id]) {

                        holder.chkImage.setChecked(false);
                        mThumbnailsSelection[id] = false;
                        mTotalSlctdImgCnt--;
                        setSelectedImageCount();

                        for(int i = 0; i< mArrylstAlrdySlctdImgPath.size(); i++){
                            if(mArrylstAlrdySlctdImgPath.get(i).equals(mArrylstAllImgFilePaths.get(id))){
                                mArrylstAlrdySlctdImgPath.remove(i);
                                break;
                            }
                        }
                    } else {

                        if(mTotalSlctdImgCnt > 9) {

                            Toast.makeText(ActvtyCustomGallerySelectImgs.this, getString(R.string.cant_add_morethan_10_items), Toast.LENGTH_SHORT).show();
                        }else {

                            holder.chkImage.setChecked(true);
                            holder.imgThumb.setBackgroundColor(ContextCompat.getColor(ActvtyCustomGallerySelectImgs.this, R.color.white));
                            mThumbnailsSelection[id] = true;
                            mTotalSlctdImgCnt++;

                            setSelectedImageCount();

                            mArrylstAlrdySlctdImgPath.add(mArrylstAllImgFilePaths.get(id));
                        }
                    }
                }
            });

            return convertView;
        }
    }

    public class ViewHolder {
        ImageView imgThumb;
        CheckBox chkImage;
        int vwPosition;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_customgallery, menu);

        MenuItem item = menu.findItem(R.id.action_count);
        MenuItemCompat.setActionView(item, R.layout.lo_view_textvw_count);
        mTvSlctdImgCnt = (TextView) MenuItemCompat.getActionView(item);
        mTvSlctdImgCnt.setText(String.valueOf(mTotalSlctdImgCnt));

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                break;
            case R.id.action_done:

                Intent i = new Intent();
                i.putExtra(PUT_EXTRA_ARRAYLST_ALREADY_SELECTED_IMGS_PATHS, mArrylstAlrdySlctdImgPath);
                i.putExtra(PUT_EXTRA_TOTAL_SELECTED_IMG_COUNT, mTotalSlctdImgCnt);
                setResult(Activity.RESULT_OK, i);
                finish();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    // Set Selected image count and update on ActionBar menu UI
    private void setSelectedImageCount() {

        invalidateOptionsMenu();
    }
}
