package com.dimtutac.mobileapp;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.dimtutac.mobileapp.common.AppConstants;
import com.dimtutac.mobileapp.common.GeneralFunctions;
import com.dimtutac.mobileapp.common.LogHelper;
import com.dimtutac.mobileapp.common.UserSessionManager;
import com.dimtutac.mobileapp.models.adminconfigurations.MdlAdminConfigRes;
import com.dimtutac.mobileapp.navgtndrawer.NavgtnDrwrMain;
import com.dimtutac.mobileapp.retrofitapi.APIAdminConfig;
import com.dimtutac.mobileapp.retrofitapi.APIRetroBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActvtyMain extends AppCompatActivity {

    private static final String TAG = ActvtyMain.class.getSimpleName();

    @BindView(R.id.cordntrlyot_actvty_main) CoordinatorLayout mCrdntrlyot;
    @BindView(R.id.imgvw_actvty_main_backgrnd) ImageView mImgvwBackgrnd;
    @BindView(R.id.prgrsbr_actvty_main) ProgressBar mPrgrsbrMain;

    @BindString(R.string.no_intrnt_cnctn) String mNoInternetConnMsg;
    @BindString(R.string.server_problem_sml) String mServerPrblmMsg;
    @BindString(R.string.internal_problem_sml) String mInternalPrblmMsg;

    @BindDrawable(R.drawable.img_bg_splash) Drawable mDrwblDefault;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lo_actvty_main);
        ButterKnife.bind(this);

        // If Remember Login: FALSE --> Clear all stored details
        UserSessionManager session = new UserSessionManager(this);
        if(!session.isRememberLogin())
            session.logoutUser(true);

        getSplashLogoData();

        // Wait for 15sec, If splashLogoImage not come from server then display LanguageSelection UI
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if(mPrgrsbrMain.getVisibility() == View.VISIBLE)
                    startHomeActivity();
            }
        }, 15000);
    }

    private void getSplashLogoData() {

        if (GeneralFunctions.isNetConnected(this))
            getSplashLogoAPI();
        else {
            Snackbar.make(mCrdntrlyot, mNoInternetConnMsg, Snackbar.LENGTH_LONG).show();
            startHomeActivity();
        }
    }

    private void startHomeActivity() {

        mPrgrsbrMain.setVisibility(View.GONE);
        int navDrwrPostn = getIntent().getIntExtra(AppConstants.PUT_EXTRA_NAVDRWER_POSTN, AppConstants.NAVDRWER_HOME);
        startActivity(new Intent(ActvtyMain.this, NavgtnDrwrMain.class)
                .putExtra(AppConstants.PUT_EXTRA_NAVDRWER_POSTN, navDrwrPostn));
        finish();
    }

    private void getSplashLogoAPI() {

        mPrgrsbrMain.setVisibility(View.VISIBLE);

        APIAdminConfig objApiAdminConfig = APIRetroBuilder.getRetroBuilder(true).create(APIAdminConfig.class);
        Call<MdlAdminConfigRes> callMdlAdminConfigRes = objApiAdminConfig.callGetSpashScreenLogo();
        callMdlAdminConfigRes.enqueue(new Callback<MdlAdminConfigRes>() {
            @Override
            public void onResponse(Call<MdlAdminConfigRes> call, Response<MdlAdminConfigRes> response) {

                if(response.isSuccessful()) {

                    try {
                        if(response.body().isSuccess()) {

                            String splashLogoImageName = response.body().getMdlAdminConfigResData().getValue();
                            if(!TextUtils.isEmpty(splashLogoImageName)) {

                                Picasso.with(ActvtyMain.this)
                                        .load(AppConstants.ATCHMNT_ROOT_URL.concat(splashLogoImageName))
                                        .placeholder(R.drawable.drwbl_bg_colorprimary)
                                        .into(new Target() {
                                            @Override
                                            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {

                                                mImgvwBackgrnd.setImageBitmap(bitmap);
                                                mPrgrsbrMain.setVisibility(View.GONE);
                                                new Handler().postDelayed(new Runnable() {
                                                    @Override
                                                    public void run() {

                                                        startHomeActivity();
                                                    }
                                                }, 5000);
                                            }

                                            @Override
                                            public void onBitmapFailed(Drawable errorDrawable) {

                                            }

                                            @Override
                                            public void onPrepareLoad(Drawable placeHolderDrawable) {

                                            }
                                        });
                            }
                        } else
                            startHomeActivity();
                    } catch (Exception e) {

                        e.printStackTrace();
                        LogHelper.printLog(AppConstants.LOGTYPE_ERROR, TAG, e.getMessage());
                        startHomeActivity();
                    }
                } else
                    startHomeActivity();
            }

            @Override
            public void onFailure(Call<MdlAdminConfigRes> call, Throwable t) {

                startHomeActivity();
            }
        });
    }
}
