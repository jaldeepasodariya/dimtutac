package com.dimtutac.mobileapp;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.dimtutac.mobileapp.adapter.RcyclrAttchdImgsAdapter;
import com.dimtutac.mobileapp.common.AppConstants;
import com.dimtutac.mobileapp.common.LogHelper;
import com.dimtutac.mobileapp.common.RecyclerViewEmptySupport;
import com.dimtutac.mobileapp.common.UserSessionManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Jalotsav on 1/25/2018.
 */

public class ActvtyCheckin extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private static final String TAG = ActvtyCheckin.class.getSimpleName();

    UserSessionManager session;

    @BindView(R.id.cordntrlyot_frgmnt_checkin) CoordinatorLayout mCrdntrlyot;
    @BindView(R.id.lnrlyot_frgmnt_checkin_vwtop) LinearLayout mLnrlyotVwTop;
    @BindView(R.id.imgvw_frgmnt_checkin_imagepicker) ImageView mImgvwImagePicker;
    @BindView(R.id.lnrlyot_recyclremptyvw_appearhere) LinearLayout mLnrlyotAppearHere;
    @BindView(R.id.tv_recyclremptyvw_appearhere) TextView mTvAppearHere;
    @BindView(R.id.rcyclrvw_frgmnt_checkin) RecyclerViewEmptySupport mRecyclerView;
    @BindView(R.id.prgrsbr_frgmnt_checkin) ProgressBar mPrgrsbrMain;

    @BindString(R.string.no_intrnt_cnctn) String mNoInternetConnMsg;
    @BindString(R.string.server_problem_sml) String mServerPrblmMsg;
    @BindString(R.string.internal_problem_sml) String mInternalPrblmMsg;
    @BindString(R.string.attchd_imgs_appear_here) String mAttchdImgsAppearHere;
    @BindString(R.string.allow_permtn_atchmnt) String mAllowPermsnMsg;
    @BindString(R.string.cant_add_morethan_10_items) String mCantAddMoreThan10Items;
    @BindString(R.string.selctd_file_mustbe_lessthan_5mb) String mSelctLessThan5MBMsg;
    @BindString(R.string.would_liketo_access_location_msg) String mLiketoAccessLoctnMsg;

    @BindDrawable(R.mipmap.ic_logo_black) Drawable mDrwblDefault;

    RecyclerView.LayoutManager mLayoutManager;
    RcyclrAttchdImgsAdapter mAdapter;
    Uri mImageUri;
    public ArrayList<String> mArrylstSelectedImages = new ArrayList<>();
    public int mImgCount = 0;

    String mBranchId, mRestaurantId, mBranchTitle, mBranchPhotoURL;
    GoogleApiClient googleApiClient;
    LocationManager locationManager;
    LocationRequest locationRequest;
    LocationSettingsRequest.Builder locationSettingsRequest;
    Location mLastLocation;
    PendingResult<LocationSettingsResult> pendingResult;
    boolean mIsRequestPickImage, mIsRequestStartNextActivity;
    String mLatitude = "", mLongitude = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lo_frgmnt_checkin);
        ButterKnife.bind(this);

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        session = new UserSessionManager(this);

        mBranchId = getIntent().getStringExtra(AppConstants.PUT_EXTRA_BRANCH_ID);
        mRestaurantId = getIntent().getStringExtra(AppConstants.PUT_EXTRA_RESTAURANT_ID);
        mBranchTitle = getIntent().getStringExtra(AppConstants.PUT_EXTRA_BRANCH_NAME);
        mBranchPhotoURL = getIntent().getStringExtra(AppConstants.PUT_EXTRA_PHOTO_URL);

        mLayoutManager = new GridLayoutManager(this, 3);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setEmptyView(mLnrlyotAppearHere);

        mTvAppearHere.setText(mAttchdImgsAppearHere);

        AppConstants.ARRYLST_ATTCHDIMGS_PATH.clear();

        mArrylstSelectedImages = new ArrayList<>();
        mAdapter = new RcyclrAttchdImgsAdapter(this, this, mArrylstSelectedImages, mDrwblDefault);
        mRecyclerView.setAdapter(mAdapter);

        if (!session.isUserLoggedIn()) {

            startActivityForResult(new Intent(this, ActvtySignin.class), AppConstants.REQUEST_SIGNIN);
        }

        connectGoogleApiClient();
    }

    private void connectGoogleApiClient() {

        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        googleApiClient.connect();
    }

    @OnClick({R.id.lnrlyot_frgmnt_checkin_vwtop, R.id.appcmptbtn_frgmnt_checkin_next})
    public void onClickView(View view) {

        switch (view.getId()) {
            case R.id.lnrlyot_frgmnt_checkin_vwtop:

                if(mAdapter.getItemCount() == 10)
                    Snackbar.make(mCrdntrlyot, mCantAddMoreThan10Items, Snackbar.LENGTH_LONG).show();
                else {
                    mIsRequestPickImage = true;
                    checkAppPermission(mImgvwImagePicker);
                }
                break;
            case R.id.appcmptbtn_frgmnt_checkin_next:

                if(mPrgrsbrMain.getVisibility() != View.VISIBLE) {
                    AlertDialog.Builder alrtDlg = new AlertDialog.Builder(this);
                    alrtDlg.setMessage(getString(R.string.would_liketo_access_location_msg));
                    alrtDlg.setNegativeButton(getString(R.string.no_sml).toUpperCase(), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();

                            startActivityForResult(new Intent(ActvtyCheckin.this, ActvtyCheckinLocationList.class), AppConstants.REQUEST_CHECKIN_DO);
                        }
                    });
                    alrtDlg.setPositiveButton(getString(R.string.yes_sml).toUpperCase(), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            dialogInterface.dismiss();

                            mIsRequestPickImage = false;
                            checkAppPermission(null);
                        }
                    });

                    alrtDlg.show();
                }
                break;
        }
    }

    // Check Storage & Camera permission for use
    private void checkAppPermission(View view) {

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (!isCheckSelfPermission())
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA,
                        Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, AppConstants.REQUEST_APP_PERMISSION_CAMERASTORAGE);
            else {
                if(mIsRequestPickImage)
                    showImagePickPopupmenu(view);
                else enableLocation();
            }
        } else {
            if(mIsRequestPickImage)
                showImagePickPopupmenu(view);
            else enableLocation();
        }
    }

    private boolean isCheckSelfPermission(){

        int selfPermsnStorage = ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int selfPermsnCamera = ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        int selfPermsnFineLocation = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        int selfPermsnCoarseLocation = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
        return  selfPermsnStorage == PackageManager.PERMISSION_GRANTED
                && selfPermsnCamera == PackageManager.PERMISSION_GRANTED
                && selfPermsnFineLocation == PackageManager.PERMISSION_GRANTED
                && selfPermsnCoarseLocation == PackageManager.PERMISSION_GRANTED;
    }

    // Show pop-up for pick or capture image
    private void showImagePickPopupmenu(View view) {

        PopupMenu mPopupmenu = new PopupMenu(this, view);
        mPopupmenu.getMenuInflater().inflate(R.menu.menu_popup_imagepick, mPopupmenu.getMenu());
        mPopupmenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.action_camera:

                        Intent intntCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(intntCamera, AppConstants.REQUEST_OPEN_CAMERA);
                        break;
                    case R.id.action_gallery:

                        Intent intnt_cstmphotoglry = new Intent(ActvtyCheckin.this, ActvtyCustomGallery.class);
                        intnt_cstmphotoglry.putExtra(AppConstants.PUT_EXTRA_ARRAYLST_ALREADY_SELECTED_IMGS_PATHS, AppConstants.ARRYLST_ATTCHDIMGS_PATH);
                        intnt_cstmphotoglry.putExtra(AppConstants.PUT_EXTRA_TOTAL_SELECTED_IMG_COUNT, mImgCount);
                        startActivityForResult(intnt_cstmphotoglry, AppConstants.REQUEST_PICK_IMAGE_MULTIPLE);

                        /*Intent intntGallery = new Intent();
                        intntGallery.setAction(Intent.ACTION_PICK);
                        intntGallery.setType("image");
                        startActivityForResult(Intent.createChooser(intntGallery, "Select a Image"), AppConstants.REQUEST_PICK_IMAGE);*/
                        break;
                }
                return false;
            }
        });
        mPopupmenu.show();
    }

    private Uri getImageUri(Intent data) {

        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        /*ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, bytes);*/
        String path = MediaStore.Images.Media.insertImage(this.getContentResolver(), thumbnail, "Title", null);

        /*File destination = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (Exception e) {
            e.printStackTrace();
        }*/
        return Uri.parse(path);
    }

    // Update UI and set image in attached array list
    private void updateUIAttchdImage(String picturePath) {

        try {

            File file = new File(picturePath);
            double fileSizeInMB = (file.length() / (1024 * 1024));
            LogHelper.printLog(AppConstants.LOGTYPE_INFO, TAG, "File Size in MB is: " + fileSizeInMB);

            if (fileSizeInMB > 5)
                Snackbar.make(mCrdntrlyot, mSelctLessThan5MBMsg, Snackbar.LENGTH_LONG).show();
            else {

                mAdapter.addItem(String.valueOf(picturePath));
                AppConstants.ARRYLST_ATTCHDIMGS_PATH.add(String.valueOf(picturePath));
                mImgCount++;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Get Picture path from image URI
    private String getPicturePathFromURI() {

        String picturePath = "";
        try {

            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = this.getContentResolver().query(mImageUri, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return picturePath;
    }

    private void enableLocation() {

        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

            getLocation();
        } else {
            locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(1 * 1000);
            locationRequest.setFastestInterval(1 * 1000);

            locationSettingsRequest = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);

            locationRequestResult();
        }
    }

    public void locationRequestResult() {
        pendingResult = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, locationSettingsRequest.build());
        pendingResult.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
                Status status = locationSettingsResult.getStatus();

                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        getLocation();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:

                        try {
                            status.startResolutionForResult(ActvtyCheckin.this, AppConstants.REQUEST_ENABLE_LOCATION);
                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the settings so we won't show the dialog.
                        break;
                }
            }

        });
    }

    private void getLocation() {

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        } else {
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);

            if (mLastLocation != null) {
                mLatitude = String.valueOf(mLastLocation.getLatitude());
                mLongitude = String.valueOf(mLastLocation.getLongitude());

                startActivityForResult(new Intent(this, ActvtyCheckinDo.class)
                        .putExtra(AppConstants.PUT_EXTRA_BRANCH_ID, mBranchId)
                        .putExtra(AppConstants.PUT_EXTRA_BRANCH_NAME, mBranchTitle)
                        .putExtra(AppConstants.PUT_EXTRA_RESTAURANT_ID, mRestaurantId)
                        .putExtra(AppConstants.PUT_EXTRA_PHOTO_URL, mBranchPhotoURL)
                        .putExtra(AppConstants.PUT_EXTRA_LATITUDE, mLatitude)
                        .putExtra(AppConstants.PUT_EXTRA_LONGITUDE, mLongitude), AppConstants.REQUEST_CHECKIN_DO);
            } else {

                if (!googleApiClient.isConnected())
                    googleApiClient.connect();

                mPrgrsbrMain.setVisibility(View.VISIBLE);

                mIsRequestStartNextActivity = true;
                LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if(requestCode == AppConstants.REQUEST_APP_PERMISSION_CAMERASTORAGE) {

            if(grantResults.length > 0) {

                if(grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED
                        && grantResults[2] == PackageManager.PERMISSION_GRANTED
                        && grantResults[3] == PackageManager.PERMISSION_GRANTED) {

                    if(mIsRequestPickImage)
                        mLnrlyotVwTop.performClick();
                    else
                        enableLocation();
                } else
                    Snackbar.make(mCrdntrlyot, mAllowPermsnMsg, Snackbar.LENGTH_LONG).show();
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent result) {
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case AppConstants.REQUEST_SIGNIN:

                    break;
                case AppConstants.REQUEST_OPEN_CAMERA:

                    mImageUri = getImageUri(result);
                    LogHelper.printLog(AppConstants.LOGTYPE_INFO, TAG, "onActivityResult - CAMERA: " + getPicturePathFromURI());

                    updateUIAttchdImage(getPicturePathFromURI());
                    break;
                case AppConstants.REQUEST_PICK_IMAGE_MULTIPLE:

                    ArrayList<String> arrylstSlctdImagesPath = result.getStringArrayListExtra(AppConstants.PUT_EXTRA_ARRAYLST_ALREADY_SELECTED_IMGS_PATHS);
                    LogHelper.printLog(AppConstants.LOGTYPE_INFO, TAG, "onActivityResult - GALLERY: " + arrylstSlctdImagesPath.toString());

                    if(arrylstSlctdImagesPath.size() > 0) {

                        mArrylstSelectedImages = new ArrayList<>();
                        mAdapter = new RcyclrAttchdImgsAdapter(this, this, mArrylstSelectedImages, mDrwblDefault);
                        mRecyclerView.setAdapter(mAdapter);
                        mImgCount = 0;
                        AppConstants.ARRYLST_ATTCHDIMGS_PATH.clear();
                    }

                    for(String picturePath : arrylstSlctdImagesPath) {

                        updateUIAttchdImage(picturePath);
                    }

                    /*mImageUri = result.getData();
                    LogHelper.printLog(AppConstants.LOGTYPE_INFO, TAG, "onActivityResult - GALLERY: " + getPicturePathFromURI());

                    updateUIAttchdImage(getPicturePathFromURI());*/
                    break;
                case AppConstants.REQUEST_ENABLE_LOCATION:
                    getLocation();
                    break;
                case AppConstants.REQUEST_CHECKIN_DO:
                        onBackPressed();
                    break;
            }
        } else if (resultCode == Activity.RESULT_CANCELED) {

            switch (requestCode) {
                case AppConstants.REQUEST_SIGNIN:

                    onBackPressed();
                    break;
                case AppConstants.REQUEST_ENABLE_LOCATION:

                    startActivityForResult(new Intent(this, ActvtyCheckinLocationList.class), AppConstants.REQUEST_CHECKIN_DO);
                    break;
            }
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

//        getLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {

        mPrgrsbrMain.setVisibility(View.GONE);

        mLastLocation = location;
        mLatitude = String.valueOf(mLastLocation.getLatitude());
        mLongitude = String.valueOf(mLastLocation.getLongitude());

        if(mIsRequestStartNextActivity) {

            startActivityForResult(new Intent(this, ActvtyCheckinDo.class)
                    .putExtra(AppConstants.PUT_EXTRA_BRANCH_ID, mBranchId)
                    .putExtra(AppConstants.PUT_EXTRA_BRANCH_NAME, mBranchTitle)
                    .putExtra(AppConstants.PUT_EXTRA_RESTAURANT_ID, mRestaurantId)
                    .putExtra(AppConstants.PUT_EXTRA_PHOTO_URL, mBranchPhotoURL)
                    .putExtra(AppConstants.PUT_EXTRA_LATITUDE, mLatitude)
                    .putExtra(AppConstants.PUT_EXTRA_LONGITUDE, mLongitude), AppConstants.REQUEST_CHECKIN_DO);
            mIsRequestStartNextActivity = false;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart() {
        super.onStart();

        if (googleApiClient != null) {
            googleApiClient.connect();
        }
    }

    @Override
    public void onStop() {

        if (googleApiClient != null) {
            googleApiClient.disconnect();
        }
        super.onStop();
    }
}
